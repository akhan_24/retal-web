<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'Individual Properties';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body class="home">
    <?php include "includes/header.php"; ?>

    <section class="ipSec-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="content-left">
                        <h4 class="sub-heading">COMMUNITIES</h4>
                        <a href="javascript:;"><h5>BACK TO PROPERTIES</h5></a>
                        <figure>
                            <img src="assets/images/ipimg-2.png"/>
                        </figure>
                        <h1 class="property-name">Ewan <br><span class="fst-italic">Al Nawras</span></h1>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-right">
                        <figure>
                            <img src="assets/images/ipimg-1.jpg"/>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-2 lirt homeTxtImgSec animToStart">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="content-right dt">
                        <div class="countDv">
                            <div class="row">
                                <div class="col-lg-4 col-6">
                                    <h5>PROJECT TYPE</h5>
                                    <p>Residential</p>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <h5>PROJECT VALUE (SAR)</h5>
                                    <p>110 million</p>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <h5>NUMBER OF UNITS</h5>
                                    <p>78</p>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <h5>NUMBER OF TYPES</h5>
                                    <p>2</p>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <h5>BUILT UP AREA</h5>
                                    <p>20/sqm</p>
                                </div>
                                <div class="col-lg-4 col-6">
                                    <h5>PROJECT PROGRESS</h5>
                                    <p>Complete</p>
                                </div>
                            </div>
                        </div>
                        <p >Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar, in the Eastern Province of Saudi Arabia near the Bahrain causeway and GCC highway.</p>
                        <div class="download-box">
                            <p>Key Project Downloads</p>
                            <a href="#" class="btn btn-outline-secondary">PROJECT BROCHURE</a>
                            <a href="#" class="btn btn-outline-secondary">WARRANTIES</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <figure><img src="assets/images/ipimg-2.jpg"/></figure>
                    <div class="search-pro">
                        <a type="button" class="btn btn-call">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                <g id="Group_165" data-name="Group 165" transform="translate(-1268 -676)">
                                    <path id="Path_54" data-name="Path 54" d="M9.839,10.212a9.411,9.411,0,0,1,2.835.739c1.062.553.55,1.794.044,2.587-.74,1.159-1.767,1.274-3.037,1.126A12.454,12.454,0,0,1-.652,4.338C-.8,3.065-.684,2.036.472,1.294,1.262.787,2.5.274,3.052,1.338A9.473,9.473,0,0,1,3.789,4.18c.2,2.289-3.046,1.558-1.5,3.3l2.656,2.651,1.625,1.607c1.713,1.511,1-1.72,3.272-1.522Z" transform="translate(1268.703 675.286)" fill-rule="evenodd"></path>
                                    <circle id="Ellipse_33" data-name="Ellipse 33" cx="1.5" cy="1.5" r="1.5" transform="translate(1275 676)"></circle>
                                    <circle id="Ellipse_34" data-name="Ellipse 34" cx="1.5" cy="1.5" r="1.5" transform="translate(1279 676)"></circle>
                                    <circle id="Ellipse_35" data-name="Ellipse 35" cx="1.5" cy="1.5" r="1.5" transform="translate(1279 680)"></circle>
                                    <circle id="Ellipse_36" data-name="Ellipse 36" cx="1.5" cy="1.5" r="1.5" transform="translate(1275 680)"></circle>
                                </g>
                            </svg>
                        </a>
                        <ul>
                            <li>
                                <a href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="10.707" height="10.708" viewBox="0 0 10.707 10.708">
                                        <g id="Group_1754" data-name="Group 1754" transform="translate(-1266.796 -676.416)">
                                            <path id="Path_754" data-name="Path 754" d="M1267.15,676.771l10,10" fill="none" stroke="#000" stroke-width="1"></path>
                                            <path id="Path_755" data-name="Path 755" d="M0,0,10,10" transform="translate(1277.149 676.77) rotate(90)" fill="none" stroke="#000" stroke-width="1"></path>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="Group_1736" data-name="Group 1736" transform="translate(-1268 -676)">
                                            <path id="Path_54" data-name="Path 54" d="M9.839,10.212a9.411,9.411,0,0,1,2.835.739c1.062.553.55,1.794.044,2.587-.74,1.159-1.767,1.274-3.037,1.126A12.454,12.454,0,0,1-.652,4.338C-.8,3.065-.684,2.036.472,1.294,1.262.787,2.5.274,3.052,1.338A9.473,9.473,0,0,1,3.789,4.18c.2,2.289-3.046,1.558-1.5,3.3l2.656,2.651,1.625,1.607c1.713,1.511,1-1.72,3.272-1.522Z" transform="translate(1268.703 675.286)" fill-rule="evenodd"></path>
                                        </g>
                                        <path id="Path_200" data-name="Path 200" d="M1587.7,4099l2.583,2.583,2.583-2.583" transform="translate(4108.582 -1587.285) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"></path>
                                        <path id="Path_750" data-name="Path 750" d="M-14583-9038.03h6.493V-9033" transform="translate(14590 9041.028)" fill="none" stroke="#000" stroke-width="1"></path>
                                    </svg>
                                    <span>Call me back</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                        <g id="Group_1737" data-name="Group 1737" transform="translate(-1268 -676)">
                                            <path id="Path_54" data-name="Path 54" d="M9.839,10.212a9.411,9.411,0,0,1,2.835.739c1.062.553.55,1.794.044,2.587-.74,1.159-1.767,1.274-3.037,1.126A12.454,12.454,0,0,1-.652,4.338C-.8,3.065-.684,2.036.472,1.294,1.262.787,2.5.274,3.052,1.338A9.473,9.473,0,0,1,3.789,4.18c.2,2.289-3.046,1.558-1.5,3.3l2.656,2.651,1.625,1.607c1.713,1.511,1-1.72,3.272-1.522Z" transform="translate(1268.703 675.286)" fill-rule="evenodd"></path>
                                        </g>
                                    </svg>
                                    <span>Call Now</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <svg id="Group_1739" data-name="Group 1739" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18.081" viewBox="0 0 18 18.081">
                                        <defs>
                                            <clipPath id="clip-path">
                                                <rect id="Rectangle_505" data-name="Rectangle 505" width="18" height="18.081"></rect>
                                            </clipPath>
                                        </defs>
                                        <g id="Group_1738" data-name="Group 1738" clip-path="url(#clip-path)">
                                            <path id="Path_751" data-name="Path 751" d="M6.8,5.214c-.168-.373-.344-.38-.5-.387-.131-.006-.28-.005-.429-.005a.823.823,0,0,0-.6.28,2.51,2.51,0,0,0-.784,1.868A4.356,4.356,0,0,0,5.4,9.285a9.192,9.192,0,0,0,3.825,3.38c1.892.746,2.277.6,2.687.56a2.262,2.262,0,0,0,1.512-1.065,1.871,1.871,0,0,0,.131-1.065c-.056-.093-.205-.149-.429-.261s-1.325-.654-1.53-.728-.355-.112-.5.112-.578.728-.709.878-.261.168-.485.056a6.125,6.125,0,0,1-1.8-1.111A6.745,6.745,0,0,1,6.849,8.491c-.131-.224-.014-.345.1-.457s.224-.261.336-.392a1.535,1.535,0,0,0,.224-.373.412.412,0,0,0-.019-.392C7.432,6.764,7,5.656,6.8,5.214Zm8.58-2.586A8.963,8.963,0,0,0,1.271,13.438L0,18.081l4.751-1.246a8.956,8.956,0,0,0,4.283,1.091h0a8.963,8.963,0,0,0,6.34-15.3M9.038,16.412h0a7.44,7.44,0,0,1-3.791-1.038l-.272-.161-2.819.739L2.9,13.2l-.177-.282a7.449,7.449,0,1,1,6.31,3.49" fill-rule="evenodd"></path>
                                        </g>
                                    </svg>
                                    <span>Whatsapp</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <h4 class="unit-number">
                        <figure><img src="assets/svg/icon-marker.svg"/></figure>
                        <span>AL KHOBAR</span>
                    </h4>
                </div>

                <div class="col-md-2"></div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="content-right mb">
                        <p class="">Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar, in the Eastern Province of Saudi Arabia near the Bahrain causeway and GCC highway.</p>
                        
                        <div class="download-box ">
                            <p>Key Project Downloads</p>
                            <a href="#" class="btn btn-outline-secondary">PROJECT BROCHURE</a>
                            <a href="#" class="btn btn-outline-secondary">WARRANTIES</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-3">
        <div class="container-fluid">
            <h4 class="sub-heading">ABOUT THE PROJECT</h4>
            <div class="aboutSlider">
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content-left">
                                <h3 class="main-heading anim-head"><span class="letters">Lorem</span> <span class="fst-italic letters">ipsum</span> <span class="letters">dolor sit amet</span></h3>
                                <div class="row">
                                    <div class="col-3">
                                        <a data-fancybox href="assets/images/ipimg-3.jpg"><figure><img src="assets/svg/icon-play.svg"/></figure></a>
                                    </div>                                    
                                    <div class="col-9">
                                        <div><a data-fancybox href="assets/images/ipimg-3.jpg" class="btn btn-primary">WATCH PROJECT VIDEO</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <figure><img src="assets/images/ipimg-3.jpg"/></figure>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content-left">
                                <h3 class="main-heading">Lorem <span class="fst-italic">ipsum</span> dolor sit amet</h3>
                                <div class="row">
                                    <div class="col-3">
                                        <a data-fancybox href="assets/images/ipimg-3.jpg"><figure><img src="assets/svg/icon-play.svg"/></figure></a>
                                    </div>                                    
                                    <div class="col-9">
                                        <div><a data-fancybox href="assets/images/ipimg-3.jpg" class="btn btn-primary">WATCH PROJECT VIDEO</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <figure><img src="assets/images/ipimg-3.jpg"/></figure>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content-left">
                                <h3 class="main-heading">Lorem <span class="fst-italic">ipsum</span> dolor sit amet</h3>
                                <div class="row">
                                    <div class="col-3">
                                        <a data-fancybox href="assets/images/ipimg-3.jpg"><figure><img src="assets/svg/icon-play.svg"/></figure></a>
                                    </div>                                    
                                    <div class="col-9">
                                        <div><a data-fancybox href="assets/images/ipimg-3.jpg" class="btn btn-primary">WATCH PROJECT VIDEO</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <figure><img src="assets/images/ipimg-3.jpg"/></figure>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="content-left">
                                <h3 class="main-heading">Lorem <span class="fst-italic">ipsum</span> dolor sit amet</h3>
                                <div class="row">
                                    <div class="col-3">
                                        <a data-fancybox href="assets/images/ipimg-3.jpg"><figure><img src="assets/svg/icon-play.svg"/></figure></a>
                                    </div>                                    
                                    <div class="col-9">
                                        <div><a data-fancybox href="assets/images/ipimg-3.jpg" class="btn btn-primary">WATCH PROJECT VIDEO</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <figure><img src="assets/images/ipimg-3.jpg"/></figure>
                        </div>
                    </div>
                </div>
            </div>
            <span class="slideInfo"></span>
        </div>
    </section>
    <section class="ipSec-4 lirt homeTxtImgSec animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="content-right">
                        <p>In 2012 Retal for Structural Development, was founded with remarkable capabilities and professional work team, to join Al Fozan group of companies. <br><br>
                        Retal started intensely to compete in the field of development and implementation of residential and commercial projects in the Kingdom of Saudi Arabia, providing residential solutions to cope with the hastened developments, in a manner that satisfies clients' requirements with creative methods and high quality.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <figure class="img-figure img-figure-1"><img src="assets/images/ipimg-4.jpg"/></figure>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="image-box">
                        <figure class="img-figure img-figure-2"><img src="assets/images/ipimg-5.jpg"/></figure>
                        <a href="javascript:;" class="btn btn-livestream"><span class="icon-live">•</span><span>LIVE STREAM</span></a>
                        <a class="btn-fullscreen" data-fancybox href="assets/images/ipimg-5.jpg">
                            <figure><img src="assets/svg/icon-fullscreen.svg"/></figure>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-left">
                        <p>Retal started intensely to compete in the field of development and implementation of residential and commercial projects in the Kingdom of Saudi Arabia, providing residential solutions to cope with the hastened developments, in a manner that satisfies clients' requirements with creative methods and high quality.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-5 homeTxtImgSec animToStart">
        <div class="container">
            <h4 class="sub-heading">360 TOUR</h4>
            <div class="row">
                <div class="col-md-8">
                    <figure><img src="assets/images/ipimg-6.jpg"/></figure>
                </div>
                <div class="col-md-4">
                    <div class="content-right">
                        <h3 class="main-heading">Take a walk from where <span class="fst-italic">you</span> are</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                        <a class="btn btn-360 btn-white shadow">
                            <figure><img src="assets/svg/icon-360.svg"/></figure>
                        </a>
                        <a class="btn btn-tour btn-white shadow">TAKE THE TOUR</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-6 homeTxtImgSec animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="content-right mb">
                        <h4 class="sub-heading">QUALITY OF LIFE</h4>
                    </div>
                    <figure><img src="assets/images/ipimg-7.jpg"/></figure>
                </div>
                <div class="col-md-4">
                    <div class="content-right dt">
                        <h4 class="sub-heading">QUALITY OF LIFE</h4>
                        <h3 class="main-heading">World-Class <span class="fst-italic">Amenities</span></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-7 animToStart">
        <div class="container">
            <div class="show-mobile">
                <div class="row">
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="icon-box-mobile">
                            <div class="icon-box">
                                <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            </div>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="show-desktop">
                <div class="row">
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                            <h3>Library</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                            <h3>Playground</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                            <h3>Retail</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-views.svg"/></figure>
                            <h3>Views</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                            <h3>Cabana</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                            <h3>Library</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                            <h3>Playground</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                            <h3>Retail</h3>
                        </div>
                    </div>
                    <div class="col">
                        <div class="icon-box">
                            <figure><img src="assets/svg/icon-views.svg"/></figure>
                            <h3>Views</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-8 homeTxtImgSec animToStart">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                    <div class="content-left">
                        <h4 class="sub-heading">Projects Features</h4>
                        <h3 class="main-heading">Convenience <span class="fst-italic">at its Finest</span></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="show-mobile">
                        <div class="masonry-grid">
                            <div class="row">
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-8.jpg"/></figure>
                                        <h4>FEATURE ONE</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-9.jpg"/></figure>
                                        <h4>FEATURE TWO</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-10.jpg"/></figure>
                                        <h4>FEATURE THREE</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-11.jpg"/></figure>
                                        <h4>FEATURE FOUR</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-12.jpg"/></figure>
                                        <h4>FEATURE FIVE</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-13.jpg"/></figure>
                                        <h4>FEATURE SIX</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-8.jpg"/></figure>
                                        <h4>FEATURE SEVEN</h4>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-9.jpg"/></figure>
                                        <h4>FEATURE EIGHT</h4>
                                    </div>
                                </div>
                                <div class="col-6 d-none">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-10.jpg"/></figure>
                                        <h4>FEATURE NINE</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="show-desktop">
                        <div class="masonry-grid">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-8.jpg"/></figure>
                                        <h4>FEATURE ONE</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-9.jpg"/></figure>
                                        <h4>FEATURE TWO</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-10.jpg"/></figure>
                                        <h4>FEATURE THREE</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-11.jpg"/></figure>
                                        <h4>FEATURE FOUR</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-12.jpg"/></figure>
                                        <h4>FEATURE FIVE</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-13.jpg"/></figure>
                                        <h4>FEATURE SIX</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-8.jpg"/></figure>
                                        <h4>FEATURE SEVEN</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-9.jpg"/></figure>
                                        <h4>FEATURE EIGHT</h4>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="masonry-box">
                                        <figure><img src="assets/images/ipimg-10.jpg"/></figure>
                                        <h4>FEATURE NINE</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-9 homeTxtImgSec animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="sub-heading">INVESTMENT OPPORTUNITIES</h4>
                </div>
                <div class="col-9">
                    <h3 class="main-heading"><span class="fst-italic">State of the Art Spaces</span> <br>for your Business</h3>
                </div>
                <div class="col-md-3 col-9">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                </div>
            </div>
            <div class="opportunites">
                <div class="row">
                    <div class="col-md-4 col-6">
                        <div class="opportunity-box">
                            <a href="javascript:;" class="opportunity-link">
                                <figure><img src="assets/images/ipimg-16.jpg"/></figure>
                                <h3>Cafes</h3>
                                <h5>12 Available Slots</h5>
                                <p>Lorem ipsum dolor sit amet consectetuer.</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="opportunity-box">
                            <a href="javascript:;" class="opportunity-link">
                                <figure><img src="assets/images/ipimg-14.jpg"/></figure>
                                <h3>Gyms</h3>
                                <h5>2 Available Slots</h5>
                                <p>Lorem ipsum dolor sit amet consectetuer.</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="opportunity-box">
                            <a href="javascript:;" class="opportunity-link">
                                <figure><img src="assets/images/ipimg-15.jpg"/></figure>
                                <h3>Fashion Outlets</h3>
                                <h5>40 Available Slots</h5>
                                <p>Lorem ipsum dolor sit amet consectetuer.</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="opportunity-box">
                            <a href="javascript:;" class="opportunity-link">
                                <figure><img src="assets/images/ipimg-18.jpg"/></figure>
                                <h3>Cinema</h3>
                                <h5>1 Available Slots</h5>
                                <p>Lorem ipsum dolor sit amet consectetuer.</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="opportunity-box">
                            <a href="javascript:;" class="opportunity-link">
                                <figure><img src="assets/images/ipimg-19.jpg"/></figure>
                                <h3>Bookstores</h3>
                                <h5>4 Available Slots</h5>
                                <p>Lorem ipsum dolor sit amet consectetuer.</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="opportunity-box">
                            <a href="javascript:;" class="opportunity-link">
                                <figure><img src="assets/images/ipimg-17.jpg"/></figure>
                                <h3>Restaurants</h3>
                                <h5>20 Available Slots</h5>
                                <p>Lorem ipsum dolor sit amet consectetuer.</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-10 animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="show-mobile">
                        <div class="row">
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="icon-box-mobile">
                                    <div class="icon-box">
                                        <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    </div>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="show-desktop">
                        <div class="row">
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-library.svg"/></figure>
                                    <h3>Library</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                    <h3>Playground</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                    <h3>Retail</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                    <h3>Cabana</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-library.svg"/></figure>
                                    <h3>Library</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                    <h3>Playground</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                    <h3>Retail</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-views.svg"/></figure>
                                    <h3>Views</h3>
                                </div>
                            </div>
                            <div class="col">
                                <div class="icon-box">
                                    <figure><img src="assets/svg/icon-views.svg"/></figure>
                                    <h3>Views</h3>
                                </div>
                            </div>
                            <div class="col"></div>
                            <div class="col"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="content-right">
                        <h4 class="sub-heading">Projects Features</h4>
                        <h3 class="main-heading">Convenience <span class="fst-italic">at its Finest</span></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-11 animToStart">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                    <h4 class="sub-heading">Events</h4>
                    <div class="content-left">
                        <h3 class="main-heading">Magnificent <span class="fst-italic">Locations for Remarkable Events</span></h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="eventSlider">
                        <div class="item">
                            <figure><img src="assets/images/ipimg-20.jpg"/></figure>
                            <div class="content-box">
                                <h3>Weddings</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                <a href="javascript:;" class="btn btn-primary">INQUIRE NOW</a>
                            </div>
                        </div>
                        <div class="item">
                            <figure><img src="assets/images/ipimg-20.jpg"/></figure>
                            <div class="content-box">
                                <h3>Weddings</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                <a href="javascript:;" class="btn btn-primary">INQUIRE NOW</a>
                            </div>
                        </div>
                        <div class="item">
                            <figure><img src="assets/images/ipimg-20.jpg"/></figure>
                            <div class="content-box">
                                <h3>Weddings</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                <a href="javascript:;" class="btn btn-primary">INQUIRE NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-12 animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="content-left">
                        <h3 class="main-heading">Retal’s <span class="fst-italic">Partners</span></h3>
                        <p>Retal have created strong partnerships, working together with its collaborators for years on some of Saudi Arabia’s key developments. <br><br>As the collaboration grows stronger, they energize each other to always create beautiful places that inspire and engage. </p>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-7">
                    <h4 class="sub-heading">FINANCIERS</h4>
                    <div class="logos">
                        <div class="row">
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-21.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-22.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-23.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-24.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-25.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-26.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-27.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-28.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-29.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-30.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-31.png"/></figure>
                            </div>
                            <div class="col-md-3 col-4">
                                <figure><img src="assets/images/ipimg-32.png"/></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-13 animToStart">
        <div class="container">
            <h4 class="sub-heading">PROJECT UNITS</h4>

            <div class="row nav-row">
                <div class="col-md-12">
                        
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="typea-tab" data-bs-toggle="tab" data-bs-target="#typea" type="button" role="tab" aria-controls="typea" aria-selected="true">TYPE A</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="typeb-tab" data-bs-toggle="tab" data-bs-target="#typeb" type="button" role="tab" aria-controls="typeb" aria-selected="false">TYPE B</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="typec-tab" data-bs-toggle="tab" data-bs-target="#typec" type="button" role="tab" aria-controls="typec" aria-selected="false">TYPE C</button>
                        </li>
                    </ul>

                    <div class="tab-content tab-innercontent" id="myTabContent">
                        <div class="tab-pane fade show active" id="typea" role="tabpanel" aria-labelledby="typea-tab">
                            <ul class="nav nav-tabs" id="typeaTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="aua1-tab" data-bs-toggle="tab" data-bs-target="#aua1" type="button" role="tab" aria-controls="aua1" aria-selected="true">U/A1</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="aua2-tab" data-bs-toggle="tab" data-bs-target="#aua2" type="button" role="tab" aria-controls="aua2" aria-selected="false">U/A2</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="aua3-tab" data-bs-toggle="tab" data-bs-target="#aua3" type="button" role="tab" aria-controls="aua3" aria-selected="false">U/A3</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="aua4-tab" data-bs-toggle="tab" data-bs-target="#aua4" type="button" role="tab" aria-controls="aua4" aria-selected="false">U/A4</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="aua5-tab" data-bs-toggle="tab" data-bs-target="#aua5" type="button" role="tab" aria-controls="aua5" aria-selected="false">U/A5</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="aua6-tab" data-bs-toggle="tab" data-bs-target="#aua6" type="button" role="tab" aria-controls="aua6" aria-selected="false">U/A6</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="aua7-tab" data-bs-toggle="tab" data-bs-target="#aua7" type="button" role="tab" aria-controls="aua7" aria-selected="false">U/A7</button>
                                </li>
                            </ul>


                            <div class="tab-content" id="typeaTabContent">
                                <div class="tab-pane fade show active" id="aua1" role="tabpanel" aria-labelledby="ua1-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>
                                                <div class="project-print show-mobile">
                                                    <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    <p class="project-plan">Key Plan</p>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="show-desktop">
                                                    <div class="project-main">
                                                        <h3 class="project-name">UNIT A2</h3>
                                                        <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                    </div>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row bottom-row">
                                                    <div class="col-md-12 show-desktop">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3 show-desktop">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6 col-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-6">
                                                        <div class="show-desktop">
                                                            <div class="project-zoom">
                                                                <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                                <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                            </div>
                                                        </div>
                                                        <div class="show-mobile">
                                                            <div class="project-zoom">
                                                                <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                                <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                                <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aua2" role="tabpanel" aria-labelledby="ua2-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aua3" role="tabpanel" aria-labelledby="ua3-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aua4" role="tabpanel" aria-labelledby="ua4-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aua5" role="tabpanel" aria-labelledby="ua5-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aua6" role="tabpanel" aria-labelledby="ua6-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="aua7" role="tabpanel" aria-labelledby="ua7-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="typeb" role="tabpanel" aria-labelledby="typeb-tab">
                            <ul class="nav nav-tabs" id="typebTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="bua1-tab" data-bs-toggle="tab" data-bs-target="#bua1" type="button" role="tab" aria-controls="bua1" aria-selected="true">U/A1</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bua2-tab" data-bs-toggle="tab" data-bs-target="#bua2" type="button" role="tab" aria-controls="bua2" aria-selected="false">U/A2</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bua3-tab" data-bs-toggle="tab" data-bs-target="#bua3" type="button" role="tab" aria-controls="bua3" aria-selected="false">U/A3</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bua4-tab" data-bs-toggle="tab" data-bs-target="#bua4" type="button" role="tab" aria-controls="bua4" aria-selected="false">U/A4</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bua5-tab" data-bs-toggle="tab" data-bs-target="#bua5" type="button" role="tab" aria-controls="bua5" aria-selected="false">U/A5</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bua6-tab" data-bs-toggle="tab" data-bs-target="#bua6" type="button" role="tab" aria-controls="bua6" aria-selected="false">U/A6</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="bua7-tab" data-bs-toggle="tab" data-bs-target="#bua7" type="button" role="tab" aria-controls="bua7" aria-selected="false">U/A7</button>
                                </li>
                            </ul>
                            
                            <div class="tab-content" id="typebTabContent">
                                <div class="tab-pane fade show active" id="bua1" role="tabpanel" aria-labelledby="bua1-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bua2" role="tabpanel" aria-labelledby="bua2-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bua3" role="tabpanel" aria-labelledby="bua3-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bua4" role="tabpanel" aria-labelledby="bua4-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bua5" role="tabpanel" aria-labelledby="bua5-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bua6" role="tabpanel" aria-labelledby="bua6-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bua7" role="tabpanel" aria-labelledby="bua7-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="typec" role="tabpanel" aria-labelledby="typec-tab">
                            <ul class="nav nav-tabs" id="typecTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="cua1-tab" data-bs-toggle="tab" data-bs-target="#cua1" type="button" role="tab" aria-controls="cua1" aria-selected="true">U/A1</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="cua2-tab" data-bs-toggle="tab" data-bs-target="#cua2" type="button" role="tab" aria-controls="cua2" aria-selected="false">U/A2</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="cua3-tab" data-bs-toggle="tab" data-bs-target="#cua3" type="button" role="tab" aria-controls="cua3" aria-selected="false">U/A3</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="cua4-tab" data-bs-toggle="tab" data-bs-target="#cua4" type="button" role="tab" aria-controls="cua4" aria-selected="false">U/A4</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="cua5-tab" data-bs-toggle="tab" data-bs-target="#cua5" type="button" role="tab" aria-controls="cua5" aria-selected="false">U/A5</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="cua6-tab" data-bs-toggle="tab" data-bs-target="#cua6" type="button" role="tab" aria-controls="cua6" aria-selected="false">U/A6</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="cua7-tab" data-bs-toggle="tab" data-bs-target="#cua7" type="button" role="tab" aria-controls="cua7" aria-selected="false">U/A7</button>
                                </li>
                            </ul>
            
                            <div class="tab-content" id="typecTabContent">
                                <div class="tab-pane fade show active" id="cua1" role="tabpanel" aria-labelledby="cua1-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cua2" role="tabpanel" aria-labelledby="cua2-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cua3" role="tabpanel" aria-labelledby="cua3-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cua4" role="tabpanel" aria-labelledby="cua4-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cua5" role="tabpanel" aria-labelledby="cua5-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cua6" role="tabpanel" aria-labelledby="cua6-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="cua7" role="tabpanel" aria-labelledby="cua7-tab">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12">
                                            <div class="content-left">
                                                <h3 class="main-heading">
                                                    <span class="project-name">U/A2</span>
                                                    <span class="project-area">1,650</span>/<small>sqm</small>
                                                </h3>
                                                <div class="property-box">
                                                    <p class="project-property">3 BHK</p>
                                                    <p class="project-plot">Plot Area: 1,034 sqm</p>
                                                    <p class="project-interior">A/C Interior Area: 549 sqm</p>
                                                    <p class="project-terrace">Terrace Area: 105 sqm</p>
                                                    <p class="project-residence">Total Residence: 654 sqm</p>
                                                    <p class="project-floor">Floors: 2—6</p>
                                                </div>
                                                <div class="price-box">
                                                    <p>2,400,000 SAR</p>
                                                    <small>STARTING FROM</small>
                                                </div>

                                                
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-cabana.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-library.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-playground.svg"/></figure>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="icon-box">
                                                            <figure><img src="assets/svg/icon-retail.svg"/></figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-12">
                                            <div class="content-right">
                                                <div class="project-main">
                                                    <h3 class="project-name">UNIT A2</h3>
                                                    <a href="javascript:;"><figure><img src="assets/svg/icon-download.svg"/></figure></a>
                                                </div>
                                                
                                                <div class="projectSlider">
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                    <div class="item">
                                                        <figure><img src="assets/images/ipimg-38.jpg"/></figure>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h3 class="project-plan">KEY PLAN</h3>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="project-map" href="javascript:;"><figure><img src="assets/images/ipimg-33.png"/></figure></a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="projectSlider-nav">
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-34.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-35.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-36.jpg"/></figure>
                                                            </div>
                                                            <div class="item">
                                                                <figure><img src="assets/images/ipimg-37.jpg"/></figure>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="project-zoom">
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-fullscreen.svg"/></figure></a>
                                                            <a href="javascript:;"><figure><img src="assets/svg/icon-zoom.svg"/></figure></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-14 animToStart">
        <div class="compare-unit">
            <a href="javascript:;" class="compare-btn">COMPARE UNITS</a>
        </div>
        <div class="container">
            <div class="row unit-row">
                <div class="col-md-12">
                    <div class="content-center">
                        <h3 class="main-heading2">Which unit is is your perfect match?</h3>
                        <p>Compare the various units <br>and find your perfect match</p>
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="unit-box unit-box-floorplan">
                        <h4 class="unit-number">01</h4>
                        <div class="image-box">
                            <figure><img src="assets/images/ipimg-39.png"/></figure>
                            <a href="javascript:;" class="btn btn-white">VIEW FLOORPLAN</a>
                        </div>
                        <div class="content-box">
                            <h3>UA2</h3>
                            <p>Lorem ipsum dolor sit amet consectetuer adipiscing ipsum dolor sit amet consectetuer.</p>
                            <div class="btn-box">
                                <a href="javascript:;" class="btn btn-outline-dark">3 BHK</a>
                                <a href="javascript:;" class="btn btn-outline-dark">38 UNITS REMAINING</a>
                                <a href="javascript:;" class="btn btn-outline-dark">INFO +</a>
                            </div>
                            <hr class="separator">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <p>3,100 sq.ft</p>
                                    <p>Interior Area</p>
                                </li>
                                <li class="list-group-item">
                                    <p>200 sq.ft</p>
                                    <p>Balcony Area</p>
                                </li>
                                <li class="list-group-item">
                                    <p>4 BHK</p>
                                    <p>Bed</p>
                                </li>
                                <li class="list-group-item">
                                    <p>INTEGRATED APPLIANCES</p>
                                    <p>Kitchen</p>
                                </li>
                                </li>
                                <li class="list-group-item">
                                    <p>MARBLE</p>
                                    <p>Countertop</p>
                                </li>
                            </ul>
                            <a href="javascript:;" class="btn btn-dark book-btn">BOOK NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-6 show-desktop">
                    <div class="unit-box unit-box-floorplan">
                        <h4 class="unit-number">02</h4>
                        <div class="image-box">
                            <figure><img src="assets/images/ipimg-39.png"/></figure>
                            <a href="javascript:;" class="btn btn-white">VIEW FLOORPLAN</a>
                        </div>
                        <div class="content-box">
                            <h3>UA6</h3>
                            <p>Lorem ipsum dolor sit amet consectetuer adipiscing ipsum dolor sit amet consectetuer.</p>
                            <div class="btn-box">
                                <a href="javascript:;" class="btn btn-outline-dark">6 BHK</a>
                                <a href="javascript:;" class="btn btn-outline-dark">38 UNITS REMAINING</a>
                                <a href="javascript:;" class="btn btn-outline-dark">INFO +</a>
                            </div>
                            <hr class="separator">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <p>2,900 sq.ft</p>
                                    <p>Interior Area</p>
                                </li>
                                <li class="list-group-item">
                                    <p>500 sq.ft</p>
                                    <p>Balcony Area</p>
                                </li>
                                <li class="list-group-item">
                                    <p>3 BHK</p>
                                    <p>Bed</p>
                                </li>
                                <li class="list-group-item">
                                    <p>NO INTEGRATED APPLIANCES</p>
                                    <p>Kitchen</p>
                                </li>
                                </li>
                                <li class="list-group-item">
                                    <p>GRANITE</p>
                                    <p>Countertop</p>
                                </li>
                            </ul>
                            <a href="javascript:;" class="btn btn-dark book-btn">BOOK NOW</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="unit-box unit-box-addunit">
                        <h4 class="unit-number">&nbsp;</h4>
                        <div class="image-box">
                            <div class="add-unit">
                                <a href="javascript:;" class="btn btn-white">ADD UNIT  +</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-15 animToStart">
        <div class="container">
            <h4 class="sub-heading">LOCATION</h4>
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <div class="content-left">
                        <h3 class="main-heading">At the heart <br>of the <span class="fst-italic">city</span></h3>
                        <div class="location-box">
                            <div class="show-mobile">
                                <div class="row">
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>RESIDENTIAL</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Coya Communities</li>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>CULTURAL</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>HOTELS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>PARKS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>PARKS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>SHOPPING</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-6">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>HOSPITALS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="show-desktop">
                                <div class="row">
                                    <div class="col-md-4">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>RESIDENTIAL</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Coya Communities</li>
                                        </ul>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>CULTURAL</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>HOTELS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>PARKS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>PARKS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>SHOPPING</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <h6>HOSPITALS</h6>
                                            </li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                            <li class="list-group-item">Lorem ipsum</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6">
                    <figure><img src="assets/images/ipimg-40.jpg"/></figure>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-16 animToStart">
        <div class="container">
            <h4 class="sub-heading">MASTERPLAN</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="content-left">
                        <h3 class="main-heading">City that <span class="fst-italic">Breathes</span></h3>
                    </div>
                    <div class="show-mobile">
                        <div class="masterplan-box">
                            <figure><img src="assets/images/ipimg-41.jpg"/></figure>
                            <div class="content-box">
                                <h3>Villa <br>Type A</h3>
                                <p>Lorem ipsum dolor sit amet, consect adipiscing elit, sed diam nonummy euismod tincidunt ut laoreet.</p>
                            </div>
                        </div>
                    </div>
                    <div class="show-desktop">
                        <div class="masterplan-box">
                            <h3>Villa <br>Type A</h3>
                            <p>Lorem ipsum dolor sit amet, consect adipiscing elit, sed diam nonummy euismod tincidunt ut laoreet.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-17 animToStart">
        <div class="container">
            <h4 class="sub-heading">MEDIA</h4>
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="content-left">
                        <h3 class="main-heading">Downloads</h3>
                        <div class="download-box">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-outline-secondary">PROJECT BROCHURE</a>
                                    <a href="#" class="btn btn-outline-secondary">REVIEW COMPARISON SHEET</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="#" class="btn btn-outline-secondary">PROJECT WARRANTIES</a>
                                    <a href="#" class="btn btn-outline-secondary">SPECS SHEET</a>
                                </div>
                            </div>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
                        <p>For more info contact: <a href="mailto:marketing@retal.ae">marketing@retal.ae</a></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 show-desktop">
                    <div class="content-right">
                        <p>WANT TO KNOW MORE?</p>
                        <a href="javascript:;">
                            <div class="news-box">
                                <h3>News</h3>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-18 animToStart">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-12">
                    <div class="content-left">
                        <h3 class="main-heading">Booking & Inquires</h3>
                        
                        <ul class="nav nav-tabs" id="bookingTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="bookapartment-tab" data-bs-toggle="tab" data-bs-target="#bookapartment" type="button" role="tab" aria-controls="bookapartment" aria-selected="true">BOOK APARTMENT</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="bookshow-tab" data-bs-toggle="tab" data-bs-target="#bookshow" type="button" role="tab" aria-controls="bookshow" aria-selected="false">BOOK SHOW APARTMENT VIEWING</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="register-tab" data-bs-toggle="tab" data-bs-target="#register" type="button" role="tab" aria-controls="register" aria-selected="false">REGISTER INTEREST</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="waiting-tab" data-bs-toggle="tab" data-bs-target="#waiting" type="button" role="tab" aria-controls="waiting" aria-selected="false">WAITING LIST</button>
                            </li>
                        </ul>
    
                        <div class="tab-content tab-innercontent" id="bookingTabContent">
                            <div class="tab-pane fade show active" id="bookapartment" role="tabpanel" aria-labelledby="bookapartment-tab">
                                <p>Call us through our toll free 920 021 022 for all inquiries related to Retal's projects. <br>If calling from outside of the KSA please contact us on <a href="tel:+966138071777">+966(13)8071777</a></p>
                                <form class="row g-0">
                                    <div class="col-md-6">
                                        <select id="inputState" class="form-select">
                                        <option selected>SELECT YOUR REQUEST</option>
                                        <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputPassword4" placeholder="FULL NAME">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputAddress" placeholder="MOBILE">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="email" class="form-control" id="inputEmail4" placeholder="EMAIL">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-send">SEND</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-cancel">CANCEL</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                <label class="form-check-label" for="inlineRadio1">I have read the <a href="javascript:;">Privacy Policy</a></label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                <label class="form-check-label" for="inlineRadio2">I agree with the <a href="javascript:;">Terms & Conditions</a></label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="bookshow" role="tabpanel" aria-labelledby="bookshow-tab">
                                <p>Call us through our toll free 920 021 022 for all inquiries related to Retal's projects. <br>If calling from outside of the KSA please contact us on <a href="tel:+966138071777">+966(13)8071777</a></p>
                                <form class="row g-0">
                                    <div class="col-md-6">
                                        <select id="inputState" class="form-select">
                                        <option selected>SELECT YOUR REQUEST</option>
                                        <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputPassword4" placeholder="FULL NAME">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputAddress" placeholder="MOBILE">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="email" class="form-control" id="inputEmail4" placeholder="EMAIL">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-send">SEND</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-cancel">CANCEL</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                <label class="form-check-label" for="inlineRadio1">I have read the <a href="javascript:;">Privacy Policy</a></label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                <label class="form-check-label" for="inlineRadio2">I agree with the <a href="javascript:;">Terms & Conditions</a></label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                                <p>Call us through our toll free 920 021 022 for all inquiries related to Retal's projects. <br>If calling from outside of the KSA please contact us on <a href="tel:+966138071777">+966(13)8071777</a></p>
                                <form class="row g-0">
                                    <div class="col-md-6">
                                        <select id="inputState" class="form-select">
                                        <option selected>SELECT YOUR REQUEST</option>
                                        <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputPassword4" placeholder="FULL NAME">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputAddress" placeholder="MOBILE">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="email" class="form-control" id="inputEmail4" placeholder="EMAIL">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-send">SEND</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-cancel">CANCEL</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                <label class="form-check-label" for="inlineRadio1">I have read the <a href="javascript:;">Privacy Policy</a></label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                <label class="form-check-label" for="inlineRadio2">I agree with the <a href="javascript:;">Terms & Conditions</a></label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="waiting" role="tabpanel" aria-labelledby="waiting-tab">
                                <p>Call us through our toll free 920 021 022 for all inquiries related to Retal's projects. <br>If calling from outside of the KSA please contact us on <a href="tel:+966138071777">+966(13)8071777</a></p>
                                <form class="row g-0">
                                    <div class="col-md-6">
                                        <select id="inputState" class="form-select">
                                        <option selected>SELECT YOUR REQUEST</option>
                                        <option>...</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputPassword4" placeholder="FULL NAME">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" id="inputAddress" placeholder="MOBILE">
                                    </div>
                                    <div class="col-md-2">
                                        <input type="email" class="form-control" id="inputEmail4" placeholder="EMAIL">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-send">SEND</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary btn-cancel">CANCEL</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                <label class="form-check-label" for="inlineRadio1">I have read the <a href="javascript:;">Privacy Policy</a></label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                <label class="form-check-label" for="inlineRadio2">I agree with the <a href="javascript:;">Terms & Conditions</a></label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ipSec-19 animToStart">
        <div class="container">
            <h4 class="sub-heading">SIMILAR PROJECTS</h4>
            <div class="row unit-row">
                <div class="col-md-4 col-6">
                    <div class="unit-box">
                        <h4 class="unit-number">
                            <span>AL KHOBAR</span>
                            <figure><img src="assets/svg/icon-marker.svg"/></figure>
                        </h4>
                        <div class="image-box">
                            <figure><img src="assets/images/ipimg-43.jpg"/></figure>
                            <a href="javascript:;" class="btn btn-white"><figure><img src="assets/svg/icon-heart.svg"/></figure></a>
                        </div>
                        <div class="content-box">
                            <h3>Ewan Al Qayrawan</h3>
                            <span>2,400,000 SAR</span>
                            <p>Ewan Al Qayrawan is a residential villa community of more than 150 villas in a well-developed and rapidly growing location close to…</p>
                            <div class="btn-box">
                                <a href="javascript:;" class="btn btn-outline-dark">RESIDENTIAL</a>
                                <a href="javascript:;" class="btn btn-outline-dark">164 UNITS</a>
                                <a href="javascript:;" class="btn btn-primary">EXPLORE +</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="unit-box">
                        <h4 class="unit-number">
                            <span>DAMMAM</span>
                            <figure><img src="assets/svg/icon-marker.svg"/></figure>
                        </h4>
                        <div class="image-box">
                            <figure><img src="assets/images/ipimg-44.jpg"/></figure>
                            <a href="javascript:;" class="btn btn-white"><figure><img src="assets/svg/icon-heartfill.svg"/></figure></a>
                        </div>
                        <div class="content-box">
                            <h3>Sakanat Al Safa</h3>
                            <span>2,400,000 SAR</span>
                            <p>Sakanat Al Safa is a residential development featuring eight luxury villas and forty-eight duplex apartments. It is located in…</p>
                            <div class="btn-box">
                                <a href="javascript:;" class="btn btn-outline-dark">RESIDENTIAL</a>
                                <a href="javascript:;" class="btn btn-outline-dark">56 UNITS</a>
                                <a href="javascript:;" class="btn btn-primary">EXPLORE +</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 show-desktop">
                    <div class="unit-box">
                        <h4 class="unit-number">
                            <span>AL KHOBAR</span>
                            <figure><img src="assets/svg/icon-marker.svg"/></figure>
                        </h4>
                        <div class="image-box">
                            <figure><img src="assets/images/ipimg-45.jpg"/></figure>
                            <a href="javascript:;" class="btn btn-white"><figure><img src="assets/svg/icon-heart.svg"/></figure></a>
                        </div>
                        <div class="content-box">
                            <h3>Al Dhawahi</h3>
                            <span>2,400,000 SAR</span>
                            <p>Al Dawahi is a residential villa community featuring 133 residential units of different architectural sizes. It is located near Aramco…</p>
                            <div class="btn-box">
                                <a href="javascript:;" class="btn btn-outline-dark">RESIDENTIAL</a>
                                <a href="javascript:;" class="btn btn-outline-dark">333 UNITS</a>
                                <a href="javascript:;" class="btn btn-primary">EXPLORE +</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include "includes/footer.php"; ?>
    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>