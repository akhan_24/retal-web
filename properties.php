<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'home';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body>
    <?php include "includes/header.php"; ?>


    <section class="community">
        <div class="row">
            <div class="col-md-6">
                <div class="txtDv">
                    <h3 class="anim-head"><span class="letters">Communities</span></h3>
                    <div class="row g-0">
                        <div class="col-md-4">
                            <div class="project project01">
                                <figure data-anim><img src="assets/images/property-page-03.png" /></figure>
                                <p>Nesaj Town 2   /   Mixed-Use</p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p>Our developments have common underlying factors and are shaped by four key pillars i.e., changing mindsets, establishing credibility, evoking emotions, and discovering new points of differentiation. In addition, every moment of interaction is a marvel on its own, i.e., architecture and design, landscaped areas, walkways, gardens, and recreational features. </p>
                            <a class="btn">View all projects on map <i class="fal fa-globe-americas"></i></a>
                            <div class="project project02">
                                <figure data-anim><img src="assets/images/property-page-02.png" /></figure>
                                <p>Ewan Al Qayrawan   /   Residential</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <figure data-anim data-anim-delay="200"><img src="assets/images/property-page-01.png" /></figure>
            </div>
        </div>
    </section>
    <section class="propertyPageSlide">
        <div class="container">
            <h4>Filter by property type</h4>
        </div>
        <div class="sliderDv">
            <div class="item active" data-filter="residential">
                <figure data-anim><img src="assets/images/property-slider-01.png"/></figure>
                <p>Residential</p>
                <div class="activity">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
                        <g id="Group_331" data-name="Group 331" transform="translate(1828.59 -637) rotate(90)">
                            <g id="Group_327" data-name="Group 327" transform="translate(-0.113 -0.494)">
                                <path id="Path_553" data-name="Path 553" d="M32,0A32,32,0,1,1,0,32,32,32,0,0,1,32,0Z" transform="translate(637.113 1765.084)" fill="#fff"/>
                            </g>
                            <g id="Group_330" data-name="Group 330" transform="translate(-1.344 -0.494)">
                                <g id="Group_329" data-name="Group 329">
                                    <g id="Group_328" data-name="Group 328">
                                        <line id="Line_71" data-name="Line 71" x2="11.311" transform="translate(664.344 1797.084)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_135" data-name="Path 135" d="M671.655,1793.084l4,4-4,4" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="item" data-filter="mixeduse">
                <figure data-anim data-anim-delay="200"><img src="assets/images/property-slider-02.png"/></figure>
                <p>Mixed-Use</p>
                <div class="activity">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
                        <g id="Group_331" data-name="Group 331" transform="translate(1828.59 -637) rotate(90)">
                            <g id="Group_327" data-name="Group 327" transform="translate(-0.113 -0.494)">
                                <path id="Path_553" data-name="Path 553" d="M32,0A32,32,0,1,1,0,32,32,32,0,0,1,32,0Z" transform="translate(637.113 1765.084)" fill="#fff"/>
                            </g>
                            <g id="Group_330" data-name="Group 330" transform="translate(-1.344 -0.494)">
                                <g id="Group_329" data-name="Group 329">
                                    <g id="Group_328" data-name="Group 328">
                                        <line id="Line_71" data-name="Line 71" x2="11.311" transform="translate(664.344 1797.084)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_135" data-name="Path 135" d="M671.655,1793.084l4,4-4,4" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="item" data-filter="commercial">
                <figure data-anim data-anim-delay="400"><img src="assets/images/property-slider-03.png"/></figure>
                <p>Commercial</p>
                <div class="activity">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
                        <g id="Group_331" data-name="Group 331" transform="translate(1828.59 -637) rotate(90)">
                            <g id="Group_327" data-name="Group 327" transform="translate(-0.113 -0.494)">
                                <path id="Path_553" data-name="Path 553" d="M32,0A32,32,0,1,1,0,32,32,32,0,0,1,32,0Z" transform="translate(637.113 1765.084)" fill="#fff"/>
                            </g>
                            <g id="Group_330" data-name="Group 330" transform="translate(-1.344 -0.494)">
                                <g id="Group_329" data-name="Group 329">
                                    <g id="Group_328" data-name="Group 328">
                                        <line id="Line_71" data-name="Line 71" x2="11.311" transform="translate(664.344 1797.084)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_135" data-name="Path 135" d="M671.655,1793.084l4,4-4,4" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="item" data-filter="residential">
                <figure data-anim data-anim-delay="600"><img src="assets/images/property-slider-01.png"/></figure>
                <p>Residential</p>
                <div class="activity">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
                        <g id="Group_331" data-name="Group 331" transform="translate(1828.59 -637) rotate(90)">
                            <g id="Group_327" data-name="Group 327" transform="translate(-0.113 -0.494)">
                                <path id="Path_553" data-name="Path 553" d="M32,0A32,32,0,1,1,0,32,32,32,0,0,1,32,0Z" transform="translate(637.113 1765.084)" fill="#fff"/>
                            </g>
                            <g id="Group_330" data-name="Group 330" transform="translate(-1.344 -0.494)">
                                <g id="Group_329" data-name="Group 329">
                                    <g id="Group_328" data-name="Group 328">
                                        <line id="Line_71" data-name="Line 71" x2="11.311" transform="translate(664.344 1797.084)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_135" data-name="Path 135" d="M671.655,1793.084l4,4-4,4" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="item" data-filter="mixeduse">
                <figure data-anim data-anim-delay="800"><img src="assets/images/property-slider-02.png"/></figure>
                <p>Mixed-Use</p>
                <div class="activity">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
                        <g id="Group_331" data-name="Group 331" transform="translate(1828.59 -637) rotate(90)">
                            <g id="Group_327" data-name="Group 327" transform="translate(-0.113 -0.494)">
                                <path id="Path_553" data-name="Path 553" d="M32,0A32,32,0,1,1,0,32,32,32,0,0,1,32,0Z" transform="translate(637.113 1765.084)" fill="#fff"/>
                            </g>
                            <g id="Group_330" data-name="Group 330" transform="translate(-1.344 -0.494)">
                                <g id="Group_329" data-name="Group 329">
                                    <g id="Group_328" data-name="Group 328">
                                        <line id="Line_71" data-name="Line 71" x2="11.311" transform="translate(664.344 1797.084)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_135" data-name="Path 135" d="M671.655,1793.084l4,4-4,4" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="item" data-filter="commercial">
                <figure data-anim data-anim-delay="1000"><img src="assets/images/property-slider-03.png"/></figure>
                <p>Commercial</p>
                <div class="activity">
                    <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64">
                        <g id="Group_331" data-name="Group 331" transform="translate(1828.59 -637) rotate(90)">
                            <g id="Group_327" data-name="Group 327" transform="translate(-0.113 -0.494)">
                                <path id="Path_553" data-name="Path 553" d="M32,0A32,32,0,1,1,0,32,32,32,0,0,1,32,0Z" transform="translate(637.113 1765.084)" fill="#fff"/>
                            </g>
                            <g id="Group_330" data-name="Group 330" transform="translate(-1.344 -0.494)">
                                <g id="Group_329" data-name="Group 329">
                                    <g id="Group_328" data-name="Group 328">
                                        <line id="Line_71" data-name="Line 71" x2="11.311" transform="translate(664.344 1797.084)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_135" data-name="Path 135" d="M671.655,1793.084l4,4-4,4" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
        </div>
        <div class="btnDv">
            <a class="btn">View all properties</a>
        </div>
    </section>
    <section class="filterDv">
        <div class="container">
            <p>Filter By: </p>
            <div class="fieldDv">
                <select>
                    <option>City</option>
                </select>
            </div>
            <div class="fieldDv">
                <select>
                    <option>Property Type</option>
                </select>
            </div>
            <div class="fieldDv">
                <select>
                    <option>Area</option>
                </select>
            </div>
        </div>
    </section>

    <section class="listingDv">
        <div class="container">
            <div class="row g-1">
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-01.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-02.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 txtItem">
                    <h3 class="anim-head"><span class="letters">Vibrancy, </span><span class="letters">wellbeing, </span><span class="letters">livelihood.</span></h3>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-01.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-02.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-01.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 txtItem">
                    <h3 class="anim-head"><span class="letters">Homes exuding </span><span class="letters">class, permanence, and quality. </span></h3>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-01.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-02.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-01.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 txtItem">
                    <h3 class="anim-head"><span class="letters">Every element is </span><span class="letters">carefully considered.</span></h3>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-02.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 txtItem">
                    <h3 class="anim-head"><span class="letters">Paying attention </span><span class="letters">to balanced integration.</span></h3>
                </div>
                <div class="col-md-4 property">
                    <p>Dammam <svg xmlns="http://www.w3.org/2000/svg" width="9.127" height="12.956" viewBox="0 0 9.127 12.956">
                            <path id="Path_138" data-name="Path 138" d="M723.229,3060.775c0,3.242-4.313,8.019-4.313,8.019s-4.314-4.777-4.314-8.019a4.314,4.314,0,1,1,8.627,0Zm-2.779,0a1.535,1.535,0,1,0-1.534,1.534A1.545,1.545,0,0,0,720.45,3060.775Z" transform="translate(-714.352 -3056.211)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="0.5"/>
                        </svg>
                    </p>
                    <div class="imgArea">
                        <figure data-anim><img src="assets/images/property-listing-01.png"/></figure>
                        <div class="markingBtn">
                            <a href="#" class="compare">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.649" height="13.209" viewBox="0 0 13.649 13.209">
                                    <g id="Group_1782" data-name="Group 1782" transform="translate(-403.658 -2213.895)" opacity="0.5">
                                        <path id="Path_766" data-name="Path 766" d="M110.174,11.677,106.81,8.314l3.364-3.364" transform="translate(303.771 2209.299)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_441" data-name="Line 441" x1="6.728" transform="translate(410.58 2217.612)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_767" data-name="Path 767" d="M60.67,91.074l3.364,3.364L60.67,97.8" transform="translate(346.352 2128.95)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_442" data-name="Line 442" x2="6.728" transform="translate(403.658 2223.388)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </a>
                            <a href="#" class="fav">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10.754" viewBox="0 0 12 10.754">
                                    <g id="Group_475" data-name="Group 475" transform="translate(-10.985 -15.035)" opacity="0.501">
                                        <g id="Path_169" data-name="Path 169" transform="translate(0)" fill="none">
                                            <path d="M14.178,15.035a3.118,3.118,0,0,0-2.264.971,3.406,3.406,0,0,0,0,4.676l4.734,4.964a.461.461,0,0,0,.652.016l.016-.016,4.739-4.96a3.4,3.4,0,0,0,0-4.676,3.118,3.118,0,0,0-4.527,0l-.543.562-.543-.567A3.117,3.117,0,0,0,14.178,15.035Z" stroke="none"/>
                                            <path d="M 14.17818641662598 16.03500175476074 C 13.59241676330566 16.03500175476074 13.0452766418457 16.26996040344238 12.63758659362793 16.69656181335449 C 11.77055549621582 17.60391044616699 11.77087593078613 19.08215141296387 12.6383056640625 19.99181175231934 L 16.98281097412109 24.54787254333496 L 21.33202743530273 19.99582099914551 C 22.19991683959961 19.08757019042969 22.19991683959961 17.6097412109375 21.33198547363281 16.70144081115723 C 20.92269515991211 16.2730712890625 20.37557601928711 16.03715133666992 19.79141616821289 16.03715133666992 C 19.20725631713867 16.03715133666992 18.6601448059082 16.2730712890625 18.25085639953613 16.70144081115723 L 18.24712562561035 16.70532035827637 L 16.98166656494141 18.01559066772461 L 15.71884536743164 16.69661140441895 C 15.31110572814941 16.26996040344238 14.76395606994629 16.03500175476074 14.17818641662598 16.03500175476074 M 14.17818641662598 15.03500175476074 C 15.00002670288086 15.03500175476074 15.82171630859375 15.35688018798828 16.44178581237793 16.00570106506348 L 16.98480606079102 16.5728702545166 L 17.5278263092041 16.0106201171875 C 18.76796531677246 14.71266174316406 20.81487655639648 14.71266174316406 22.05501556396484 16.0106201171875 C 23.29515647888184 17.30844116210938 23.29515647888184 19.38887023925781 22.05501556396484 20.68668174743652 C 20.47592544555664 22.33927154541016 18.89546585083008 23.99371147155762 17.31637573242188 25.64631080627441 C 17.3112964630127 25.65169143676758 17.30606651306152 25.65693092346191 17.30068588256836 25.66200065612793 C 17.11629676818848 25.83778190612793 16.82425498962402 25.83071136474609 16.64847564697266 25.64631080627441 L 11.91459655761719 20.68191146850586 C 10.67614555358887 19.38317108154297 10.6744556427002 17.30351066589355 11.91459655761719 16.00570106506348 C 12.5346565246582 15.35688018798828 13.35650634765625 15.03500175476074 14.17818641662598 15.03500175476074 Z" stroke="none" fill="#000"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </div>
                        <picture><img src="assets/images/property-listing-watermark.png"/></picture>
                    </div>
                    <div class="txtArea">
                        <div class="headDv">
                            <h3>Ewan Al Nawras</h3>
                            <div class="price">
                                <h6>Starting from</h6>
                                <p>2,400,000 <span class="abbr">SAR</span></p>
                            </div>
                        </div>
                        <div class="detDv">
                            <p>Ewan Al Nawras is a villa community with more than 75 deluxe residential villas located in Al Nawras, a district of Al Khobar…</p>
                        </div>
                        <div class="footDv">
                            <ul>
                                <li>Branded Villas</li>
                                <li>2,500 sqft</li>
                                <li>78 Units</li>
                            </ul>
                            <a href="#" class="btn">Explore +</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 txtItem">
                    <h3 class="anim-head"><span class="letters">From social points to </span><span class="letters">high quality public spaces.</span></h3>
                </div>
            </div>
        </div>
    </section>


    <?php include "includes/footer.php"; ?>
    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>