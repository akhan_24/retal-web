<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'Blog';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body class="page">
    <?php include "includes/header.php"; ?>
    
    <div class="page-wrapper">

        <!-- MEDIA CENTER HERO SECTION -->
        <div class="page-media-center escape-transHeader mb-35">
            <div class="container  pt-5">
                
                <div class="page-back-link mb-25">
                    <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                        <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                            <g id="Group_18" data-name="Group 18">
                            <g id="Group_17" data-name="Group 17">
                                <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <path id="Path_14" data-name="Path 14" d="M-1515.5,866.983l-4.244-4.244,4.244-4.244" transform="translate(1519.74 -858.496)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                            </g>
                        </g>
                    </svg>
                    BACK TO ABOUT</a>
                </div>
                <div class="media-center-title">
                    <h1>Blog</h1>
                </div>

                <!-- <div class="row mc-intro-row">
                    <div class="col-lg-1 col-md-12"></div>
                    <div class="col-lg-4 col-md-6">
                        <div class="mc-intro">
                            <span>ACHIEVEMENTS</span>
                            <h2>Retal wins Developer of the Year at ABA 2020</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</p>
                        </div>
                        <div class="mc-intro-action">
                            <a href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="mc-hero">
                            <figure>
                                <img src="assets/images/media-center-hero.png">
                            </figure>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

        <!-- MEDIA CENTER TABS -->
        <div class="container-fluid">
            <!-- MEDIA CENTER TABS CONTENT *PRESS -->
            <div class="row mc-tabs-row" id="pressRelease">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-11 col-md-12">
                    <div class="mc-tabs-content-wrap">
                        <div class="row mc-content-row pr-0">
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>

                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>

                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mc-content-load">
                            <a href="#">LOAD MORE <span>+</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        



    </div>


    <?php include "includes/footer.php"; ?>
    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>






