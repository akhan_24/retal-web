<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'home';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body class="ipo transHeader">
    <?php include "includes/header.php"; ?>

    <section class="ipobanner">
        <figure data-anim><img src="assets/images/ipo-banner.png"/></figure>
        <h2 class="anim-head"><span class="letters">Shaping the future of</span> <span class="letters">Saudi Arabia</span></h2>
    </section>
    <section class="anouncingSec">
        <div class="container">
            <h2 class="anim-head"><span class="letters">Announcing our Intention to Offer.</span> <span class="letters">We're going public.</span></h2>
            <p>We offer a full-service integrated platform, delivering solutions beyond construction. Our business model is scalable and is principally focused on off-plan sales in premier locations across Saudi Arabia. We have an engaged and experienced leadership team that generates superior returns and with strong economic and demographic headwinds over the long-term, Retal is poised to be Saudi Arabia’s homebuilder champion for years to come.</p>

            <div class="colItem row">
                <div class="col-md-5">
                    <p>On the [date | month] [2021], Retal formally announced its Intention to Offer [X]% of its share capital to the public through an initial public offering (“IPO”) and listing its share on the Main Market of the Saudi Exchange. Institutional and retail investors will be allowed the opportunity to acquire shares and participate in the long-term growth plans of the Company and the Saudi real estate market.</p>
                </div>
                <div class="col-md-7">
                    <figure data-anim><img src="assets/images/ipo-logo-01.png"/></figure>
                </div>
            </div>

        </div>
    </section>
    <section class="retalInNumbers">
        <div class="container">
            <h4>Key Metrics</h4>
            <h2 class="anim-head"><span class="letters">Retal </span> <span class="letters">in numbers</span></h2>
            <div class="numbersDv row g-2">
                <div class="col-md-3">
                    <div class="numbersItem">
                        <h5>30+</h5>
                        <p>Projects completed or working on</p>
                    </div>
                    <div class="numbersItem">
                        <h5>07</h5>
                        <p>Customer centres in Saudi Arabia</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="numbersItem">
                        <h5>6K+</h5>
                        <p>Units developed or under construction</p>
                    </div>
                    <div class="numbersItem">
                        <h5>02</h5>
                        <p>digital platforms serving sales and homeowners</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="numbersItem">
                        <h5>5.3B</h5>
                        <p>SAR is the total value of our projects</p>
                    </div>
                    <div class="numbersItem">
                        <h5>07</h5>
                        <p>Customer centres in Saudi Arabia</p>
                    </div>
                    <div class="numbersItem">
                        <h5>07</h5>
                        <p>Customer centres in Saudi Arabia</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="numbersItem">
                        <h5>30+</h5>
                        <p>Projects completed or working on</p>
                    </div>
                    <div class="numbersItem">
                        <h5>07</h5>
                        <p>Customer centres in Saudi Arabia</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="investmentHigh">
        <div class="container">
            <h4>Investment Highlights</h4>
            <div class="row">
                <div class="col-md-6">
                    <figure data-anim><img src="assets/images/ipo-img-01.png"/></figure>
                </div>
                <div class="col-md-6">
                    <p>In a short timeframe, Retal has launched and achieved significant traction as a next generation builder and a trusted expert in building residential and commercial real estate. The Company aims to be the #1 real estate developer in Saudi Arabia.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="faqSec">
        <div class="container">
            <div class="accord active">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="timeLine ipo">
        <div class="container">
            <h4>IPO TIMELINE</h4>
        </div>

        <div class="container">
            <div class="timelineSliderDv">
                <div class="item" data-year="1960">
                    <div class="row mb-4">
                        <div class="col-md-8">
                            <figure class="slideImg"><img src="assets/images/timeline-slide-img.png"/></figure>
                        </div>
                        <div class="col-md-4">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>
                                        9th Dec,<br>
                                        2021
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="slideTitle">
                                <h3>
                                    Start of Participating<br>
                                    Entities Book Building
                                </h3>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="slideDesc">
                                <p class="text-align-right">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item" data-year="1970">
                    <div class="row mb-4">
                        <div class="col-md-8">
                            <figure class="slideImg"><img src="assets/images/feture-slider-01.png"/></figure>
                        </div>
                        <div class="col-md-4">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>
                                        9th Dec,<br>
                                        2021
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="slideTitle">
                                <h3>
                                    Start of Participating<br>
                                    Entities Book Building
                                </h3>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="slideDesc">
                                <p class="text-align-right">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item" data-year="1980">
                    <div class="row mb-4">
                        <div class="col-md-8">
                            <figure class="slideImg"><img src="assets/images/feture-slider-01.png"/></figure>
                        </div>
                        <div class="col-md-4">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>
                                        9th Dec,<br>
                                        2021
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="slideTitle">
                                <h3>
                                    Start of Participating<br>
                                    Entities Book Building
                                </h3>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="slideDesc">
                                <p class="text-align-right">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item" data-year="2022">
                    <div class="row mb-4">
                        <div class="col-md-8">
                            <figure class="slideImg"><img src="assets/images/feture-slider-01.png"/></figure>
                        </div>
                        <div class="col-md-4">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>
                                        9th Dec,<br>
                                        2021
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="slideTitle">
                                <h3>
                                    Start of Participating<br>
                                    Entities Book Building
                                </h3>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="slideDesc">
                                <p class="text-align-right">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="keynotes">
        <div class="container">
            <h4>Key Documents</h4>
            <div class="keynoteList">
                <table>
                    <thead>
                    <tr>
                        <th>Document</th>
                        <th>Download</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Intention to Float</td>
                        <td>
                            <a href="#" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37"><g id="Group_1711" data-name="Group 1711" transform="translate(0.5 0.5)"><g id="Group_6" data-name="Group 6"><path id="Path_3" data-name="Path 3" d="M0,.087v4.9a.087.087,0,0,0,.127.077l4.73-2.478a.087.087,0,0,0,0-.154L.126.009A.087.087,0,0,0,0,.087Z" transform="translate(20.538 16.273) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/><line id="Line_337" data-name="Line 337" y2="7.252" transform="translate(18 8.935)" fill="none" stroke="#000" stroke-width="1"/><line id="Line_338" data-name="Line 338" x2="14.505" transform="translate(10.748 25.615)" fill="none" stroke="#000" stroke-width="1"/></g></g></svg>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Prospectus</td>
                        <td>
                            <a href="#" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37"><g id="Group_1711" data-name="Group 1711" transform="translate(0.5 0.5)"><g id="Group_6" data-name="Group 6"><path id="Path_3" data-name="Path 3" d="M0,.087v4.9a.087.087,0,0,0,.127.077l4.73-2.478a.087.087,0,0,0,0-.154L.126.009A.087.087,0,0,0,0,.087Z" transform="translate(20.538 16.273) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/><line id="Line_337" data-name="Line 337" y2="7.252" transform="translate(18 8.935)" fill="none" stroke="#000" stroke-width="1"/><line id="Line_338" data-name="Line 338" x2="14.505" transform="translate(10.748 25.615)" fill="none" stroke="#000" stroke-width="1"/></g></g></svg>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Price Range</td>
                        <td>
                            <a href="#" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37"><g id="Group_1711" data-name="Group 1711" transform="translate(0.5 0.5)"><g id="Group_6" data-name="Group 6"><path id="Path_3" data-name="Path 3" d="M0,.087v4.9a.087.087,0,0,0,.127.077l4.73-2.478a.087.087,0,0,0,0-.154L.126.009A.087.087,0,0,0,0,.087Z" transform="translate(20.538 16.273) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/><line id="Line_337" data-name="Line 337" y2="7.252" transform="translate(18 8.935)" fill="none" stroke="#000" stroke-width="1"/><line id="Line_338" data-name="Line 338" x2="14.505" transform="translate(10.748 25.615)" fill="none" stroke="#000" stroke-width="1"/></g></g></svg>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Pricing</td>
                        <td>
                            <a href="#" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37"><g id="Group_1711" data-name="Group 1711" transform="translate(0.5 0.5)"><g id="Group_6" data-name="Group 6"><path id="Path_3" data-name="Path 3" d="M0,.087v4.9a.087.087,0,0,0,.127.077l4.73-2.478a.087.087,0,0,0,0-.154L.126.009A.087.087,0,0,0,0,.087Z" transform="translate(20.538 16.273) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/><line id="Line_337" data-name="Line 337" y2="7.252" transform="translate(18 8.935)" fill="none" stroke="#000" stroke-width="1"/><line id="Line_338" data-name="Line 338" x2="14.505" transform="translate(10.748 25.615)" fill="none" stroke="#000" stroke-width="1"/></g></g></svg>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Retail Conclusion</td>
                        <td>
                            <a href="#" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37"><g id="Group_1711" data-name="Group 1711" transform="translate(0.5 0.5)"><g id="Group_6" data-name="Group 6"><path id="Path_3" data-name="Path 3" d="M0,.087v4.9a.087.087,0,0,0,.127.077l4.73-2.478a.087.087,0,0,0,0-.154L.126.009A.087.087,0,0,0,0,.087Z" transform="translate(20.538 16.273) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/><line id="Line_337" data-name="Line 337" y2="7.252" transform="translate(18 8.935)" fill="none" stroke="#000" stroke-width="1"/><line id="Line_338" data-name="Line 338" x2="14.505" transform="translate(10.748 25.615)" fill="none" stroke="#000" stroke-width="1"/></g></g></svg>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Listing and Commencement of Trading on Tadawul</td>
                        <td>
                            <a href="#" class="btn">
                                <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37" viewBox="0 0 37 37"><g id="Group_1711" data-name="Group 1711" transform="translate(0.5 0.5)"><g id="Group_6" data-name="Group 6"><path id="Path_3" data-name="Path 3" d="M0,.087v4.9a.087.087,0,0,0,.127.077l4.73-2.478a.087.087,0,0,0,0-.154L.126.009A.087.087,0,0,0,0,.087Z" transform="translate(20.538 16.273) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/><line id="Line_337" data-name="Line 337" y2="7.252" transform="translate(18 8.935)" fill="none" stroke="#000" stroke-width="1"/><line id="Line_338" data-name="Line 338" x2="14.505" transform="translate(10.748 25.615)" fill="none" stroke="#000" stroke-width="1"/></g></g></svg>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="faqSec">
        <div class="container">
            <h4>Frequently Asked Questions</h4>
            <h2 class="anim-head"><span class="letters">FAQ</span></h2>
            <div class="accord active">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
            <div class="accord">
                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                <div class="inner">
                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="subscribe">
        <div class="container">

            <h4>Subscribe</h4>
            <h3>How to <span>get involved</span></h3>
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h6>
                            <small>
                                SUBSCRIBE 
                                <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                                    <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)" fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"></line>
                                </svg>
                            </small>
                        </h6>
                    </div>
                </div>
            </div>-->
            
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="leftBox">
                        <h3>How to <span>get involved</span></h3>
                    </div>
                </div>
            </div>-->
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 col-1">
                    <div class="numberBox">
                        <span class="countFirst">
                            1
                        </span>
                        <span class="dash"></span>
                        <span class="countLast">
                            4
                        </span>
                    </div>
                </div>

                <div class="col-md-10 col-11">
                    <div class="sliderDvOverFlow">
                        <div class="subscribeSliderDv">
                            <div class="item">
                                <div class="slideBox">
                                    <h4>Step 1</h4>
                                    <figure data-anim><img src="<?php echo $dir; ?>/assets/images/subscribe-slide-img1.png" alt=""></figure>
                                    <p>Ensure you have an active current account with one of the participating banks.</p>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a type="button" class="btn btn-link">
                                            PARTICIPATING BANKS
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                                                <g id="Group_532" data-name="Group 532" transform="translate(-1528.033 -3846.107)">
                                                    <g id="Group_531" data-name="Group 531">
                                                    <line id="Line_79" data-name="Line 79" x2="12" transform="translate(1528.033 3850.704)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_172" data-name="Path 172" d="M1535.789,3846.46l4.244,4.244-4.244,4.244" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slideBox">
                                    <h4>Step 2</h4>
                                    <figure data-anim><img src="<?php echo $dir; ?>/assets/images/subscribe-slide-img2.png" alt=""></figure>
                                    <p>Visit (retal.com.sa/ipo) to get more information about the company and its public offering through the ITF and prospectus.</p>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a type="button" class="btn btn-link">
                                            ITF
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                                                <g id="Group_532" data-name="Group 532" transform="translate(-1528.033 -3846.107)">
                                                    <g id="Group_531" data-name="Group 531">
                                                    <line id="Line_79" data-name="Line 79" x2="12" transform="translate(1528.033 3850.704)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_172" data-name="Path 172" d="M1535.789,3846.46l4.244,4.244-4.244,4.244" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>

                                        <a type="button" class="btn btn-link">
                                            PROSPECTUS
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                                                <g id="Group_532" data-name="Group 532" transform="translate(-1528.033 -3846.107)">
                                                    <g id="Group_531" data-name="Group 531">
                                                    <line id="Line_79" data-name="Line 79" x2="12" transform="translate(1528.033 3850.704)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_172" data-name="Path 172" d="M1535.789,3846.46l4.244,4.244-4.244,4.244" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slideBox">
                                    <h4>Step 3</h4>
                                    <figure data-anim><img src="<?php echo $dir; ?>/assets/images/subscribe-slide-img3.png" alt=""></figure>
                                    <p>Visit (retal.com.sa/ipo) to get more information about the company and its public offering through the ITF and prospectus.</p>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a type="button" class="btn btn-link">
                                            ITF
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                                                <g id="Group_532" data-name="Group 532" transform="translate(-1528.033 -3846.107)">
                                                    <g id="Group_531" data-name="Group 531">
                                                    <line id="Line_79" data-name="Line 79" x2="12" transform="translate(1528.033 3850.704)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_172" data-name="Path 172" d="M1535.789,3846.46l4.244,4.244-4.244,4.244" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>

                                        <a type="button" class="btn btn-link">
                                            PROSPECTUS
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                                                <g id="Group_532" data-name="Group 532" transform="translate(-1528.033 -3846.107)">
                                                    <g id="Group_531" data-name="Group 531">
                                                    <line id="Line_79" data-name="Line 79" x2="12" transform="translate(1528.033 3850.704)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_172" data-name="Path 172" d="M1535.789,3846.46l4.244,4.244-4.244,4.244" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="financAdvisor">
        <div class="container">
            <h4>Financial Advisors</h4>
            <h2 class="anim-head"><span class="letters">If you are interested to invest in [Vida], please contact one of the Receiving Banks listed below which are collaborating with the Company in order to help investors and shareholders.</span></h2>
            <div class="advisorList">
                <div class="row">
                    <div class="col-md-4 col-6">
                        <div class="advisorItem">
                            <figure><img src="assets/images/ipo-logo-02.png"/></figure>
                            <h5>Samba Bank</h5>
                            <p><a href="#">email@bank.com</a><br/><a href="#">+966 11 524 9100</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="advisorItem">
                            <figure><img src="assets/images/ipo-logo-03.png"/></figure>
                            <h5>Samba Bank</h5>
                            <p><a href="#">email@bank.com</a><br/><a href="#">+966 11 524 9100</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="advisorItem">
                            <figure><img src="assets/images/ipo-logo-04.png"/></figure>
                            <h5>Samba Bank</h5>
                            <p><a href="#">email@bank.com</a><br/><a href="#">+966 11 524 9100</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="advisorItem">
                            <figure><img src="assets/images/ipo-logo-05.png"/></figure>
                            <h5>Samba Bank</h5>
                            <p><a href="#">email@bank.com</a><br/><a href="#">+966 11 524 9100</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="advisorItem">
                            <figure><img src="assets/images/ipo-logo-06.png"/></figure>
                            <h5>Samba Bank</h5>
                            <p><a href="#">email@bank.com</a><br/><a href="#">+966 11 524 9100</a></p>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="advisorItem">
                            <figure><img src="assets/images/ipo-logo-07.png"/></figure>
                            <h5>Samba Bank</h5>
                            <p><a href="#">email@bank.com</a><br/><a href="#">+966 11 524 9100</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php include "includes/footer.php"; ?>

    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>