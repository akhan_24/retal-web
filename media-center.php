<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'home';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body class="page">
    <?php include "includes/header.php"; ?>
    
    <div class="page-wrapper">

        <!-- MEDIA CENTER HERO SECTION -->
        <div class="page-media-center escape-transHeader mb-35">
            <div class="container">
                
                <div class="page-back-link mb-25">
                    <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                        <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                            <g id="Group_18" data-name="Group 18">
                            <g id="Group_17" data-name="Group 17">
                                <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <path id="Path_14" data-name="Path 14" d="M-1515.5,866.983l-4.244-4.244,4.244-4.244" transform="translate(1519.74 -858.496)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                            </g>
                        </g>
                    </svg>
                    BACK TO ABOUT</a>
                </div>
                <div class="media-center-title">
                    <h1>Media Centre</h1>
                </div>

                <div class="row mc-intro-row">
                    <div class="col-lg-1 col-md-12"></div>
                    <div class="col-lg-4 col-md-6">
                        <div class="mc-intro">
                            <span>ACHIEVEMENTS</span>
                            <h2>Retal wins Developer of the Year at ABA 2020</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna</p>
                        </div>
                        <div class="mc-intro-action">
                            <a href="#">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="mc-hero">
                            <figure>
                                <img src="assets/images/media-center-hero.png">
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- MEDIA CENTER SEARCH -->
        <div class="page-media-center-search mb--5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 col-md-12"></div>
                    <div class="col-lg-4 col-md-12">
                        <div class="pmcs-wrapper">
                            <input type="text" name="search_media_center" placeholder="SEARCH MEDIA CENTRE">
                            <button type="submit">
                                <svg xmlns="http://www.w3.org/2000/svg" width="11.41" height="11.164" viewBox="0 0 11.41 11.164">
                                    <g id="Group_1319" data-name="Group 1319" transform="translate(0.5 0.5)" opacity="0.25">
                                        <circle id="Ellipse_156" data-name="Ellipse 156" cx="4.339" cy="4.339" r="4.339" fill="rgba(0,0,0,0)" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_264" data-name="Line 264" x1="3.056" y1="2.776" transform="translate(7.518 7.518)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12"></div>
                </div>
            </div>
        </div>

        <!-- MEDIA CENTER TABS -->
        <div class="container">
            <div class="row navDv">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-11 col-md-12">
                    <div class="mc-scrolling-tabs">
                        <ul>
                            <li>
                                <a href="#pressRelease" class="t-active">PRESS RELEASES</a>
                            </li>
                            <li>
                                <a href="#events">EVENTS</a>
                            </li>
                            <li>
                                <a href="#gallery">GALLERY</a>
                            </li>
                            <li>
                                <a href="#downloads">DOWNLOADS</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- MEDIA CENTER TABS CONTENT *PRESS -->
            <div class="row mc-tabs-row" id="pressRelease">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-11 col-md-12">
                    <div class="mc-tabs-head">
                        <div class="mc-tabs-title">
                            <h4>Press Releases</h4>
                        </div>
                        <div class="mc-tabs-filter">
                            <label>Filter By</label>
                            <ul>
                                <li>
                                    <a href="#">Achievements</a>
                                </li>
                                <li>
                                    <a href="#" class="f-active">Year</a>
                                </li>
                                <li>
                                    <a href="#">Month</a>
                                </li>
                                <li>
                                    <a href="#">Subject</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mc-tabs-content-wrap">
                        <div class="mc-filter-name">
                            <h6>2021</h6>
                        </div>
                        <div class="row mc-content-row">
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit, sed diam </h3>
                                            <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="mc-content-load">
                            <a href="#">LOAD MORE <span>+</span></a>
                        </div>
                    </div>

                </div>
            </div>

             <!-- MEDIA CENTER TABS CONTENT *EVENTS -->
            <div class="row mc-tabs-row d-none" id="events">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-11 col-md-12">
                    <div class="mc-tabs-head">
                        <div class="mc-tabs-title">
                            <h4>EVENTS</h4>
                        </div>
                        <div class="mc-tabs-filter">
                            <label>Filter By</label>
                            <ul>
                                <li>
                                    <a href="#"  class="f-active">Year</a>
                                </li>
                                <li>
                                    <a href="#">Month</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mc-tabs-content-wrap">
                        <div class="mc-filter-name">
                            <h6>2021</h6>
                        </div>
                        <div class="row mc-content-row">
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                            <a href="#" class="i-action">LEARN MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="mc-content-load">
                            <a href="#">LOAD MORE <span>+</span></a>
                        </div>
                    </div>

                </div>
            </div>

             <!-- MEDIA CENTER TABS CONTENT *GALLERY -->
            <div class="row mc-tabs-row d-none" id="gallery">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-11 col-md-12">
                    <div class="mc-tabs-head">
                        <div class="mc-tabs-title">
                            <h4>GALLERY</h4>
                        </div>
                        <div class="mc-tabs-filter">
                            <label>Filter By</label>
                             <ul>
                                <li>
                                    <a href="#">Achievements</a>
                                </li>
                                <li>
                                    <a href="#" class="f-active">Year</a>
                                </li>
                                <li>
                                    <a href="#">Month</a>
                                </li>
                                <li>
                                    <a href="#">Subject</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mc-tabs-content-wrap">
                        <div class="mc-filter-name">
                            <h6>2021</h6>
                        </div>
                        <div class="row mc-content-row">
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="info-box">
                                    <div class="info-box-top">
                                        <div class="info-box-feature">
                                            <figure>
                                                <img src="assets/images/dummy_image_1.png" alt="">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="info-box-bottom">
                                        <div class="info-box-content">
                                            <span class="i-date">AUGUST 22, 2021</span>
                                            <h3 class="i-title">Reference Event Name</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="mc-content-load">
                            <a href="#">LOAD MORE <span>+</span></a>
                        </div>
                    </div>

                </div>
            </div>

            <!-- MEDIA CENTER TABS CONTENT *GALLERY -->
            <div class="row mc-tabs-row d-none" id="downloads">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-11 col-md-12">
                    
                    <div class="mc-tabs-head">
                        <div class="mc-tabs-title">
                            <h4>Downloads</h4>
                        </div>
                    </div>
                    <div class="mc-tabs-content-wrap">
                       <div class="row download-row">
                           <div class="col-lg-4 col-md-6">
                                <div class="download-box">
                                    <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                    <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg" width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                        <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                            <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3" transform="translate(-707.46 -7101.118)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </svg>
                                </div>
                           </div>
                           <div class="col-lg-4 col-md-6">
                               <div class="download-box">
                                    <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                    <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg" width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                        <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                            <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3" transform="translate(-707.46 -7101.118)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </svg>
                                </div>
                           </div>
                           <div class="col-lg-4 col-md-6">
                               <div class="download-box">
                                    <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                    <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg" width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                        <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                            <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3" transform="translate(-707.46 -7101.118)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </svg>
                                </div>
                           </div>
                           <div class="col-lg-4 col-md-6">
                               <div class="download-box">
                                    <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                    <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg" width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                        <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                            <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3" transform="translate(-707.46 -7101.118)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    </svg>
                                </div>
                           </div>
                       </div>
                       <div class="download-details">
                           <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh  euismod tincidunt ut laoreet dolore magna  aliquam erat volutpat. Ut wisi enim ad minim  veniam, quis nostrud exerci tation.
                            </p>
                            <p>For more info contact: <a href="mailto:marketing@retal.ae">marketing@retal.ae</a></p>
                       </div>
                    </div>

                </div>
            </div>


        </div>

    </div>


    <?php include "includes/footer.php"; ?>
    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>






