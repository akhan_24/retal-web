<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'About';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body class="home">
<?php include "includes/header.php"; ?>

<section class="about-retal">
    <div class="container-fluid">
        <div class="bgretal">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1366"
                 height="930.979" viewBox="0 0 1366 930.979">
                <defs>
                    <clipPath id="clip-path">
                        <rect id="Rectangle_220" data-name="Rectangle 220" width="1366" height="930.979"
                              transform="translate(277 130.813)" fill="none"/>
                    </clipPath>
                </defs>
                <g id="Group_980" data-name="Group 980" transform="translate(-277 -130.813)"
                   clip-path="url(#clip-path)">
                    <g id="Group_979" data-name="Group 979" transform="translate(241.926 1)">
                        <g id="Group_978" data-name="Group 978" transform="translate(277 94.338)">
                            <path id="Path_316" data-name="Path 316"
                                  d="M1094.18,326.879s13.073,5.922,16.631,7.419l.041.027a6.524,6.524,0,0,0,.855.324c.014,0,0-.007.014-.007,2.634.958,5.172.517,8.013-2.242,18.59,18.418,24.726,42.641,35.111,65.14,2.247-3.406,4.082-6.764,4.716-10.315,2.689-15.335,4.62-30.8,7.654-46.061,1.875-9.461,4.069-19.19,8.219-27.8,5.406-11.219,11.722-11.233,19.8-1.655,2.4,2.848,4.565,5.882,8.191,10.612,2.869-7.992,6.206-13.811,6.855-19.914.786-7.46-9.309-17.969-17.514-20.244-6.44-1.786-12.771,1.647-18.217,10a26.969,26.969,0,0,0-3.158,6.033c-2.22,6.777-4.8,13.514-6.137,20.479-2.757,14.308-7.695,46.661-7.695,46.661l-1.379-3.275s-3.337-8.984-4.937-13.515c-5.9-16.534-14.756-31.132-28.947-41.813a57.955,57.955,0,0,0-15.307-8.164c-5.516-1.972-8.564-.11-10.826,5.144a131.764,131.764,0,0,0-5.2,13.646c-1.682,5.53-1.062,7.544,3.227,9.523Z"
                                  transform="translate(-421.005 -127.521)" fill="#f8f3f1"/>
                            <path id="Path_317" data-name="Path 317"
                                  d="M1202,542.509l-61.12-35.849-28.795,49.212,53.77,31.58L1143.4,625.887l61.092,35.8,28.96-49.143-53.894-31.574Z"
                                  transform="translate(-424.898 -167.363)" fill="#f8f3f1"/>
                            <path id="Path_318" data-name="Path 318"
                                  d="M1198.131,863.207c-117.468,78.848-213.782,141.533-268.558,141.533-52.722,0-63.878-39.021-65.975-61.52-1.076-8.853.234-18.694,1.214-24.354,6.068-28.505,29.236-96.851,66.953-150.132C946.01,818.918,972.723,874,1015.5,867.868c53.08-7.668,72.76-39.952,72.76-101.182s-49.191-110.518-49.191-110.518l-5.544,59.624c28.574,26.885,37.731,68.029,37.731,68.029-16.7,24.017-42.958,28.4-42.958,28.4-37.207,8.054-51.77-22.52-67.05-61.182-15.293-38.634-21.168-104.36-57.548-377.594C867.156,100.137,701.172,94.338,701.172,94.338s-21.927,32.822-16.438,69.974c5.53,37.089,83.13,423.034,83.13,423.034s28.464,118.674,28.464,145.408c0,10.329-40.6,53.853-116.682,103.423,0,0-188.9,135.12-402.645,166.928v54.577C477.268,1033.7,699.806,873.8,699.806,873.8,761.231,831,805.07,783.642,812.214,736.05c0,0,6.62-76.551-5.557-139.927C794.631,532.7,722.52,179.55,719.2,158.74,848.8,195.478,872.272,375.3,872.272,375.3S913.313,679.729,923.7,734.43c1.751,9.24,4.04,19.431,6.867,29.926-33.1,35.828-89.073,109.345-91.928,197.7a117.247,117.247,0,0,0,1.241,17.272c4.634,23.947,23.665,81.468,99.885,81.468,96.218,0,221.767-119.048,328.341-188.056,0,0,53.894-40.786,132.969-61.741V758.46C1344.38,773.016,1277.137,804.8,1198.131,863.207Z"
                                  transform="translate(-277 -94.338)" fill="#f8f3f1"/>
                        </g>
                    </g>
                </g>
            </svg>
        </div>

        <div class="retalContent">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="content">
                            <h2 class="animToStart"><span>Retal represents the quintessence of</span> <span
                                        class="high">craftsmanship</span> <span>byembracing the </span> <span
                                        class="high">virtues of urbanism.</span></h2>
                            <h2 class="animToStart"><span>Today, our total asset value accounts for</span> <span
                                        class="high">SAR 4 billion,with 2617 units under construction and 1940 completed units.</span>
                            </h2>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="anchorBox">
                            <button class="btn btn-anchor">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="21" viewBox="0 0 13 21">
                                    <g id="Group_1011" data-name="Group 1011" transform="translate(-1458.191 -745.576)">
                                        <rect id="Rectangle_230" data-name="Rectangle 230" width="12" height="20" rx="6"
                                              transform="translate(1458.691 746.076)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                        <line id="Line_216" data-name="Line 216" y2="4"
                                              transform="translate(1464.691 750.076)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ceo-messages">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-11">
                <div class="leftBox">
                    <h6>
                        <small>
                            LEADERSHIP
                            <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                                <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                      fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </small>
                    </h6>
                    <h3 class="anim-head"><span class="letters">CEO's</span> <span class="letters high">Message</span>
                    </h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>

            <div class="col-md-4 col-12">
                <div class="leftBox">
                    <p>At Retal, we are place-makers, timekeepers, and caregivers.</p>
                    <p>We perceive every vacant plot as an opportunity to build legacies and timeless places for future
                        generations. By enhancing the appeal of a precinct, we give people a reason to stay and
                        linger.</p>

                    <a href="#ceoMessageDv" class="btn btn-discover popLnk">
                        DISCOVER THE VISION
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                             viewBox="0 0 12.707 9.195">
                            <g id="Group_1372" data-name="Group 1372" transform="translate(12 8.841) rotate(180)">
                                <g id="Group_18" data-name="Group 18">
                                    <g id="Group_17" data-name="Group 17">
                                        <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)"
                                              fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_14" data-name="Path 14"
                                              d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                              transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="col-md-7 col-12" style="position: relative;">
                <div class="rightBox">
                    <figure data-anim><img src="<?php echo $dir; ?>/assets/images/ceo-img.png" alt=""></figure>
                    <h4>Abdullah Faisal Al-Braikan</h4>
                    <h3>CEO</h3>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="corporate-info">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="rightBox mb">
                    <h6>
                        <small>
                            CORPORATE INFORMATION
                            <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                                <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                      fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </small>
                    </h6>
                </div>
            </div>
            <div class="col-md-7 col-12 p-0">
                <div class="leftBox">
                    <figure data-anim><img src="<?php echo $dir; ?>/assets/images/corporate-info-img.png" alt="">
                    </figure>
                </div>
            </div>

            <div class="col-md-5 col-12">
                <div class="rightBox dt">
                    <h6>
                        <small>
                            CORPORATE INFORMATION
                            <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                                <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                      fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </small>
                    </h6>

                    <h3 class="anim-head"><span class="letters">The Real Estate</span> <span class="letters fst-italic">Champions in KSA</span>
                    </h3>

                    <p>The transformation journey stemmed in the Eastern Province of Saudi Arabia in 2012 when the Retal
                        Construction Company joined the stellar Al Fozan Group of Companies and commenced the
                        development of iconic residential, commercial, and mixed-use properties.</p>

                    <a href="#corporateInfo" class="btn btn-discover popLnk">
                        DIVE DEEP INTO OUR JOURNEY
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                             viewBox="0 0 12.707 9.195">
                            <g id="Group_1372" data-name="Group 1372" transform="translate(12 8.841) rotate(180)">
                                <g id="Group_18" data-name="Group 18">
                                    <g id="Group_17" data-name="Group 17">
                                        <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)"
                                              fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                        <path id="Path_14" data-name="Path 14"
                                              d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                              transform="translate(1519.74 -858.496)" fill="none" stroke="#fff"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="corporate-governance">
    <div class="container-fluid">
        <div class="governance">
            <div class="">
                <div class="row">
                    <div class="col-md-5 col-12">
                        <div class="content">
                            <h6>
                                <small>
                                    CORPORATE GOVERNANCE
                                    <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1"
                                         viewBox="0 0 31.652 1">
                                        <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                              fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                                    </svg>
                                </small>
                            </h6>

                            <h3 class="anim-head"><span class="letters">Retal's</span><br><span
                                        class="letters fst-italic">Management</span></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="detailBox">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="leftBox">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.</p>
                        <p>Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                            consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                            consequat, vel illum dolore.</p>
                    </div>
                </div>

                <div class="col-md-8 col-12">
                    <div class="rightBox">
                        <a href="#boardOfDirectors" class="board-member popLnk">
                            <span>BOARD MEMBERS</span>
                            <span>
                                    VISIT PAGE
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.39" height="10.161"
                                         viewBox="0 0 12.39 10.161">
                                        <g id="Group_533" data-name="Group 533"
                                           transform="translate(-1529.717 -3846.107)">
                                            <g id="Group_532" data-name="Group 532"
                                               transform="translate(1528.033 3846.46)">
                                            <g id="Group_531" data-name="Group 531" transform="translate(0)">
                                                <line id="Line_79" data-name="Line 79" x2="12"
                                                      transform="translate(1.684 4.722)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path id="Path_172" data-name="Path 172"
                                                      d="M1535.789,3846.461l4.727,4.727-4.727,4.728"
                                                      transform="translate(-1527.15 -3846.461)" fill="none"
                                                      stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                            </g>
                                        </g>
                                    </svg>
                                </span>
                        </a>

                        <a href="#executiveManagment" class="board-member popLnk">
                            Executive managment
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.39" height="10.161"
                                 viewBox="0 0 12.39 10.161">
                                <g id="Group_533" data-name="Group 533" transform="translate(-1529.717 -3846.107)">
                                    <g id="Group_532" data-name="Group 532" transform="translate(1528.033 3846.46)">
                                        <g id="Group_531" data-name="Group 531" transform="translate(0)">
                                            <line id="Line_79" data-name="Line 79" x2="12"
                                                  transform="translate(1.684 4.722)" fill="none" stroke="#000"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                            <path id="Path_172" data-name="Path 172"
                                                  d="M1535.789,3846.461l4.727,4.727-4.727,4.728"
                                                  transform="translate(-1527.15 -3846.461)" fill="none" stroke="#000"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>

                        <h3>Download</h3>

                        <a href="#" class="board-member">
                            Corporate Governance Report
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.748" height="13.742"
                                 viewBox="0 0 11.748 13.742">
                                <g id="Group_646" data-name="Group 646" transform="translate(1.96 5.328)">
                                    <path id="Path_188" data-name="Path 188"
                                          d="M707.46,7101.117l3.914,3.914,3.914-3.914"
                                          transform="translate(-707.46 -7101.117)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                                <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                      fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                      fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="group-structure">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5"></div>

            <div class="col-md-7">
                <div class="mainBox">
                    <h3>GROUP STRUCTURE</h3>

                    <figure><img src="<?php echo $dir; ?>/assets/images/group-retal-img.png" alt=""></figure>
                </div>

                <div class="rightBox">
                    <h5>STRUCTURE LEVEL</h5>

                    <div class="row">
                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>
                    </div>

                    <br>
                    <br>

                    <h5>STRUCTURE LEVEL</h5>

                    <div class="row">
                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="stBox">
                                <figure><img src="<?php echo $dir; ?>/assets/images/barnd-logo.png" alt=""></figure>
                                <h6>BRAND LOGO</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="awards">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="leftBox">
                    <h6>
                        <small>
                            AWARDS
                            <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                                <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                      fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>
                        </small>
                    </h6>

                    <p class="anim-head"><span class="letters">By exemplifying the key attributes of placemaking attributes, we envision revolutionising the Kingdom into a top destination of choice.</span>
                    </p>

                    <div class="business-award mb">
                        <div class="row">
                            <div class="col-12">
                                <button class="btn btn-awards">
                                    <h4>ARABIAN BUSINESS AWARDS</h4>

                                    <h5>DEVELOPER OF THE YEAR 2015</h5>
                                </button>

                                <button class="btn btn-awards">
                                    <h4>ARABIAN BUSINESS AWARDS</h4>

                                    <h5>DEVELOPER OF THE YEAR 2015</h5>
                                </button>

                                <button class="btn btn-awards">
                                    <h4>ARABIAN BUSINESS AWARDS</h4>

                                    <h5>DEVELOPER OF THE YEAR 2015</h5>
                                </button>

                                <button class="btn btn-awards">
                                    <h4>ARABIAN BUSINESS AWARDS</h4>

                                    <h5>DEVELOPER OF THE YEAR 2015</h5>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="rightBox">
                    <div class="row">
                        <div class="col-6">
                            <div class="awBox">
                                <figure data-anim>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="49.128" height="49.13" viewBox="0 0 49.128 49.13">
                                        <defs>
                                            <clipPath id="clip-path">
                                                <path id="Path_340" data-name="Path 340"
                                                      d="M-157.372,336.236a1.738,1.738,0,0,1-.921-.619,1.772,1.772,0,0,1-.331-1.1,3.464,3.464,0,0,1,.336-1.382,6.309,6.309,0,0,1,.906-1.407,8.564,8.564,0,0,1,1.333-1.273,8.643,8.643,0,0,1,1.64-1,6.173,6.173,0,0,1,1.663-.517,3.5,3.5,0,0,1,1.384.032,1.868,1.868,0,0,1,.97.562,1.727,1.727,0,0,1,.4,1.064,3.238,3.238,0,0,1-.288,1.4,6.049,6.049,0,0,1-.9,1.454,8.552,8.552,0,0,1-1.38,1.335,8.605,8.605,0,0,1-1.71,1.027,6.085,6.085,0,0,1-1.708.5,3.915,3.915,0,0,1-.527.035,2.929,2.929,0,0,1-.864-.119m5.374-12.38a13.52,13.52,0,0,0-3.407,1.106,18.2,18.2,0,0,0-3.347,2.051,18.051,18.051,0,0,0-2.743,2.6,13.43,13.43,0,0,0-1.894,2.871,7.387,7.387,0,0,0-.772,2.859,3.758,3.758,0,0,0,.6,2.312,3.538,3.538,0,0,0,1.856,1.34,6.763,6.763,0,0,0,2.886.216,14.14,14.14,0,0,0,3.568-1.345,16.55,16.55,0,0,0,3.414-2.265,17.709,17.709,0,0,0,2.927-2.8,16.351,16.351,0,0,0,2.138-2.784,5.656,5.656,0,0,0,.8-2.981,3.529,3.529,0,0,0-1.045-2.158,4,4,0,0,0-2.108-1.014,6.432,6.432,0,0,0-1.367-.137,8.8,8.8,0,0,0-1.509.134"
                                                      transform="translate(164.164 -323.722)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-2">
                                                <path id="Path_341" data-name="Path 341"
                                                      d="M-400.858,68.961l.006-.1a1,1,0,0,0-.006.1m14.335-18.02a24.977,24.977,0,0,0-2.961,1.591c-3.347,2.085-5.336,3.182-7.789,6.187a14.847,14.847,0,0,0-2.25,3.294c-.339.691-.746,1.837-.746,1.837l-.169,1.285-.267,1.74-.129,1.556-.017.425c.005-.1.012-.2.022-.3.01-.137.031-.276.051-.417s.046-.288.076-.438.067-.3.107-.448a18.771,18.771,0,0,1,2.169-4.969,20.177,20.177,0,0,1,3.765-4.629,39.607,39.607,0,0,1,4.717-4.228c1.207-.888,4.151-2.742,5.89-2.978.128-.015.156-.027.114-.027a9.824,9.824,0,0,0-2.583.522"
                                                      transform="translate(400.858 -50.419)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-3">
                                                <path id="Path_382" data-name="Path 382"
                                                      d="M-411.349,73.838l-.124-.2c.04.07.081.137.124.2m34.479-29.783h0l-.039-.04a.379.379,0,0,0,.036.04m-.036-.04c-.068-.075-.137-.147-.207-.214Zm-11.82-4.517a17.372,17.372,0,0,0-3.618,1.2,34.554,34.554,0,0,0-4.55,2.376,45.77,45.77,0,0,0-6.2,4.589,47,47,0,0,0-5.232,5.369,35.189,35.189,0,0,0-3.889,5.7,17.34,17.34,0,0,0-1.754,5.558,9.035,9.035,0,0,0-.031.982,25.892,25.892,0,0,0,.624,3.806,15.027,15.027,0,0,0,.433,1.588c.245.714.763,1.775.763,1.775l.257.452.449.733a5.2,5.2,0,0,1-.405-.867,6.275,6.275,0,0,1-.316-1.233,8.09,8.09,0,0,1-.1-1.39,11.261,11.261,0,0,1,.125-1.529,19.269,19.269,0,0,1,2.063-5.954,35.708,35.708,0,0,1,4.032-6.051,47.247,47.247,0,0,1,5.455-5.648,46.371,46.371,0,0,1,6.507-4.785,35.213,35.213,0,0,1,5.376-2.675,21.258,21.258,0,0,1,4.852-1.293,10.935,10.935,0,0,1,4.071.144,5.867,5.867,0,0,1,2.7,1.447l-1.027-1.064-1.349-1.2-.685-.534-1.54-.614a18.337,18.337,0,0,0-2.978-.808,18.66,18.66,0,0,0-2.559-.222,7.1,7.1,0,0,0-1.475.134"
                                                      transform="translate(414 -39.364)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-4">
                                                <path id="Path_426" data-name="Path 426"
                                                      d="M-368.21,124.633l-.568-.2c.184.075.372.142.568.2m20.616-39a21.818,21.818,0,0,0-5.72,1.489,36.4,36.4,0,0,0-6.3,3.314,47.483,47.483,0,0,0-6.745,5.337,47.032,47.032,0,0,0-5.5,6.192,34.969,34.969,0,0,0-3.869,6.545,18.785,18.785,0,0,0-1.684,6.344,10.669,10.669,0,0,0,.076,2.1,7.561,7.561,0,0,0,.481,1.8,5.925,5.925,0,0,0,.874,1.494,5.777,5.777,0,0,0,1.252,1.166l1.647,1.049,1.571.835,1.741.771.989.35a6.575,6.575,0,0,1-1.431-.791,5.856,5.856,0,0,1-1.441-1.539,6.6,6.6,0,0,1-.855-2.036,9.448,9.448,0,0,1-.239-2.5,17.017,17.017,0,0,1,1.433-5.969,31.008,31.008,0,0,1,3.515-6.147,41.981,41.981,0,0,1,5.088-5.79,41.981,41.981,0,0,1,6.3-4.944,31.484,31.484,0,0,1,6.541-3.241,22.9,22.9,0,0,1,6.616-1.256,8.324,8.324,0,0,1,4.567,1.221c1.244.825,1.484,2.215,1.894,3.9l.654-.142-2.1-5.546-.542-1.213a6.139,6.139,0,0,0-3.3-2.528,10.022,10.022,0,0,0-3.174-.4,23.191,23.191,0,0,0-2.344.124"
                                                      transform="translate(377.429 -85.511)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-5">
                                                <path id="Path_464" data-name="Path 464"
                                                      d="M-274.1,217.774c.276-.094.553-.2.833-.3Zm.833-.3.071-.028.311-.124-.382.152M-269,188.212a30.356,30.356,0,0,0-6.4,3.065,39.272,39.272,0,0,0-6.074,4.663,38.128,38.128,0,0,0-4.844,5.464,27.611,27.611,0,0,0-3.257,5.77,14.612,14.612,0,0,0-1.176,5.551,7.806,7.806,0,0,0,.551,2.884,5.739,5.739,0,0,0,1.514,2.13A5.428,5.428,0,0,0-286.3,219a17.08,17.08,0,0,0,3.2.438l.261-.005,1.841-.117,1.8-.229.894-.159,1.644-.365,1.588-.455.435-.142.542-.2a15.355,15.355,0,0,1-3.452.77,8.694,8.694,0,0,1-3.709-.363,4.988,4.988,0,0,1-2.464-1.8,5.65,5.65,0,0,1-.983-3.082,11.2,11.2,0,0,1,.818-4.455,21.016,21.016,0,0,1,2.538-4.651,29.32,29.32,0,0,1,3.872-4.4,30.216,30.216,0,0,1,4.892-3.731,23.368,23.368,0,0,1,5.159-2.386,13.385,13.385,0,0,1,4.574-.666,6.51,6.51,0,0,1,3.526,1.116,4.911,4.911,0,0,1,1.912,3.006c.145-.4,1.02-.706,1.135-1.116s.216-.83.3-1.248.336-2.7.357-3.127a4.651,4.651,0,0,0-.2-.721,5.724,5.724,0,0,0-2.293-2.322,11.07,11.07,0,0,0-5.188-1.35c-.082,0-.165,0-.247,0a17.66,17.66,0,0,0-5.458.972"
                                                      transform="translate(290.759 -187.24)" fill="none"></path>
                                            </clipPath>
                                        </defs>
                                        <g id="Group_1020" data-name="Group 1020" transform="translate(27.644 31.631)">
                                            <g id="Group_1019" data-name="Group 1019" transform="translate(0 0)"
                                               clip-path="url(#clip-path)">
                                                <path id="Path_322" data-name="Path 322"
                                                      d="M-188.164,174.075a18.6,18.6,0,0,0-18.592-18.6,18.607,18.607,0,0,0-18.6,18.6,18.608,18.608,0,0,0,18.6,18.607,18.605,18.605,0,0,0,18.592-18.607m-1.156.01h0a17.543,17.543,0,0,1-17.53,17.543,17.542,17.542,0,0,1-17.531-17.543,17.541,17.541,0,0,1,17.531-17.54,17.543,17.543,0,0,1,17.53,17.54"
                                                      transform="translate(218.581 -174.088)" fill="#da091d"></path>
                                                <path id="Path_323" data-name="Path 323"
                                                      d="M-181.786,182.628a17.541,17.541,0,0,0-17.53-17.54,17.541,17.541,0,0,0-17.532,17.54,17.542,17.542,0,0,0,17.532,17.543,17.541,17.541,0,0,0,17.53-17.543m-1.033.01h0a16.482,16.482,0,0,1-16.47,16.481,16.484,16.484,0,0,1-16.47-16.481,16.482,16.482,0,0,1,16.47-16.476,16.48,16.48,0,0,1,16.47,16.476"
                                                      transform="translate(211.019 -182.641)" fill="#dc2121"></path>
                                                <path id="Path_324" data-name="Path 324"
                                                      d="M-174.307,191.18A16.477,16.477,0,0,0-190.773,174.7a16.477,16.477,0,0,0-16.47,16.476,16.48,16.48,0,0,0,16.47,16.481,16.479,16.479,0,0,0,16.466-16.481m-1.033.01h0a15.417,15.417,0,0,1-15.406,15.417,15.418,15.418,0,0,1-15.407-15.417,15.417,15.417,0,0,1,15.407-15.415A15.417,15.417,0,0,1-175.34,191.19"
                                                      transform="translate(202.476 -191.193)" fill="#de3027"></path>
                                                <path id="Path_325" data-name="Path 325"
                                                      d="M-165.737,199.71a15.417,15.417,0,0,0-15.406-15.415A15.419,15.419,0,0,0-196.55,199.71a15.419,15.419,0,0,0,15.408,15.417,15.417,15.417,0,0,0,15.406-15.417m-1.156.01h0a14.355,14.355,0,0,1-14.345,14.353A14.353,14.353,0,0,1-195.58,199.72a14.353,14.353,0,0,1,14.344-14.351,14.355,14.355,0,0,1,14.345,14.351"
                                                      transform="translate(192.967 -199.722)" fill="#e03e2f"></path>
                                                <path id="Path_326" data-name="Path 326"
                                                      d="M-159.357,208.262A14.354,14.354,0,0,0-173.7,193.911a14.354,14.354,0,0,0-14.343,14.351A14.355,14.355,0,0,0-173.7,222.618a14.355,14.355,0,0,0,14.345-14.356m-1.034.01h0a13.292,13.292,0,0,1-13.282,13.292,13.291,13.291,0,0,1-13.281-13.292,13.289,13.289,0,0,1,13.281-13.289,13.289,13.289,0,0,1,13.282,13.289"
                                                      transform="translate(185.403 -208.274)" fill="#e34b37"></path>
                                                <path id="Path_327" data-name="Path 327"
                                                      d="M-151.879,216.815a13.289,13.289,0,0,0-13.281-13.287,13.291,13.291,0,0,0-13.282,13.287A13.291,13.291,0,0,0-165.16,230.1a13.29,13.29,0,0,0,13.281-13.289m-1.034.01h0a12.227,12.227,0,0,1-12.218,12.228,12.227,12.227,0,0,1-12.219-12.228A12.227,12.227,0,0,1-165.131,204.6a12.226,12.226,0,0,1,12.218,12.225"
                                                      transform="translate(176.862 -216.827)" fill="#e55741"></path>
                                                <path id="Path_328" data-name="Path 328"
                                                      d="M-143.3,225.346a12.227,12.227,0,0,0-12.218-12.225,12.229,12.229,0,0,0-12.219,12.225,12.227,12.227,0,0,0,12.219,12.225A12.225,12.225,0,0,0-143.3,225.346m-1.156.01h0A11.163,11.163,0,0,1-155.61,236.52a11.163,11.163,0,0,1-11.154-11.164,11.161,11.161,0,0,1,11.154-11.161,11.161,11.161,0,0,1,11.156,11.161"
                                                      transform="translate(167.34 -225.359)" fill="#e7634a"></path>
                                                <path id="Path_329" data-name="Path 329"
                                                      d="M-136.921,233.9a11.162,11.162,0,0,0-11.154-11.162A11.163,11.163,0,0,0-159.233,233.9a11.164,11.164,0,0,0,11.158,11.164A11.163,11.163,0,0,0-136.921,233.9m-1.034.01h0a10.1,10.1,0,0,1-10.093,10.1,10.1,10.1,0,0,1-10.1-10.1,10.1,10.1,0,0,1,10.1-10.1,10.1,10.1,0,0,1,10.093,10.1"
                                                      transform="translate(159.779 -233.909)" fill="#ea6e55"></path>
                                                <path id="Path_330" data-name="Path 330"
                                                      d="M-129.441,242.449a10.1,10.1,0,0,0-10.093-10.1,10.1,10.1,0,0,0-10.094,10.1,10.1,10.1,0,0,0,10.094,10.1,10.1,10.1,0,0,0,10.093-10.1m-1.033.01h0a9.038,9.038,0,0,1-9.032,9.039,9.039,9.039,0,0,1-9.031-9.039,9.037,9.037,0,0,1,9.031-9.036,9.036,9.036,0,0,1,9.032,9.036"
                                                      transform="translate(151.236 -242.461)" fill="#ec7a5f"></path>
                                                <path id="Path_331" data-name="Path 331"
                                                      d="M-120.871,250.981a9.038,9.038,0,0,0-9.032-9.036,9.039,9.039,0,0,0-9.031,9.036,9.037,9.037,0,0,0,9.031,9.036,9.036,9.036,0,0,0,9.032-9.036m-1.156.01h0A7.974,7.974,0,0,1-130,258.963a7.971,7.971,0,0,1-7.97-7.972,7.973,7.973,0,0,1,7.97-7.972,7.975,7.975,0,0,1,7.968,7.972"
                                                      transform="translate(141.727 -250.993)" fill="#ef876c"></path>
                                                <path id="Path_332" data-name="Path 332"
                                                      d="M-114.493,259.533a7.972,7.972,0,0,0-7.967-7.972,7.974,7.974,0,0,0-7.97,7.972,7.975,7.975,0,0,0,7.97,7.975,7.973,7.973,0,0,0,7.967-7.975m-1.033.01h0a6.909,6.909,0,0,1-6.906,6.911,6.91,6.91,0,0,1-6.907-6.911,6.912,6.912,0,0,1,6.907-6.91,6.911,6.911,0,0,1,6.906,6.91"
                                                      transform="translate(134.163 -259.546)" fill="#f19379"></path>
                                                <path id="Path_333" data-name="Path 333"
                                                      d="M-107.014,268.083a6.91,6.91,0,0,0-6.907-6.908,6.91,6.91,0,0,0-6.906,6.908,6.911,6.911,0,0,0,6.906,6.911,6.91,6.91,0,0,0,6.907-6.911m-1.034.01h0a5.847,5.847,0,0,1-5.844,5.847,5.847,5.847,0,0,1-5.844-5.847,5.848,5.848,0,0,1,5.844-5.847,5.849,5.849,0,0,1,5.844,5.847"
                                                      transform="translate(125.622 -268.096)" fill="#f3a187"></path>
                                                <path id="Path_334" data-name="Path 334"
                                                      d="M-98.423,276.615a5.847,5.847,0,0,0-5.842-5.847,5.847,5.847,0,0,0-5.844,5.847,5.845,5.845,0,0,0,5.844,5.847,5.845,5.845,0,0,0,5.842-5.847m-1.156.01h0a4.784,4.784,0,0,1-4.78,4.783,4.785,4.785,0,0,1-4.783-4.783,4.785,4.785,0,0,1,4.783-4.783,4.784,4.784,0,0,1,4.78,4.783"
                                                      transform="translate(116.09 -276.627)" fill="#f5ae98"></path>
                                                <path id="Path_335" data-name="Path 335"
                                                      d="M-92.056,285.168a4.783,4.783,0,0,0-4.78-4.783,4.784,4.784,0,0,0-4.782,4.783,4.784,4.784,0,0,0,4.782,4.785,4.784,4.784,0,0,0,4.78-4.785m-1.034.01h0a3.719,3.719,0,0,1-3.719,3.721,3.721,3.721,0,0,1-3.719-3.721,3.723,3.723,0,0,1,3.719-3.721,3.721,3.721,0,0,1,3.719,3.721"
                                                      transform="translate(108.539 -285.18)" fill="#f8beaa"></path>
                                                <path id="Path_336" data-name="Path 336"
                                                      d="M-84.586,293.72A3.719,3.719,0,0,0-88.305,290a3.72,3.72,0,0,0-3.719,3.719,3.721,3.721,0,0,0,3.719,3.721,3.719,3.719,0,0,0,3.719-3.721m-1.033.01h0a2.659,2.659,0,0,1-2.657,2.658,2.657,2.657,0,0,1-2.655-2.658,2.656,2.656,0,0,1,2.655-2.657,2.659,2.659,0,0,1,2.657,2.657"
                                                      transform="translate(100.006 -293.732)" fill="#facdbb"></path>
                                                <path id="Path_337" data-name="Path 337"
                                                      d="M-77.107,302.251a2.656,2.656,0,0,0-2.655-2.657,2.658,2.658,0,0,0-2.657,2.657,2.657,2.657,0,0,0,2.657,2.657,2.655,2.655,0,0,0,2.655-2.657m-1.034.01h0a1.593,1.593,0,0,1-1.593,1.6,1.594,1.594,0,0,1-1.594-1.6,1.594,1.594,0,0,1,1.594-1.594,1.593,1.593,0,0,1,1.593,1.594"
                                                      transform="translate(91.464 -302.264)" fill="#fcdbcd"></path>
                                                <path id="Path_338" data-name="Path 338"
                                                      d="M-69.63,311.9a1.593,1.593,0,0,0-1.593-1.594,1.6,1.6,0,0,0-1.594,1.594,1.6,1.6,0,0,0,1.594,1.6,1.593,1.593,0,0,0,1.593-1.6m-1.033-.112h0a.533.533,0,0,1-.532.532.531.531,0,0,1-.53-.532.532.532,0,0,1,.53-.532.535.535,0,0,1,.532.532"
                                                      transform="translate(82.924 -311.794)" fill="#feece3"></path>
                                                <path id="Path_339" data-name="Path 339"
                                                      d="M-62.139,319.334a.534.534,0,0,0-.532-.532.532.532,0,0,0-.53.532.533.533,0,0,0,.53.535.534.534,0,0,0,.532-.535"
                                                      transform="translate(74.372 -319.346)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <g id="Group_1022" data-name="Group 1022" transform="translate(1.455 1.391)">
                                            <g id="Group_1021" data-name="Group 1021" transform="translate(0 0)"
                                               clip-path="url(#clip-path-2)">
                                                <rect id="Rectangle_231" data-name="Rectangle 231" width="16.862"
                                                      height="0.977" transform="translate(0.002 0.057)"
                                                      fill="#da091d"></rect>
                                                <rect id="Rectangle_232" data-name="Rectangle 232" width="16.862"
                                                      height="0.977" transform="translate(0.002 1.086)"
                                                      fill="#dc2121"></rect>
                                                <rect id="Rectangle_233" data-name="Rectangle 233" width="16.862"
                                                      height="0.979" transform="translate(0.002 2.116)"
                                                      fill="#de3027"></rect>
                                                <rect id="Rectangle_234" data-name="Rectangle 234" width="16.862"
                                                      height="0.977" transform="translate(0.002 3.145)"
                                                      fill="#e03e2f"></rect>
                                                <rect id="Rectangle_235" data-name="Rectangle 235" width="16.862"
                                                      height="0.977" transform="translate(0.002 4.176)"
                                                      fill="#e34b37"></rect>
                                                <rect id="Rectangle_236" data-name="Rectangle 236" width="16.862"
                                                      height="0.977" transform="translate(0.002 5.205)"
                                                      fill="#e55741"></rect>
                                                <rect id="Rectangle_237" data-name="Rectangle 237" width="16.862"
                                                      height="0.98" transform="translate(0.002 6.234)"
                                                      fill="#e7634a"></rect>
                                                <rect id="Rectangle_238" data-name="Rectangle 238" width="16.862"
                                                      height="0.977" transform="translate(0.002 7.264)"
                                                      fill="#ea6e55"></rect>
                                                <rect id="Rectangle_239" data-name="Rectangle 239" width="16.862"
                                                      height="0.977" transform="translate(0.002 8.295)"
                                                      fill="#ec7a5f"></rect>
                                                <rect id="Rectangle_240" data-name="Rectangle 240" width="16.862"
                                                      height="0.98" transform="translate(0.002 9.322)"
                                                      fill="#ef876c"></rect>
                                                <rect id="Rectangle_241" data-name="Rectangle 241" width="16.862"
                                                      height="0.977" transform="translate(0.002 10.354)"
                                                      fill="#f19379"></rect>
                                                <rect id="Rectangle_242" data-name="Rectangle 242" width="16.862"
                                                      height="0.977" transform="translate(0.002 11.383)"
                                                      fill="#f3a187"></rect>
                                                <rect id="Rectangle_243" data-name="Rectangle 243" width="16.862"
                                                      height="0.977" transform="translate(0.002 12.414)"
                                                      fill="#f5ae98"></rect>
                                                <rect id="Rectangle_244" data-name="Rectangle 244" width="16.862"
                                                      height="0.977" transform="translate(0.002 13.444)"
                                                      fill="#f8beaa"></rect>
                                                <rect id="Rectangle_245" data-name="Rectangle 245" width="16.862"
                                                      height="0.977" transform="translate(0.002 14.473)"
                                                      fill="#facdbb"></rect>
                                                <rect id="Rectangle_246" data-name="Rectangle 246" width="16.862"
                                                      height="0.977" transform="translate(0.002 15.504)"
                                                      fill="#fcdbcd"></rect>
                                                <rect id="Rectangle_247" data-name="Rectangle 247" width="16.862"
                                                      height="0.979" transform="translate(0.002 16.531)"
                                                      fill="#feece3"></rect>
                                                <rect id="Rectangle_248" data-name="Rectangle 248" width="16.862"
                                                      height="0.977" transform="translate(0.002 17.563)"
                                                      fill="#fff"></rect>
                                            </g>
                                        </g>
                                        <g id="Group_1024" data-name="Group 1024" transform="translate(0 0.168)">
                                            <g id="Group_1023" data-name="Group 1023" transform="translate(0 0)"
                                               clip-path="url(#clip-path-3)">
                                                <path id="Path_342" data-name="Path 342"
                                                      d="M-475.391-293.114a41.7,41.7,0,0,0-41.674-41.7,41.7,41.7,0,0,0-41.675,41.7,41.7,41.7,0,0,0,41.675,41.7,41.7,41.7,0,0,0,41.674-41.7m-1.142-.018h0a40.647,40.647,0,0,1-40.622,40.644,40.647,40.647,0,0,1-40.619-40.644,40.649,40.649,0,0,1,40.619-40.646,40.649,40.649,0,0,1,40.622,40.646"
                                                      transform="translate(542.725 293.41)" fill="#da091d"></path>
                                                <path id="Path_343" data-name="Path 343"
                                                      d="M-469.076-284.641a40.644,40.644,0,0,0-40.619-40.644,40.647,40.647,0,0,0-40.622,40.644A40.648,40.648,0,0,0-509.694-244a40.645,40.645,0,0,0,40.619-40.644m-1.02-.018h0a39.589,39.589,0,0,1-39.565,39.587,39.589,39.589,0,0,1-39.565-39.587,39.591,39.591,0,0,1,39.565-39.59,39.591,39.591,0,0,1,39.565,39.59"
                                                      transform="translate(535.233 284.937)" fill="#db181f"></path>
                                                <path id="Path_344" data-name="Path 344"
                                                      d="M-461.627-277.27a39.593,39.593,0,0,0-39.565-39.59,39.591,39.591,0,0,0-39.565,39.59,39.589,39.589,0,0,0,39.565,39.587,39.591,39.591,0,0,0,39.565-39.587m-1.023.1h0a38.535,38.535,0,0,1-38.511,38.531,38.534,38.534,0,0,1-38.508-38.531A38.535,38.535,0,0,1-501.161-315.7a38.536,38.536,0,0,1,38.511,38.533"
                                                      transform="translate(526.732 277.445)" fill="#dc1f21"></path>
                                                <path id="Path_345" data-name="Path 345"
                                                      d="M-453.1-267.659a38.535,38.535,0,0,0-38.512-38.531,38.532,38.532,0,0,0-38.508,38.531,38.534,38.534,0,0,0,38.508,38.531A38.536,38.536,0,0,0-453.1-267.659m-1.146-.018h0A37.477,37.477,0,0,1-491.7-230.2a37.477,37.477,0,0,1-37.455-37.474A37.479,37.479,0,0,1-491.7-305.153a37.479,37.479,0,0,1,37.454,37.477"
                                                      transform="translate(517.271 267.955)" fill="#dd2623"></path>
                                                <path id="Path_346" data-name="Path 346"
                                                      d="M-446.794-259.208a37.482,37.482,0,0,0-37.455-37.48,37.482,37.482,0,0,0-37.457,37.48,37.478,37.478,0,0,0,37.457,37.474,37.478,37.478,0,0,0,37.455-37.474m-1.024-.018h0a36.423,36.423,0,0,1-36.4,36.42,36.423,36.423,0,0,1-36.4-36.42,36.423,36.423,0,0,1,36.4-36.423,36.424,36.424,0,0,1,36.4,36.423"
                                                      transform="translate(509.789 259.504)" fill="#de2c26"></path>
                                                <path id="Path_347" data-name="Path 347"
                                                      d="M-439.366-251.816a36.427,36.427,0,0,0-36.4-36.423,36.425,36.425,0,0,0-36.4,36.423,36.425,36.425,0,0,0,36.4,36.42,36.426,36.426,0,0,0,36.4-36.42m-1.024.1h0a35.367,35.367,0,0,1-35.344,35.364,35.365,35.365,0,0,1-35.344-35.364,35.367,35.367,0,0,1,35.344-35.366,35.369,35.369,0,0,1,35.344,35.366"
                                                      transform="translate(501.306 251.99)" fill="#df3429"></path>
                                                <path id="Path_348" data-name="Path 348"
                                                      d="M-431.943-242.224a35.368,35.368,0,0,0-35.344-35.366,35.37,35.37,0,0,0-35.345,35.366,35.368,35.368,0,0,0,35.345,35.364,35.366,35.366,0,0,0,35.344-35.364m-1.02-.018h0a34.312,34.312,0,0,1-34.291,34.308,34.311,34.311,0,0,1-34.287-34.308,34.311,34.311,0,0,1,34.287-34.31,34.313,34.313,0,0,1,34.291,34.31"
                                                      transform="translate(492.825 242.52)" fill="#e0392c"></path>
                                                <path id="Path_349" data-name="Path 349"
                                                      d="M-424.515-233.733a34.31,34.31,0,0,0-34.287-34.31,34.313,34.313,0,0,0-34.291,34.31A34.312,34.312,0,0,0-458.8-199.426a34.31,34.31,0,0,0,34.287-34.307m-1.021-.018h0A33.256,33.256,0,0,1-458.77-200.5,33.255,33.255,0,0,1-492-233.751a33.257,33.257,0,0,1,33.234-33.256,33.258,33.258,0,0,1,33.235,33.256"
                                                      transform="translate(484.342 234.029)" fill="#e03f2f"></path>
                                                <path id="Path_350" data-name="Path 350"
                                                      d="M-417.087-225.26a33.258,33.258,0,0,0-33.234-33.256,33.258,33.258,0,0,0-33.236,33.256,33.256,33.256,0,0,0,33.236,33.253,33.256,33.256,0,0,0,33.234-33.253m-1.023-.018h0a32.2,32.2,0,0,1-32.181,32.2,32.2,32.2,0,0,1-32.178-32.2,32.2,32.2,0,0,1,32.178-32.2,32.2,32.2,0,0,1,32.181,32.2"
                                                      transform="translate(475.862 225.556)" fill="#e14433"></path>
                                                <path id="Path_351" data-name="Path 351"
                                                      d="M-409.66-216.769a32.2,32.2,0,0,0-32.181-32.2,32.2,32.2,0,0,0-32.179,32.2,32.2,32.2,0,0,0,32.179,32.2,32.2,32.2,0,0,0,32.181-32.2m-1.023-.018h0a31.144,31.144,0,0,1-31.124,31.14,31.143,31.143,0,0,1-31.124-31.14,31.145,31.145,0,0,1,31.124-31.143,31.146,31.146,0,0,1,31.124,31.143"
                                                      transform="translate(467.379 217.066)" fill="#e34a36"></path>
                                                <path id="Path_352" data-name="Path 352"
                                                      d="M-401.131-208.276a31.147,31.147,0,0,0-31.127-31.143,31.146,31.146,0,0,0-31.124,31.143,31.144,31.144,0,0,0,31.124,31.14,31.145,31.145,0,0,0,31.127-31.14m-1.146-.018h0a30.09,30.09,0,0,1-30.07,30.087,30.09,30.09,0,0,1-30.068-30.087,30.09,30.09,0,0,1,30.068-30.089,30.091,30.091,0,0,1,30.07,30.089"
                                                      transform="translate(457.918 208.572)" fill="#e4503a"></path>
                                                <path id="Path_353" data-name="Path 353"
                                                      d="M-394.806-199.785a30.09,30.09,0,0,0-30.07-30.087,30.089,30.089,0,0,0-30.07,30.087,30.09,30.09,0,0,0,30.07,30.086,30.092,30.092,0,0,0,30.07-30.086m-1.024-.018h0a29.031,29.031,0,0,1-29.014,29.03,29.031,29.031,0,0,1-29.014-29.03,29.035,29.035,0,0,1,29.014-29.032A29.034,29.034,0,0,1-395.83-199.8"
                                                      transform="translate(450.415 200.082)" fill="#e5553e"></path>
                                                <path id="Path_354" data-name="Path 354"
                                                      d="M-387.38-191.313A29.036,29.036,0,0,0-416.4-220.345a29.036,29.036,0,0,0-29.014,29.032A29.036,29.036,0,0,0-416.4-162.28a29.036,29.036,0,0,0,29.016-29.032m-1.023-.018h0a27.978,27.978,0,0,1-27.961,27.973,27.976,27.976,0,0,1-27.96-27.973,27.978,27.978,0,0,1,27.96-27.976A27.98,27.98,0,0,1-388.4-191.33"
                                                      transform="translate(441.935 191.609)" fill="#e55a42"></path>
                                                <path id="Path_355" data-name="Path 355"
                                                      d="M-378.853-182.822A27.975,27.975,0,0,0-406.811-210.8a27.977,27.977,0,0,0-27.959,27.976,27.98,27.98,0,0,0,27.959,27.976,27.978,27.978,0,0,0,27.957-27.976M-380-182.84h0a26.919,26.919,0,0,1-26.9,26.917,26.917,26.917,0,0,1-26.9-26.917,26.921,26.921,0,0,1,26.9-26.919A26.923,26.923,0,0,1-380-182.84"
                                                      transform="translate(432.472 183.118)" fill="#e65e46"></path>
                                                <path id="Path_356" data-name="Path 356"
                                                      d="M-372.528-174.329a26.92,26.92,0,0,0-26.9-26.919,26.92,26.92,0,0,0-26.9,26.919,26.92,26.92,0,0,0,26.9,26.919,26.92,26.92,0,0,0,26.9-26.919m-1.02-.018h0A25.867,25.867,0,0,1-399.4-148.481a25.87,25.87,0,0,1-25.851-25.866A25.868,25.868,0,0,1-399.4-200.211a25.865,25.865,0,0,1,25.849,25.865"
                                                      transform="translate(424.969 174.625)" fill="#e8644b"></path>
                                                <path id="Path_357" data-name="Path 357"
                                                      d="M-365.11-165.858a25.867,25.867,0,0,0-25.849-25.866,25.866,25.866,0,0,0-25.851,25.866,25.867,25.867,0,0,0,25.851,25.865,25.868,25.868,0,0,0,25.849-25.865m-1.023-.018h0a24.81,24.81,0,0,1-24.794,24.809,24.809,24.809,0,0,1-24.793-24.809,24.809,24.809,0,0,1,24.793-24.809,24.81,24.81,0,0,1,24.794,24.809"
                                                      transform="translate(416.499 166.155)" fill="#e96950"></path>
                                                <path id="Path_358" data-name="Path 358"
                                                      d="M-356.571-157.365a24.812,24.812,0,0,0-24.8-24.809,24.81,24.81,0,0,0-24.792,24.809,24.81,24.81,0,0,0,24.792,24.809,24.812,24.812,0,0,0,24.8-24.809m-1.146-.018h0a23.756,23.756,0,0,1-23.74,23.752A23.755,23.755,0,0,1-405.2-157.383a23.753,23.753,0,0,1,23.739-23.752,23.754,23.754,0,0,1,23.74,23.752"
                                                      transform="translate(407.027 157.661)" fill="#ea6e55"></path>
                                                <path id="Path_359" data-name="Path 359"
                                                      d="M-350.257-148.874A23.753,23.753,0,0,0-374-172.627a23.754,23.754,0,0,0-23.74,23.753A23.756,23.756,0,0,0-374-125.122a23.755,23.755,0,0,0,23.739-23.752m-1.023-.018h0a22.7,22.7,0,0,1-22.683,22.7,22.7,22.7,0,0,1-22.685-22.7,22.7,22.7,0,0,1,22.685-22.7,22.7,22.7,0,0,1,22.683,22.7"
                                                      transform="translate(399.536 149.171)" fill="#ea7258"></path>
                                                <path id="Path_360" data-name="Path 360"
                                                      d="M-342.829-140.4a22.7,22.7,0,0,0-22.686-22.7A22.7,22.7,0,0,0-388.2-140.4a22.7,22.7,0,0,0,22.683,22.7,22.7,22.7,0,0,0,22.686-22.7m-1.024-.018h0a21.645,21.645,0,0,1-21.628,21.642,21.645,21.645,0,0,1-21.63-21.642,21.643,21.643,0,0,1,21.63-21.642,21.643,21.643,0,0,1,21.628,21.642"
                                                      transform="translate(391.053 140.698)" fill="#eb785e"></path>
                                                <path id="Path_361" data-name="Path 361"
                                                      d="M-334.3-131.911a21.642,21.642,0,0,0-21.627-21.642,21.642,21.642,0,0,0-21.629,21.642,21.644,21.644,0,0,0,21.629,21.642A21.643,21.643,0,0,0-334.3-131.911m-1.142-.018h0a20.587,20.587,0,0,1-20.573,20.585,20.587,20.587,0,0,1-20.575-20.585,20.587,20.587,0,0,1,20.575-20.585,20.587,20.587,0,0,1,20.573,20.585"
                                                      transform="translate(381.592 132.207)" fill="#ed7e62"></path>
                                                <path id="Path_362" data-name="Path 362"
                                                      d="M-327.976-123.42a20.588,20.588,0,0,0-20.575-20.586,20.588,20.588,0,0,0-20.573,20.586,20.588,20.588,0,0,0,20.573,20.585,20.588,20.588,0,0,0,20.575-20.585m-1.023-.018h0a19.532,19.532,0,0,1-19.519,19.529,19.531,19.531,0,0,1-19.519-19.529,19.529,19.529,0,0,1,19.519-19.529A19.53,19.53,0,0,1-329-123.438"
                                                      transform="translate(374.089 123.716)" fill="#ee8469"></path>
                                                <path id="Path_363" data-name="Path 363"
                                                      d="M-320.552-114.927a19.529,19.529,0,0,0-19.516-19.529,19.53,19.53,0,0,0-19.519,19.529A19.532,19.532,0,0,0-340.069-95.4a19.531,19.531,0,0,0,19.516-19.529m-1.02-.018h0A18.478,18.478,0,0,1-340.038-96.47,18.477,18.477,0,0,1-358.5-114.945a18.48,18.48,0,0,1,18.462-18.477,18.48,18.48,0,0,1,18.465,18.477"
                                                      transform="translate(365.609 115.223)" fill="#ef896e"></path>
                                                <path id="Path_364" data-name="Path 364"
                                                      d="M-312.022-106.457a18.478,18.478,0,0,0-18.465-18.475,18.476,18.476,0,0,0-18.462,18.475,18.474,18.474,0,0,0,18.462,18.472,18.476,18.476,0,0,0,18.465-18.472m-1.145-.018h0a17.421,17.421,0,0,1-17.408,17.419,17.422,17.422,0,0,1-17.41-17.419,17.422,17.422,0,0,1,17.41-17.421,17.422,17.422,0,0,1,17.408,17.421"
                                                      transform="translate(356.148 106.753)" fill="#f08e74"></path>
                                                <path id="Path_365" data-name="Path 365"
                                                      d="M-305.7-97.964a17.42,17.42,0,0,0-17.409-17.418,17.42,17.42,0,0,0-17.409,17.418,17.421,17.421,0,0,0,17.409,17.418A17.421,17.421,0,0,0-305.7-97.964m-1.023-.018h0a16.363,16.363,0,0,1-16.352,16.362,16.364,16.364,0,0,1-16.356-16.362,16.366,16.366,0,0,1,16.356-16.364A16.365,16.365,0,0,1-306.72-97.981"
                                                      transform="translate(348.645 98.26)" fill="#f19379"></path>
                                                <path id="Path_366" data-name="Path 366"
                                                      d="M-298.27-90.594a16.368,16.368,0,0,0-16.355-16.364,16.366,16.366,0,0,0-16.352,16.364,16.363,16.363,0,0,0,16.352,16.362A16.365,16.365,0,0,0-298.27-90.594m-1.023.1h0a15.306,15.306,0,0,1-15.3,15.305,15.308,15.308,0,0,1-15.3-15.305,15.31,15.31,0,0,1,15.3-15.308,15.308,15.308,0,0,1,15.3,15.308"
                                                      transform="translate(340.162 90.768)" fill="#f29a81"></path>
                                                <path id="Path_367" data-name="Path 367"
                                                      d="M-289.741-80.98a15.306,15.306,0,0,0-15.3-15.305,15.3,15.3,0,0,0-15.3,15.305,15.306,15.306,0,0,0,15.3,15.305,15.308,15.308,0,0,0,15.3-15.305M-290.887-81h0a14.254,14.254,0,0,1-14.244,14.252A14.254,14.254,0,0,1-319.374-81a14.256,14.256,0,0,1,14.243-14.254A14.256,14.256,0,0,1-290.887-81"
                                                      transform="translate(330.701 81.276)" fill="#f3a085"></path>
                                                <path id="Path_368" data-name="Path 368"
                                                      d="M-283.415-72.53a14.255,14.255,0,0,0-14.245-14.254A14.252,14.252,0,0,0-311.9-72.53a14.252,14.252,0,0,0,14.241,14.251A14.255,14.255,0,0,0-283.415-72.53m-1.024-.018h0a13.2,13.2,0,0,1-13.187,13.2,13.2,13.2,0,0,1-13.189-13.2,13.2,13.2,0,0,1,13.189-13.2,13.2,13.2,0,0,1,13.187,13.2"
                                                      transform="translate(323.199 72.826)" fill="#f4a68d"></path>
                                                <path id="Path_369" data-name="Path 369"
                                                      d="M-276.01-65.139a13.2,13.2,0,0,0-13.189-13.2,13.2,13.2,0,0,0-13.19,13.2A13.2,13.2,0,0,0-289.2-51.944,13.2,13.2,0,0,0-276.01-65.139m-1.023.1h0A12.141,12.141,0,0,1-289.168-52.9,12.139,12.139,0,0,1-301.3-65.034a12.139,12.139,0,0,1,12.132-12.141,12.141,12.141,0,0,1,12.135,12.141"
                                                      transform="translate(314.738 65.313)" fill="#f5ac95"></path>
                                                <path id="Path_370" data-name="Path 370"
                                                      d="M-268.585-55.546a12.14,12.14,0,0,0-12.132-12.141,12.142,12.142,0,0,0-12.133,12.141,12.141,12.141,0,0,0,12.133,12.138,12.14,12.14,0,0,0,12.132-12.138m-1.021-.018h0a11.085,11.085,0,0,1-11.078,11.082,11.085,11.085,0,0,1-11.078-11.082,11.085,11.085,0,0,1,11.078-11.084,11.085,11.085,0,0,1,11.078,11.084"
                                                      transform="translate(306.256 55.842)" fill="#f6b29b"></path>
                                                <path id="Path_371" data-name="Path 371"
                                                      d="M-261.16-47.074a11.085,11.085,0,0,0-11.077-11.087,11.087,11.087,0,0,0-11.078,11.087A11.084,11.084,0,0,0-272.237-35.99,11.082,11.082,0,0,0-261.16-47.074m-1.021-.018h0A10.029,10.029,0,0,1-272.2-37.063a10.029,10.029,0,0,1-10.022-10.028A10.029,10.029,0,0,1-272.2-57.122a10.029,10.029,0,0,1,10.024,10.03"
                                                      transform="translate(297.775 47.37)" fill="#f7b9a4"></path>
                                                <path id="Path_372" data-name="Path 372"
                                                      d="M-253.72-39.683a10.029,10.029,0,0,0-10.024-10.03,10.028,10.028,0,0,0-10.022,10.03,10.027,10.027,0,0,0,10.022,10.028A10.029,10.029,0,0,0-253.72-39.683m-1.023.1h0a8.973,8.973,0,0,1-8.968,8.971,8.972,8.972,0,0,1-8.968-8.971,8.973,8.973,0,0,1,8.968-8.974,8.973,8.973,0,0,1,8.968,8.974"
                                                      transform="translate(289.283 39.857)" fill="#f8c0ac"></path>
                                                <path id="Path_373" data-name="Path 373"
                                                      d="M-246.313-30.09a8.974,8.974,0,0,0-8.967-8.974,8.976,8.976,0,0,0-8.97,8.974,8.974,8.974,0,0,0,8.97,8.971,8.972,8.972,0,0,0,8.967-8.971m-1.023-.018h0a7.917,7.917,0,0,1-7.914,7.915,7.915,7.915,0,0,1-7.911-7.915,7.915,7.915,0,0,1,7.911-7.917,7.917,7.917,0,0,1,7.914,7.917"
                                                      transform="translate(280.82 30.386)" fill="#fac7b3"></path>
                                                <path id="Path_374" data-name="Path 374"
                                                      d="M-237.784-21.619a7.921,7.921,0,0,0-7.914-7.92,7.922,7.922,0,0,0-7.915,7.92A7.917,7.917,0,0,0-245.7-13.7a7.917,7.917,0,0,0,7.914-7.915m-1.146-.018h0a6.863,6.863,0,0,1-6.857,6.861,6.863,6.863,0,0,1-6.857-6.861,6.863,6.863,0,0,1,6.857-6.864,6.863,6.863,0,0,1,6.857,6.864"
                                                      transform="translate(271.359 21.915)" fill="#facdba"></path>
                                                <path id="Path_375" data-name="Path 375"
                                                      d="M-231.459-13.127a6.864,6.864,0,0,0-6.86-6.864,6.863,6.863,0,0,0-6.858,6.864,6.863,6.863,0,0,0,6.858,6.863,6.864,6.864,0,0,0,6.86-6.863m-1.023-.018h0a5.806,5.806,0,0,1-5.8,5.8,5.8,5.8,0,0,1-5.8-5.8,5.805,5.805,0,0,1,5.8-5.807,5.806,5.806,0,0,1,5.8,5.807"
                                                      transform="translate(263.857 13.423)" fill="#fbd3c3"></path>
                                                <path id="Path_376" data-name="Path 376"
                                                      d="M-224.033-4.636a5.809,5.809,0,0,0-5.8-5.807,5.808,5.808,0,0,0-5.8,5.807,5.808,5.808,0,0,0,5.8,5.807,5.809,5.809,0,0,0,5.8-5.807m-1.023-.018h0A4.751,4.751,0,0,1-229.8.095a4.749,4.749,0,0,1-4.747-4.748A4.75,4.75,0,0,1-229.8-9.4a4.751,4.751,0,0,1,4.748,4.75"
                                                      transform="translate(255.376 4.932)" fill="#fcd9ca"></path>
                                                <path id="Path_377" data-name="Path 377"
                                                      d="M-215.5,3.856a4.751,4.751,0,0,0-4.748-4.751,4.751,4.751,0,0,0-4.747,4.751,4.751,4.751,0,0,0,4.747,4.75,4.751,4.751,0,0,0,4.748-4.75m-1.143-.018h0a3.7,3.7,0,0,1-3.694,3.7,3.7,3.7,0,0,1-3.69-3.7,3.693,3.693,0,0,1,3.69-3.7,3.7,3.7,0,0,1,3.694,3.7"
                                                      transform="translate(245.905 -3.56)" fill="#fde0d3"></path>
                                                <path id="Path_378" data-name="Path 378"
                                                      d="M-209.183,12.328a3.7,3.7,0,0,0-3.69-3.7,3.7,3.7,0,0,0-3.694,3.7,3.7,3.7,0,0,0,3.694,3.694,3.693,3.693,0,0,0,3.69-3.694m-1.021-.018h0a2.641,2.641,0,0,1-2.637,2.64,2.641,2.641,0,0,1-2.636-2.64,2.639,2.639,0,0,1,2.636-2.64,2.64,2.64,0,0,1,2.637,2.64"
                                                      transform="translate(238.413 -12.032)" fill="#fde7dc"></path>
                                                <path id="Path_379" data-name="Path 379"
                                                      d="M-201.753,20.819a2.641,2.641,0,0,0-2.639-2.64,2.64,2.64,0,0,0-2.637,2.64,2.639,2.639,0,0,0,2.637,2.64,2.641,2.641,0,0,0,2.639-2.64m-1.023-.018h0a1.585,1.585,0,0,1-1.584,1.584A1.584,1.584,0,0,1-205.94,20.8a1.582,1.582,0,0,1,1.581-1.583,1.584,1.584,0,0,1,1.584,1.583"
                                                      transform="translate(229.93 -20.523)" fill="#feeee6"></path>
                                                <path id="Path_380" data-name="Path 380"
                                                      d="M-193.224,29.312a1.584,1.584,0,0,0-1.584-1.584,1.584,1.584,0,0,0-1.583,1.584,1.585,1.585,0,0,0,1.583,1.583,1.586,1.586,0,0,0,1.584-1.583m-1.146-.018h0a.529.529,0,0,1-.527.527.528.528,0,0,1-.527-.527.526.526,0,0,1,.527-.527.527.527,0,0,1,.527.527"
                                                      transform="translate(220.469 -29.015)" fill="#fef4f0"></path>
                                                <path id="Path_381" data-name="Path 381"
                                                      d="M-186.89,37.8a.528.528,0,0,0-.527-.527.526.526,0,0,0-.527.527.527.527,0,0,0,.527.527.53.53,0,0,0,.527-.527"
                                                      transform="translate(212.957 -37.507)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <g id="Group_1026" data-name="Group 1026" transform="translate(4.046 5.274)">
                                            <g id="Group_1025" data-name="Group 1025" transform="translate(0)"
                                               clip-path="url(#clip-path-4)">
                                                <path id="Path_383" data-name="Path 383"
                                                      d="M-433.055-270.164a44.478,44.478,0,0,0-44.45-44.474,44.477,44.477,0,0,0-44.447,44.474A44.477,44.477,0,0,0-477.5-225.689a44.478,44.478,0,0,0,44.45-44.475m-1.057.122h0a43.43,43.43,0,0,1-43.4,43.425,43.429,43.429,0,0,1-43.4-43.425,43.43,43.43,0,0,1,43.4-43.43,43.431,43.431,0,0,1,43.4,43.43"
                                                      transform="translate(505.961 270.362)" fill="#da091d"></path>
                                                <path id="Path_384" data-name="Path 384"
                                                      d="M-425.708-260.648a43.434,43.434,0,0,0-43.4-43.431,43.433,43.433,0,0,0-43.4,43.431,43.431,43.431,0,0,0,43.4,43.425,43.431,43.431,0,0,0,43.4-43.425m-1.056,0h0a42.385,42.385,0,0,1-42.358,42.379,42.384,42.384,0,0,1-42.356-42.379,42.383,42.383,0,0,1,42.356-42.381,42.384,42.384,0,0,1,42.358,42.381"
                                                      transform="translate(497.568 260.972)" fill="#db161f"></path>
                                                <path id="Path_385" data-name="Path 385"
                                                      d="M-418.344-252.235A42.386,42.386,0,0,0-460.7-294.619a42.386,42.386,0,0,0-42.357,42.384A42.385,42.385,0,0,0-460.7-209.856a42.385,42.385,0,0,0,42.358-42.379m-1.057,0h0a41.336,41.336,0,0,1-41.311,41.332,41.338,41.338,0,0,1-41.311-41.332,41.337,41.337,0,0,1,41.311-41.335A41.335,41.335,0,0,1-419.4-252.238"
                                                      transform="translate(489.158 252.558)" fill="#dc1d21"></path>
                                                <path id="Path_386" data-name="Path 386"
                                                      d="M-410.978-243.824a41.339,41.339,0,0,0-41.311-41.337A41.337,41.337,0,0,0-493.6-243.824a41.336,41.336,0,0,0,41.311,41.332,41.338,41.338,0,0,0,41.311-41.332m-1.056,0h0A40.29,40.29,0,0,1-452.3-203.54a40.289,40.289,0,0,1-40.265-40.286A40.289,40.289,0,0,1-452.3-284.115a40.291,40.291,0,0,1,40.265,40.288"
                                                      transform="translate(480.746 244.147)" fill="#dc2423"></path>
                                                <path id="Path_387" data-name="Path 387"
                                                      d="M-403.611-235.412A40.293,40.293,0,0,0-443.875-275.7a40.291,40.291,0,0,0-40.265,40.291,40.29,40.29,0,0,0,40.265,40.286,40.292,40.292,0,0,0,40.265-40.286m-1.056,0h0a39.243,39.243,0,0,1-39.218,39.239A39.242,39.242,0,0,1-483.1-235.415a39.243,39.243,0,0,1,39.219-39.242,39.244,39.244,0,0,1,39.218,39.242"
                                                      transform="translate(472.333 235.735)" fill="#dd2a25"></path>
                                                <path id="Path_388" data-name="Path 388"
                                                      d="M-396.266-226.981a39.244,39.244,0,0,0-39.221-39.242,39.244,39.244,0,0,0-39.219,39.242,39.247,39.247,0,0,0,39.219,39.242,39.248,39.248,0,0,0,39.221-39.242m-1.057,0h0a38.2,38.2,0,0,1-38.172,38.2,38.2,38.2,0,0,1-38.175-38.2,38.2,38.2,0,0,1,38.175-38.2,38.2,38.2,0,0,1,38.172,38.2"
                                                      transform="translate(463.943 227.304)" fill="#de3027"></path>
                                                <path id="Path_389" data-name="Path 389"
                                                      d="M-388.9-218.588a38.2,38.2,0,0,0-38.175-38.2,38.2,38.2,0,0,0-38.173,38.2,38.2,38.2,0,0,0,38.173,38.2,38.2,38.2,0,0,0,38.175-38.2m-1.056,0h0a37.152,37.152,0,0,1-37.129,37.148,37.15,37.15,0,0,1-37.126-37.148,37.151,37.151,0,0,1,37.126-37.151,37.152,37.152,0,0,1,37.129,37.151"
                                                      transform="translate(455.531 218.912)" fill="#df362a"></path>
                                                <path id="Path_390" data-name="Path 390"
                                                      d="M-381.533-210.177a37.153,37.153,0,0,0-37.129-37.151,37.151,37.151,0,0,0-37.126,37.151,37.152,37.152,0,0,0,37.126,37.149,37.154,37.154,0,0,0,37.129-37.149m-1.057,0h0a36.106,36.106,0,0,1-36.082,36.1,36.106,36.106,0,0,1-36.081-36.1,36.106,36.106,0,0,1,36.081-36.1,36.107,36.107,0,0,1,36.082,36.1"
                                                      transform="translate(447.118 210.5)" fill="#e03d2d"></path>
                                                <path id="Path_391" data-name="Path 391"
                                                      d="M-374.187-201.764a36.108,36.108,0,0,0-36.083-36.1,36.108,36.108,0,0,0-36.082,36.1,36.107,36.107,0,0,0,36.082,36.1,36.107,36.107,0,0,0,36.083-36.1m-1.056,0h0a35.059,35.059,0,0,1-35.037,35.056,35.059,35.059,0,0,1-35.035-35.056,35.059,35.059,0,0,1,35.035-35.058,35.059,35.059,0,0,1,35.037,35.058"
                                                      transform="translate(438.727 202.088)" fill="#e14230"></path>
                                                <path id="Path_392" data-name="Path 392"
                                                      d="M-366.821-193.353a35.059,35.059,0,0,0-35.036-35.058,35.06,35.06,0,0,0-35.037,35.058A35.057,35.057,0,0,0-401.857-158.3a35.057,35.057,0,0,0,35.036-35.053m-1.056,0h0a34.012,34.012,0,0,1-33.991,34.009,34.011,34.011,0,0,1-33.989-34.009,34.013,34.013,0,0,1,33.989-34.011,34.014,34.014,0,0,1,33.991,34.011"
                                                      transform="translate(430.314 193.676)" fill="#e24634"></path>
                                                <path id="Path_393" data-name="Path 393"
                                                      d="M-359.455-184.941a34.015,34.015,0,0,0-33.991-34.012,34.014,34.014,0,0,0-33.99,34.012,34.009,34.009,0,0,0,33.99,34.006,34.01,34.01,0,0,0,33.991-34.006m-1.057,0h0a32.965,32.965,0,0,1-32.945,32.96,32.965,32.965,0,0,1-32.944-32.96,32.967,32.967,0,0,1,32.944-32.965,32.967,32.967,0,0,1,32.945,32.965"
                                                      transform="translate(421.903 185.265)" fill="#e34b38"></path>
                                                <path id="Path_394" data-name="Path 394"
                                                      d="M-352.089-176.528a32.967,32.967,0,0,0-32.945-32.965,32.966,32.966,0,0,0-32.944,32.965,32.964,32.964,0,0,0,32.944,32.96,32.965,32.965,0,0,0,32.945-32.96m-1.056,0h0a31.918,31.918,0,0,1-31.9,31.913,31.917,31.917,0,0,1-31.9-31.913,31.917,31.917,0,0,1,31.9-31.916,31.918,31.918,0,0,1,31.9,31.916"
                                                      transform="translate(413.492 176.851)" fill="#e4523b"></path>
                                                <path id="Path_395" data-name="Path 395"
                                                      d="M-344.744-168.117a31.919,31.919,0,0,0-31.9-31.919,31.918,31.918,0,0,0-31.9,31.919,31.917,31.917,0,0,0,31.9,31.913,31.918,31.918,0,0,0,31.9-31.913m-1.057,0h0a30.874,30.874,0,0,1-30.852,30.869,30.874,30.874,0,0,1-30.855-30.869,30.875,30.875,0,0,1,30.855-30.872A30.874,30.874,0,0,1-345.8-168.12"
                                                      transform="translate(405.099 168.441)" fill="#e5563f"></path>
                                                <path id="Path_396" data-name="Path 396"
                                                      d="M-337.378-159.726A30.875,30.875,0,0,0-368.232-190.6a30.875,30.875,0,0,0-30.852,30.875,30.874,30.874,0,0,0,30.852,30.869,30.874,30.874,0,0,0,30.854-30.869m-1.056,0h0a29.827,29.827,0,0,1-29.808,29.823,29.826,29.826,0,0,1-29.805-29.823,29.826,29.826,0,0,1,29.805-29.825,29.827,29.827,0,0,1,29.808,29.825"
                                                      transform="translate(396.688 160.05)" fill="#e55a43"></path>
                                                <path id="Path_397" data-name="Path 397"
                                                      d="M-331.113-151.313a29.828,29.828,0,0,0-29.808-29.828,29.827,29.827,0,0,0-29.806,29.828,29.826,29.826,0,0,0,29.806,29.823,29.827,29.827,0,0,0,29.808-29.823m-.934,0h0a28.781,28.781,0,0,1-28.762,28.776,28.779,28.779,0,0,1-28.76-28.776,28.778,28.778,0,0,1,28.76-28.779,28.78,28.78,0,0,1,28.762,28.779"
                                                      transform="translate(389.255 151.636)" fill="#e65e46"></path>
                                                <path id="Path_398" data-name="Path 398"
                                                      d="M-322.646-142.9a28.782,28.782,0,0,0-28.762-28.781A28.78,28.78,0,0,0-380.166-142.9a28.779,28.779,0,0,0,28.759,28.776A28.781,28.781,0,0,0-322.646-142.9m-1.055,0h0a27.735,27.735,0,0,1-27.716,27.73,27.734,27.734,0,0,1-27.716-27.73,27.733,27.733,0,0,1,27.716-27.732A27.734,27.734,0,0,1-323.7-142.905"
                                                      transform="translate(379.863 143.226)" fill="#e8644a"></path>
                                                <path id="Path_399" data-name="Path 399"
                                                      d="M-315.3-134.471A27.733,27.733,0,0,0-343.016-162.2a27.733,27.733,0,0,0-27.716,27.732,27.734,27.734,0,0,0,27.716,27.73,27.734,27.734,0,0,0,27.715-27.73m-1.056,0h0a26.687,26.687,0,0,1-26.67,26.683A26.686,26.686,0,0,1-369.7-134.473a26.687,26.687,0,0,1,26.668-26.686,26.687,26.687,0,0,1,26.67,26.686"
                                                      transform="translate(371.473 134.794)" fill="#e8684f"></path>
                                                <path id="Path_400" data-name="Path 400"
                                                      d="M-307.933-126.057a26.688,26.688,0,0,0-26.67-26.686,26.687,26.687,0,0,0-26.669,26.686A26.688,26.688,0,0,0-334.6-99.374a26.689,26.689,0,0,0,26.67-26.683m-1.057,0h0a25.64,25.64,0,0,1-25.624,25.637,25.64,25.64,0,0,1-25.623-25.637A25.641,25.641,0,0,1-334.614-151.7,25.641,25.641,0,0,1-308.99-126.06"
                                                      transform="translate(363.06 126.381)" fill="#e96c53"></path>
                                                <path id="Path_401" data-name="Path 401"
                                                      d="M-300.568-117.647a25.64,25.64,0,0,0-25.623-25.639,25.639,25.639,0,0,0-25.623,25.639,25.638,25.638,0,0,0,25.623,25.634,25.639,25.639,0,0,0,25.623-25.634m-1.055,0h0A24.593,24.593,0,0,1-326.2-93.059a24.592,24.592,0,0,1-24.579-24.59A24.594,24.594,0,0,1-326.2-142.242a24.6,24.6,0,0,1,24.578,24.593"
                                                      transform="translate(354.648 117.97)" fill="#ea7157"></path>
                                                <path id="Path_402" data-name="Path 402"
                                                      d="M-293.221-109.235A24.6,24.6,0,0,0-317.8-133.828a24.6,24.6,0,0,0-24.58,24.593A24.593,24.593,0,0,0-317.8-84.648a24.592,24.592,0,0,0,24.577-24.587m-1.057,0h0a23.548,23.548,0,0,1-23.531,23.546,23.549,23.549,0,0,1-23.532-23.546,23.547,23.547,0,0,1,23.532-23.546,23.546,23.546,0,0,1,23.531,23.546"
                                                      transform="translate(346.256 109.559)" fill="#eb755b"></path>
                                                <path id="Path_403" data-name="Path 403"
                                                      d="M-285.856-100.822a23.548,23.548,0,0,0-23.534-23.546,23.547,23.547,0,0,0-23.53,23.546,23.548,23.548,0,0,0,23.53,23.543,23.549,23.549,0,0,0,23.534-23.543m-1.057,0h0a22.5,22.5,0,0,1-22.485,22.5,22.5,22.5,0,0,1-22.487-22.5,22.5,22.5,0,0,1,22.487-22.5,22.5,22.5,0,0,1,22.485,22.5"
                                                      transform="translate(337.845 101.145)" fill="#ec7b60"></path>
                                                <path id="Path_404" data-name="Path 404"
                                                      d="M-279.59-92.432a22.5,22.5,0,0,0-22.487-22.5,22.5,22.5,0,0,0-22.484,22.5,22.5,22.5,0,0,0,22.484,22.5,22.5,22.5,0,0,0,22.487-22.5m-.935,0h0a21.453,21.453,0,0,1-21.441,21.45A21.452,21.452,0,0,1-323.4-92.434a21.455,21.455,0,0,1,21.438-21.455,21.456,21.456,0,0,1,21.441,21.455"
                                                      transform="translate(330.412 92.755)" fill="#ed8166"></path>
                                                <path id="Path_405" data-name="Path 405"
                                                      d="M-271.122-84.02a21.457,21.457,0,0,0-21.442-21.456A21.455,21.455,0,0,0-314-84.02a21.452,21.452,0,0,0,21.438,21.45,21.454,21.454,0,0,0,21.442-21.45m-1.057,0h0a20.406,20.406,0,0,1-20.4,20.4,20.407,20.407,0,0,1-20.394-20.4,20.408,20.408,0,0,1,20.394-20.406,20.407,20.407,0,0,1,20.4,20.406"
                                                      transform="translate(321.02 84.344)" fill="#ee866c"></path>
                                                <path id="Path_406" data-name="Path 406"
                                                      d="M-263.779-75.607a20.41,20.41,0,0,0-20.394-20.409,20.408,20.408,0,0,0-20.4,20.409,20.406,20.406,0,0,0,20.4,20.4,20.407,20.407,0,0,0,20.394-20.4m-1.056,0h0a19.36,19.36,0,0,1-19.349,19.357A19.36,19.36,0,0,1-303.531-75.61a19.36,19.36,0,0,1,19.348-19.36,19.361,19.361,0,0,1,19.349,19.36"
                                                      transform="translate(312.63 75.93)" fill="#ef8b71"></path>
                                                <path id="Path_407" data-name="Path 407"
                                                      d="M-256.411-67.2A19.363,19.363,0,0,0-275.76-86.559,19.363,19.363,0,0,0-295.108-67.2,19.361,19.361,0,0,0-275.76-47.839,19.361,19.361,0,0,0-256.411-67.2m-1.056,0h0a18.313,18.313,0,0,1-18.3,18.311,18.312,18.312,0,0,1-18.3-18.311,18.313,18.313,0,0,1,18.3-18.313,18.314,18.314,0,0,1,18.3,18.313"
                                                      transform="translate(304.217 67.52)" fill="#f08f76"></path>
                                                <path id="Path_408" data-name="Path 408"
                                                      d="M-249.046-58.785a18.315,18.315,0,0,0-18.3-18.316,18.314,18.314,0,0,0-18.3,18.316,18.312,18.312,0,0,0,18.3,18.311,18.313,18.313,0,0,0,18.3-18.311m-1.057,0h0a17.268,17.268,0,0,1-17.256,17.264,17.268,17.268,0,0,1-17.257-17.264,17.268,17.268,0,0,1,17.257-17.267A17.268,17.268,0,0,1-250.1-58.788"
                                                      transform="translate(295.805 59.108)" fill="#f1957b"></path>
                                                <path id="Path_409" data-name="Path 409"
                                                      d="M-241.682-50.351a17.266,17.266,0,0,0-17.256-17.267,17.266,17.266,0,0,0-17.256,17.267,17.267,17.267,0,0,0,17.256,17.264,17.267,17.267,0,0,0,17.256-17.264m-1.055,0h0a16.22,16.22,0,0,1-16.21,16.218A16.221,16.221,0,0,1-275.16-50.354a16.222,16.222,0,0,1,16.213-16.22,16.221,16.221,0,0,1,16.21,16.22"
                                                      transform="translate(287.395 50.675)" fill="#f29b81"></path>
                                                <path id="Path_410" data-name="Path 410"
                                                      d="M-234.334-41.941a16.22,16.22,0,0,0-16.213-16.22,16.219,16.219,0,0,0-16.209,16.22,16.22,16.22,0,0,0,16.209,16.217,16.221,16.221,0,0,0,16.213-16.217m-1.056,0h0A15.177,15.177,0,0,1-250.554-26.77a15.177,15.177,0,0,1-15.166-15.174,15.178,15.178,0,0,1,15.166-15.176,15.177,15.177,0,0,1,15.164,15.176"
                                                      transform="translate(279.002 42.264)" fill="#f3a087"></path>
                                                <path id="Path_411" data-name="Path 411"
                                                      d="M-226.968-33.55a15.176,15.176,0,0,0-15.166-15.176A15.176,15.176,0,0,0-257.3-33.55a15.177,15.177,0,0,0,15.164,15.174A15.177,15.177,0,0,0-226.968-33.55m-1.056,0h0a14.13,14.13,0,0,1-14.12,14.127,14.129,14.129,0,0,1-14.117-14.127,14.128,14.128,0,0,1,14.117-14.13,14.129,14.129,0,0,1,14.12,14.13"
                                                      transform="translate(270.59 33.873)" fill="#f4a68d"></path>
                                                <path id="Path_412" data-name="Path 412"
                                                      d="M-219.6-25.136a14.13,14.13,0,0,0-14.121-14.13,14.127,14.127,0,0,0-14.117,14.13,14.126,14.126,0,0,0,14.117,14.125A14.129,14.129,0,0,0-219.6-25.136m-1.056,0h0a13.082,13.082,0,0,1-13.074,13.081A13.079,13.079,0,0,1-246.8-25.139a13.08,13.08,0,0,1,13.071-13.083,13.083,13.083,0,0,1,13.074,13.083"
                                                      transform="translate(262.18 25.46)" fill="#f5ac94"></path>
                                                <path id="Path_413" data-name="Path 413"
                                                      d="M-212.256-16.726a13.083,13.083,0,0,0-13.074-13.083A13.082,13.082,0,0,0-238.4-16.726,13.081,13.081,0,0,0-225.331-3.648a13.082,13.082,0,0,0,13.074-13.078m-1.057,0h0A12.036,12.036,0,0,1-225.341-4.694a12.036,12.036,0,0,1-12.028-12.034,12.037,12.037,0,0,1,12.028-12.037,12.037,12.037,0,0,1,12.028,12.037"
                                                      transform="translate(253.787 17.049)" fill="#f6b099"></path>
                                                <path id="Path_414" data-name="Path 414"
                                                      d="M-204.892-8.314a12.036,12.036,0,0,0-12.028-12.037A12.036,12.036,0,0,0-228.946-8.314,12.035,12.035,0,0,0-216.919,3.717,12.035,12.035,0,0,0-204.892-8.314m-1.056,0h0A10.986,10.986,0,0,1-216.929,2.668,10.987,10.987,0,0,1-227.911-8.317a10.989,10.989,0,0,1,10.981-10.99,10.989,10.989,0,0,1,10.981,10.99"
                                                      transform="translate(245.375 8.638)" fill="#f7b7a2"></path>
                                                <path id="Path_415" data-name="Path 415"
                                                      d="M-197.524.1A10.989,10.989,0,0,0-208.5-10.891,10.989,10.989,0,0,0-219.486.1,10.988,10.988,0,0,0-208.5,11.084,10.988,10.988,0,0,0-197.524.1m-1.056,0h0a9.941,9.941,0,0,1-9.935,9.939A9.939,9.939,0,0,1-218.45.1a9.939,9.939,0,0,1,9.935-9.941A9.942,9.942,0,0,1-198.58.1"
                                                      transform="translate(236.962 0.224)" fill="#f8bda9"></path>
                                                <path id="Path_416" data-name="Path 416"
                                                      d="M-190.159,8.51a9.944,9.944,0,0,0-9.935-9.944,9.942,9.942,0,0,0-9.935,9.944,9.941,9.941,0,0,0,9.935,9.939,9.943,9.943,0,0,0,9.935-9.939m-1.055,0h0A8.893,8.893,0,0,1-200.1,17.4,8.894,8.894,0,0,1-209,8.507,8.9,8.9,0,0,1-200.1-.387a8.894,8.894,0,0,1,8.889,8.895"
                                                      transform="translate(228.552 -8.186)" fill="#f9c3b0"></path>
                                                <path id="Path_417" data-name="Path 417"
                                                      d="M-182.812,16.921a8.9,8.9,0,0,0-8.891-8.9,8.9,8.9,0,0,0-8.889,8.9,8.9,8.9,0,0,0,8.889,8.892,8.9,8.9,0,0,0,8.891-8.892m-1.056,0h0a7.848,7.848,0,0,1-7.842,7.848,7.85,7.85,0,0,1-7.845-7.848,7.849,7.849,0,0,1,7.845-7.848,7.847,7.847,0,0,1,7.842,7.848"
                                                      transform="translate(220.159 -16.598)" fill="#fac9b6"></path>
                                                <path id="Path_418" data-name="Path 418"
                                                      d="M-175.446,25.334a7.852,7.852,0,0,0-7.845-7.85,7.849,7.849,0,0,0-7.843,7.85,7.849,7.849,0,0,0,7.843,7.848,7.852,7.852,0,0,0,7.845-7.848m-1.057,0h0a6.8,6.8,0,0,1-6.8,6.8,6.8,6.8,0,0,1-6.8-6.8,6.8,6.8,0,0,1,6.8-6.8,6.8,6.8,0,0,1,6.8,6.8"
                                                      transform="translate(211.747 -25.011)" fill="#facfbd"></path>
                                                <path id="Path_419" data-name="Path 419"
                                                      d="M-168.082,33.745a6.8,6.8,0,0,0-6.8-6.8,6.8,6.8,0,0,0-6.8,6.8,6.8,6.8,0,0,0,6.8,6.8,6.8,6.8,0,0,0,6.8-6.8m-1.055,0h0a5.759,5.759,0,0,1-5.754,5.755,5.758,5.758,0,0,1-5.752-5.755,5.758,5.758,0,0,1,5.752-5.757,5.759,5.759,0,0,1,5.754,5.757"
                                                      transform="translate(203.337 -33.422)" fill="#fbd6c6"></path>
                                                <path id="Path_420" data-name="Path 420"
                                                      d="M-160.734,42.156a5.758,5.758,0,0,0-5.754-5.757,5.758,5.758,0,0,0-5.752,5.757,5.758,5.758,0,0,0,5.752,5.755,5.757,5.757,0,0,0,5.754-5.755m-1.056,0h0a4.71,4.71,0,0,1-4.707,4.708,4.71,4.71,0,0,1-4.707-4.708,4.71,4.71,0,0,1,4.707-4.711,4.71,4.71,0,0,1,4.707,4.711"
                                                      transform="translate(194.944 -41.833)" fill="#fcdbcd"></path>
                                                <path id="Path_421" data-name="Path 421"
                                                      d="M-153.369,50.57a4.709,4.709,0,0,0-4.707-4.711,4.71,4.71,0,0,0-4.706,4.711,4.711,4.711,0,0,0,4.706,4.708,4.71,4.71,0,0,0,4.707-4.708m-1.057,0h0a3.662,3.662,0,0,1-3.66,3.662,3.663,3.663,0,0,1-3.66-3.662,3.663,3.663,0,0,1,3.66-3.664,3.663,3.663,0,0,1,3.66,3.664"
                                                      transform="translate(186.532 -50.246)" fill="#fde2d6"></path>
                                                <path id="Path_422" data-name="Path 422"
                                                      d="M-147.1,58.981a3.661,3.661,0,0,0-3.66-3.664,3.661,3.661,0,0,0-3.66,3.664,3.66,3.66,0,0,0,3.66,3.659,3.66,3.66,0,0,0,3.66-3.659m-.935,0h0a2.617,2.617,0,0,1-2.614,2.615,2.616,2.616,0,0,1-2.614-2.615,2.617,2.617,0,0,1,2.614-2.618,2.617,2.617,0,0,1,2.614,2.618"
                                                      transform="translate(179.099 -58.658)" fill="#fde8df"></path>
                                                <path id="Path_423" data-name="Path 423"
                                                      d="M-138.637,67.392a2.616,2.616,0,0,0-2.614-2.618,2.615,2.615,0,0,0-2.614,2.618A2.614,2.614,0,0,0-141.251,70a2.614,2.614,0,0,0,2.614-2.613m-1.057,0h0a1.569,1.569,0,0,1-1.567,1.569,1.573,1.573,0,0,1-1.571-1.569,1.573,1.573,0,0,1,1.571-1.571,1.57,1.57,0,0,1,1.567,1.571"
                                                      transform="translate(169.708 -67.069)" fill="#feeee8"></path>
                                                <path id="Path_424" data-name="Path 424"
                                                      d="M-131.292,75.805a1.568,1.568,0,0,0-1.569-1.571,1.569,1.569,0,0,0-1.57,1.571,1.568,1.568,0,0,0,1.57,1.566,1.567,1.567,0,0,0,1.569-1.566m-1.056,0h0a.521.521,0,0,1-.522.519.523.523,0,0,1-.525-.519.526.526,0,0,1,.525-.525.524.524,0,0,1,.522.525"
                                                      transform="translate(161.317 -75.482)" fill="#fef5f2"></path>
                                                <path id="Path_425" data-name="Path 425"
                                                      d="M-123.925,84.214a.522.522,0,0,0-.521-.522.522.522,0,0,0-.524.522.524.524,0,0,0,.524.524.524.524,0,0,0,.521-.524"
                                                      transform="translate(152.904 -83.893)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <g id="Group_1028" data-name="Group 1028" transform="translate(13.636 16.529)">
                                            <g id="Group_1027" data-name="Group 1027" transform="translate(0)"
                                               clip-path="url(#clip-path-5)">
                                                <path id="Path_427" data-name="Path 427"
                                                      d="M-339.563-117.239a38.089,38.089,0,0,0-38.065-38.088A38.091,38.091,0,0,0-415.7-117.239a38.091,38.091,0,0,0,38.068,38.088,38.089,38.089,0,0,0,38.065-38.088m-1.081.075h0A37.048,37.048,0,0,1-377.669-80.12a37.047,37.047,0,0,1-37.022-37.044,37.047,37.047,0,0,1,37.022-37.044,37.047,37.047,0,0,1,37.024,37.044"
                                                      transform="translate(401.872 117.423)" fill="#da091d"></path>
                                                <path id="Path_428" data-name="Path 428"
                                                      d="M-332.216-107.764a37.049,37.049,0,0,0-37.024-37.049,37.05,37.05,0,0,0-37.022,37.049A37.049,37.049,0,0,0-369.24-70.72a37.048,37.048,0,0,0,37.024-37.044m-1.081-.05h0a36,36,0,0,1-35.98,36,36,36,0,0,1-35.981-36,36,36,0,0,1,35.981-36,36,36,0,0,1,35.98,36"
                                                      transform="translate(393.482 108.072)" fill="#db181f"></path>
                                                <path id="Path_429" data-name="Path 429"
                                                      d="M-324.874-99.374a36.006,36.006,0,0,0-35.982-36.005,36.007,36.007,0,0,0-35.98,36.005,36,36,0,0,0,35.98,36,36,36,0,0,0,35.982-36m-1.082-.05h0a34.961,34.961,0,0,1-34.938,34.959,34.962,34.962,0,0,1-34.94-34.959,34.962,34.962,0,0,1,34.94-34.959,34.961,34.961,0,0,1,34.938,34.959"
                                                      transform="translate(385.099 99.682)" fill="#dc2021"></path>
                                                <path id="Path_430" data-name="Path 430"
                                                      d="M-318.658-92.085A34.961,34.961,0,0,0-353.6-127.044a34.961,34.961,0,0,0-34.94,34.959A34.963,34.963,0,0,0-353.6-57.127a34.962,34.962,0,0,0,34.937-34.959m-.96.075h0a33.916,33.916,0,0,1-33.9,33.915,33.916,33.916,0,0,1-33.893-33.915,33.917,33.917,0,0,1,33.893-33.915,33.918,33.918,0,0,1,33.9,33.915"
                                                      transform="translate(377.717 92.269)" fill="#dd2723"></path>
                                                <path id="Path_431" data-name="Path 431"
                                                      d="M-310.2-82.588A33.916,33.916,0,0,0-344.092-116.5a33.917,33.917,0,0,0-33.9,33.917,33.916,33.916,0,0,0,33.9,33.912A33.915,33.915,0,0,0-310.2-82.588m-1.081-.05h0a32.874,32.874,0,0,1-32.853,32.871,32.873,32.873,0,0,1-32.852-32.871,32.873,32.873,0,0,1,32.852-32.87,32.874,32.874,0,0,1,32.853,32.87"
                                                      transform="translate(368.336 82.896)" fill="#de2e26"></path>
                                                <path id="Path_432" data-name="Path 432"
                                                      d="M-302.869-74.2a32.872,32.872,0,0,0-32.852-32.873A32.873,32.873,0,0,0-368.574-74.2a32.872,32.872,0,0,0,32.853,32.868A32.871,32.871,0,0,0-302.869-74.2m-1.081-.05h0a31.83,31.83,0,0,1-31.809,31.829,31.832,31.832,0,0,1-31.812-31.829,31.83,31.83,0,0,1,31.812-31.826A31.828,31.828,0,0,1-303.95-74.247"
                                                      transform="translate(359.964 74.505)" fill="#df3529"></path>
                                                <path id="Path_433" data-name="Path 433"
                                                      d="M-295.521-65.825A31.832,31.832,0,0,0-327.33-97.657a31.832,31.832,0,0,0-31.809,31.832A31.83,31.83,0,0,0-327.33-34a31.83,31.83,0,0,0,31.809-31.826m-1.081-.05h0A30.788,30.788,0,0,1-327.37-35.09a30.787,30.787,0,0,1-30.766-30.785A30.787,30.787,0,0,1-327.37-96.658,30.787,30.787,0,0,1-296.6-65.875"
                                                      transform="translate(351.573 66.134)" fill="#e03d2d"></path>
                                                <path id="Path_434" data-name="Path 434"
                                                      d="M-289.291-58.537a30.787,30.787,0,0,0-30.765-30.785,30.788,30.788,0,0,0-30.769,30.785,30.788,30.788,0,0,0,30.769,30.785,30.787,30.787,0,0,0,30.765-30.785m-.959.075h0a29.743,29.743,0,0,1-29.725,29.741A29.744,29.744,0,0,1-349.7-58.463,29.742,29.742,0,0,1-319.976-88.2a29.741,29.741,0,0,1,29.725,29.738"
                                                      transform="translate(344.179 58.721)" fill="#e14331"></path>
                                                <path id="Path_435" data-name="Path 435"
                                                      d="M-280.852-49.042a29.744,29.744,0,0,0-29.724-29.743A29.745,29.745,0,0,0-340.3-49.042,29.744,29.744,0,0,0-310.575-19.3a29.742,29.742,0,0,0,29.724-29.738m-1.081-.05h0a28.7,28.7,0,0,1-28.681,28.7,28.7,28.7,0,0,1-28.681-28.7,28.7,28.7,0,0,1,28.681-28.694,28.7,28.7,0,0,1,28.681,28.694"
                                                      transform="translate(334.818 49.35)" fill="#e24835"></path>
                                                <path id="Path_436" data-name="Path 436"
                                                      d="M-273.5-40.649a28.7,28.7,0,0,0-28.681-28.7,28.7,28.7,0,0,0-28.68,28.7,28.7,28.7,0,0,0,28.68,28.694A28.7,28.7,0,0,0-273.5-40.649m-1.083-.05h0a27.656,27.656,0,0,1-27.636,27.655A27.657,27.657,0,0,1-329.86-40.7a27.655,27.655,0,0,1,27.639-27.652A27.653,27.653,0,0,1-274.585-40.7"
                                                      transform="translate(326.426 40.957)" fill="#e34e39"></path>
                                                <path id="Path_437" data-name="Path 437"
                                                      d="M-267.274-33.361a27.654,27.654,0,0,0-27.638-27.652,27.655,27.655,0,0,0-27.639,27.652A27.656,27.656,0,0,0-294.912-5.708a27.656,27.656,0,0,0,27.638-27.653m-.961.075h0a26.61,26.61,0,0,1-26.6,26.611,26.61,26.61,0,0,1-26.594-26.611A26.614,26.614,0,0,1-294.83-59.9a26.613,26.613,0,0,1,26.6,26.611"
                                                      transform="translate(319.033 33.544)" fill="#e4543e"></path>
                                                <path id="Path_438" data-name="Path 438"
                                                      d="M-258.814-24.991A26.612,26.612,0,0,0-285.408-51.6,26.612,26.612,0,0,0-312-24.991,26.609,26.609,0,0,0-285.408,1.618a26.61,26.61,0,0,0,26.594-26.609m-1.083.075h0A25.57,25.57,0,0,1-285.449.651,25.57,25.57,0,0,1-311-24.916a25.57,25.57,0,0,1,25.552-25.567A25.57,25.57,0,0,1-259.9-24.916"
                                                      transform="translate(309.652 25.175)" fill="#e55a42"></path>
                                                <path id="Path_439" data-name="Path 439"
                                                      d="M-251.486-15.5a25.568,25.568,0,0,0-25.552-25.57A25.567,25.567,0,0,0-302.589-15.5a25.566,25.566,0,0,0,25.551,25.564A25.567,25.567,0,0,0-251.486-15.5m-1.081-.05h0A24.523,24.523,0,0,1-277.076,8.978a24.523,24.523,0,0,1-24.508-24.523,24.523,24.523,0,0,1,24.508-24.523,24.523,24.523,0,0,1,24.508,24.523"
                                                      transform="translate(301.28 15.803)" fill="#e65e46"></path>
                                                <path id="Path_440" data-name="Path 440"
                                                      d="M-244.137-7.1a24.526,24.526,0,0,0-24.51-24.526A24.525,24.525,0,0,0-293.155-7.1a24.524,24.524,0,0,0,24.508,24.521A24.525,24.525,0,0,0-244.137-7.1m-1.083-.05h0a23.481,23.481,0,0,1-23.467,23.479A23.48,23.48,0,0,1-292.152-7.152a23.482,23.482,0,0,1,23.465-23.479A23.482,23.482,0,0,1-245.22-7.152"
                                                      transform="translate(292.89 7.411)" fill="#e8644b"></path>
                                                <path id="Path_441" data-name="Path 441"
                                                      d="M-237.911.186a23.48,23.48,0,0,0-23.464-23.479A23.481,23.481,0,0,0-284.843.186a23.481,23.481,0,0,0,23.468,23.479A23.48,23.48,0,0,0-237.911.186m-.96.075h0A22.438,22.438,0,0,1-261.294,22.7,22.441,22.441,0,0,1-283.718.26a22.438,22.438,0,0,1,22.424-22.435A22.436,22.436,0,0,1-238.871.26"
                                                      transform="translate(285.498 -0.002)" fill="#e96a50"></path>
                                                <path id="Path_442" data-name="Path 442"
                                                      d="M-229.461,8.578a22.436,22.436,0,0,0-22.423-22.435A22.436,22.436,0,0,0-274.306,8.578a22.436,22.436,0,0,0,22.421,22.435A22.436,22.436,0,0,0-229.461,8.578m-1.081.075h0a21.4,21.4,0,0,1-21.38,21.393A21.4,21.4,0,0,1-273.3,8.652a21.4,21.4,0,0,1,21.381-21.393,21.4,21.4,0,0,1,21.38,21.393"
                                                      transform="translate(276.126 -8.394)" fill="#ea6f55"></path>
                                                <path id="Path_443" data-name="Path 443"
                                                      d="M-222.122,18.073A21.393,21.393,0,0,0-243.5-3.32a21.394,21.394,0,0,0-21.38,21.393A21.392,21.392,0,0,0-243.5,39.462a21.391,21.391,0,0,0,21.38-21.388m-1.081-.05h0a20.35,20.35,0,0,1-20.336,20.349,20.35,20.35,0,0,1-20.339-20.349A20.35,20.35,0,0,1-243.539-2.326,20.35,20.35,0,0,1-223.2,18.023"
                                                      transform="translate(267.744 -17.765)" fill="#eb745a"></path>
                                                <path id="Path_444" data-name="Path 444"
                                                      d="M-214.792,26.464A20.35,20.35,0,0,0-235.128,6.115a20.352,20.352,0,0,0-20.339,20.349,20.351,20.351,0,0,0,20.339,20.344,20.349,20.349,0,0,0,20.336-20.344m-1.081-.05h0a19.306,19.306,0,0,1-19.3,19.305,19.3,19.3,0,0,1-19.293-19.305,19.306,19.306,0,0,1,19.293-19.3,19.309,19.309,0,0,1,19.3,19.3"
                                                      transform="translate(259.372 -26.156)" fill="#ec7a5f"></path>
                                                <path id="Path_445" data-name="Path 445"
                                                      d="M-208.544,33.734a19.307,19.307,0,0,0-19.3-19.3,19.3,19.3,0,0,0-19.293,19.3,19.3,19.3,0,0,0,19.293,19.305,19.307,19.307,0,0,0,19.3-19.305m-.961.075h0A18.263,18.263,0,0,1-227.756,52.07a18.262,18.262,0,0,1-18.251-18.261,18.262,18.262,0,0,1,18.251-18.261A18.263,18.263,0,0,1-209.5,33.809"
                                                      transform="translate(251.959 -33.55)" fill="#ed8166"></path>
                                                <path id="Path_446" data-name="Path 446"
                                                      d="M-200.1,43.229a18.263,18.263,0,0,0-18.251-18.264,18.263,18.263,0,0,0-18.251,18.264A18.262,18.262,0,0,0-218.356,61.49,18.262,18.262,0,0,0-200.1,43.229m-1.083-.05h0A17.218,17.218,0,0,1-218.394,60.4,17.217,17.217,0,0,1-235.6,43.179a17.217,17.217,0,0,1,17.208-17.217,17.218,17.218,0,0,1,17.207,17.217"
                                                      transform="translate(242.599 -42.92)" fill="#ef876c"></path>
                                                <path id="Path_447" data-name="Path 447"
                                                      d="M-192.775,51.6a17.222,17.222,0,0,0-17.209-17.222A17.223,17.223,0,0,0-227.193,51.6a17.221,17.221,0,0,0,17.209,17.217A17.221,17.221,0,0,0-192.775,51.6m-1.081-.05h0a16.177,16.177,0,0,1-16.167,16.176,16.175,16.175,0,0,1-16.164-16.176,16.175,16.175,0,0,1,16.164-16.173,16.176,16.176,0,0,1,16.167,16.173"
                                                      transform="translate(234.226 -51.291)" fill="#ef8c72"></path>
                                                <path id="Path_448" data-name="Path 448"
                                                      d="M-186.528,58.889a16.175,16.175,0,0,0-16.163-16.175,16.176,16.176,0,0,0-16.167,16.175,16.176,16.176,0,0,0,16.167,16.176,16.175,16.175,0,0,0,16.163-16.176m-.96.075h0A15.133,15.133,0,0,1-202.611,74.1a15.134,15.134,0,0,1-15.123-15.132,15.134,15.134,0,0,1,15.123-15.131,15.133,15.133,0,0,1,15.123,15.131"
                                                      transform="translate(226.814 -58.706)" fill="#f19378"></path>
                                                <path id="Path_449" data-name="Path 449"
                                                      d="M-179.2,67.28a15.131,15.131,0,0,0-15.123-15.131A15.134,15.134,0,0,0-209.445,67.28a15.134,15.134,0,0,0,15.123,15.132A15.131,15.131,0,0,0-179.2,67.28m-.959.075h0a14.089,14.089,0,0,1-14.079,14.087,14.089,14.089,0,0,1-14.08-14.087,14.089,14.089,0,0,1,14.08-14.087,14.089,14.089,0,0,1,14.079,14.087"
                                                      transform="translate(218.442 -67.097)" fill="#f29980"></path>
                                                <path id="Path_450" data-name="Path 450"
                                                      d="M-170.738,76.775a14.09,14.09,0,0,0-14.08-14.09A14.089,14.089,0,0,0-198.9,76.775,14.088,14.088,0,0,0-184.818,90.86a14.089,14.089,0,0,0,14.08-14.085m-1.083-.05h0a13.044,13.044,0,0,1-13.035,13.043,13.045,13.045,0,0,1-13.038-13.043,13.045,13.045,0,0,1,13.038-13.043A13.044,13.044,0,0,1-171.82,76.725"
                                                      transform="translate(209.06 -76.467)" fill="#f3a085"></path>
                                                <path id="Path_451" data-name="Path 451"
                                                      d="M-163.41,85.168a13.045,13.045,0,0,0-13.035-13.046,13.047,13.047,0,0,0-13.038,13.046,13.046,13.046,0,0,0,13.038,13.041A13.043,13.043,0,0,0-163.41,85.168m-1.081-.05h0a12,12,0,0,1-11.994,12,12,12,0,0,1-11.992-12,12,12,0,0,1,11.992-12,12,12,0,0,1,11.994,12"
                                                      transform="translate(200.689 -84.86)" fill="#f4a68d"></path>
                                                <path id="Path_452" data-name="Path 452"
                                                      d="M-157.162,92.436a12,12,0,0,0-11.994-12,12,12,0,0,0-11.992,12,12,12,0,0,0,11.992,12,12,12,0,0,0,11.994-12m-.959.075h0a10.956,10.956,0,0,1-10.95,10.955A10.957,10.957,0,0,1-180.023,92.51a10.957,10.957,0,0,1,10.951-10.955,10.956,10.956,0,0,1,10.95,10.955"
                                                      transform="translate(193.276 -92.252)" fill="#f5ac95"></path>
                                                <path id="Path_453" data-name="Path 453"
                                                      d="M-148.732,100.828a10.958,10.958,0,0,0-10.95-10.957,10.958,10.958,0,0,0-10.951,10.957,10.959,10.959,0,0,0,10.951,10.958,10.958,10.958,0,0,0,10.95-10.958m-1.081.075h0a9.916,9.916,0,0,1-9.907,9.916,9.914,9.914,0,0,1-9.907-9.916,9.91,9.91,0,0,1,9.907-9.911,9.912,9.912,0,0,1,9.907,9.911"
                                                      transform="translate(183.926 -100.645)" fill="#f6b29d"></path>
                                                <path id="Path_454" data-name="Path 454"
                                                      d="M-141.382,110.323a9.916,9.916,0,0,0-9.907-9.916,9.914,9.914,0,0,0-9.907,9.916,9.913,9.913,0,0,0,9.907,9.911,9.915,9.915,0,0,0,9.907-9.911m-1.083-.05h0a8.87,8.87,0,0,1-8.863,8.872,8.872,8.872,0,0,1-8.866-8.872,8.871,8.871,0,0,1,8.866-8.869,8.87,8.87,0,0,1,8.863,8.869"
                                                      transform="translate(175.533 -110.015)" fill="#f7baa5"></path>
                                                <path id="Path_455" data-name="Path 455"
                                                      d="M-134.045,118.714a8.87,8.87,0,0,0-8.863-8.872,8.87,8.87,0,0,0-8.866,8.872,8.869,8.869,0,0,0,8.866,8.867,8.869,8.869,0,0,0,8.863-8.867m-1.081-.05h0a7.829,7.829,0,0,1-7.823,7.828,7.829,7.829,0,0,1-7.822-7.828,7.829,7.829,0,0,1,7.822-7.828,7.829,7.829,0,0,1,7.823,7.828"
                                                      transform="translate(167.152 -118.406)" fill="#f9c2ae"></path>
                                                <path id="Path_456" data-name="Path 456"
                                                      d="M-127.817,126a7.825,7.825,0,0,0-7.823-7.825A7.827,7.827,0,0,0-143.462,126a7.827,7.827,0,0,0,7.822,7.826A7.825,7.825,0,0,0-127.817,126m-.959.075h0a6.785,6.785,0,0,1-6.779,6.784,6.784,6.784,0,0,1-6.779-6.784,6.784,6.784,0,0,1,6.779-6.784,6.785,6.785,0,0,1,6.779,6.784"
                                                      transform="translate(159.76 -125.819)" fill="#fac8b5"></path>
                                                <path id="Path_457" data-name="Path 457"
                                                      d="M-119.365,135.5a6.783,6.783,0,0,0-6.779-6.784,6.783,6.783,0,0,0-6.779,6.784,6.781,6.781,0,0,0,6.779,6.779,6.782,6.782,0,0,0,6.779-6.779m-1.083-.05h0a5.738,5.738,0,0,1-5.735,5.74,5.74,5.74,0,0,1-5.737-5.74,5.74,5.74,0,0,1,5.737-5.74,5.738,5.738,0,0,1,5.735,5.74"
                                                      transform="translate(150.387 -135.19)" fill="#facfbd"></path>
                                                <path id="Path_458" data-name="Path 458"
                                                      d="M-112.028,143.87a5.742,5.742,0,0,0-5.735-5.742,5.742,5.742,0,0,0-5.737,5.742,5.741,5.741,0,0,0,5.737,5.737,5.741,5.741,0,0,0,5.735-5.737m-1.081-.05h0a4.7,4.7,0,0,1-4.695,4.7,4.7,4.7,0,0,1-4.691-4.7,4.7,4.7,0,0,1,4.691-4.7,4.7,4.7,0,0,1,4.695,4.7"
                                                      transform="translate(142.007 -143.561)" fill="#fbd6c8"></path>
                                                <path id="Path_459" data-name="Path 459"
                                                      d="M-104.679,152.262a4.7,4.7,0,0,0-4.694-4.7,4.7,4.7,0,0,0-4.691,4.7,4.694,4.694,0,0,0,4.691,4.693,4.7,4.7,0,0,0,4.694-4.693m-1.083-.05h0a3.653,3.653,0,0,1-3.649,3.654,3.654,3.654,0,0,1-3.651-3.654,3.652,3.652,0,0,1,3.651-3.652,3.65,3.65,0,0,1,3.649,3.652"
                                                      transform="translate(133.615 -151.954)" fill="#fcded0"></path>
                                                <path id="Path_460" data-name="Path 460"
                                                      d="M-98.452,159.55A3.652,3.652,0,0,0-102.1,155.9a3.652,3.652,0,0,0-3.649,3.651A3.652,3.652,0,0,0-102.1,163.2a3.652,3.652,0,0,0,3.651-3.652m-.96.075h0a2.609,2.609,0,0,1-2.606,2.61,2.607,2.607,0,0,1-2.606-2.61,2.607,2.607,0,0,1,2.606-2.61,2.609,2.609,0,0,1,2.606,2.61"
                                                      transform="translate(126.223 -159.367)" fill="#fde5db"></path>
                                                <path id="Path_461" data-name="Path 461"
                                                      d="M-90.021,169.046a2.609,2.609,0,0,0-2.607-2.61,2.611,2.611,0,0,0-2.61,2.61,2.612,2.612,0,0,0,2.61,2.608,2.61,2.61,0,0,0,2.607-2.608M-91.1,169h0a1.567,1.567,0,0,1-1.565,1.566A1.567,1.567,0,0,1-94.232,169a1.567,1.567,0,0,1,1.563-1.566A1.566,1.566,0,0,1-91.1,169"
                                                      transform="translate(116.872 -168.738)" fill="#feede5"></path>
                                                <path id="Path_462" data-name="Path 462"
                                                      d="M-82.662,177.436a1.564,1.564,0,0,0-1.564-1.566,1.563,1.563,0,0,0-1.565,1.566A1.564,1.564,0,0,0-84.225,179a1.565,1.565,0,0,0,1.564-1.563m-1.083-.05h0a.522.522,0,0,1-.522.522.523.523,0,0,1-.522-.522.523.523,0,0,1,.522-.522.522.522,0,0,1,.522.522"
                                                      transform="translate(108.469 -177.128)" fill="#fef4f0"></path>
                                                <path id="Path_463" data-name="Path 463"
                                                      d="M-76.435,184.706a.522.522,0,0,0-.521-.522.522.522,0,0,0-.522.522.522.522,0,0,0,.522.522.522.522,0,0,0,.521-.522"
                                                      transform="translate(101.077 -184.522)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <path id="Path_465" data-name="Path 465"
                                              d="M-344.518,41.025a6.836,6.836,0,0,1-1.551,2.491,17.928,17.928,0,0,1-2.986,2.57,28.371,28.371,0,0,1-3.989,2.307,28.42,28.42,0,0,1-4.493,1.693,18.089,18.089,0,0,1-4.057.7,7.173,7.173,0,0,1-2.854-.36,2.288,2.288,0,0,1-1.411-1.273,2.656,2.656,0,0,1,.209-2.036,5.97,5.97,0,0,1,.4-.728,8.528,8.528,0,0,1,.529-.738c.195-.246.411-.5.649-.743s.486-.492.756-.738l.723-.634.712-.574.675-.51.675-.472.664-.435.644-.39.639-.363a13.739,13.739,0,0,0-1.932,1.8,5.8,5.8,0,0,0-.814,1.235,1.2,1.2,0,0,0-.062.982,1.164,1.164,0,0,0,.736.589,3.835,3.835,0,0,0,1.426.137,9.336,9.336,0,0,0,1.991-.37,14.143,14.143,0,0,0,2.182-.828,14.472,14.472,0,0,0,1.948-1.116,9.355,9.355,0,0,0,1.484-1.245,3.64,3.64,0,0,0,.812-1.216,1.082,1.082,0,0,0-.039-.952,1.3,1.3,0,0,0-.817-.532,3.221,3.221,0,0,0-1.423.027A16.554,16.554,0,0,0-355,39.66a7.111,7.111,0,0,0-1.163.44,19.254,19.254,0,0,1,3.6-1.484A17.364,17.364,0,0,1-350.129,38a9.581,9.581,0,0,1,2.6-.1,5.3,5.3,0,0,1,1.919.477,2.168,2.168,0,0,1,1.064,1.049,2.3,2.3,0,0,1,.023,1.606"
                                              transform="translate(371.292 -37.849)" fill="#da091d"></path>
                                        <path id="Path_466" data-name="Path 466"
                                              d="M-232.63,369.177a12.493,12.493,0,0,1-.636,1.444,12.274,12.274,0,0,1-.827,1.2s-.184.244-.3.395c-.337.43-.894,1.074-.894,1.074l-.748.815a21.9,21.9,0,0,1-2.622,2.3,26.807,26.807,0,0,1-3.161,2.023,27.745,27.745,0,0,1-3.541,1.623,25.657,25.657,0,0,1-3.753,1.1c-.248.055-.5.1-.741.147s-.486.08-.725.117-.476.062-.708.09-.462.045-.69.06l-1.385.062-.306,0-1.482-.045-1.506-.134s-.26-.032-.423-.055c-.626-.089-.979-.149-1.6-.276s-1.6-.39-1.6-.39l-.807-.271-.22-.107a10.025,10.025,0,0,0,2.05.547,17.708,17.708,0,0,0,2.375.189,22.978,22.978,0,0,0,2.657-.129,27.867,27.867,0,0,0,2.912-.467,34.275,34.275,0,0,0,6.76-2.22,35.388,35.388,0,0,0,6.043-3.423,27.716,27.716,0,0,0,4.7-4.154,13.542,13.542,0,0,0,1.186-1.531c.016-.018-.005.02-.005.02"
                                              transform="translate(278.203 -332.494)" fill="#da091d"></path>
                                        <path id="Path_467" data-name="Path 467"
                                              d="M-378.946,42.387l1.62.609,1.6.736.562.291a5.13,5.13,0,0,1,1.163.825,3.838,3.838,0,0,1,.8,1.059,3.814,3.814,0,0,1,.38,1.305,5.03,5.03,0,0,1-.082,1.541,11.126,11.126,0,0,1-2.449,4.629,28.482,28.482,0,0,1-5.226,4.81,45.16,45.16,0,0,1-7.227,4.286,45.924,45.924,0,0,1-8.219,3.015,30.376,30.376,0,0,1-6.735,1.017,13.619,13.619,0,0,1-4.88-.654,5.124,5.124,0,0,1-2.734-2.041,4.2,4.2,0,0,1-.433-3.137l.114-.517.461-1.715.576-1.661.6-1.4.544-1.144-.091.231c.02-.042-.021.047-.021.047a4.126,4.126,0,0,0-.355,2.963,3.291,3.291,0,0,0,1.941,1.959,9.716,9.716,0,0,0,4.063.656,24.726,24.726,0,0,0,5.873-.917,38.761,38.761,0,0,0,6.539-2.434,38.793,38.793,0,0,0,5.781-3.381,24.367,24.367,0,0,0,4.251-3.779,9.32,9.32,0,0,0,2.093-3.637,3.713,3.713,0,0,0,.076-1.611,2.756,2.756,0,0,0-.6-1.26,3.7,3.7,0,0,0-1.182-.912,6.235,6.235,0,0,0-1.089-.418l-.6-.159a5.093,5.093,0,0,1,.6.114s.581.142.95.249c.529.154,1.344.435,1.344.435"
                                              transform="translate(411.272 -41.175)" fill="#da091d"></path>
                                        <path id="Path_468" data-name="Path 468"
                                              d="M-370.64,104.148l1.2,1.511,1.091,1.623.49.828a5.436,5.436,0,0,1,.372.81,5.572,5.572,0,0,1,.234.885,5.913,5.913,0,0,1,.094.967,7.3,7.3,0,0,1-.058,1.042,12.979,12.979,0,0,1-2.638,5.859,31.238,31.238,0,0,1-6.1,6.026,48.683,48.683,0,0,1-8.647,5.265,49.469,49.469,0,0,1-9.944,3.535,34.638,34.638,0,0,1-6.676.949,19.979,19.979,0,0,1-5.292-.445,10.022,10.022,0,0,1-3.706-1.621,5.741,5.741,0,0,1-2-2.585l-.382-1.1-.522-1.882-.369-1.9-.073-.532-.107-1.071c-.014-.1,0,0,0,0a4.638,4.638,0,0,0,1.263,2.65,7.45,7.45,0,0,0,3.365,1.9,17.172,17.172,0,0,0,5.29.572,33.594,33.594,0,0,0,6.967-1.021,49.087,49.087,0,0,0,9.388-3.388,48.708,48.708,0,0,0,8.207-4.929,30.763,30.763,0,0,0,5.852-5.588,12.289,12.289,0,0,0,2.623-5.389,6.376,6.376,0,0,0,.071-1.36,4.961,4.961,0,0,0-.224-1.206,4.7,4.7,0,0,0-.493-1.054,4.814,4.814,0,0,0-.434-.569v-.008l1,1.049Z"
                                              transform="translate(413.712 -95.722)" fill="#da091d"></path>
                                        <path id="Path_469" data-name="Path 469"
                                              d="M-323.133,222.047a13.392,13.392,0,0,1-.05,3.945,12.179,12.179,0,0,1-2.309,5.641,27.059,27.059,0,0,1-5.443,5.665,40.881,40.881,0,0,1-7.773,4.828,41.072,41.072,0,0,1-9,3.125,31.838,31.838,0,0,1-4.544.666,22.73,22.73,0,0,1-3.948-.047,14.976,14.976,0,0,1-3.259-.689,9.625,9.625,0,0,1-2.5-1.248l-1.487-1.153-1.242-1.121s-.845-.825-1.336-1.4c-.352-.41-.861-1.086-.861-1.086h0a7.439,7.439,0,0,0,2.164,1.688,12.563,12.563,0,0,0,3.691,1.2,21.863,21.863,0,0,0,4.841.261,34.413,34.413,0,0,0,5.842-.823,46.638,46.638,0,0,0,9.8-3.438,46.049,46.049,0,0,0,8.487-5.225,29.8,29.8,0,0,0,5.949-6.056,13.013,13.013,0,0,0,2.526-5.954c.02-.2.035-.405.041-.6a4.128,4.128,0,0,0-.043-.592,7.522,7.522,0,0,1,.249.868s.158.937.216,1.543"
                                              transform="translate(372.14 -199.499)" fill="#da091d"></path>
                                    </svg>
                                </figure>

                                <h5>reddot design award</h5>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="awBox">
                                <figure data-anim>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="49.128" height="49.129"
                                         viewBox="0 0 49.128 49.129">
                                        <path id="Path_340" data-name="Path 340"
                                              d="M-157.372,336.236a1.738,1.738,0,0,1-.921-.619,1.772,1.772,0,0,1-.331-1.1,3.464,3.464,0,0,1,.336-1.382,6.309,6.309,0,0,1,.906-1.407,8.564,8.564,0,0,1,1.333-1.273,8.643,8.643,0,0,1,1.64-1,6.173,6.173,0,0,1,1.663-.517,3.5,3.5,0,0,1,1.384.032,1.868,1.868,0,0,1,.97.562,1.727,1.727,0,0,1,.4,1.064,3.238,3.238,0,0,1-.288,1.4,6.049,6.049,0,0,1-.9,1.454,8.552,8.552,0,0,1-1.38,1.335,8.605,8.605,0,0,1-1.71,1.027,6.085,6.085,0,0,1-1.708.5,3.915,3.915,0,0,1-.527.035,2.929,2.929,0,0,1-.864-.119m5.374-12.38a13.52,13.52,0,0,0-3.407,1.106,18.2,18.2,0,0,0-3.347,2.051,18.051,18.051,0,0,0-2.743,2.6,13.43,13.43,0,0,0-1.894,2.871,7.387,7.387,0,0,0-.772,2.859,3.758,3.758,0,0,0,.6,2.312,3.538,3.538,0,0,0,1.856,1.34,6.763,6.763,0,0,0,2.886.216,14.14,14.14,0,0,0,3.568-1.345,16.55,16.55,0,0,0,3.414-2.265,17.709,17.709,0,0,0,2.927-2.8,16.351,16.351,0,0,0,2.138-2.784,5.656,5.656,0,0,0,.8-2.981,3.529,3.529,0,0,0-1.045-2.158,4,4,0,0,0-2.108-1.014,6.432,6.432,0,0,0-1.367-.137,8.8,8.8,0,0,0-1.509.134"
                                              transform="translate(191.807 -292.09)" fill="#bfbfbf"></path>
                                        <path id="Path_341" data-name="Path 341"
                                              d="M-400.858,68.961l.006-.1a1,1,0,0,0-.006.1m14.335-18.02a24.977,24.977,0,0,0-2.961,1.591c-3.347,2.085-5.336,3.182-7.789,6.187a14.847,14.847,0,0,0-2.25,3.294c-.339.691-.746,1.837-.746,1.837l-.169,1.285-.267,1.74-.129,1.556-.017.425c.005-.1.012-.2.022-.3.01-.137.031-.276.051-.417s.046-.288.076-.438.067-.3.107-.448a18.771,18.771,0,0,1,2.169-4.969,20.177,20.177,0,0,1,3.765-4.629,39.607,39.607,0,0,1,4.717-4.228c1.207-.888,4.151-2.742,5.89-2.978.128-.015.156-.027.114-.027a9.824,9.824,0,0,0-2.583.522"
                                              transform="translate(402.313 -49.028)" fill="#bfbfbf"></path>
                                        <path id="Path_382" data-name="Path 382"
                                              d="M-411.349,73.838l-.124-.2c.04.07.081.137.124.2m34.479-29.783h0l-.039-.04a.379.379,0,0,0,.036.04m-.036-.04c-.068-.075-.137-.147-.207-.214Zm-11.82-4.517a17.372,17.372,0,0,0-3.618,1.2,34.554,34.554,0,0,0-4.55,2.376,45.77,45.77,0,0,0-6.2,4.589,47,47,0,0,0-5.232,5.369,35.189,35.189,0,0,0-3.889,5.7,17.34,17.34,0,0,0-1.754,5.558,9.035,9.035,0,0,0-.031.982,25.892,25.892,0,0,0,.624,3.806,15.027,15.027,0,0,0,.433,1.588c.245.714.763,1.775.763,1.775l.257.452.449.733a5.2,5.2,0,0,1-.405-.867,6.275,6.275,0,0,1-.316-1.233,8.09,8.09,0,0,1-.1-1.39,11.261,11.261,0,0,1,.125-1.529,19.269,19.269,0,0,1,2.063-5.954,35.708,35.708,0,0,1,4.032-6.051,47.247,47.247,0,0,1,5.455-5.648,46.371,46.371,0,0,1,6.507-4.785,35.213,35.213,0,0,1,5.376-2.675,21.258,21.258,0,0,1,4.852-1.293,10.935,10.935,0,0,1,4.071.144,5.867,5.867,0,0,1,2.7,1.447l-1.027-1.064-1.349-1.2-.685-.534-1.54-.614a18.337,18.337,0,0,0-2.978-.808,18.66,18.66,0,0,0-2.559-.222,7.1,7.1,0,0,0-1.475.134"
                                              transform="translate(414 -39.196)" fill="#bfbfbf"></path>
                                        <path id="Path_426" data-name="Path 426"
                                              d="M-368.21,124.633l-.568-.2c.184.075.372.142.568.2m20.616-39a21.818,21.818,0,0,0-5.72,1.489,36.4,36.4,0,0,0-6.3,3.314,47.483,47.483,0,0,0-6.745,5.337,47.032,47.032,0,0,0-5.5,6.192,34.969,34.969,0,0,0-3.869,6.545,18.785,18.785,0,0,0-1.684,6.344,10.669,10.669,0,0,0,.076,2.1,7.561,7.561,0,0,0,.481,1.8,5.925,5.925,0,0,0,.874,1.494,5.777,5.777,0,0,0,1.252,1.166l1.647,1.049,1.571.835,1.741.771.989.35a6.575,6.575,0,0,1-1.431-.791,5.856,5.856,0,0,1-1.441-1.539,6.6,6.6,0,0,1-.855-2.036,9.448,9.448,0,0,1-.239-2.5,17.017,17.017,0,0,1,1.433-5.969,31.008,31.008,0,0,1,3.515-6.147,41.981,41.981,0,0,1,5.088-5.79,41.981,41.981,0,0,1,6.3-4.944,31.484,31.484,0,0,1,6.541-3.241,22.9,22.9,0,0,1,6.616-1.256,8.324,8.324,0,0,1,4.567,1.221c1.244.825,1.484,2.215,1.894,3.9l.654-.142-2.1-5.546-.542-1.213a6.139,6.139,0,0,0-3.3-2.528,10.022,10.022,0,0,0-3.174-.4,23.191,23.191,0,0,0-2.344.124"
                                              transform="translate(381.476 -80.236)" fill="#bfbfbf"></path>
                                        <path id="Path_464" data-name="Path 464"
                                              d="M-274.1,217.774c.276-.094.553-.2.833-.3Zm.833-.3.071-.028.311-.124-.382.152M-269,188.212a30.356,30.356,0,0,0-6.4,3.065,39.272,39.272,0,0,0-6.074,4.663,38.128,38.128,0,0,0-4.844,5.464,27.611,27.611,0,0,0-3.257,5.77,14.612,14.612,0,0,0-1.176,5.551,7.806,7.806,0,0,0,.551,2.884,5.739,5.739,0,0,0,1.514,2.13A5.428,5.428,0,0,0-286.3,219a17.08,17.08,0,0,0,3.2.438l.261-.005,1.841-.117,1.8-.229.894-.159,1.644-.365,1.588-.455.435-.142.542-.2a15.355,15.355,0,0,1-3.452.77,8.694,8.694,0,0,1-3.709-.363,4.988,4.988,0,0,1-2.464-1.8,5.65,5.65,0,0,1-.983-3.082,11.2,11.2,0,0,1,.818-4.455,21.016,21.016,0,0,1,2.538-4.651,29.32,29.32,0,0,1,3.872-4.4,30.216,30.216,0,0,1,4.892-3.731,23.368,23.368,0,0,1,5.159-2.386,13.385,13.385,0,0,1,4.574-.666,6.51,6.51,0,0,1,3.526,1.116,4.911,4.911,0,0,1,1.912,3.006c.145-.4,1.02-.706,1.135-1.116s.216-.83.3-1.248.336-2.7.357-3.127a4.651,4.651,0,0,0-.2-.721,5.724,5.724,0,0,0-2.293-2.322,11.07,11.07,0,0,0-5.188-1.35c-.082,0-.165,0-.247,0a17.66,17.66,0,0,0-5.458.972"
                                              transform="translate(304.395 -170.71)" fill="#bfbfbf"></path>
                                        <path id="Path_465" data-name="Path 465"
                                              d="M-344.518,41.025a6.836,6.836,0,0,1-1.551,2.491,17.928,17.928,0,0,1-2.986,2.57,28.371,28.371,0,0,1-3.989,2.307,28.42,28.42,0,0,1-4.493,1.693,18.089,18.089,0,0,1-4.057.7,7.173,7.173,0,0,1-2.854-.36,2.288,2.288,0,0,1-1.411-1.273,2.656,2.656,0,0,1,.209-2.036,5.97,5.97,0,0,1,.4-.728,8.528,8.528,0,0,1,.529-.738c.195-.246.411-.5.649-.743s.486-.492.756-.738l.723-.634.712-.574.675-.51.675-.472.664-.435.644-.39.639-.363a13.739,13.739,0,0,0-1.932,1.8,5.8,5.8,0,0,0-.814,1.235,1.2,1.2,0,0,0-.062.982,1.164,1.164,0,0,0,.736.589,3.835,3.835,0,0,0,1.426.137,9.336,9.336,0,0,0,1.991-.37,14.143,14.143,0,0,0,2.182-.828,14.472,14.472,0,0,0,1.948-1.116,9.355,9.355,0,0,0,1.484-1.245,3.64,3.64,0,0,0,.812-1.216,1.082,1.082,0,0,0-.039-.952,1.3,1.3,0,0,0-.817-.532,3.221,3.221,0,0,0-1.423.027A16.554,16.554,0,0,0-355,39.66a7.111,7.111,0,0,0-1.163.44,19.254,19.254,0,0,1,3.6-1.484A17.364,17.364,0,0,1-350.129,38a9.581,9.581,0,0,1,2.6-.1,5.3,5.3,0,0,1,1.919.477,2.168,2.168,0,0,1,1.064,1.049,2.3,2.3,0,0,1,.023,1.606"
                                              transform="translate(371.291 -37.849)"></path>
                                        <path id="Path_466" data-name="Path 466"
                                              d="M-232.63,369.177a12.493,12.493,0,0,1-.636,1.444,12.274,12.274,0,0,1-.827,1.2s-.184.244-.3.395c-.337.43-.894,1.074-.894,1.074l-.748.815a21.9,21.9,0,0,1-2.622,2.3,26.807,26.807,0,0,1-3.161,2.023,27.745,27.745,0,0,1-3.541,1.623,25.657,25.657,0,0,1-3.753,1.1c-.248.055-.5.1-.741.147s-.486.08-.725.117-.476.062-.708.09-.462.045-.69.06l-1.385.062-.306,0-1.482-.045-1.506-.134s-.26-.032-.423-.055c-.626-.089-.979-.149-1.6-.276s-1.6-.39-1.6-.39l-.807-.271-.22-.107a10.025,10.025,0,0,0,2.05.547,17.708,17.708,0,0,0,2.375.189,22.978,22.978,0,0,0,2.657-.129,27.867,27.867,0,0,0,2.912-.467,34.275,34.275,0,0,0,6.76-2.22,35.388,35.388,0,0,0,6.043-3.423,27.716,27.716,0,0,0,4.7-4.154,13.542,13.542,0,0,0,1.186-1.531c.016-.018-.005.02-.005.02"
                                              transform="translate(278.203 -332.494)"></path>
                                        <path id="Path_467" data-name="Path 467"
                                              d="M-378.946,42.387l1.62.609,1.6.736.562.291a5.13,5.13,0,0,1,1.163.825,3.838,3.838,0,0,1,.8,1.059,3.814,3.814,0,0,1,.38,1.305,5.03,5.03,0,0,1-.082,1.541,11.126,11.126,0,0,1-2.449,4.629,28.482,28.482,0,0,1-5.226,4.81,45.16,45.16,0,0,1-7.227,4.286,45.924,45.924,0,0,1-8.219,3.015,30.376,30.376,0,0,1-6.735,1.017,13.619,13.619,0,0,1-4.88-.654,5.124,5.124,0,0,1-2.734-2.041,4.2,4.2,0,0,1-.433-3.137l.114-.517.461-1.715.576-1.661.6-1.4.544-1.144-.091.231c.02-.042-.021.047-.021.047a4.126,4.126,0,0,0-.355,2.963,3.291,3.291,0,0,0,1.941,1.959,9.716,9.716,0,0,0,4.063.656,24.726,24.726,0,0,0,5.873-.917,38.761,38.761,0,0,0,6.539-2.434,38.793,38.793,0,0,0,5.781-3.381,24.367,24.367,0,0,0,4.251-3.779,9.32,9.32,0,0,0,2.093-3.637,3.713,3.713,0,0,0,.076-1.611,2.756,2.756,0,0,0-.6-1.26,3.7,3.7,0,0,0-1.182-.912,6.235,6.235,0,0,0-1.089-.418l-.6-.159a5.093,5.093,0,0,1,.6.114s.581.142.95.249c.529.154,1.344.435,1.344.435"
                                              transform="translate(411.272 -41.175)"></path>
                                        <path id="Path_468" data-name="Path 468"
                                              d="M-370.64,104.148l1.2,1.511,1.091,1.623.49.828a5.436,5.436,0,0,1,.372.81,5.572,5.572,0,0,1,.234.885,5.913,5.913,0,0,1,.094.967,7.3,7.3,0,0,1-.058,1.042,12.979,12.979,0,0,1-2.638,5.859,31.238,31.238,0,0,1-6.1,6.026,48.683,48.683,0,0,1-8.647,5.265,49.469,49.469,0,0,1-9.944,3.535,34.638,34.638,0,0,1-6.676.949,19.979,19.979,0,0,1-5.292-.445,10.022,10.022,0,0,1-3.706-1.621,5.741,5.741,0,0,1-2-2.585l-.382-1.1-.522-1.882-.369-1.9-.073-.532-.107-1.071c-.014-.1,0,0,0,0a4.638,4.638,0,0,0,1.263,2.65,7.45,7.45,0,0,0,3.365,1.9,17.172,17.172,0,0,0,5.29.572,33.594,33.594,0,0,0,6.967-1.021,49.087,49.087,0,0,0,9.388-3.388,48.708,48.708,0,0,0,8.207-4.929,30.763,30.763,0,0,0,5.852-5.588,12.289,12.289,0,0,0,2.623-5.389,6.376,6.376,0,0,0,.071-1.36,4.961,4.961,0,0,0-.224-1.206,4.7,4.7,0,0,0-.493-1.054,4.814,4.814,0,0,0-.434-.569v-.008l1,1.049Z"
                                              transform="translate(413.711 -95.721)"></path>
                                        <path id="Path_469" data-name="Path 469"
                                              d="M-323.133,222.047a13.392,13.392,0,0,1-.05,3.945,12.179,12.179,0,0,1-2.309,5.641,27.059,27.059,0,0,1-5.443,5.665,40.881,40.881,0,0,1-7.773,4.828,41.072,41.072,0,0,1-9,3.125,31.838,31.838,0,0,1-4.544.666,22.73,22.73,0,0,1-3.948-.047,14.976,14.976,0,0,1-3.259-.689,9.625,9.625,0,0,1-2.5-1.248l-1.487-1.153-1.242-1.121s-.845-.825-1.336-1.4c-.352-.41-.861-1.086-.861-1.086h0a7.439,7.439,0,0,0,2.164,1.688,12.563,12.563,0,0,0,3.691,1.2,21.863,21.863,0,0,0,4.841.261,34.413,34.413,0,0,0,5.842-.823,46.638,46.638,0,0,0,9.8-3.438,46.049,46.049,0,0,0,8.487-5.225,29.8,29.8,0,0,0,5.949-6.056,13.013,13.013,0,0,0,2.526-5.954c.02-.2.035-.405.041-.6a4.128,4.128,0,0,0-.043-.592,7.522,7.522,0,0,1,.249.868s.158.937.216,1.543"
                                              transform="translate(372.14 -199.499)"></path>
                                    </svg>
                                </figure>

                                <h5>reddot design award</h5>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="awBox">
                                <figure data-anim>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="49.128" height="49.129"
                                         viewBox="0 0 49.128 49.129">
                                        <path id="Path_340" data-name="Path 340"
                                              d="M-157.372,336.236a1.738,1.738,0,0,1-.921-.619,1.772,1.772,0,0,1-.331-1.1,3.464,3.464,0,0,1,.336-1.382,6.309,6.309,0,0,1,.906-1.407,8.564,8.564,0,0,1,1.333-1.273,8.643,8.643,0,0,1,1.64-1,6.173,6.173,0,0,1,1.663-.517,3.5,3.5,0,0,1,1.384.032,1.868,1.868,0,0,1,.97.562,1.727,1.727,0,0,1,.4,1.064,3.238,3.238,0,0,1-.288,1.4,6.049,6.049,0,0,1-.9,1.454,8.552,8.552,0,0,1-1.38,1.335,8.605,8.605,0,0,1-1.71,1.027,6.085,6.085,0,0,1-1.708.5,3.915,3.915,0,0,1-.527.035,2.929,2.929,0,0,1-.864-.119m5.374-12.38a13.52,13.52,0,0,0-3.407,1.106,18.2,18.2,0,0,0-3.347,2.051,18.051,18.051,0,0,0-2.743,2.6,13.43,13.43,0,0,0-1.894,2.871,7.387,7.387,0,0,0-.772,2.859,3.758,3.758,0,0,0,.6,2.312,3.538,3.538,0,0,0,1.856,1.34,6.763,6.763,0,0,0,2.886.216,14.14,14.14,0,0,0,3.568-1.345,16.55,16.55,0,0,0,3.414-2.265,17.709,17.709,0,0,0,2.927-2.8,16.351,16.351,0,0,0,2.138-2.784,5.656,5.656,0,0,0,.8-2.981,3.529,3.529,0,0,0-1.045-2.158,4,4,0,0,0-2.108-1.014,6.432,6.432,0,0,0-1.367-.137,8.8,8.8,0,0,0-1.509.134"
                                              transform="translate(191.807 -292.09)" fill="#bfbfbf"></path>
                                        <path id="Path_341" data-name="Path 341"
                                              d="M-400.858,68.961l.006-.1a1,1,0,0,0-.006.1m14.335-18.02a24.977,24.977,0,0,0-2.961,1.591c-3.347,2.085-5.336,3.182-7.789,6.187a14.847,14.847,0,0,0-2.25,3.294c-.339.691-.746,1.837-.746,1.837l-.169,1.285-.267,1.74-.129,1.556-.017.425c.005-.1.012-.2.022-.3.01-.137.031-.276.051-.417s.046-.288.076-.438.067-.3.107-.448a18.771,18.771,0,0,1,2.169-4.969,20.177,20.177,0,0,1,3.765-4.629,39.607,39.607,0,0,1,4.717-4.228c1.207-.888,4.151-2.742,5.89-2.978.128-.015.156-.027.114-.027a9.824,9.824,0,0,0-2.583.522"
                                              transform="translate(402.313 -49.028)" fill="#bfbfbf"></path>
                                        <path id="Path_382" data-name="Path 382"
                                              d="M-411.349,73.838l-.124-.2c.04.07.081.137.124.2m34.479-29.783h0l-.039-.04a.379.379,0,0,0,.036.04m-.036-.04c-.068-.075-.137-.147-.207-.214Zm-11.82-4.517a17.372,17.372,0,0,0-3.618,1.2,34.554,34.554,0,0,0-4.55,2.376,45.77,45.77,0,0,0-6.2,4.589,47,47,0,0,0-5.232,5.369,35.189,35.189,0,0,0-3.889,5.7,17.34,17.34,0,0,0-1.754,5.558,9.035,9.035,0,0,0-.031.982,25.892,25.892,0,0,0,.624,3.806,15.027,15.027,0,0,0,.433,1.588c.245.714.763,1.775.763,1.775l.257.452.449.733a5.2,5.2,0,0,1-.405-.867,6.275,6.275,0,0,1-.316-1.233,8.09,8.09,0,0,1-.1-1.39,11.261,11.261,0,0,1,.125-1.529,19.269,19.269,0,0,1,2.063-5.954,35.708,35.708,0,0,1,4.032-6.051,47.247,47.247,0,0,1,5.455-5.648,46.371,46.371,0,0,1,6.507-4.785,35.213,35.213,0,0,1,5.376-2.675,21.258,21.258,0,0,1,4.852-1.293,10.935,10.935,0,0,1,4.071.144,5.867,5.867,0,0,1,2.7,1.447l-1.027-1.064-1.349-1.2-.685-.534-1.54-.614a18.337,18.337,0,0,0-2.978-.808,18.66,18.66,0,0,0-2.559-.222,7.1,7.1,0,0,0-1.475.134"
                                              transform="translate(414 -39.196)" fill="#bfbfbf"></path>
                                        <path id="Path_426" data-name="Path 426"
                                              d="M-368.21,124.633l-.568-.2c.184.075.372.142.568.2m20.616-39a21.818,21.818,0,0,0-5.72,1.489,36.4,36.4,0,0,0-6.3,3.314,47.483,47.483,0,0,0-6.745,5.337,47.032,47.032,0,0,0-5.5,6.192,34.969,34.969,0,0,0-3.869,6.545,18.785,18.785,0,0,0-1.684,6.344,10.669,10.669,0,0,0,.076,2.1,7.561,7.561,0,0,0,.481,1.8,5.925,5.925,0,0,0,.874,1.494,5.777,5.777,0,0,0,1.252,1.166l1.647,1.049,1.571.835,1.741.771.989.35a6.575,6.575,0,0,1-1.431-.791,5.856,5.856,0,0,1-1.441-1.539,6.6,6.6,0,0,1-.855-2.036,9.448,9.448,0,0,1-.239-2.5,17.017,17.017,0,0,1,1.433-5.969,31.008,31.008,0,0,1,3.515-6.147,41.981,41.981,0,0,1,5.088-5.79,41.981,41.981,0,0,1,6.3-4.944,31.484,31.484,0,0,1,6.541-3.241,22.9,22.9,0,0,1,6.616-1.256,8.324,8.324,0,0,1,4.567,1.221c1.244.825,1.484,2.215,1.894,3.9l.654-.142-2.1-5.546-.542-1.213a6.139,6.139,0,0,0-3.3-2.528,10.022,10.022,0,0,0-3.174-.4,23.191,23.191,0,0,0-2.344.124"
                                              transform="translate(381.476 -80.236)" fill="#bfbfbf"></path>
                                        <path id="Path_464" data-name="Path 464"
                                              d="M-274.1,217.774c.276-.094.553-.2.833-.3Zm.833-.3.071-.028.311-.124-.382.152M-269,188.212a30.356,30.356,0,0,0-6.4,3.065,39.272,39.272,0,0,0-6.074,4.663,38.128,38.128,0,0,0-4.844,5.464,27.611,27.611,0,0,0-3.257,5.77,14.612,14.612,0,0,0-1.176,5.551,7.806,7.806,0,0,0,.551,2.884,5.739,5.739,0,0,0,1.514,2.13A5.428,5.428,0,0,0-286.3,219a17.08,17.08,0,0,0,3.2.438l.261-.005,1.841-.117,1.8-.229.894-.159,1.644-.365,1.588-.455.435-.142.542-.2a15.355,15.355,0,0,1-3.452.77,8.694,8.694,0,0,1-3.709-.363,4.988,4.988,0,0,1-2.464-1.8,5.65,5.65,0,0,1-.983-3.082,11.2,11.2,0,0,1,.818-4.455,21.016,21.016,0,0,1,2.538-4.651,29.32,29.32,0,0,1,3.872-4.4,30.216,30.216,0,0,1,4.892-3.731,23.368,23.368,0,0,1,5.159-2.386,13.385,13.385,0,0,1,4.574-.666,6.51,6.51,0,0,1,3.526,1.116,4.911,4.911,0,0,1,1.912,3.006c.145-.4,1.02-.706,1.135-1.116s.216-.83.3-1.248.336-2.7.357-3.127a4.651,4.651,0,0,0-.2-.721,5.724,5.724,0,0,0-2.293-2.322,11.07,11.07,0,0,0-5.188-1.35c-.082,0-.165,0-.247,0a17.66,17.66,0,0,0-5.458.972"
                                              transform="translate(304.395 -170.71)" fill="#bfbfbf"></path>
                                        <path id="Path_465" data-name="Path 465"
                                              d="M-344.518,41.025a6.836,6.836,0,0,1-1.551,2.491,17.928,17.928,0,0,1-2.986,2.57,28.371,28.371,0,0,1-3.989,2.307,28.42,28.42,0,0,1-4.493,1.693,18.089,18.089,0,0,1-4.057.7,7.173,7.173,0,0,1-2.854-.36,2.288,2.288,0,0,1-1.411-1.273,2.656,2.656,0,0,1,.209-2.036,5.97,5.97,0,0,1,.4-.728,8.528,8.528,0,0,1,.529-.738c.195-.246.411-.5.649-.743s.486-.492.756-.738l.723-.634.712-.574.675-.51.675-.472.664-.435.644-.39.639-.363a13.739,13.739,0,0,0-1.932,1.8,5.8,5.8,0,0,0-.814,1.235,1.2,1.2,0,0,0-.062.982,1.164,1.164,0,0,0,.736.589,3.835,3.835,0,0,0,1.426.137,9.336,9.336,0,0,0,1.991-.37,14.143,14.143,0,0,0,2.182-.828,14.472,14.472,0,0,0,1.948-1.116,9.355,9.355,0,0,0,1.484-1.245,3.64,3.64,0,0,0,.812-1.216,1.082,1.082,0,0,0-.039-.952,1.3,1.3,0,0,0-.817-.532,3.221,3.221,0,0,0-1.423.027A16.554,16.554,0,0,0-355,39.66a7.111,7.111,0,0,0-1.163.44,19.254,19.254,0,0,1,3.6-1.484A17.364,17.364,0,0,1-350.129,38a9.581,9.581,0,0,1,2.6-.1,5.3,5.3,0,0,1,1.919.477,2.168,2.168,0,0,1,1.064,1.049,2.3,2.3,0,0,1,.023,1.606"
                                              transform="translate(371.291 -37.849)"></path>
                                        <path id="Path_466" data-name="Path 466"
                                              d="M-232.63,369.177a12.493,12.493,0,0,1-.636,1.444,12.274,12.274,0,0,1-.827,1.2s-.184.244-.3.395c-.337.43-.894,1.074-.894,1.074l-.748.815a21.9,21.9,0,0,1-2.622,2.3,26.807,26.807,0,0,1-3.161,2.023,27.745,27.745,0,0,1-3.541,1.623,25.657,25.657,0,0,1-3.753,1.1c-.248.055-.5.1-.741.147s-.486.08-.725.117-.476.062-.708.09-.462.045-.69.06l-1.385.062-.306,0-1.482-.045-1.506-.134s-.26-.032-.423-.055c-.626-.089-.979-.149-1.6-.276s-1.6-.39-1.6-.39l-.807-.271-.22-.107a10.025,10.025,0,0,0,2.05.547,17.708,17.708,0,0,0,2.375.189,22.978,22.978,0,0,0,2.657-.129,27.867,27.867,0,0,0,2.912-.467,34.275,34.275,0,0,0,6.76-2.22,35.388,35.388,0,0,0,6.043-3.423,27.716,27.716,0,0,0,4.7-4.154,13.542,13.542,0,0,0,1.186-1.531c.016-.018-.005.02-.005.02"
                                              transform="translate(278.203 -332.494)"></path>
                                        <path id="Path_467" data-name="Path 467"
                                              d="M-378.946,42.387l1.62.609,1.6.736.562.291a5.13,5.13,0,0,1,1.163.825,3.838,3.838,0,0,1,.8,1.059,3.814,3.814,0,0,1,.38,1.305,5.03,5.03,0,0,1-.082,1.541,11.126,11.126,0,0,1-2.449,4.629,28.482,28.482,0,0,1-5.226,4.81,45.16,45.16,0,0,1-7.227,4.286,45.924,45.924,0,0,1-8.219,3.015,30.376,30.376,0,0,1-6.735,1.017,13.619,13.619,0,0,1-4.88-.654,5.124,5.124,0,0,1-2.734-2.041,4.2,4.2,0,0,1-.433-3.137l.114-.517.461-1.715.576-1.661.6-1.4.544-1.144-.091.231c.02-.042-.021.047-.021.047a4.126,4.126,0,0,0-.355,2.963,3.291,3.291,0,0,0,1.941,1.959,9.716,9.716,0,0,0,4.063.656,24.726,24.726,0,0,0,5.873-.917,38.761,38.761,0,0,0,6.539-2.434,38.793,38.793,0,0,0,5.781-3.381,24.367,24.367,0,0,0,4.251-3.779,9.32,9.32,0,0,0,2.093-3.637,3.713,3.713,0,0,0,.076-1.611,2.756,2.756,0,0,0-.6-1.26,3.7,3.7,0,0,0-1.182-.912,6.235,6.235,0,0,0-1.089-.418l-.6-.159a5.093,5.093,0,0,1,.6.114s.581.142.95.249c.529.154,1.344.435,1.344.435"
                                              transform="translate(411.272 -41.175)"></path>
                                        <path id="Path_468" data-name="Path 468"
                                              d="M-370.64,104.148l1.2,1.511,1.091,1.623.49.828a5.436,5.436,0,0,1,.372.81,5.572,5.572,0,0,1,.234.885,5.913,5.913,0,0,1,.094.967,7.3,7.3,0,0,1-.058,1.042,12.979,12.979,0,0,1-2.638,5.859,31.238,31.238,0,0,1-6.1,6.026,48.683,48.683,0,0,1-8.647,5.265,49.469,49.469,0,0,1-9.944,3.535,34.638,34.638,0,0,1-6.676.949,19.979,19.979,0,0,1-5.292-.445,10.022,10.022,0,0,1-3.706-1.621,5.741,5.741,0,0,1-2-2.585l-.382-1.1-.522-1.882-.369-1.9-.073-.532-.107-1.071c-.014-.1,0,0,0,0a4.638,4.638,0,0,0,1.263,2.65,7.45,7.45,0,0,0,3.365,1.9,17.172,17.172,0,0,0,5.29.572,33.594,33.594,0,0,0,6.967-1.021,49.087,49.087,0,0,0,9.388-3.388,48.708,48.708,0,0,0,8.207-4.929,30.763,30.763,0,0,0,5.852-5.588,12.289,12.289,0,0,0,2.623-5.389,6.376,6.376,0,0,0,.071-1.36,4.961,4.961,0,0,0-.224-1.206,4.7,4.7,0,0,0-.493-1.054,4.814,4.814,0,0,0-.434-.569v-.008l1,1.049Z"
                                              transform="translate(413.711 -95.721)"></path>
                                        <path id="Path_469" data-name="Path 469"
                                              d="M-323.133,222.047a13.392,13.392,0,0,1-.05,3.945,12.179,12.179,0,0,1-2.309,5.641,27.059,27.059,0,0,1-5.443,5.665,40.881,40.881,0,0,1-7.773,4.828,41.072,41.072,0,0,1-9,3.125,31.838,31.838,0,0,1-4.544.666,22.73,22.73,0,0,1-3.948-.047,14.976,14.976,0,0,1-3.259-.689,9.625,9.625,0,0,1-2.5-1.248l-1.487-1.153-1.242-1.121s-.845-.825-1.336-1.4c-.352-.41-.861-1.086-.861-1.086h0a7.439,7.439,0,0,0,2.164,1.688,12.563,12.563,0,0,0,3.691,1.2,21.863,21.863,0,0,0,4.841.261,34.413,34.413,0,0,0,5.842-.823,46.638,46.638,0,0,0,9.8-3.438,46.049,46.049,0,0,0,8.487-5.225,29.8,29.8,0,0,0,5.949-6.056,13.013,13.013,0,0,0,2.526-5.954c.02-.2.035-.405.041-.6a4.128,4.128,0,0,0-.043-.592,7.522,7.522,0,0,1,.249.868s.158.937.216,1.543"
                                              transform="translate(372.14 -199.499)"></path>
                                    </svg>
                                </figure>

                                <h5>reddot design award</h5>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="awBox">
                                <figure data-anim>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                         width="49.128" height="49.13" viewBox="0 0 49.128 49.13">
                                        <defs>
                                            <clipPath id="clip-path">
                                                <path id="Path_340" data-name="Path 340"
                                                      d="M-157.372,336.236a1.738,1.738,0,0,1-.921-.619,1.772,1.772,0,0,1-.331-1.1,3.464,3.464,0,0,1,.336-1.382,6.309,6.309,0,0,1,.906-1.407,8.564,8.564,0,0,1,1.333-1.273,8.643,8.643,0,0,1,1.64-1,6.173,6.173,0,0,1,1.663-.517,3.5,3.5,0,0,1,1.384.032,1.868,1.868,0,0,1,.97.562,1.727,1.727,0,0,1,.4,1.064,3.238,3.238,0,0,1-.288,1.4,6.049,6.049,0,0,1-.9,1.454,8.552,8.552,0,0,1-1.38,1.335,8.605,8.605,0,0,1-1.71,1.027,6.085,6.085,0,0,1-1.708.5,3.915,3.915,0,0,1-.527.035,2.929,2.929,0,0,1-.864-.119m5.374-12.38a13.52,13.52,0,0,0-3.407,1.106,18.2,18.2,0,0,0-3.347,2.051,18.051,18.051,0,0,0-2.743,2.6,13.43,13.43,0,0,0-1.894,2.871,7.387,7.387,0,0,0-.772,2.859,3.758,3.758,0,0,0,.6,2.312,3.538,3.538,0,0,0,1.856,1.34,6.763,6.763,0,0,0,2.886.216,14.14,14.14,0,0,0,3.568-1.345,16.55,16.55,0,0,0,3.414-2.265,17.709,17.709,0,0,0,2.927-2.8,16.351,16.351,0,0,0,2.138-2.784,5.656,5.656,0,0,0,.8-2.981,3.529,3.529,0,0,0-1.045-2.158,4,4,0,0,0-2.108-1.014,6.432,6.432,0,0,0-1.367-.137,8.8,8.8,0,0,0-1.509.134"
                                                      transform="translate(164.164 -323.722)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-2">
                                                <path id="Path_341" data-name="Path 341"
                                                      d="M-400.858,68.961l.006-.1a1,1,0,0,0-.006.1m14.335-18.02a24.977,24.977,0,0,0-2.961,1.591c-3.347,2.085-5.336,3.182-7.789,6.187a14.847,14.847,0,0,0-2.25,3.294c-.339.691-.746,1.837-.746,1.837l-.169,1.285-.267,1.74-.129,1.556-.017.425c.005-.1.012-.2.022-.3.01-.137.031-.276.051-.417s.046-.288.076-.438.067-.3.107-.448a18.771,18.771,0,0,1,2.169-4.969,20.177,20.177,0,0,1,3.765-4.629,39.607,39.607,0,0,1,4.717-4.228c1.207-.888,4.151-2.742,5.89-2.978.128-.015.156-.027.114-.027a9.824,9.824,0,0,0-2.583.522"
                                                      transform="translate(400.858 -50.419)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-3">
                                                <path id="Path_382" data-name="Path 382"
                                                      d="M-411.349,73.838l-.124-.2c.04.07.081.137.124.2m34.479-29.783h0l-.039-.04a.379.379,0,0,0,.036.04m-.036-.04c-.068-.075-.137-.147-.207-.214Zm-11.82-4.517a17.372,17.372,0,0,0-3.618,1.2,34.554,34.554,0,0,0-4.55,2.376,45.77,45.77,0,0,0-6.2,4.589,47,47,0,0,0-5.232,5.369,35.189,35.189,0,0,0-3.889,5.7,17.34,17.34,0,0,0-1.754,5.558,9.035,9.035,0,0,0-.031.982,25.892,25.892,0,0,0,.624,3.806,15.027,15.027,0,0,0,.433,1.588c.245.714.763,1.775.763,1.775l.257.452.449.733a5.2,5.2,0,0,1-.405-.867,6.275,6.275,0,0,1-.316-1.233,8.09,8.09,0,0,1-.1-1.39,11.261,11.261,0,0,1,.125-1.529,19.269,19.269,0,0,1,2.063-5.954,35.708,35.708,0,0,1,4.032-6.051,47.247,47.247,0,0,1,5.455-5.648,46.371,46.371,0,0,1,6.507-4.785,35.213,35.213,0,0,1,5.376-2.675,21.258,21.258,0,0,1,4.852-1.293,10.935,10.935,0,0,1,4.071.144,5.867,5.867,0,0,1,2.7,1.447l-1.027-1.064-1.349-1.2-.685-.534-1.54-.614a18.337,18.337,0,0,0-2.978-.808,18.66,18.66,0,0,0-2.559-.222,7.1,7.1,0,0,0-1.475.134"
                                                      transform="translate(414 -39.364)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-4">
                                                <path id="Path_426" data-name="Path 426"
                                                      d="M-368.21,124.633l-.568-.2c.184.075.372.142.568.2m20.616-39a21.818,21.818,0,0,0-5.72,1.489,36.4,36.4,0,0,0-6.3,3.314,47.483,47.483,0,0,0-6.745,5.337,47.032,47.032,0,0,0-5.5,6.192,34.969,34.969,0,0,0-3.869,6.545,18.785,18.785,0,0,0-1.684,6.344,10.669,10.669,0,0,0,.076,2.1,7.561,7.561,0,0,0,.481,1.8,5.925,5.925,0,0,0,.874,1.494,5.777,5.777,0,0,0,1.252,1.166l1.647,1.049,1.571.835,1.741.771.989.35a6.575,6.575,0,0,1-1.431-.791,5.856,5.856,0,0,1-1.441-1.539,6.6,6.6,0,0,1-.855-2.036,9.448,9.448,0,0,1-.239-2.5,17.017,17.017,0,0,1,1.433-5.969,31.008,31.008,0,0,1,3.515-6.147,41.981,41.981,0,0,1,5.088-5.79,41.981,41.981,0,0,1,6.3-4.944,31.484,31.484,0,0,1,6.541-3.241,22.9,22.9,0,0,1,6.616-1.256,8.324,8.324,0,0,1,4.567,1.221c1.244.825,1.484,2.215,1.894,3.9l.654-.142-2.1-5.546-.542-1.213a6.139,6.139,0,0,0-3.3-2.528,10.022,10.022,0,0,0-3.174-.4,23.191,23.191,0,0,0-2.344.124"
                                                      transform="translate(377.429 -85.511)" fill="none"></path>
                                            </clipPath>
                                            <clipPath id="clip-path-5">
                                                <path id="Path_464" data-name="Path 464"
                                                      d="M-274.1,217.774c.276-.094.553-.2.833-.3Zm.833-.3.071-.028.311-.124-.382.152M-269,188.212a30.356,30.356,0,0,0-6.4,3.065,39.272,39.272,0,0,0-6.074,4.663,38.128,38.128,0,0,0-4.844,5.464,27.611,27.611,0,0,0-3.257,5.77,14.612,14.612,0,0,0-1.176,5.551,7.806,7.806,0,0,0,.551,2.884,5.739,5.739,0,0,0,1.514,2.13A5.428,5.428,0,0,0-286.3,219a17.08,17.08,0,0,0,3.2.438l.261-.005,1.841-.117,1.8-.229.894-.159,1.644-.365,1.588-.455.435-.142.542-.2a15.355,15.355,0,0,1-3.452.77,8.694,8.694,0,0,1-3.709-.363,4.988,4.988,0,0,1-2.464-1.8,5.65,5.65,0,0,1-.983-3.082,11.2,11.2,0,0,1,.818-4.455,21.016,21.016,0,0,1,2.538-4.651,29.32,29.32,0,0,1,3.872-4.4,30.216,30.216,0,0,1,4.892-3.731,23.368,23.368,0,0,1,5.159-2.386,13.385,13.385,0,0,1,4.574-.666,6.51,6.51,0,0,1,3.526,1.116,4.911,4.911,0,0,1,1.912,3.006c.145-.4,1.02-.706,1.135-1.116s.216-.83.3-1.248.336-2.7.357-3.127a4.651,4.651,0,0,0-.2-.721,5.724,5.724,0,0,0-2.293-2.322,11.07,11.07,0,0,0-5.188-1.35c-.082,0-.165,0-.247,0a17.66,17.66,0,0,0-5.458.972"
                                                      transform="translate(290.759 -187.24)" fill="none"></path>
                                            </clipPath>
                                        </defs>
                                        <g id="Group_1020" data-name="Group 1020" transform="translate(27.644 31.631)">
                                            <g id="Group_1019" data-name="Group 1019" transform="translate(0 0)"
                                               clip-path="url(#clip-path)">
                                                <path id="Path_322" data-name="Path 322"
                                                      d="M-188.164,174.075a18.6,18.6,0,0,0-18.592-18.6,18.607,18.607,0,0,0-18.6,18.6,18.608,18.608,0,0,0,18.6,18.607,18.605,18.605,0,0,0,18.592-18.607m-1.156.01h0a17.543,17.543,0,0,1-17.53,17.543,17.542,17.542,0,0,1-17.531-17.543,17.541,17.541,0,0,1,17.531-17.54,17.543,17.543,0,0,1,17.53,17.54"
                                                      transform="translate(218.581 -174.088)" fill="#da091d"></path>
                                                <path id="Path_323" data-name="Path 323"
                                                      d="M-181.786,182.628a17.541,17.541,0,0,0-17.53-17.54,17.541,17.541,0,0,0-17.532,17.54,17.542,17.542,0,0,0,17.532,17.543,17.541,17.541,0,0,0,17.53-17.543m-1.033.01h0a16.482,16.482,0,0,1-16.47,16.481,16.484,16.484,0,0,1-16.47-16.481,16.482,16.482,0,0,1,16.47-16.476,16.48,16.48,0,0,1,16.47,16.476"
                                                      transform="translate(211.019 -182.641)" fill="#dc2121"></path>
                                                <path id="Path_324" data-name="Path 324"
                                                      d="M-174.307,191.18A16.477,16.477,0,0,0-190.773,174.7a16.477,16.477,0,0,0-16.47,16.476,16.48,16.48,0,0,0,16.47,16.481,16.479,16.479,0,0,0,16.466-16.481m-1.033.01h0a15.417,15.417,0,0,1-15.406,15.417,15.418,15.418,0,0,1-15.407-15.417,15.417,15.417,0,0,1,15.407-15.415A15.417,15.417,0,0,1-175.34,191.19"
                                                      transform="translate(202.476 -191.193)" fill="#de3027"></path>
                                                <path id="Path_325" data-name="Path 325"
                                                      d="M-165.737,199.71a15.417,15.417,0,0,0-15.406-15.415A15.419,15.419,0,0,0-196.55,199.71a15.419,15.419,0,0,0,15.408,15.417,15.417,15.417,0,0,0,15.406-15.417m-1.156.01h0a14.355,14.355,0,0,1-14.345,14.353A14.353,14.353,0,0,1-195.58,199.72a14.353,14.353,0,0,1,14.344-14.351,14.355,14.355,0,0,1,14.345,14.351"
                                                      transform="translate(192.967 -199.722)" fill="#e03e2f"></path>
                                                <path id="Path_326" data-name="Path 326"
                                                      d="M-159.357,208.262A14.354,14.354,0,0,0-173.7,193.911a14.354,14.354,0,0,0-14.343,14.351A14.355,14.355,0,0,0-173.7,222.618a14.355,14.355,0,0,0,14.345-14.356m-1.034.01h0a13.292,13.292,0,0,1-13.282,13.292,13.291,13.291,0,0,1-13.281-13.292,13.289,13.289,0,0,1,13.281-13.289,13.289,13.289,0,0,1,13.282,13.289"
                                                      transform="translate(185.403 -208.274)" fill="#e34b37"></path>
                                                <path id="Path_327" data-name="Path 327"
                                                      d="M-151.879,216.815a13.289,13.289,0,0,0-13.281-13.287,13.291,13.291,0,0,0-13.282,13.287A13.291,13.291,0,0,0-165.16,230.1a13.29,13.29,0,0,0,13.281-13.289m-1.034.01h0a12.227,12.227,0,0,1-12.218,12.228,12.227,12.227,0,0,1-12.219-12.228A12.227,12.227,0,0,1-165.131,204.6a12.226,12.226,0,0,1,12.218,12.225"
                                                      transform="translate(176.862 -216.827)" fill="#e55741"></path>
                                                <path id="Path_328" data-name="Path 328"
                                                      d="M-143.3,225.346a12.227,12.227,0,0,0-12.218-12.225,12.229,12.229,0,0,0-12.219,12.225,12.227,12.227,0,0,0,12.219,12.225A12.225,12.225,0,0,0-143.3,225.346m-1.156.01h0A11.163,11.163,0,0,1-155.61,236.52a11.163,11.163,0,0,1-11.154-11.164,11.161,11.161,0,0,1,11.154-11.161,11.161,11.161,0,0,1,11.156,11.161"
                                                      transform="translate(167.34 -225.359)" fill="#e7634a"></path>
                                                <path id="Path_329" data-name="Path 329"
                                                      d="M-136.921,233.9a11.162,11.162,0,0,0-11.154-11.162A11.163,11.163,0,0,0-159.233,233.9a11.164,11.164,0,0,0,11.158,11.164A11.163,11.163,0,0,0-136.921,233.9m-1.034.01h0a10.1,10.1,0,0,1-10.093,10.1,10.1,10.1,0,0,1-10.1-10.1,10.1,10.1,0,0,1,10.1-10.1,10.1,10.1,0,0,1,10.093,10.1"
                                                      transform="translate(159.779 -233.909)" fill="#ea6e55"></path>
                                                <path id="Path_330" data-name="Path 330"
                                                      d="M-129.441,242.449a10.1,10.1,0,0,0-10.093-10.1,10.1,10.1,0,0,0-10.094,10.1,10.1,10.1,0,0,0,10.094,10.1,10.1,10.1,0,0,0,10.093-10.1m-1.033.01h0a9.038,9.038,0,0,1-9.032,9.039,9.039,9.039,0,0,1-9.031-9.039,9.037,9.037,0,0,1,9.031-9.036,9.036,9.036,0,0,1,9.032,9.036"
                                                      transform="translate(151.236 -242.461)" fill="#ec7a5f"></path>
                                                <path id="Path_331" data-name="Path 331"
                                                      d="M-120.871,250.981a9.038,9.038,0,0,0-9.032-9.036,9.039,9.039,0,0,0-9.031,9.036,9.037,9.037,0,0,0,9.031,9.036,9.036,9.036,0,0,0,9.032-9.036m-1.156.01h0A7.974,7.974,0,0,1-130,258.963a7.971,7.971,0,0,1-7.97-7.972,7.973,7.973,0,0,1,7.97-7.972,7.975,7.975,0,0,1,7.968,7.972"
                                                      transform="translate(141.727 -250.993)" fill="#ef876c"></path>
                                                <path id="Path_332" data-name="Path 332"
                                                      d="M-114.493,259.533a7.972,7.972,0,0,0-7.967-7.972,7.974,7.974,0,0,0-7.97,7.972,7.975,7.975,0,0,0,7.97,7.975,7.973,7.973,0,0,0,7.967-7.975m-1.033.01h0a6.909,6.909,0,0,1-6.906,6.911,6.91,6.91,0,0,1-6.907-6.911,6.912,6.912,0,0,1,6.907-6.91,6.911,6.911,0,0,1,6.906,6.91"
                                                      transform="translate(134.163 -259.546)" fill="#f19379"></path>
                                                <path id="Path_333" data-name="Path 333"
                                                      d="M-107.014,268.083a6.91,6.91,0,0,0-6.907-6.908,6.91,6.91,0,0,0-6.906,6.908,6.911,6.911,0,0,0,6.906,6.911,6.91,6.91,0,0,0,6.907-6.911m-1.034.01h0a5.847,5.847,0,0,1-5.844,5.847,5.847,5.847,0,0,1-5.844-5.847,5.848,5.848,0,0,1,5.844-5.847,5.849,5.849,0,0,1,5.844,5.847"
                                                      transform="translate(125.622 -268.096)" fill="#f3a187"></path>
                                                <path id="Path_334" data-name="Path 334"
                                                      d="M-98.423,276.615a5.847,5.847,0,0,0-5.842-5.847,5.847,5.847,0,0,0-5.844,5.847,5.845,5.845,0,0,0,5.844,5.847,5.845,5.845,0,0,0,5.842-5.847m-1.156.01h0a4.784,4.784,0,0,1-4.78,4.783,4.785,4.785,0,0,1-4.783-4.783,4.785,4.785,0,0,1,4.783-4.783,4.784,4.784,0,0,1,4.78,4.783"
                                                      transform="translate(116.09 -276.627)" fill="#f5ae98"></path>
                                                <path id="Path_335" data-name="Path 335"
                                                      d="M-92.056,285.168a4.783,4.783,0,0,0-4.78-4.783,4.784,4.784,0,0,0-4.782,4.783,4.784,4.784,0,0,0,4.782,4.785,4.784,4.784,0,0,0,4.78-4.785m-1.034.01h0a3.719,3.719,0,0,1-3.719,3.721,3.721,3.721,0,0,1-3.719-3.721,3.723,3.723,0,0,1,3.719-3.721,3.721,3.721,0,0,1,3.719,3.721"
                                                      transform="translate(108.539 -285.18)" fill="#f8beaa"></path>
                                                <path id="Path_336" data-name="Path 336"
                                                      d="M-84.586,293.72A3.719,3.719,0,0,0-88.305,290a3.72,3.72,0,0,0-3.719,3.719,3.721,3.721,0,0,0,3.719,3.721,3.719,3.719,0,0,0,3.719-3.721m-1.033.01h0a2.659,2.659,0,0,1-2.657,2.658,2.657,2.657,0,0,1-2.655-2.658,2.656,2.656,0,0,1,2.655-2.657,2.659,2.659,0,0,1,2.657,2.657"
                                                      transform="translate(100.006 -293.732)" fill="#facdbb"></path>
                                                <path id="Path_337" data-name="Path 337"
                                                      d="M-77.107,302.251a2.656,2.656,0,0,0-2.655-2.657,2.658,2.658,0,0,0-2.657,2.657,2.657,2.657,0,0,0,2.657,2.657,2.655,2.655,0,0,0,2.655-2.657m-1.034.01h0a1.593,1.593,0,0,1-1.593,1.6,1.594,1.594,0,0,1-1.594-1.6,1.594,1.594,0,0,1,1.594-1.594,1.593,1.593,0,0,1,1.593,1.594"
                                                      transform="translate(91.464 -302.264)" fill="#fcdbcd"></path>
                                                <path id="Path_338" data-name="Path 338"
                                                      d="M-69.63,311.9a1.593,1.593,0,0,0-1.593-1.594,1.6,1.6,0,0,0-1.594,1.594,1.6,1.6,0,0,0,1.594,1.6,1.593,1.593,0,0,0,1.593-1.6m-1.033-.112h0a.533.533,0,0,1-.532.532.531.531,0,0,1-.53-.532.532.532,0,0,1,.53-.532.535.535,0,0,1,.532.532"
                                                      transform="translate(82.924 -311.794)" fill="#feece3"></path>
                                                <path id="Path_339" data-name="Path 339"
                                                      d="M-62.139,319.334a.534.534,0,0,0-.532-.532.532.532,0,0,0-.53.532.533.533,0,0,0,.53.535.534.534,0,0,0,.532-.535"
                                                      transform="translate(74.372 -319.346)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <g id="Group_1022" data-name="Group 1022" transform="translate(1.455 1.391)">
                                            <g id="Group_1021" data-name="Group 1021" transform="translate(0 0)"
                                               clip-path="url(#clip-path-2)">
                                                <rect id="Rectangle_231" data-name="Rectangle 231" width="16.862"
                                                      height="0.977" transform="translate(0.002 0.057)"
                                                      fill="#da091d"></rect>
                                                <rect id="Rectangle_232" data-name="Rectangle 232" width="16.862"
                                                      height="0.977" transform="translate(0.002 1.086)"
                                                      fill="#dc2121"></rect>
                                                <rect id="Rectangle_233" data-name="Rectangle 233" width="16.862"
                                                      height="0.979" transform="translate(0.002 2.116)"
                                                      fill="#de3027"></rect>
                                                <rect id="Rectangle_234" data-name="Rectangle 234" width="16.862"
                                                      height="0.977" transform="translate(0.002 3.145)"
                                                      fill="#e03e2f"></rect>
                                                <rect id="Rectangle_235" data-name="Rectangle 235" width="16.862"
                                                      height="0.977" transform="translate(0.002 4.176)"
                                                      fill="#e34b37"></rect>
                                                <rect id="Rectangle_236" data-name="Rectangle 236" width="16.862"
                                                      height="0.977" transform="translate(0.002 5.205)"
                                                      fill="#e55741"></rect>
                                                <rect id="Rectangle_237" data-name="Rectangle 237" width="16.862"
                                                      height="0.98" transform="translate(0.002 6.234)"
                                                      fill="#e7634a"></rect>
                                                <rect id="Rectangle_238" data-name="Rectangle 238" width="16.862"
                                                      height="0.977" transform="translate(0.002 7.264)"
                                                      fill="#ea6e55"></rect>
                                                <rect id="Rectangle_239" data-name="Rectangle 239" width="16.862"
                                                      height="0.977" transform="translate(0.002 8.295)"
                                                      fill="#ec7a5f"></rect>
                                                <rect id="Rectangle_240" data-name="Rectangle 240" width="16.862"
                                                      height="0.98" transform="translate(0.002 9.322)"
                                                      fill="#ef876c"></rect>
                                                <rect id="Rectangle_241" data-name="Rectangle 241" width="16.862"
                                                      height="0.977" transform="translate(0.002 10.354)"
                                                      fill="#f19379"></rect>
                                                <rect id="Rectangle_242" data-name="Rectangle 242" width="16.862"
                                                      height="0.977" transform="translate(0.002 11.383)"
                                                      fill="#f3a187"></rect>
                                                <rect id="Rectangle_243" data-name="Rectangle 243" width="16.862"
                                                      height="0.977" transform="translate(0.002 12.414)"
                                                      fill="#f5ae98"></rect>
                                                <rect id="Rectangle_244" data-name="Rectangle 244" width="16.862"
                                                      height="0.977" transform="translate(0.002 13.444)"
                                                      fill="#f8beaa"></rect>
                                                <rect id="Rectangle_245" data-name="Rectangle 245" width="16.862"
                                                      height="0.977" transform="translate(0.002 14.473)"
                                                      fill="#facdbb"></rect>
                                                <rect id="Rectangle_246" data-name="Rectangle 246" width="16.862"
                                                      height="0.977" transform="translate(0.002 15.504)"
                                                      fill="#fcdbcd"></rect>
                                                <rect id="Rectangle_247" data-name="Rectangle 247" width="16.862"
                                                      height="0.979" transform="translate(0.002 16.531)"
                                                      fill="#feece3"></rect>
                                                <rect id="Rectangle_248" data-name="Rectangle 248" width="16.862"
                                                      height="0.977" transform="translate(0.002 17.563)"
                                                      fill="#fff"></rect>
                                            </g>
                                        </g>
                                        <g id="Group_1024" data-name="Group 1024" transform="translate(0 0.168)">
                                            <g id="Group_1023" data-name="Group 1023" transform="translate(0 0)"
                                               clip-path="url(#clip-path-3)">
                                                <path id="Path_342" data-name="Path 342"
                                                      d="M-475.391-293.114a41.7,41.7,0,0,0-41.674-41.7,41.7,41.7,0,0,0-41.675,41.7,41.7,41.7,0,0,0,41.675,41.7,41.7,41.7,0,0,0,41.674-41.7m-1.142-.018h0a40.647,40.647,0,0,1-40.622,40.644,40.647,40.647,0,0,1-40.619-40.644,40.649,40.649,0,0,1,40.619-40.646,40.649,40.649,0,0,1,40.622,40.646"
                                                      transform="translate(542.725 293.41)" fill="#da091d"></path>
                                                <path id="Path_343" data-name="Path 343"
                                                      d="M-469.076-284.641a40.644,40.644,0,0,0-40.619-40.644,40.647,40.647,0,0,0-40.622,40.644A40.648,40.648,0,0,0-509.694-244a40.645,40.645,0,0,0,40.619-40.644m-1.02-.018h0a39.589,39.589,0,0,1-39.565,39.587,39.589,39.589,0,0,1-39.565-39.587,39.591,39.591,0,0,1,39.565-39.59,39.591,39.591,0,0,1,39.565,39.59"
                                                      transform="translate(535.233 284.937)" fill="#db181f"></path>
                                                <path id="Path_344" data-name="Path 344"
                                                      d="M-461.627-277.27a39.593,39.593,0,0,0-39.565-39.59,39.591,39.591,0,0,0-39.565,39.59,39.589,39.589,0,0,0,39.565,39.587,39.591,39.591,0,0,0,39.565-39.587m-1.023.1h0a38.535,38.535,0,0,1-38.511,38.531,38.534,38.534,0,0,1-38.508-38.531A38.535,38.535,0,0,1-501.161-315.7a38.536,38.536,0,0,1,38.511,38.533"
                                                      transform="translate(526.732 277.445)" fill="#dc1f21"></path>
                                                <path id="Path_345" data-name="Path 345"
                                                      d="M-453.1-267.659a38.535,38.535,0,0,0-38.512-38.531,38.532,38.532,0,0,0-38.508,38.531,38.534,38.534,0,0,0,38.508,38.531A38.536,38.536,0,0,0-453.1-267.659m-1.146-.018h0A37.477,37.477,0,0,1-491.7-230.2a37.477,37.477,0,0,1-37.455-37.474A37.479,37.479,0,0,1-491.7-305.153a37.479,37.479,0,0,1,37.454,37.477"
                                                      transform="translate(517.271 267.955)" fill="#dd2623"></path>
                                                <path id="Path_346" data-name="Path 346"
                                                      d="M-446.794-259.208a37.482,37.482,0,0,0-37.455-37.48,37.482,37.482,0,0,0-37.457,37.48,37.478,37.478,0,0,0,37.457,37.474,37.478,37.478,0,0,0,37.455-37.474m-1.024-.018h0a36.423,36.423,0,0,1-36.4,36.42,36.423,36.423,0,0,1-36.4-36.42,36.423,36.423,0,0,1,36.4-36.423,36.424,36.424,0,0,1,36.4,36.423"
                                                      transform="translate(509.789 259.504)" fill="#de2c26"></path>
                                                <path id="Path_347" data-name="Path 347"
                                                      d="M-439.366-251.816a36.427,36.427,0,0,0-36.4-36.423,36.425,36.425,0,0,0-36.4,36.423,36.425,36.425,0,0,0,36.4,36.42,36.426,36.426,0,0,0,36.4-36.42m-1.024.1h0a35.367,35.367,0,0,1-35.344,35.364,35.365,35.365,0,0,1-35.344-35.364,35.367,35.367,0,0,1,35.344-35.366,35.369,35.369,0,0,1,35.344,35.366"
                                                      transform="translate(501.306 251.99)" fill="#df3429"></path>
                                                <path id="Path_348" data-name="Path 348"
                                                      d="M-431.943-242.224a35.368,35.368,0,0,0-35.344-35.366,35.37,35.37,0,0,0-35.345,35.366,35.368,35.368,0,0,0,35.345,35.364,35.366,35.366,0,0,0,35.344-35.364m-1.02-.018h0a34.312,34.312,0,0,1-34.291,34.308,34.311,34.311,0,0,1-34.287-34.308,34.311,34.311,0,0,1,34.287-34.31,34.313,34.313,0,0,1,34.291,34.31"
                                                      transform="translate(492.825 242.52)" fill="#e0392c"></path>
                                                <path id="Path_349" data-name="Path 349"
                                                      d="M-424.515-233.733a34.31,34.31,0,0,0-34.287-34.31,34.313,34.313,0,0,0-34.291,34.31A34.312,34.312,0,0,0-458.8-199.426a34.31,34.31,0,0,0,34.287-34.307m-1.021-.018h0A33.256,33.256,0,0,1-458.77-200.5,33.255,33.255,0,0,1-492-233.751a33.257,33.257,0,0,1,33.234-33.256,33.258,33.258,0,0,1,33.235,33.256"
                                                      transform="translate(484.342 234.029)" fill="#e03f2f"></path>
                                                <path id="Path_350" data-name="Path 350"
                                                      d="M-417.087-225.26a33.258,33.258,0,0,0-33.234-33.256,33.258,33.258,0,0,0-33.236,33.256,33.256,33.256,0,0,0,33.236,33.253,33.256,33.256,0,0,0,33.234-33.253m-1.023-.018h0a32.2,32.2,0,0,1-32.181,32.2,32.2,32.2,0,0,1-32.178-32.2,32.2,32.2,0,0,1,32.178-32.2,32.2,32.2,0,0,1,32.181,32.2"
                                                      transform="translate(475.862 225.556)" fill="#e14433"></path>
                                                <path id="Path_351" data-name="Path 351"
                                                      d="M-409.66-216.769a32.2,32.2,0,0,0-32.181-32.2,32.2,32.2,0,0,0-32.179,32.2,32.2,32.2,0,0,0,32.179,32.2,32.2,32.2,0,0,0,32.181-32.2m-1.023-.018h0a31.144,31.144,0,0,1-31.124,31.14,31.143,31.143,0,0,1-31.124-31.14,31.145,31.145,0,0,1,31.124-31.143,31.146,31.146,0,0,1,31.124,31.143"
                                                      transform="translate(467.379 217.066)" fill="#e34a36"></path>
                                                <path id="Path_352" data-name="Path 352"
                                                      d="M-401.131-208.276a31.147,31.147,0,0,0-31.127-31.143,31.146,31.146,0,0,0-31.124,31.143,31.144,31.144,0,0,0,31.124,31.14,31.145,31.145,0,0,0,31.127-31.14m-1.146-.018h0a30.09,30.09,0,0,1-30.07,30.087,30.09,30.09,0,0,1-30.068-30.087,30.09,30.09,0,0,1,30.068-30.089,30.091,30.091,0,0,1,30.07,30.089"
                                                      transform="translate(457.918 208.572)" fill="#e4503a"></path>
                                                <path id="Path_353" data-name="Path 353"
                                                      d="M-394.806-199.785a30.09,30.09,0,0,0-30.07-30.087,30.089,30.089,0,0,0-30.07,30.087,30.09,30.09,0,0,0,30.07,30.086,30.092,30.092,0,0,0,30.07-30.086m-1.024-.018h0a29.031,29.031,0,0,1-29.014,29.03,29.031,29.031,0,0,1-29.014-29.03,29.035,29.035,0,0,1,29.014-29.032A29.034,29.034,0,0,1-395.83-199.8"
                                                      transform="translate(450.415 200.082)" fill="#e5553e"></path>
                                                <path id="Path_354" data-name="Path 354"
                                                      d="M-387.38-191.313A29.036,29.036,0,0,0-416.4-220.345a29.036,29.036,0,0,0-29.014,29.032A29.036,29.036,0,0,0-416.4-162.28a29.036,29.036,0,0,0,29.016-29.032m-1.023-.018h0a27.978,27.978,0,0,1-27.961,27.973,27.976,27.976,0,0,1-27.96-27.973,27.978,27.978,0,0,1,27.96-27.976A27.98,27.98,0,0,1-388.4-191.33"
                                                      transform="translate(441.935 191.609)" fill="#e55a42"></path>
                                                <path id="Path_355" data-name="Path 355"
                                                      d="M-378.853-182.822A27.975,27.975,0,0,0-406.811-210.8a27.977,27.977,0,0,0-27.959,27.976,27.98,27.98,0,0,0,27.959,27.976,27.978,27.978,0,0,0,27.957-27.976M-380-182.84h0a26.919,26.919,0,0,1-26.9,26.917,26.917,26.917,0,0,1-26.9-26.917,26.921,26.921,0,0,1,26.9-26.919A26.923,26.923,0,0,1-380-182.84"
                                                      transform="translate(432.472 183.118)" fill="#e65e46"></path>
                                                <path id="Path_356" data-name="Path 356"
                                                      d="M-372.528-174.329a26.92,26.92,0,0,0-26.9-26.919,26.92,26.92,0,0,0-26.9,26.919,26.92,26.92,0,0,0,26.9,26.919,26.92,26.92,0,0,0,26.9-26.919m-1.02-.018h0A25.867,25.867,0,0,1-399.4-148.481a25.87,25.87,0,0,1-25.851-25.866A25.868,25.868,0,0,1-399.4-200.211a25.865,25.865,0,0,1,25.849,25.865"
                                                      transform="translate(424.969 174.625)" fill="#e8644b"></path>
                                                <path id="Path_357" data-name="Path 357"
                                                      d="M-365.11-165.858a25.867,25.867,0,0,0-25.849-25.866,25.866,25.866,0,0,0-25.851,25.866,25.867,25.867,0,0,0,25.851,25.865,25.868,25.868,0,0,0,25.849-25.865m-1.023-.018h0a24.81,24.81,0,0,1-24.794,24.809,24.809,24.809,0,0,1-24.793-24.809,24.809,24.809,0,0,1,24.793-24.809,24.81,24.81,0,0,1,24.794,24.809"
                                                      transform="translate(416.499 166.155)" fill="#e96950"></path>
                                                <path id="Path_358" data-name="Path 358"
                                                      d="M-356.571-157.365a24.812,24.812,0,0,0-24.8-24.809,24.81,24.81,0,0,0-24.792,24.809,24.81,24.81,0,0,0,24.792,24.809,24.812,24.812,0,0,0,24.8-24.809m-1.146-.018h0a23.756,23.756,0,0,1-23.74,23.752A23.755,23.755,0,0,1-405.2-157.383a23.753,23.753,0,0,1,23.739-23.752,23.754,23.754,0,0,1,23.74,23.752"
                                                      transform="translate(407.027 157.661)" fill="#ea6e55"></path>
                                                <path id="Path_359" data-name="Path 359"
                                                      d="M-350.257-148.874A23.753,23.753,0,0,0-374-172.627a23.754,23.754,0,0,0-23.74,23.753A23.756,23.756,0,0,0-374-125.122a23.755,23.755,0,0,0,23.739-23.752m-1.023-.018h0a22.7,22.7,0,0,1-22.683,22.7,22.7,22.7,0,0,1-22.685-22.7,22.7,22.7,0,0,1,22.685-22.7,22.7,22.7,0,0,1,22.683,22.7"
                                                      transform="translate(399.536 149.171)" fill="#ea7258"></path>
                                                <path id="Path_360" data-name="Path 360"
                                                      d="M-342.829-140.4a22.7,22.7,0,0,0-22.686-22.7A22.7,22.7,0,0,0-388.2-140.4a22.7,22.7,0,0,0,22.683,22.7,22.7,22.7,0,0,0,22.686-22.7m-1.024-.018h0a21.645,21.645,0,0,1-21.628,21.642,21.645,21.645,0,0,1-21.63-21.642,21.643,21.643,0,0,1,21.63-21.642,21.643,21.643,0,0,1,21.628,21.642"
                                                      transform="translate(391.053 140.698)" fill="#eb785e"></path>
                                                <path id="Path_361" data-name="Path 361"
                                                      d="M-334.3-131.911a21.642,21.642,0,0,0-21.627-21.642,21.642,21.642,0,0,0-21.629,21.642,21.644,21.644,0,0,0,21.629,21.642A21.643,21.643,0,0,0-334.3-131.911m-1.142-.018h0a20.587,20.587,0,0,1-20.573,20.585,20.587,20.587,0,0,1-20.575-20.585,20.587,20.587,0,0,1,20.575-20.585,20.587,20.587,0,0,1,20.573,20.585"
                                                      transform="translate(381.592 132.207)" fill="#ed7e62"></path>
                                                <path id="Path_362" data-name="Path 362"
                                                      d="M-327.976-123.42a20.588,20.588,0,0,0-20.575-20.586,20.588,20.588,0,0,0-20.573,20.586,20.588,20.588,0,0,0,20.573,20.585,20.588,20.588,0,0,0,20.575-20.585m-1.023-.018h0a19.532,19.532,0,0,1-19.519,19.529,19.531,19.531,0,0,1-19.519-19.529,19.529,19.529,0,0,1,19.519-19.529A19.53,19.53,0,0,1-329-123.438"
                                                      transform="translate(374.089 123.716)" fill="#ee8469"></path>
                                                <path id="Path_363" data-name="Path 363"
                                                      d="M-320.552-114.927a19.529,19.529,0,0,0-19.516-19.529,19.53,19.53,0,0,0-19.519,19.529A19.532,19.532,0,0,0-340.069-95.4a19.531,19.531,0,0,0,19.516-19.529m-1.02-.018h0A18.478,18.478,0,0,1-340.038-96.47,18.477,18.477,0,0,1-358.5-114.945a18.48,18.48,0,0,1,18.462-18.477,18.48,18.48,0,0,1,18.465,18.477"
                                                      transform="translate(365.609 115.223)" fill="#ef896e"></path>
                                                <path id="Path_364" data-name="Path 364"
                                                      d="M-312.022-106.457a18.478,18.478,0,0,0-18.465-18.475,18.476,18.476,0,0,0-18.462,18.475,18.474,18.474,0,0,0,18.462,18.472,18.476,18.476,0,0,0,18.465-18.472m-1.145-.018h0a17.421,17.421,0,0,1-17.408,17.419,17.422,17.422,0,0,1-17.41-17.419,17.422,17.422,0,0,1,17.41-17.421,17.422,17.422,0,0,1,17.408,17.421"
                                                      transform="translate(356.148 106.753)" fill="#f08e74"></path>
                                                <path id="Path_365" data-name="Path 365"
                                                      d="M-305.7-97.964a17.42,17.42,0,0,0-17.409-17.418,17.42,17.42,0,0,0-17.409,17.418,17.421,17.421,0,0,0,17.409,17.418A17.421,17.421,0,0,0-305.7-97.964m-1.023-.018h0a16.363,16.363,0,0,1-16.352,16.362,16.364,16.364,0,0,1-16.356-16.362,16.366,16.366,0,0,1,16.356-16.364A16.365,16.365,0,0,1-306.72-97.981"
                                                      transform="translate(348.645 98.26)" fill="#f19379"></path>
                                                <path id="Path_366" data-name="Path 366"
                                                      d="M-298.27-90.594a16.368,16.368,0,0,0-16.355-16.364,16.366,16.366,0,0,0-16.352,16.364,16.363,16.363,0,0,0,16.352,16.362A16.365,16.365,0,0,0-298.27-90.594m-1.023.1h0a15.306,15.306,0,0,1-15.3,15.305,15.308,15.308,0,0,1-15.3-15.305,15.31,15.31,0,0,1,15.3-15.308,15.308,15.308,0,0,1,15.3,15.308"
                                                      transform="translate(340.162 90.768)" fill="#f29a81"></path>
                                                <path id="Path_367" data-name="Path 367"
                                                      d="M-289.741-80.98a15.306,15.306,0,0,0-15.3-15.305,15.3,15.3,0,0,0-15.3,15.305,15.306,15.306,0,0,0,15.3,15.305,15.308,15.308,0,0,0,15.3-15.305M-290.887-81h0a14.254,14.254,0,0,1-14.244,14.252A14.254,14.254,0,0,1-319.374-81a14.256,14.256,0,0,1,14.243-14.254A14.256,14.256,0,0,1-290.887-81"
                                                      transform="translate(330.701 81.276)" fill="#f3a085"></path>
                                                <path id="Path_368" data-name="Path 368"
                                                      d="M-283.415-72.53a14.255,14.255,0,0,0-14.245-14.254A14.252,14.252,0,0,0-311.9-72.53a14.252,14.252,0,0,0,14.241,14.251A14.255,14.255,0,0,0-283.415-72.53m-1.024-.018h0a13.2,13.2,0,0,1-13.187,13.2,13.2,13.2,0,0,1-13.189-13.2,13.2,13.2,0,0,1,13.189-13.2,13.2,13.2,0,0,1,13.187,13.2"
                                                      transform="translate(323.199 72.826)" fill="#f4a68d"></path>
                                                <path id="Path_369" data-name="Path 369"
                                                      d="M-276.01-65.139a13.2,13.2,0,0,0-13.189-13.2,13.2,13.2,0,0,0-13.19,13.2A13.2,13.2,0,0,0-289.2-51.944,13.2,13.2,0,0,0-276.01-65.139m-1.023.1h0A12.141,12.141,0,0,1-289.168-52.9,12.139,12.139,0,0,1-301.3-65.034a12.139,12.139,0,0,1,12.132-12.141,12.141,12.141,0,0,1,12.135,12.141"
                                                      transform="translate(314.738 65.313)" fill="#f5ac95"></path>
                                                <path id="Path_370" data-name="Path 370"
                                                      d="M-268.585-55.546a12.14,12.14,0,0,0-12.132-12.141,12.142,12.142,0,0,0-12.133,12.141,12.141,12.141,0,0,0,12.133,12.138,12.14,12.14,0,0,0,12.132-12.138m-1.021-.018h0a11.085,11.085,0,0,1-11.078,11.082,11.085,11.085,0,0,1-11.078-11.082,11.085,11.085,0,0,1,11.078-11.084,11.085,11.085,0,0,1,11.078,11.084"
                                                      transform="translate(306.256 55.842)" fill="#f6b29b"></path>
                                                <path id="Path_371" data-name="Path 371"
                                                      d="M-261.16-47.074a11.085,11.085,0,0,0-11.077-11.087,11.087,11.087,0,0,0-11.078,11.087A11.084,11.084,0,0,0-272.237-35.99,11.082,11.082,0,0,0-261.16-47.074m-1.021-.018h0A10.029,10.029,0,0,1-272.2-37.063a10.029,10.029,0,0,1-10.022-10.028A10.029,10.029,0,0,1-272.2-57.122a10.029,10.029,0,0,1,10.024,10.03"
                                                      transform="translate(297.775 47.37)" fill="#f7b9a4"></path>
                                                <path id="Path_372" data-name="Path 372"
                                                      d="M-253.72-39.683a10.029,10.029,0,0,0-10.024-10.03,10.028,10.028,0,0,0-10.022,10.03,10.027,10.027,0,0,0,10.022,10.028A10.029,10.029,0,0,0-253.72-39.683m-1.023.1h0a8.973,8.973,0,0,1-8.968,8.971,8.972,8.972,0,0,1-8.968-8.971,8.973,8.973,0,0,1,8.968-8.974,8.973,8.973,0,0,1,8.968,8.974"
                                                      transform="translate(289.283 39.857)" fill="#f8c0ac"></path>
                                                <path id="Path_373" data-name="Path 373"
                                                      d="M-246.313-30.09a8.974,8.974,0,0,0-8.967-8.974,8.976,8.976,0,0,0-8.97,8.974,8.974,8.974,0,0,0,8.97,8.971,8.972,8.972,0,0,0,8.967-8.971m-1.023-.018h0a7.917,7.917,0,0,1-7.914,7.915,7.915,7.915,0,0,1-7.911-7.915,7.915,7.915,0,0,1,7.911-7.917,7.917,7.917,0,0,1,7.914,7.917"
                                                      transform="translate(280.82 30.386)" fill="#fac7b3"></path>
                                                <path id="Path_374" data-name="Path 374"
                                                      d="M-237.784-21.619a7.921,7.921,0,0,0-7.914-7.92,7.922,7.922,0,0,0-7.915,7.92A7.917,7.917,0,0,0-245.7-13.7a7.917,7.917,0,0,0,7.914-7.915m-1.146-.018h0a6.863,6.863,0,0,1-6.857,6.861,6.863,6.863,0,0,1-6.857-6.861,6.863,6.863,0,0,1,6.857-6.864,6.863,6.863,0,0,1,6.857,6.864"
                                                      transform="translate(271.359 21.915)" fill="#facdba"></path>
                                                <path id="Path_375" data-name="Path 375"
                                                      d="M-231.459-13.127a6.864,6.864,0,0,0-6.86-6.864,6.863,6.863,0,0,0-6.858,6.864,6.863,6.863,0,0,0,6.858,6.863,6.864,6.864,0,0,0,6.86-6.863m-1.023-.018h0a5.806,5.806,0,0,1-5.8,5.8,5.8,5.8,0,0,1-5.8-5.8,5.805,5.805,0,0,1,5.8-5.807,5.806,5.806,0,0,1,5.8,5.807"
                                                      transform="translate(263.857 13.423)" fill="#fbd3c3"></path>
                                                <path id="Path_376" data-name="Path 376"
                                                      d="M-224.033-4.636a5.809,5.809,0,0,0-5.8-5.807,5.808,5.808,0,0,0-5.8,5.807,5.808,5.808,0,0,0,5.8,5.807,5.809,5.809,0,0,0,5.8-5.807m-1.023-.018h0A4.751,4.751,0,0,1-229.8.095a4.749,4.749,0,0,1-4.747-4.748A4.75,4.75,0,0,1-229.8-9.4a4.751,4.751,0,0,1,4.748,4.75"
                                                      transform="translate(255.376 4.932)" fill="#fcd9ca"></path>
                                                <path id="Path_377" data-name="Path 377"
                                                      d="M-215.5,3.856a4.751,4.751,0,0,0-4.748-4.751,4.751,4.751,0,0,0-4.747,4.751,4.751,4.751,0,0,0,4.747,4.75,4.751,4.751,0,0,0,4.748-4.75m-1.143-.018h0a3.7,3.7,0,0,1-3.694,3.7,3.7,3.7,0,0,1-3.69-3.7,3.693,3.693,0,0,1,3.69-3.7,3.7,3.7,0,0,1,3.694,3.7"
                                                      transform="translate(245.905 -3.56)" fill="#fde0d3"></path>
                                                <path id="Path_378" data-name="Path 378"
                                                      d="M-209.183,12.328a3.7,3.7,0,0,0-3.69-3.7,3.7,3.7,0,0,0-3.694,3.7,3.7,3.7,0,0,0,3.694,3.694,3.693,3.693,0,0,0,3.69-3.694m-1.021-.018h0a2.641,2.641,0,0,1-2.637,2.64,2.641,2.641,0,0,1-2.636-2.64,2.639,2.639,0,0,1,2.636-2.64,2.64,2.64,0,0,1,2.637,2.64"
                                                      transform="translate(238.413 -12.032)" fill="#fde7dc"></path>
                                                <path id="Path_379" data-name="Path 379"
                                                      d="M-201.753,20.819a2.641,2.641,0,0,0-2.639-2.64,2.64,2.64,0,0,0-2.637,2.64,2.639,2.639,0,0,0,2.637,2.64,2.641,2.641,0,0,0,2.639-2.64m-1.023-.018h0a1.585,1.585,0,0,1-1.584,1.584A1.584,1.584,0,0,1-205.94,20.8a1.582,1.582,0,0,1,1.581-1.583,1.584,1.584,0,0,1,1.584,1.583"
                                                      transform="translate(229.93 -20.523)" fill="#feeee6"></path>
                                                <path id="Path_380" data-name="Path 380"
                                                      d="M-193.224,29.312a1.584,1.584,0,0,0-1.584-1.584,1.584,1.584,0,0,0-1.583,1.584,1.585,1.585,0,0,0,1.583,1.583,1.586,1.586,0,0,0,1.584-1.583m-1.146-.018h0a.529.529,0,0,1-.527.527.528.528,0,0,1-.527-.527.526.526,0,0,1,.527-.527.527.527,0,0,1,.527.527"
                                                      transform="translate(220.469 -29.015)" fill="#fef4f0"></path>
                                                <path id="Path_381" data-name="Path 381"
                                                      d="M-186.89,37.8a.528.528,0,0,0-.527-.527.526.526,0,0,0-.527.527.527.527,0,0,0,.527.527.53.53,0,0,0,.527-.527"
                                                      transform="translate(212.957 -37.507)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <g id="Group_1026" data-name="Group 1026" transform="translate(4.046 5.274)">
                                            <g id="Group_1025" data-name="Group 1025" transform="translate(0)"
                                               clip-path="url(#clip-path-4)">
                                                <path id="Path_383" data-name="Path 383"
                                                      d="M-433.055-270.164a44.478,44.478,0,0,0-44.45-44.474,44.477,44.477,0,0,0-44.447,44.474A44.477,44.477,0,0,0-477.5-225.689a44.478,44.478,0,0,0,44.45-44.475m-1.057.122h0a43.43,43.43,0,0,1-43.4,43.425,43.429,43.429,0,0,1-43.4-43.425,43.43,43.43,0,0,1,43.4-43.43,43.431,43.431,0,0,1,43.4,43.43"
                                                      transform="translate(505.961 270.362)" fill="#da091d"></path>
                                                <path id="Path_384" data-name="Path 384"
                                                      d="M-425.708-260.648a43.434,43.434,0,0,0-43.4-43.431,43.433,43.433,0,0,0-43.4,43.431,43.431,43.431,0,0,0,43.4,43.425,43.431,43.431,0,0,0,43.4-43.425m-1.056,0h0a42.385,42.385,0,0,1-42.358,42.379,42.384,42.384,0,0,1-42.356-42.379,42.383,42.383,0,0,1,42.356-42.381,42.384,42.384,0,0,1,42.358,42.381"
                                                      transform="translate(497.568 260.972)" fill="#db161f"></path>
                                                <path id="Path_385" data-name="Path 385"
                                                      d="M-418.344-252.235A42.386,42.386,0,0,0-460.7-294.619a42.386,42.386,0,0,0-42.357,42.384A42.385,42.385,0,0,0-460.7-209.856a42.385,42.385,0,0,0,42.358-42.379m-1.057,0h0a41.336,41.336,0,0,1-41.311,41.332,41.338,41.338,0,0,1-41.311-41.332,41.337,41.337,0,0,1,41.311-41.335A41.335,41.335,0,0,1-419.4-252.238"
                                                      transform="translate(489.158 252.558)" fill="#dc1d21"></path>
                                                <path id="Path_386" data-name="Path 386"
                                                      d="M-410.978-243.824a41.339,41.339,0,0,0-41.311-41.337A41.337,41.337,0,0,0-493.6-243.824a41.336,41.336,0,0,0,41.311,41.332,41.338,41.338,0,0,0,41.311-41.332m-1.056,0h0A40.29,40.29,0,0,1-452.3-203.54a40.289,40.289,0,0,1-40.265-40.286A40.289,40.289,0,0,1-452.3-284.115a40.291,40.291,0,0,1,40.265,40.288"
                                                      transform="translate(480.746 244.147)" fill="#dc2423"></path>
                                                <path id="Path_387" data-name="Path 387"
                                                      d="M-403.611-235.412A40.293,40.293,0,0,0-443.875-275.7a40.291,40.291,0,0,0-40.265,40.291,40.29,40.29,0,0,0,40.265,40.286,40.292,40.292,0,0,0,40.265-40.286m-1.056,0h0a39.243,39.243,0,0,1-39.218,39.239A39.242,39.242,0,0,1-483.1-235.415a39.243,39.243,0,0,1,39.219-39.242,39.244,39.244,0,0,1,39.218,39.242"
                                                      transform="translate(472.333 235.735)" fill="#dd2a25"></path>
                                                <path id="Path_388" data-name="Path 388"
                                                      d="M-396.266-226.981a39.244,39.244,0,0,0-39.221-39.242,39.244,39.244,0,0,0-39.219,39.242,39.247,39.247,0,0,0,39.219,39.242,39.248,39.248,0,0,0,39.221-39.242m-1.057,0h0a38.2,38.2,0,0,1-38.172,38.2,38.2,38.2,0,0,1-38.175-38.2,38.2,38.2,0,0,1,38.175-38.2,38.2,38.2,0,0,1,38.172,38.2"
                                                      transform="translate(463.943 227.304)" fill="#de3027"></path>
                                                <path id="Path_389" data-name="Path 389"
                                                      d="M-388.9-218.588a38.2,38.2,0,0,0-38.175-38.2,38.2,38.2,0,0,0-38.173,38.2,38.2,38.2,0,0,0,38.173,38.2,38.2,38.2,0,0,0,38.175-38.2m-1.056,0h0a37.152,37.152,0,0,1-37.129,37.148,37.15,37.15,0,0,1-37.126-37.148,37.151,37.151,0,0,1,37.126-37.151,37.152,37.152,0,0,1,37.129,37.151"
                                                      transform="translate(455.531 218.912)" fill="#df362a"></path>
                                                <path id="Path_390" data-name="Path 390"
                                                      d="M-381.533-210.177a37.153,37.153,0,0,0-37.129-37.151,37.151,37.151,0,0,0-37.126,37.151,37.152,37.152,0,0,0,37.126,37.149,37.154,37.154,0,0,0,37.129-37.149m-1.057,0h0a36.106,36.106,0,0,1-36.082,36.1,36.106,36.106,0,0,1-36.081-36.1,36.106,36.106,0,0,1,36.081-36.1,36.107,36.107,0,0,1,36.082,36.1"
                                                      transform="translate(447.118 210.5)" fill="#e03d2d"></path>
                                                <path id="Path_391" data-name="Path 391"
                                                      d="M-374.187-201.764a36.108,36.108,0,0,0-36.083-36.1,36.108,36.108,0,0,0-36.082,36.1,36.107,36.107,0,0,0,36.082,36.1,36.107,36.107,0,0,0,36.083-36.1m-1.056,0h0a35.059,35.059,0,0,1-35.037,35.056,35.059,35.059,0,0,1-35.035-35.056,35.059,35.059,0,0,1,35.035-35.058,35.059,35.059,0,0,1,35.037,35.058"
                                                      transform="translate(438.727 202.088)" fill="#e14230"></path>
                                                <path id="Path_392" data-name="Path 392"
                                                      d="M-366.821-193.353a35.059,35.059,0,0,0-35.036-35.058,35.06,35.06,0,0,0-35.037,35.058A35.057,35.057,0,0,0-401.857-158.3a35.057,35.057,0,0,0,35.036-35.053m-1.056,0h0a34.012,34.012,0,0,1-33.991,34.009,34.011,34.011,0,0,1-33.989-34.009,34.013,34.013,0,0,1,33.989-34.011,34.014,34.014,0,0,1,33.991,34.011"
                                                      transform="translate(430.314 193.676)" fill="#e24634"></path>
                                                <path id="Path_393" data-name="Path 393"
                                                      d="M-359.455-184.941a34.015,34.015,0,0,0-33.991-34.012,34.014,34.014,0,0,0-33.99,34.012,34.009,34.009,0,0,0,33.99,34.006,34.01,34.01,0,0,0,33.991-34.006m-1.057,0h0a32.965,32.965,0,0,1-32.945,32.96,32.965,32.965,0,0,1-32.944-32.96,32.967,32.967,0,0,1,32.944-32.965,32.967,32.967,0,0,1,32.945,32.965"
                                                      transform="translate(421.903 185.265)" fill="#e34b38"></path>
                                                <path id="Path_394" data-name="Path 394"
                                                      d="M-352.089-176.528a32.967,32.967,0,0,0-32.945-32.965,32.966,32.966,0,0,0-32.944,32.965,32.964,32.964,0,0,0,32.944,32.96,32.965,32.965,0,0,0,32.945-32.96m-1.056,0h0a31.918,31.918,0,0,1-31.9,31.913,31.917,31.917,0,0,1-31.9-31.913,31.917,31.917,0,0,1,31.9-31.916,31.918,31.918,0,0,1,31.9,31.916"
                                                      transform="translate(413.492 176.851)" fill="#e4523b"></path>
                                                <path id="Path_395" data-name="Path 395"
                                                      d="M-344.744-168.117a31.919,31.919,0,0,0-31.9-31.919,31.918,31.918,0,0,0-31.9,31.919,31.917,31.917,0,0,0,31.9,31.913,31.918,31.918,0,0,0,31.9-31.913m-1.057,0h0a30.874,30.874,0,0,1-30.852,30.869,30.874,30.874,0,0,1-30.855-30.869,30.875,30.875,0,0,1,30.855-30.872A30.874,30.874,0,0,1-345.8-168.12"
                                                      transform="translate(405.099 168.441)" fill="#e5563f"></path>
                                                <path id="Path_396" data-name="Path 396"
                                                      d="M-337.378-159.726A30.875,30.875,0,0,0-368.232-190.6a30.875,30.875,0,0,0-30.852,30.875,30.874,30.874,0,0,0,30.852,30.869,30.874,30.874,0,0,0,30.854-30.869m-1.056,0h0a29.827,29.827,0,0,1-29.808,29.823,29.826,29.826,0,0,1-29.805-29.823,29.826,29.826,0,0,1,29.805-29.825,29.827,29.827,0,0,1,29.808,29.825"
                                                      transform="translate(396.688 160.05)" fill="#e55a43"></path>
                                                <path id="Path_397" data-name="Path 397"
                                                      d="M-331.113-151.313a29.828,29.828,0,0,0-29.808-29.828,29.827,29.827,0,0,0-29.806,29.828,29.826,29.826,0,0,0,29.806,29.823,29.827,29.827,0,0,0,29.808-29.823m-.934,0h0a28.781,28.781,0,0,1-28.762,28.776,28.779,28.779,0,0,1-28.76-28.776,28.778,28.778,0,0,1,28.76-28.779,28.78,28.78,0,0,1,28.762,28.779"
                                                      transform="translate(389.255 151.636)" fill="#e65e46"></path>
                                                <path id="Path_398" data-name="Path 398"
                                                      d="M-322.646-142.9a28.782,28.782,0,0,0-28.762-28.781A28.78,28.78,0,0,0-380.166-142.9a28.779,28.779,0,0,0,28.759,28.776A28.781,28.781,0,0,0-322.646-142.9m-1.055,0h0a27.735,27.735,0,0,1-27.716,27.73,27.734,27.734,0,0,1-27.716-27.73,27.733,27.733,0,0,1,27.716-27.732A27.734,27.734,0,0,1-323.7-142.905"
                                                      transform="translate(379.863 143.226)" fill="#e8644a"></path>
                                                <path id="Path_399" data-name="Path 399"
                                                      d="M-315.3-134.471A27.733,27.733,0,0,0-343.016-162.2a27.733,27.733,0,0,0-27.716,27.732,27.734,27.734,0,0,0,27.716,27.73,27.734,27.734,0,0,0,27.715-27.73m-1.056,0h0a26.687,26.687,0,0,1-26.67,26.683A26.686,26.686,0,0,1-369.7-134.473a26.687,26.687,0,0,1,26.668-26.686,26.687,26.687,0,0,1,26.67,26.686"
                                                      transform="translate(371.473 134.794)" fill="#e8684f"></path>
                                                <path id="Path_400" data-name="Path 400"
                                                      d="M-307.933-126.057a26.688,26.688,0,0,0-26.67-26.686,26.687,26.687,0,0,0-26.669,26.686A26.688,26.688,0,0,0-334.6-99.374a26.689,26.689,0,0,0,26.67-26.683m-1.057,0h0a25.64,25.64,0,0,1-25.624,25.637,25.64,25.64,0,0,1-25.623-25.637A25.641,25.641,0,0,1-334.614-151.7,25.641,25.641,0,0,1-308.99-126.06"
                                                      transform="translate(363.06 126.381)" fill="#e96c53"></path>
                                                <path id="Path_401" data-name="Path 401"
                                                      d="M-300.568-117.647a25.64,25.64,0,0,0-25.623-25.639,25.639,25.639,0,0,0-25.623,25.639,25.638,25.638,0,0,0,25.623,25.634,25.639,25.639,0,0,0,25.623-25.634m-1.055,0h0A24.593,24.593,0,0,1-326.2-93.059a24.592,24.592,0,0,1-24.579-24.59A24.594,24.594,0,0,1-326.2-142.242a24.6,24.6,0,0,1,24.578,24.593"
                                                      transform="translate(354.648 117.97)" fill="#ea7157"></path>
                                                <path id="Path_402" data-name="Path 402"
                                                      d="M-293.221-109.235A24.6,24.6,0,0,0-317.8-133.828a24.6,24.6,0,0,0-24.58,24.593A24.593,24.593,0,0,0-317.8-84.648a24.592,24.592,0,0,0,24.577-24.587m-1.057,0h0a23.548,23.548,0,0,1-23.531,23.546,23.549,23.549,0,0,1-23.532-23.546,23.547,23.547,0,0,1,23.532-23.546,23.546,23.546,0,0,1,23.531,23.546"
                                                      transform="translate(346.256 109.559)" fill="#eb755b"></path>
                                                <path id="Path_403" data-name="Path 403"
                                                      d="M-285.856-100.822a23.548,23.548,0,0,0-23.534-23.546,23.547,23.547,0,0,0-23.53,23.546,23.548,23.548,0,0,0,23.53,23.543,23.549,23.549,0,0,0,23.534-23.543m-1.057,0h0a22.5,22.5,0,0,1-22.485,22.5,22.5,22.5,0,0,1-22.487-22.5,22.5,22.5,0,0,1,22.487-22.5,22.5,22.5,0,0,1,22.485,22.5"
                                                      transform="translate(337.845 101.145)" fill="#ec7b60"></path>
                                                <path id="Path_404" data-name="Path 404"
                                                      d="M-279.59-92.432a22.5,22.5,0,0,0-22.487-22.5,22.5,22.5,0,0,0-22.484,22.5,22.5,22.5,0,0,0,22.484,22.5,22.5,22.5,0,0,0,22.487-22.5m-.935,0h0a21.453,21.453,0,0,1-21.441,21.45A21.452,21.452,0,0,1-323.4-92.434a21.455,21.455,0,0,1,21.438-21.455,21.456,21.456,0,0,1,21.441,21.455"
                                                      transform="translate(330.412 92.755)" fill="#ed8166"></path>
                                                <path id="Path_405" data-name="Path 405"
                                                      d="M-271.122-84.02a21.457,21.457,0,0,0-21.442-21.456A21.455,21.455,0,0,0-314-84.02a21.452,21.452,0,0,0,21.438,21.45,21.454,21.454,0,0,0,21.442-21.45m-1.057,0h0a20.406,20.406,0,0,1-20.4,20.4,20.407,20.407,0,0,1-20.394-20.4,20.408,20.408,0,0,1,20.394-20.406,20.407,20.407,0,0,1,20.4,20.406"
                                                      transform="translate(321.02 84.344)" fill="#ee866c"></path>
                                                <path id="Path_406" data-name="Path 406"
                                                      d="M-263.779-75.607a20.41,20.41,0,0,0-20.394-20.409,20.408,20.408,0,0,0-20.4,20.409,20.406,20.406,0,0,0,20.4,20.4,20.407,20.407,0,0,0,20.394-20.4m-1.056,0h0a19.36,19.36,0,0,1-19.349,19.357A19.36,19.36,0,0,1-303.531-75.61a19.36,19.36,0,0,1,19.348-19.36,19.361,19.361,0,0,1,19.349,19.36"
                                                      transform="translate(312.63 75.93)" fill="#ef8b71"></path>
                                                <path id="Path_407" data-name="Path 407"
                                                      d="M-256.411-67.2A19.363,19.363,0,0,0-275.76-86.559,19.363,19.363,0,0,0-295.108-67.2,19.361,19.361,0,0,0-275.76-47.839,19.361,19.361,0,0,0-256.411-67.2m-1.056,0h0a18.313,18.313,0,0,1-18.3,18.311,18.312,18.312,0,0,1-18.3-18.311,18.313,18.313,0,0,1,18.3-18.313,18.314,18.314,0,0,1,18.3,18.313"
                                                      transform="translate(304.217 67.52)" fill="#f08f76"></path>
                                                <path id="Path_408" data-name="Path 408"
                                                      d="M-249.046-58.785a18.315,18.315,0,0,0-18.3-18.316,18.314,18.314,0,0,0-18.3,18.316,18.312,18.312,0,0,0,18.3,18.311,18.313,18.313,0,0,0,18.3-18.311m-1.057,0h0a17.268,17.268,0,0,1-17.256,17.264,17.268,17.268,0,0,1-17.257-17.264,17.268,17.268,0,0,1,17.257-17.267A17.268,17.268,0,0,1-250.1-58.788"
                                                      transform="translate(295.805 59.108)" fill="#f1957b"></path>
                                                <path id="Path_409" data-name="Path 409"
                                                      d="M-241.682-50.351a17.266,17.266,0,0,0-17.256-17.267,17.266,17.266,0,0,0-17.256,17.267,17.267,17.267,0,0,0,17.256,17.264,17.267,17.267,0,0,0,17.256-17.264m-1.055,0h0a16.22,16.22,0,0,1-16.21,16.218A16.221,16.221,0,0,1-275.16-50.354a16.222,16.222,0,0,1,16.213-16.22,16.221,16.221,0,0,1,16.21,16.22"
                                                      transform="translate(287.395 50.675)" fill="#f29b81"></path>
                                                <path id="Path_410" data-name="Path 410"
                                                      d="M-234.334-41.941a16.22,16.22,0,0,0-16.213-16.22,16.219,16.219,0,0,0-16.209,16.22,16.22,16.22,0,0,0,16.209,16.217,16.221,16.221,0,0,0,16.213-16.217m-1.056,0h0A15.177,15.177,0,0,1-250.554-26.77a15.177,15.177,0,0,1-15.166-15.174,15.178,15.178,0,0,1,15.166-15.176,15.177,15.177,0,0,1,15.164,15.176"
                                                      transform="translate(279.002 42.264)" fill="#f3a087"></path>
                                                <path id="Path_411" data-name="Path 411"
                                                      d="M-226.968-33.55a15.176,15.176,0,0,0-15.166-15.176A15.176,15.176,0,0,0-257.3-33.55a15.177,15.177,0,0,0,15.164,15.174A15.177,15.177,0,0,0-226.968-33.55m-1.056,0h0a14.13,14.13,0,0,1-14.12,14.127,14.129,14.129,0,0,1-14.117-14.127,14.128,14.128,0,0,1,14.117-14.13,14.129,14.129,0,0,1,14.12,14.13"
                                                      transform="translate(270.59 33.873)" fill="#f4a68d"></path>
                                                <path id="Path_412" data-name="Path 412"
                                                      d="M-219.6-25.136a14.13,14.13,0,0,0-14.121-14.13,14.127,14.127,0,0,0-14.117,14.13,14.126,14.126,0,0,0,14.117,14.125A14.129,14.129,0,0,0-219.6-25.136m-1.056,0h0a13.082,13.082,0,0,1-13.074,13.081A13.079,13.079,0,0,1-246.8-25.139a13.08,13.08,0,0,1,13.071-13.083,13.083,13.083,0,0,1,13.074,13.083"
                                                      transform="translate(262.18 25.46)" fill="#f5ac94"></path>
                                                <path id="Path_413" data-name="Path 413"
                                                      d="M-212.256-16.726a13.083,13.083,0,0,0-13.074-13.083A13.082,13.082,0,0,0-238.4-16.726,13.081,13.081,0,0,0-225.331-3.648a13.082,13.082,0,0,0,13.074-13.078m-1.057,0h0A12.036,12.036,0,0,1-225.341-4.694a12.036,12.036,0,0,1-12.028-12.034,12.037,12.037,0,0,1,12.028-12.037,12.037,12.037,0,0,1,12.028,12.037"
                                                      transform="translate(253.787 17.049)" fill="#f6b099"></path>
                                                <path id="Path_414" data-name="Path 414"
                                                      d="M-204.892-8.314a12.036,12.036,0,0,0-12.028-12.037A12.036,12.036,0,0,0-228.946-8.314,12.035,12.035,0,0,0-216.919,3.717,12.035,12.035,0,0,0-204.892-8.314m-1.056,0h0A10.986,10.986,0,0,1-216.929,2.668,10.987,10.987,0,0,1-227.911-8.317a10.989,10.989,0,0,1,10.981-10.99,10.989,10.989,0,0,1,10.981,10.99"
                                                      transform="translate(245.375 8.638)" fill="#f7b7a2"></path>
                                                <path id="Path_415" data-name="Path 415"
                                                      d="M-197.524.1A10.989,10.989,0,0,0-208.5-10.891,10.989,10.989,0,0,0-219.486.1,10.988,10.988,0,0,0-208.5,11.084,10.988,10.988,0,0,0-197.524.1m-1.056,0h0a9.941,9.941,0,0,1-9.935,9.939A9.939,9.939,0,0,1-218.45.1a9.939,9.939,0,0,1,9.935-9.941A9.942,9.942,0,0,1-198.58.1"
                                                      transform="translate(236.962 0.224)" fill="#f8bda9"></path>
                                                <path id="Path_416" data-name="Path 416"
                                                      d="M-190.159,8.51a9.944,9.944,0,0,0-9.935-9.944,9.942,9.942,0,0,0-9.935,9.944,9.941,9.941,0,0,0,9.935,9.939,9.943,9.943,0,0,0,9.935-9.939m-1.055,0h0A8.893,8.893,0,0,1-200.1,17.4,8.894,8.894,0,0,1-209,8.507,8.9,8.9,0,0,1-200.1-.387a8.894,8.894,0,0,1,8.889,8.895"
                                                      transform="translate(228.552 -8.186)" fill="#f9c3b0"></path>
                                                <path id="Path_417" data-name="Path 417"
                                                      d="M-182.812,16.921a8.9,8.9,0,0,0-8.891-8.9,8.9,8.9,0,0,0-8.889,8.9,8.9,8.9,0,0,0,8.889,8.892,8.9,8.9,0,0,0,8.891-8.892m-1.056,0h0a7.848,7.848,0,0,1-7.842,7.848,7.85,7.85,0,0,1-7.845-7.848,7.849,7.849,0,0,1,7.845-7.848,7.847,7.847,0,0,1,7.842,7.848"
                                                      transform="translate(220.159 -16.598)" fill="#fac9b6"></path>
                                                <path id="Path_418" data-name="Path 418"
                                                      d="M-175.446,25.334a7.852,7.852,0,0,0-7.845-7.85,7.849,7.849,0,0,0-7.843,7.85,7.849,7.849,0,0,0,7.843,7.848,7.852,7.852,0,0,0,7.845-7.848m-1.057,0h0a6.8,6.8,0,0,1-6.8,6.8,6.8,6.8,0,0,1-6.8-6.8,6.8,6.8,0,0,1,6.8-6.8,6.8,6.8,0,0,1,6.8,6.8"
                                                      transform="translate(211.747 -25.011)" fill="#facfbd"></path>
                                                <path id="Path_419" data-name="Path 419"
                                                      d="M-168.082,33.745a6.8,6.8,0,0,0-6.8-6.8,6.8,6.8,0,0,0-6.8,6.8,6.8,6.8,0,0,0,6.8,6.8,6.8,6.8,0,0,0,6.8-6.8m-1.055,0h0a5.759,5.759,0,0,1-5.754,5.755,5.758,5.758,0,0,1-5.752-5.755,5.758,5.758,0,0,1,5.752-5.757,5.759,5.759,0,0,1,5.754,5.757"
                                                      transform="translate(203.337 -33.422)" fill="#fbd6c6"></path>
                                                <path id="Path_420" data-name="Path 420"
                                                      d="M-160.734,42.156a5.758,5.758,0,0,0-5.754-5.757,5.758,5.758,0,0,0-5.752,5.757,5.758,5.758,0,0,0,5.752,5.755,5.757,5.757,0,0,0,5.754-5.755m-1.056,0h0a4.71,4.71,0,0,1-4.707,4.708,4.71,4.71,0,0,1-4.707-4.708,4.71,4.71,0,0,1,4.707-4.711,4.71,4.71,0,0,1,4.707,4.711"
                                                      transform="translate(194.944 -41.833)" fill="#fcdbcd"></path>
                                                <path id="Path_421" data-name="Path 421"
                                                      d="M-153.369,50.57a4.709,4.709,0,0,0-4.707-4.711,4.71,4.71,0,0,0-4.706,4.711,4.711,4.711,0,0,0,4.706,4.708,4.71,4.71,0,0,0,4.707-4.708m-1.057,0h0a3.662,3.662,0,0,1-3.66,3.662,3.663,3.663,0,0,1-3.66-3.662,3.663,3.663,0,0,1,3.66-3.664,3.663,3.663,0,0,1,3.66,3.664"
                                                      transform="translate(186.532 -50.246)" fill="#fde2d6"></path>
                                                <path id="Path_422" data-name="Path 422"
                                                      d="M-147.1,58.981a3.661,3.661,0,0,0-3.66-3.664,3.661,3.661,0,0,0-3.66,3.664,3.66,3.66,0,0,0,3.66,3.659,3.66,3.66,0,0,0,3.66-3.659m-.935,0h0a2.617,2.617,0,0,1-2.614,2.615,2.616,2.616,0,0,1-2.614-2.615,2.617,2.617,0,0,1,2.614-2.618,2.617,2.617,0,0,1,2.614,2.618"
                                                      transform="translate(179.099 -58.658)" fill="#fde8df"></path>
                                                <path id="Path_423" data-name="Path 423"
                                                      d="M-138.637,67.392a2.616,2.616,0,0,0-2.614-2.618,2.615,2.615,0,0,0-2.614,2.618A2.614,2.614,0,0,0-141.251,70a2.614,2.614,0,0,0,2.614-2.613m-1.057,0h0a1.569,1.569,0,0,1-1.567,1.569,1.573,1.573,0,0,1-1.571-1.569,1.573,1.573,0,0,1,1.571-1.571,1.57,1.57,0,0,1,1.567,1.571"
                                                      transform="translate(169.708 -67.069)" fill="#feeee8"></path>
                                                <path id="Path_424" data-name="Path 424"
                                                      d="M-131.292,75.805a1.568,1.568,0,0,0-1.569-1.571,1.569,1.569,0,0,0-1.57,1.571,1.568,1.568,0,0,0,1.57,1.566,1.567,1.567,0,0,0,1.569-1.566m-1.056,0h0a.521.521,0,0,1-.522.519.523.523,0,0,1-.525-.519.526.526,0,0,1,.525-.525.524.524,0,0,1,.522.525"
                                                      transform="translate(161.317 -75.482)" fill="#fef5f2"></path>
                                                <path id="Path_425" data-name="Path 425"
                                                      d="M-123.925,84.214a.522.522,0,0,0-.521-.522.522.522,0,0,0-.524.522.524.524,0,0,0,.524.524.524.524,0,0,0,.521-.524"
                                                      transform="translate(152.904 -83.893)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <g id="Group_1028" data-name="Group 1028" transform="translate(13.636 16.529)">
                                            <g id="Group_1027" data-name="Group 1027" transform="translate(0)"
                                               clip-path="url(#clip-path-5)">
                                                <path id="Path_427" data-name="Path 427"
                                                      d="M-339.563-117.239a38.089,38.089,0,0,0-38.065-38.088A38.091,38.091,0,0,0-415.7-117.239a38.091,38.091,0,0,0,38.068,38.088,38.089,38.089,0,0,0,38.065-38.088m-1.081.075h0A37.048,37.048,0,0,1-377.669-80.12a37.047,37.047,0,0,1-37.022-37.044,37.047,37.047,0,0,1,37.022-37.044,37.047,37.047,0,0,1,37.024,37.044"
                                                      transform="translate(401.872 117.423)" fill="#da091d"></path>
                                                <path id="Path_428" data-name="Path 428"
                                                      d="M-332.216-107.764a37.049,37.049,0,0,0-37.024-37.049,37.05,37.05,0,0,0-37.022,37.049A37.049,37.049,0,0,0-369.24-70.72a37.048,37.048,0,0,0,37.024-37.044m-1.081-.05h0a36,36,0,0,1-35.98,36,36,36,0,0,1-35.981-36,36,36,0,0,1,35.981-36,36,36,0,0,1,35.98,36"
                                                      transform="translate(393.482 108.072)" fill="#db181f"></path>
                                                <path id="Path_429" data-name="Path 429"
                                                      d="M-324.874-99.374a36.006,36.006,0,0,0-35.982-36.005,36.007,36.007,0,0,0-35.98,36.005,36,36,0,0,0,35.98,36,36,36,0,0,0,35.982-36m-1.082-.05h0a34.961,34.961,0,0,1-34.938,34.959,34.962,34.962,0,0,1-34.94-34.959,34.962,34.962,0,0,1,34.94-34.959,34.961,34.961,0,0,1,34.938,34.959"
                                                      transform="translate(385.099 99.682)" fill="#dc2021"></path>
                                                <path id="Path_430" data-name="Path 430"
                                                      d="M-318.658-92.085A34.961,34.961,0,0,0-353.6-127.044a34.961,34.961,0,0,0-34.94,34.959A34.963,34.963,0,0,0-353.6-57.127a34.962,34.962,0,0,0,34.937-34.959m-.96.075h0a33.916,33.916,0,0,1-33.9,33.915,33.916,33.916,0,0,1-33.893-33.915,33.917,33.917,0,0,1,33.893-33.915,33.918,33.918,0,0,1,33.9,33.915"
                                                      transform="translate(377.717 92.269)" fill="#dd2723"></path>
                                                <path id="Path_431" data-name="Path 431"
                                                      d="M-310.2-82.588A33.916,33.916,0,0,0-344.092-116.5a33.917,33.917,0,0,0-33.9,33.917,33.916,33.916,0,0,0,33.9,33.912A33.915,33.915,0,0,0-310.2-82.588m-1.081-.05h0a32.874,32.874,0,0,1-32.853,32.871,32.873,32.873,0,0,1-32.852-32.871,32.873,32.873,0,0,1,32.852-32.87,32.874,32.874,0,0,1,32.853,32.87"
                                                      transform="translate(368.336 82.896)" fill="#de2e26"></path>
                                                <path id="Path_432" data-name="Path 432"
                                                      d="M-302.869-74.2a32.872,32.872,0,0,0-32.852-32.873A32.873,32.873,0,0,0-368.574-74.2a32.872,32.872,0,0,0,32.853,32.868A32.871,32.871,0,0,0-302.869-74.2m-1.081-.05h0a31.83,31.83,0,0,1-31.809,31.829,31.832,31.832,0,0,1-31.812-31.829,31.83,31.83,0,0,1,31.812-31.826A31.828,31.828,0,0,1-303.95-74.247"
                                                      transform="translate(359.964 74.505)" fill="#df3529"></path>
                                                <path id="Path_433" data-name="Path 433"
                                                      d="M-295.521-65.825A31.832,31.832,0,0,0-327.33-97.657a31.832,31.832,0,0,0-31.809,31.832A31.83,31.83,0,0,0-327.33-34a31.83,31.83,0,0,0,31.809-31.826m-1.081-.05h0A30.788,30.788,0,0,1-327.37-35.09a30.787,30.787,0,0,1-30.766-30.785A30.787,30.787,0,0,1-327.37-96.658,30.787,30.787,0,0,1-296.6-65.875"
                                                      transform="translate(351.573 66.134)" fill="#e03d2d"></path>
                                                <path id="Path_434" data-name="Path 434"
                                                      d="M-289.291-58.537a30.787,30.787,0,0,0-30.765-30.785,30.788,30.788,0,0,0-30.769,30.785,30.788,30.788,0,0,0,30.769,30.785,30.787,30.787,0,0,0,30.765-30.785m-.959.075h0a29.743,29.743,0,0,1-29.725,29.741A29.744,29.744,0,0,1-349.7-58.463,29.742,29.742,0,0,1-319.976-88.2a29.741,29.741,0,0,1,29.725,29.738"
                                                      transform="translate(344.179 58.721)" fill="#e14331"></path>
                                                <path id="Path_435" data-name="Path 435"
                                                      d="M-280.852-49.042a29.744,29.744,0,0,0-29.724-29.743A29.745,29.745,0,0,0-340.3-49.042,29.744,29.744,0,0,0-310.575-19.3a29.742,29.742,0,0,0,29.724-29.738m-1.081-.05h0a28.7,28.7,0,0,1-28.681,28.7,28.7,28.7,0,0,1-28.681-28.7,28.7,28.7,0,0,1,28.681-28.694,28.7,28.7,0,0,1,28.681,28.694"
                                                      transform="translate(334.818 49.35)" fill="#e24835"></path>
                                                <path id="Path_436" data-name="Path 436"
                                                      d="M-273.5-40.649a28.7,28.7,0,0,0-28.681-28.7,28.7,28.7,0,0,0-28.68,28.7,28.7,28.7,0,0,0,28.68,28.694A28.7,28.7,0,0,0-273.5-40.649m-1.083-.05h0a27.656,27.656,0,0,1-27.636,27.655A27.657,27.657,0,0,1-329.86-40.7a27.655,27.655,0,0,1,27.639-27.652A27.653,27.653,0,0,1-274.585-40.7"
                                                      transform="translate(326.426 40.957)" fill="#e34e39"></path>
                                                <path id="Path_437" data-name="Path 437"
                                                      d="M-267.274-33.361a27.654,27.654,0,0,0-27.638-27.652,27.655,27.655,0,0,0-27.639,27.652A27.656,27.656,0,0,0-294.912-5.708a27.656,27.656,0,0,0,27.638-27.653m-.961.075h0a26.61,26.61,0,0,1-26.6,26.611,26.61,26.61,0,0,1-26.594-26.611A26.614,26.614,0,0,1-294.83-59.9a26.613,26.613,0,0,1,26.6,26.611"
                                                      transform="translate(319.033 33.544)" fill="#e4543e"></path>
                                                <path id="Path_438" data-name="Path 438"
                                                      d="M-258.814-24.991A26.612,26.612,0,0,0-285.408-51.6,26.612,26.612,0,0,0-312-24.991,26.609,26.609,0,0,0-285.408,1.618a26.61,26.61,0,0,0,26.594-26.609m-1.083.075h0A25.57,25.57,0,0,1-285.449.651,25.57,25.57,0,0,1-311-24.916a25.57,25.57,0,0,1,25.552-25.567A25.57,25.57,0,0,1-259.9-24.916"
                                                      transform="translate(309.652 25.175)" fill="#e55a42"></path>
                                                <path id="Path_439" data-name="Path 439"
                                                      d="M-251.486-15.5a25.568,25.568,0,0,0-25.552-25.57A25.567,25.567,0,0,0-302.589-15.5a25.566,25.566,0,0,0,25.551,25.564A25.567,25.567,0,0,0-251.486-15.5m-1.081-.05h0A24.523,24.523,0,0,1-277.076,8.978a24.523,24.523,0,0,1-24.508-24.523,24.523,24.523,0,0,1,24.508-24.523,24.523,24.523,0,0,1,24.508,24.523"
                                                      transform="translate(301.28 15.803)" fill="#e65e46"></path>
                                                <path id="Path_440" data-name="Path 440"
                                                      d="M-244.137-7.1a24.526,24.526,0,0,0-24.51-24.526A24.525,24.525,0,0,0-293.155-7.1a24.524,24.524,0,0,0,24.508,24.521A24.525,24.525,0,0,0-244.137-7.1m-1.083-.05h0a23.481,23.481,0,0,1-23.467,23.479A23.48,23.48,0,0,1-292.152-7.152a23.482,23.482,0,0,1,23.465-23.479A23.482,23.482,0,0,1-245.22-7.152"
                                                      transform="translate(292.89 7.411)" fill="#e8644b"></path>
                                                <path id="Path_441" data-name="Path 441"
                                                      d="M-237.911.186a23.48,23.48,0,0,0-23.464-23.479A23.481,23.481,0,0,0-284.843.186a23.481,23.481,0,0,0,23.468,23.479A23.48,23.48,0,0,0-237.911.186m-.96.075h0A22.438,22.438,0,0,1-261.294,22.7,22.441,22.441,0,0,1-283.718.26a22.438,22.438,0,0,1,22.424-22.435A22.436,22.436,0,0,1-238.871.26"
                                                      transform="translate(285.498 -0.002)" fill="#e96a50"></path>
                                                <path id="Path_442" data-name="Path 442"
                                                      d="M-229.461,8.578a22.436,22.436,0,0,0-22.423-22.435A22.436,22.436,0,0,0-274.306,8.578a22.436,22.436,0,0,0,22.421,22.435A22.436,22.436,0,0,0-229.461,8.578m-1.081.075h0a21.4,21.4,0,0,1-21.38,21.393A21.4,21.4,0,0,1-273.3,8.652a21.4,21.4,0,0,1,21.381-21.393,21.4,21.4,0,0,1,21.38,21.393"
                                                      transform="translate(276.126 -8.394)" fill="#ea6f55"></path>
                                                <path id="Path_443" data-name="Path 443"
                                                      d="M-222.122,18.073A21.393,21.393,0,0,0-243.5-3.32a21.394,21.394,0,0,0-21.38,21.393A21.392,21.392,0,0,0-243.5,39.462a21.391,21.391,0,0,0,21.38-21.388m-1.081-.05h0a20.35,20.35,0,0,1-20.336,20.349,20.35,20.35,0,0,1-20.339-20.349A20.35,20.35,0,0,1-243.539-2.326,20.35,20.35,0,0,1-223.2,18.023"
                                                      transform="translate(267.744 -17.765)" fill="#eb745a"></path>
                                                <path id="Path_444" data-name="Path 444"
                                                      d="M-214.792,26.464A20.35,20.35,0,0,0-235.128,6.115a20.352,20.352,0,0,0-20.339,20.349,20.351,20.351,0,0,0,20.339,20.344,20.349,20.349,0,0,0,20.336-20.344m-1.081-.05h0a19.306,19.306,0,0,1-19.3,19.305,19.3,19.3,0,0,1-19.293-19.305,19.306,19.306,0,0,1,19.293-19.3,19.309,19.309,0,0,1,19.3,19.3"
                                                      transform="translate(259.372 -26.156)" fill="#ec7a5f"></path>
                                                <path id="Path_445" data-name="Path 445"
                                                      d="M-208.544,33.734a19.307,19.307,0,0,0-19.3-19.3,19.3,19.3,0,0,0-19.293,19.3,19.3,19.3,0,0,0,19.293,19.305,19.307,19.307,0,0,0,19.3-19.305m-.961.075h0A18.263,18.263,0,0,1-227.756,52.07a18.262,18.262,0,0,1-18.251-18.261,18.262,18.262,0,0,1,18.251-18.261A18.263,18.263,0,0,1-209.5,33.809"
                                                      transform="translate(251.959 -33.55)" fill="#ed8166"></path>
                                                <path id="Path_446" data-name="Path 446"
                                                      d="M-200.1,43.229a18.263,18.263,0,0,0-18.251-18.264,18.263,18.263,0,0,0-18.251,18.264A18.262,18.262,0,0,0-218.356,61.49,18.262,18.262,0,0,0-200.1,43.229m-1.083-.05h0A17.218,17.218,0,0,1-218.394,60.4,17.217,17.217,0,0,1-235.6,43.179a17.217,17.217,0,0,1,17.208-17.217,17.218,17.218,0,0,1,17.207,17.217"
                                                      transform="translate(242.599 -42.92)" fill="#ef876c"></path>
                                                <path id="Path_447" data-name="Path 447"
                                                      d="M-192.775,51.6a17.222,17.222,0,0,0-17.209-17.222A17.223,17.223,0,0,0-227.193,51.6a17.221,17.221,0,0,0,17.209,17.217A17.221,17.221,0,0,0-192.775,51.6m-1.081-.05h0a16.177,16.177,0,0,1-16.167,16.176,16.175,16.175,0,0,1-16.164-16.176,16.175,16.175,0,0,1,16.164-16.173,16.176,16.176,0,0,1,16.167,16.173"
                                                      transform="translate(234.226 -51.291)" fill="#ef8c72"></path>
                                                <path id="Path_448" data-name="Path 448"
                                                      d="M-186.528,58.889a16.175,16.175,0,0,0-16.163-16.175,16.176,16.176,0,0,0-16.167,16.175,16.176,16.176,0,0,0,16.167,16.176,16.175,16.175,0,0,0,16.163-16.176m-.96.075h0A15.133,15.133,0,0,1-202.611,74.1a15.134,15.134,0,0,1-15.123-15.132,15.134,15.134,0,0,1,15.123-15.131,15.133,15.133,0,0,1,15.123,15.131"
                                                      transform="translate(226.814 -58.706)" fill="#f19378"></path>
                                                <path id="Path_449" data-name="Path 449"
                                                      d="M-179.2,67.28a15.131,15.131,0,0,0-15.123-15.131A15.134,15.134,0,0,0-209.445,67.28a15.134,15.134,0,0,0,15.123,15.132A15.131,15.131,0,0,0-179.2,67.28m-.959.075h0a14.089,14.089,0,0,1-14.079,14.087,14.089,14.089,0,0,1-14.08-14.087,14.089,14.089,0,0,1,14.08-14.087,14.089,14.089,0,0,1,14.079,14.087"
                                                      transform="translate(218.442 -67.097)" fill="#f29980"></path>
                                                <path id="Path_450" data-name="Path 450"
                                                      d="M-170.738,76.775a14.09,14.09,0,0,0-14.08-14.09A14.089,14.089,0,0,0-198.9,76.775,14.088,14.088,0,0,0-184.818,90.86a14.089,14.089,0,0,0,14.08-14.085m-1.083-.05h0a13.044,13.044,0,0,1-13.035,13.043,13.045,13.045,0,0,1-13.038-13.043,13.045,13.045,0,0,1,13.038-13.043A13.044,13.044,0,0,1-171.82,76.725"
                                                      transform="translate(209.06 -76.467)" fill="#f3a085"></path>
                                                <path id="Path_451" data-name="Path 451"
                                                      d="M-163.41,85.168a13.045,13.045,0,0,0-13.035-13.046,13.047,13.047,0,0,0-13.038,13.046,13.046,13.046,0,0,0,13.038,13.041A13.043,13.043,0,0,0-163.41,85.168m-1.081-.05h0a12,12,0,0,1-11.994,12,12,12,0,0,1-11.992-12,12,12,0,0,1,11.992-12,12,12,0,0,1,11.994,12"
                                                      transform="translate(200.689 -84.86)" fill="#f4a68d"></path>
                                                <path id="Path_452" data-name="Path 452"
                                                      d="M-157.162,92.436a12,12,0,0,0-11.994-12,12,12,0,0,0-11.992,12,12,12,0,0,0,11.992,12,12,12,0,0,0,11.994-12m-.959.075h0a10.956,10.956,0,0,1-10.95,10.955A10.957,10.957,0,0,1-180.023,92.51a10.957,10.957,0,0,1,10.951-10.955,10.956,10.956,0,0,1,10.95,10.955"
                                                      transform="translate(193.276 -92.252)" fill="#f5ac95"></path>
                                                <path id="Path_453" data-name="Path 453"
                                                      d="M-148.732,100.828a10.958,10.958,0,0,0-10.95-10.957,10.958,10.958,0,0,0-10.951,10.957,10.959,10.959,0,0,0,10.951,10.958,10.958,10.958,0,0,0,10.95-10.958m-1.081.075h0a9.916,9.916,0,0,1-9.907,9.916,9.914,9.914,0,0,1-9.907-9.916,9.91,9.91,0,0,1,9.907-9.911,9.912,9.912,0,0,1,9.907,9.911"
                                                      transform="translate(183.926 -100.645)" fill="#f6b29d"></path>
                                                <path id="Path_454" data-name="Path 454"
                                                      d="M-141.382,110.323a9.916,9.916,0,0,0-9.907-9.916,9.914,9.914,0,0,0-9.907,9.916,9.913,9.913,0,0,0,9.907,9.911,9.915,9.915,0,0,0,9.907-9.911m-1.083-.05h0a8.87,8.87,0,0,1-8.863,8.872,8.872,8.872,0,0,1-8.866-8.872,8.871,8.871,0,0,1,8.866-8.869,8.87,8.87,0,0,1,8.863,8.869"
                                                      transform="translate(175.533 -110.015)" fill="#f7baa5"></path>
                                                <path id="Path_455" data-name="Path 455"
                                                      d="M-134.045,118.714a8.87,8.87,0,0,0-8.863-8.872,8.87,8.87,0,0,0-8.866,8.872,8.869,8.869,0,0,0,8.866,8.867,8.869,8.869,0,0,0,8.863-8.867m-1.081-.05h0a7.829,7.829,0,0,1-7.823,7.828,7.829,7.829,0,0,1-7.822-7.828,7.829,7.829,0,0,1,7.822-7.828,7.829,7.829,0,0,1,7.823,7.828"
                                                      transform="translate(167.152 -118.406)" fill="#f9c2ae"></path>
                                                <path id="Path_456" data-name="Path 456"
                                                      d="M-127.817,126a7.825,7.825,0,0,0-7.823-7.825A7.827,7.827,0,0,0-143.462,126a7.827,7.827,0,0,0,7.822,7.826A7.825,7.825,0,0,0-127.817,126m-.959.075h0a6.785,6.785,0,0,1-6.779,6.784,6.784,6.784,0,0,1-6.779-6.784,6.784,6.784,0,0,1,6.779-6.784,6.785,6.785,0,0,1,6.779,6.784"
                                                      transform="translate(159.76 -125.819)" fill="#fac8b5"></path>
                                                <path id="Path_457" data-name="Path 457"
                                                      d="M-119.365,135.5a6.783,6.783,0,0,0-6.779-6.784,6.783,6.783,0,0,0-6.779,6.784,6.781,6.781,0,0,0,6.779,6.779,6.782,6.782,0,0,0,6.779-6.779m-1.083-.05h0a5.738,5.738,0,0,1-5.735,5.74,5.74,5.74,0,0,1-5.737-5.74,5.74,5.74,0,0,1,5.737-5.74,5.738,5.738,0,0,1,5.735,5.74"
                                                      transform="translate(150.387 -135.19)" fill="#facfbd"></path>
                                                <path id="Path_458" data-name="Path 458"
                                                      d="M-112.028,143.87a5.742,5.742,0,0,0-5.735-5.742,5.742,5.742,0,0,0-5.737,5.742,5.741,5.741,0,0,0,5.737,5.737,5.741,5.741,0,0,0,5.735-5.737m-1.081-.05h0a4.7,4.7,0,0,1-4.695,4.7,4.7,4.7,0,0,1-4.691-4.7,4.7,4.7,0,0,1,4.691-4.7,4.7,4.7,0,0,1,4.695,4.7"
                                                      transform="translate(142.007 -143.561)" fill="#fbd6c8"></path>
                                                <path id="Path_459" data-name="Path 459"
                                                      d="M-104.679,152.262a4.7,4.7,0,0,0-4.694-4.7,4.7,4.7,0,0,0-4.691,4.7,4.694,4.694,0,0,0,4.691,4.693,4.7,4.7,0,0,0,4.694-4.693m-1.083-.05h0a3.653,3.653,0,0,1-3.649,3.654,3.654,3.654,0,0,1-3.651-3.654,3.652,3.652,0,0,1,3.651-3.652,3.65,3.65,0,0,1,3.649,3.652"
                                                      transform="translate(133.615 -151.954)" fill="#fcded0"></path>
                                                <path id="Path_460" data-name="Path 460"
                                                      d="M-98.452,159.55A3.652,3.652,0,0,0-102.1,155.9a3.652,3.652,0,0,0-3.649,3.651A3.652,3.652,0,0,0-102.1,163.2a3.652,3.652,0,0,0,3.651-3.652m-.96.075h0a2.609,2.609,0,0,1-2.606,2.61,2.607,2.607,0,0,1-2.606-2.61,2.607,2.607,0,0,1,2.606-2.61,2.609,2.609,0,0,1,2.606,2.61"
                                                      transform="translate(126.223 -159.367)" fill="#fde5db"></path>
                                                <path id="Path_461" data-name="Path 461"
                                                      d="M-90.021,169.046a2.609,2.609,0,0,0-2.607-2.61,2.611,2.611,0,0,0-2.61,2.61,2.612,2.612,0,0,0,2.61,2.608,2.61,2.61,0,0,0,2.607-2.608M-91.1,169h0a1.567,1.567,0,0,1-1.565,1.566A1.567,1.567,0,0,1-94.232,169a1.567,1.567,0,0,1,1.563-1.566A1.566,1.566,0,0,1-91.1,169"
                                                      transform="translate(116.872 -168.738)" fill="#feede5"></path>
                                                <path id="Path_462" data-name="Path 462"
                                                      d="M-82.662,177.436a1.564,1.564,0,0,0-1.564-1.566,1.563,1.563,0,0,0-1.565,1.566A1.564,1.564,0,0,0-84.225,179a1.565,1.565,0,0,0,1.564-1.563m-1.083-.05h0a.522.522,0,0,1-.522.522.523.523,0,0,1-.522-.522.523.523,0,0,1,.522-.522.522.522,0,0,1,.522.522"
                                                      transform="translate(108.469 -177.128)" fill="#fef4f0"></path>
                                                <path id="Path_463" data-name="Path 463"
                                                      d="M-76.435,184.706a.522.522,0,0,0-.521-.522.522.522,0,0,0-.522.522.522.522,0,0,0,.522.522.522.522,0,0,0,.521-.522"
                                                      transform="translate(101.077 -184.522)" fill="#fff"></path>
                                            </g>
                                        </g>
                                        <path id="Path_465" data-name="Path 465"
                                              d="M-344.518,41.025a6.836,6.836,0,0,1-1.551,2.491,17.928,17.928,0,0,1-2.986,2.57,28.371,28.371,0,0,1-3.989,2.307,28.42,28.42,0,0,1-4.493,1.693,18.089,18.089,0,0,1-4.057.7,7.173,7.173,0,0,1-2.854-.36,2.288,2.288,0,0,1-1.411-1.273,2.656,2.656,0,0,1,.209-2.036,5.97,5.97,0,0,1,.4-.728,8.528,8.528,0,0,1,.529-.738c.195-.246.411-.5.649-.743s.486-.492.756-.738l.723-.634.712-.574.675-.51.675-.472.664-.435.644-.39.639-.363a13.739,13.739,0,0,0-1.932,1.8,5.8,5.8,0,0,0-.814,1.235,1.2,1.2,0,0,0-.062.982,1.164,1.164,0,0,0,.736.589,3.835,3.835,0,0,0,1.426.137,9.336,9.336,0,0,0,1.991-.37,14.143,14.143,0,0,0,2.182-.828,14.472,14.472,0,0,0,1.948-1.116,9.355,9.355,0,0,0,1.484-1.245,3.64,3.64,0,0,0,.812-1.216,1.082,1.082,0,0,0-.039-.952,1.3,1.3,0,0,0-.817-.532,3.221,3.221,0,0,0-1.423.027A16.554,16.554,0,0,0-355,39.66a7.111,7.111,0,0,0-1.163.44,19.254,19.254,0,0,1,3.6-1.484A17.364,17.364,0,0,1-350.129,38a9.581,9.581,0,0,1,2.6-.1,5.3,5.3,0,0,1,1.919.477,2.168,2.168,0,0,1,1.064,1.049,2.3,2.3,0,0,1,.023,1.606"
                                              transform="translate(371.292 -37.849)" fill="#da091d"></path>
                                        <path id="Path_466" data-name="Path 466"
                                              d="M-232.63,369.177a12.493,12.493,0,0,1-.636,1.444,12.274,12.274,0,0,1-.827,1.2s-.184.244-.3.395c-.337.43-.894,1.074-.894,1.074l-.748.815a21.9,21.9,0,0,1-2.622,2.3,26.807,26.807,0,0,1-3.161,2.023,27.745,27.745,0,0,1-3.541,1.623,25.657,25.657,0,0,1-3.753,1.1c-.248.055-.5.1-.741.147s-.486.08-.725.117-.476.062-.708.09-.462.045-.69.06l-1.385.062-.306,0-1.482-.045-1.506-.134s-.26-.032-.423-.055c-.626-.089-.979-.149-1.6-.276s-1.6-.39-1.6-.39l-.807-.271-.22-.107a10.025,10.025,0,0,0,2.05.547,17.708,17.708,0,0,0,2.375.189,22.978,22.978,0,0,0,2.657-.129,27.867,27.867,0,0,0,2.912-.467,34.275,34.275,0,0,0,6.76-2.22,35.388,35.388,0,0,0,6.043-3.423,27.716,27.716,0,0,0,4.7-4.154,13.542,13.542,0,0,0,1.186-1.531c.016-.018-.005.02-.005.02"
                                              transform="translate(278.203 -332.494)" fill="#da091d"></path>
                                        <path id="Path_467" data-name="Path 467"
                                              d="M-378.946,42.387l1.62.609,1.6.736.562.291a5.13,5.13,0,0,1,1.163.825,3.838,3.838,0,0,1,.8,1.059,3.814,3.814,0,0,1,.38,1.305,5.03,5.03,0,0,1-.082,1.541,11.126,11.126,0,0,1-2.449,4.629,28.482,28.482,0,0,1-5.226,4.81,45.16,45.16,0,0,1-7.227,4.286,45.924,45.924,0,0,1-8.219,3.015,30.376,30.376,0,0,1-6.735,1.017,13.619,13.619,0,0,1-4.88-.654,5.124,5.124,0,0,1-2.734-2.041,4.2,4.2,0,0,1-.433-3.137l.114-.517.461-1.715.576-1.661.6-1.4.544-1.144-.091.231c.02-.042-.021.047-.021.047a4.126,4.126,0,0,0-.355,2.963,3.291,3.291,0,0,0,1.941,1.959,9.716,9.716,0,0,0,4.063.656,24.726,24.726,0,0,0,5.873-.917,38.761,38.761,0,0,0,6.539-2.434,38.793,38.793,0,0,0,5.781-3.381,24.367,24.367,0,0,0,4.251-3.779,9.32,9.32,0,0,0,2.093-3.637,3.713,3.713,0,0,0,.076-1.611,2.756,2.756,0,0,0-.6-1.26,3.7,3.7,0,0,0-1.182-.912,6.235,6.235,0,0,0-1.089-.418l-.6-.159a5.093,5.093,0,0,1,.6.114s.581.142.95.249c.529.154,1.344.435,1.344.435"
                                              transform="translate(411.272 -41.175)" fill="#da091d"></path>
                                        <path id="Path_468" data-name="Path 468"
                                              d="M-370.64,104.148l1.2,1.511,1.091,1.623.49.828a5.436,5.436,0,0,1,.372.81,5.572,5.572,0,0,1,.234.885,5.913,5.913,0,0,1,.094.967,7.3,7.3,0,0,1-.058,1.042,12.979,12.979,0,0,1-2.638,5.859,31.238,31.238,0,0,1-6.1,6.026,48.683,48.683,0,0,1-8.647,5.265,49.469,49.469,0,0,1-9.944,3.535,34.638,34.638,0,0,1-6.676.949,19.979,19.979,0,0,1-5.292-.445,10.022,10.022,0,0,1-3.706-1.621,5.741,5.741,0,0,1-2-2.585l-.382-1.1-.522-1.882-.369-1.9-.073-.532-.107-1.071c-.014-.1,0,0,0,0a4.638,4.638,0,0,0,1.263,2.65,7.45,7.45,0,0,0,3.365,1.9,17.172,17.172,0,0,0,5.29.572,33.594,33.594,0,0,0,6.967-1.021,49.087,49.087,0,0,0,9.388-3.388,48.708,48.708,0,0,0,8.207-4.929,30.763,30.763,0,0,0,5.852-5.588,12.289,12.289,0,0,0,2.623-5.389,6.376,6.376,0,0,0,.071-1.36,4.961,4.961,0,0,0-.224-1.206,4.7,4.7,0,0,0-.493-1.054,4.814,4.814,0,0,0-.434-.569v-.008l1,1.049Z"
                                              transform="translate(413.712 -95.722)" fill="#da091d"></path>
                                        <path id="Path_469" data-name="Path 469"
                                              d="M-323.133,222.047a13.392,13.392,0,0,1-.05,3.945,12.179,12.179,0,0,1-2.309,5.641,27.059,27.059,0,0,1-5.443,5.665,40.881,40.881,0,0,1-7.773,4.828,41.072,41.072,0,0,1-9,3.125,31.838,31.838,0,0,1-4.544.666,22.73,22.73,0,0,1-3.948-.047,14.976,14.976,0,0,1-3.259-.689,9.625,9.625,0,0,1-2.5-1.248l-1.487-1.153-1.242-1.121s-.845-.825-1.336-1.4c-.352-.41-.861-1.086-.861-1.086h0a7.439,7.439,0,0,0,2.164,1.688,12.563,12.563,0,0,0,3.691,1.2,21.863,21.863,0,0,0,4.841.261,34.413,34.413,0,0,0,5.842-.823,46.638,46.638,0,0,0,9.8-3.438,46.049,46.049,0,0,0,8.487-5.225,29.8,29.8,0,0,0,5.949-6.056,13.013,13.013,0,0,0,2.526-5.954c.02-.2.035-.405.041-.6a4.128,4.128,0,0,0-.043-.592,7.522,7.522,0,0,1,.249.868s.158.937.216,1.543"
                                              transform="translate(372.14 -199.499)" fill="#da091d"></path>
                                    </svg>
                                </figure>

                                <h5>reddot design award</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="business-award dt">
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-awards">
                        <h4>ARABIAN BUSINESS AWARDS</h4>

                        <h5>DEVELOPER OF THE YEAR 2015</h5>
                    </button>

                    <button class="btn btn-awards">
                        <h4>ARABIAN BUSINESS AWARDS</h4>

                        <h5>DEVELOPER OF THE YEAR 2015</h5>
                    </button>

                    <button class="btn btn-awards">
                        <h4>ARABIAN BUSINESS AWARDS</h4>

                        <h5>DEVELOPER OF THE YEAR 2015</h5>
                    </button>

                    <button class="btn btn-awards">
                        <h4>ARABIAN BUSINESS AWARDS</h4>

                        <h5>DEVELOPER OF THE YEAR 2015</h5>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="partners">
    <div class="container-fluid">
        <div class="leftBox">
            <h6>
                <small>
                    PARTNERS & SUBSIDIARIES
                    <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                        <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)" fill="none"
                              stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                    </svg>
                </small>
            </h6>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 col-12">
                <div class="leftBox">
                    <h3 class="anim-head"><span class="letters">Retal's</span><br><span class="letters fst-italic">Subsidiaries</span>
                    </h3>
                </div>
            </div>

            <div class="col-md-7 col-12">
                <div class="rightBox">
                    <div class="ptSliderDv">
                        <div class="item">
                            <div class="pt-slide">
                                <figure data-anim>
                                    <img src="<?php echo $dir; ?>/assets/images/nesa.png" alt="">
                                </figure>

                                <h4>
                                    NESAJ CO.
                                    <span class="designation">Project Management</span>
                                </h4>

                                <p>
                                    Nesaj launched in 2012 in Khobar City – Kingdom of Saudi Arabia to achieve the
                                    integral vision of projects management, which diverse during the project period in
                                    terms of design management, site supervision construction management for all types
                                    of projects such as residential, commercial and educational and so on.
                                </p>

                                <p>
                                    Nesaj is committed to providing high-end services and the best innovative
                                    engineering solutions.
                                </p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="pt-slide">
                                <figure data-anim>
                                    <img src="<?php echo $dir; ?>/assets/images/nesa.png" alt="">
                                </figure>

                                <h4>
                                    NESAJ CO.
                                    <span class="designation">Project Management</span>
                                </h4>

                                <p>
                                    Launched in 2012 in Khobar City, Nesaj aims to achieve
                                    the integral vision of projects management, which is
                                    diverse in design management and site supervision
                                    construction management for all project types. Nesaj is
                                    committed to providing high-end services with praiseworthy
                                    innovative engineering solutions based on transparency and
                                    credibility in dealing.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="numberBox">
                            <span class="countFirst">
                                1
                            </span>
                        <span class="dash"></span>
                        <span class="countLast">
                                4
                            </span>
                    </div>
                </div>
            </div>
        </div>

        <hr class="dvdr">
    </div>
</section>

<section class="retal-partners">
    <div class="row">
        <div class="col-md-5 col-12">
            <div class="leftBox">
                <h3 class="anim-head"><span class="letters">Retal's</span><br><span
                            class="letters fst-italic">Partners</span></h3>
                <p>Retal have created strong partnerships, working together with its collaborators for years on some of
                    Saudi Arabia’s key developments.</p>
                <p>As the collaboration grows stronger, they energize each other to always create beautiful places that
                    inspire and engage.</p>
            </div>
        </div>

        <div class="col-md-7 col-12">
            <div class="rightBox">
                <h4>FINANCIERS</h4>

                <hr class="dvdr">

                <div class="row">
                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo1.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo2.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo3.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo4.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo5.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo6.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo7.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/partner-logo8.png" alt="">
                        </figure>
                    </div>
                </div>

                <br>
                <br>
                <br>

                <h4>DESIGNS AND CONSULTING</h4>

                <hr class="dvdr">

                <div class="row">
                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo1.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo2.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo3.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo4.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo5.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo6.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo7.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo8.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo9.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo10.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo-11.png" alt="">
                        </figure>
                    </div>

                    <div class="col-3">
                        <figure class="partner">
                            <img src="<?php echo $dir; ?>/assets/images/dnc-logo12.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="dvdr">
</section>

<section class="more-about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-12">
                <h3 class="anim-head"><span class="letters">More about</span><br><span
                            class="letters fst-italic">Retal</span></h3>
            </div>

            <div class="col-12 col-md-8 p-0">
                <div class="rightBox">
                    <div class="row">
                        <div class="col-6">
                            <div class="abBox">
                                <figure data-anim><img src="<?php echo $dir; ?>/assets/images/mba-img1.png" alt="">
                                </figure>

                                <h6>SUSTAINABILITY</h6>

                                <h4>Environmental Policy</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                                    magna aliquam erat volutpat. Ut wisi enim ad minim veniam,
                                    quis nostrud exerci tation.
                                </p>

                                <a href="#sustainabilityDv" class="btn btn-abBox popLnk">
                                    SERVING THE GENERATIONS
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                         viewBox="0 0 12.707 9.195">
                                        <g id="Group_1373" data-name="Group 1373"
                                           transform="translate(12 8.841) rotate(180)">
                                            <g id="Group_18" data-name="Group 18">
                                                <g id="Group_17" data-name="Group 17">
                                                    <line id="Line_5" data-name="Line 5" x1="12"
                                                          transform="translate(0 4.244)" fill="none" stroke="#8fb5a1"
                                                          stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_14" data-name="Path 14"
                                                          d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                          transform="translate(1519.74 -858.496)" fill="none"
                                                          stroke="#8fb5a1" stroke-miterlimit="10" stroke-width="1"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div class="col-6 pr-0">
                            <div class="abBox">
                                <figure data-anim>
                                    <img src="<?php echo $dir; ?>/assets/images/mba-img2.png" alt="">
                                </figure>

                                <h6>CORPORATE SOCIAL RESPONSIBILITY</h6>

                                <h4>Retal Al Khayer</h4>

                                <p>
                                    As a prominent real estate developer, we have responsibilities at three core levels,
                                    i.e., nation, community, and people:
                                    <br>
                                    1. Our developments inform the vision for Saudi Arabia’s future. Due to this…
                                </p>

                                <a href="#csrDv" class="btn btn-abBox popLnk">
                                    CRAFTING RESPONSIBLY
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                         viewBox="0 0 12.707 9.195">
                                        <g id="Group_1373" data-name="Group 1373"
                                           transform="translate(12 8.841) rotate(180)">
                                            <g id="Group_18" data-name="Group 18">
                                                <g id="Group_17" data-name="Group 17">
                                                    <line id="Line_5" data-name="Line 5" x1="12"
                                                          transform="translate(0 4.244)" fill="none" stroke="#8fb5a1"
                                                          stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_14" data-name="Path 14"
                                                          d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                          transform="translate(1519.74 -858.496)" fill="none"
                                                          stroke="#8fb5a1" stroke-miterlimit="10" stroke-width="1"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="abBox">
                                <figure data-anim>
                                    <img src="<?php echo $dir; ?>/assets/images/mba-img3.png" alt="">
                                </figure>

                                <h6>PRESS RELEASES, GALLERY AND MORE</h6>

                                <h4>Media Centre</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                    minim veniam, quis nostrud exerci tation.
                                </p>

                                <a href="#mediaCenter" class="btn btn-abBox popLnk">
                                    CATCH UP WITH THE LATEST
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                         viewBox="0 0 12.707 9.195">
                                        <g id="Group_1373" data-name="Group 1373"
                                           transform="translate(12 8.841) rotate(180)">
                                            <g id="Group_18" data-name="Group 18">
                                                <g id="Group_17" data-name="Group 17">
                                                    <line id="Line_5" data-name="Line 5" x1="12"
                                                          transform="translate(0 4.244)" fill="none" stroke="#8fb5a1"
                                                          stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_14" data-name="Path 14"
                                                          d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                          transform="translate(1519.74 -858.496)" fill="none"
                                                          stroke="#8fb5a1" stroke-miterlimit="10" stroke-width="1"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div class="col-6 pr-0">
                            <div class="abBox">
                                <figure data-anim>
                                    <img src="<?php echo $dir; ?>/assets/images/mba-img4.png" alt="">
                                </figure>

                                <h6>OUR THOUGHTS</h6>

                                <h4>Blog</h4>

                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                    minim veniam, quis nostrud exerci tation.
                                </p>

                                <a href="#" class="btn btn-abBox">
                                    READ OUR PUBLICATIONS
                                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                         viewBox="0 0 12.707 9.195">
                                        <g id="Group_1373" data-name="Group 1373"
                                           transform="translate(12 8.841) rotate(180)">
                                            <g id="Group_18" data-name="Group 18">
                                                <g id="Group_17" data-name="Group 17">
                                                    <line id="Line_5" data-name="Line 5" x1="12"
                                                          transform="translate(0 4.244)" fill="none" stroke="#8fb5a1"
                                                          stroke-miterlimit="10" stroke-width="1"/>
                                                    <path id="Path_14" data-name="Path 14"
                                                          d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                          transform="translate(1519.74 -858.496)" fill="none"
                                                          stroke="#8fb5a1" stroke-miterlimit="10" stroke-width="1"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Board of directors Start -->
<div id="boardOfDirectors" class="popup">
    <div class="boardDirectors">
        <div class="anchorBox">
            <div class="container">
                <a href="#" class="anchorLink closePop">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                        <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                            <g id="Group_18" data-name="Group 18">
                                <g id="Group_17" data-name="Group 17">
                                    <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_14" data-name="Path 14" d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                          transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                    BACK TO ABOUT
                </a>
            </div>
        </div>

        <section class="corpGovernance">
            <div class="container ">
                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="titleBox">
                            <h3>
                                Corporate<br>
                                <span>Governance</span>
                            </h3>
                        </div>
                    </div>

                    <div class="col-md-8 col-12">
                        <div class="content">
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.
                            </p>

                            <p>
                                Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                                consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto
                                odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="directors">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-10 col-12 p-0">
                        <h3>Board of Directors</h3>

                        <div class="row">
                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img1.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Board of directors End -->

<!-- Excutive Managemnet Start -->
<div id="executiveManagment" class="popup">
    <div class="boardDirectors">
        <div class="anchorBox">
            <div class="container">
                <a href="#" class="anchorLink closePop">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                        <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                            <g id="Group_18" data-name="Group 18">
                                <g id="Group_17" data-name="Group 17">
                                    <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_14" data-name="Path 14" d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                          transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                    BACK TO ABOUT
                </a>
            </div>
        </div>

        <section class="corpGovernance">
            <div class="container ">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="titleBox">
                            <h3>
                                Corporate<br>
                                <span>Governance</span>
                            </h3>
                        </div>
                    </div>

                    <div class="col-12 col-md-8">
                        <div class="content">
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam.
                            </p>

                            <p>
                                Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                                consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto
                                odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="directors">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2"></div>

                    <div class="col-md-10 col-12 p-0">
                        <h3>Executive Management</h3>

                        <div class="row">
                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img1.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>

                            <div class="col-6 col-md-4">
                                <div class="directorThumbs">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/director-img.png" alt="">
                                    </figure>

                                    <h4>Abdullah Faisal</h4>

                                    <h4>Abdulaziz Al-Braikan</h4>

                                    <a href="#" class="directorLink">
                                        READ BIO →
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- Excutive Managemnet End -->


<div id="corporateInfo" class="corporrateInfo popup">
    <section class="retalJourney">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="rjBox">
                        <a href="#" class="anchorLink closePop">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                 viewBox="0 0 12.707 9.195">
                                <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                                    <g id="Group_18" data-name="Group 18">
                                        <g id="Group_17" data-name="Group 17">
                                            <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)"
                                                  fill="none" stroke="#000" stroke-miterlimit="10"
                                                  stroke-width="1"></line>
                                            <path id="Path_14" data-name="Path 14"
                                                  d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                  transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                                  stroke-miterlimit="10" stroke-width="1"></path>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            BACK TO ABOUT
                        </a>

                        <h3 class="title">The Retal<br><span>Journey</span></h3>

                        <h5>The transformation journey started in 2012 in Saudi Arabia’s Eastern Province when Retal
                            Urban Development joined the stellar Al Fozan Group of Companies and began to develop
                            residential, commercial and mixed-use properties.</h5>
                        <p>Retal represents the quintessence of craftsmanship by embracing the virtues of urbanism. The
                            last ten years have seen us reimagine properties using a holistic approach underpinning a
                            unified visionary design principle and creating meaningful destinations with urban real
                            estate solutions. Today, our total asset value accounts for SAR 7+ billion, with 7000+ units
                            developed or under construction.</p>
                        <p>In support of Saudi’s vision 2030, we have taken unprecedented steps in transforming the
                            nation into a futuristic destination, ideal to live, work and visit. By exemplifying the key
                            attributes of placemaking attributes, we envision revolutionising the Kingdom into a top
                            destination of choice. Furthermore, the real estate value chain has been unified in the
                            Retal Group via acquisition, establishments, and strategic joint ventures to deliver unique
                            products with superior quality. This includes Mimar, Arac Consulting Engineers, Bcc, Tadbier
                            and more.</p>
                    </div>

                    <div class="rjRightBottom mb">
                        <div class="countDv">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <h5>Total number of projects</h5>
                                    <p>156</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h5>Total built up area</h5>
                                    <p>10,179 Sq.M.</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h5>Total project value</h5>
                                    <p>12.5 Billion SAR</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h5>Units under construction</h5>
                                    <p>8,028</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-8">
                    <div class="rjRightBox">
                        <figure data-anim>
                            <img src="<?php echo $dir; ?>/assets/images/retal-journey-img.png" alt="">
                        </figure>
                    </div>

                    <div class="rjRightBottom dt">
                        <div class="countDv">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <h5>Total number of projects</h5>
                                    <p>156</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h5>Total built up area</h5>
                                    <p>10,179 Sq.M.</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h5>Total project value</h5>
                                    <p>12.5 Billion SAR</p>
                                </div>
                                <div class="col-md-6 col-6">
                                    <h5>Units under construction</h5>
                                    <p>8,028</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="timeLine">
        <div class="container">
            <h4>TIMELINE</h4>
            <div class="timelineSliderDv">
                <div class="item" data-year="1960">
                    <div class="row mb-4">
                        <div class="col-md-8 col-12">
                            <figure class="slideImg"><img src="assets/images/timeline-slide-img.png"/></figure>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>1960</h1>
                                    <p>Year of<br>Establishment</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="slideTitle">
                                <h3>Leader in the<br>Kingdom of Saudi Arabia.</h3>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="slideDesc">
                                <p>In 2012 Retal for Structural Development, was founded with remarkable capabilities
                                    and professional work team, to join Al Fozan group of companies. Dolor set di oder
                                    luyter.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item" data-year="1970">
                    <div class="row mb-4">
                        <div class="col-md-8 col-12">
                            <figure class="slideImg"><img src="assets/images/feture-slider-01.png"/></figure>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>1960</h1>
                                    <p>
                                        Year of<br>
                                        Establishment
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="slideTitle">
                                <h3>
                                    Leader in the<br>
                                    Kingdom of Saudi Arabia.
                                </h3>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="slideDesc">
                                <p>
                                    In 2012 Retal for Structural Development, was founded with remarkable capabilities
                                    and professional work team, to join Al Fozan group of companies. Dolor set di oder
                                    luyter.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item" data-year="1980">
                    <div class="row mb-4">
                        <div class="col-md-8 col-12">
                            <figure class="slideImg"><img src="assets/images/feture-slider-01.png"/></figure>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>1960</h1>
                                    <p>Year of<br>Establishment</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="slideTitle">
                                <h3>Leader in the<br>Kingdom of Saudi Arabia.</h3>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="slideDesc">
                                <p>In 2012 Retal for Structural Development, was founded with remarkable capabilities
                                    and professional work team, to join Al Fozan group of companies. Dolor set di oder
                                    luyter.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item" data-year="2022">
                    <div class="row mb-4">
                        <div class="col-md-8 col-12">
                            <figure class="slideImg"><img src="assets/images/feture-slider-01.png"/></figure>
                        </div>

                        <div class="col-md-4 col-12">
                            <div class="yearOfEstablish">
                                <div class="yoe-flex">
                                    <h1>1960</h1>
                                    <p>Year of<br>Establishment</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="slideTitle">
                                <h3>Leader in the<br>Kingdom of Saudi Arabia.</h3>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="slideDesc">
                                <p>In 2012 Retal for Structural Development, was founded with remarkable capabilities
                                    and professional work team, to join Al Fozan group of companies. Dolor set di oder
                                    luyter.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="investManagement">
        <div class="container">
            <div class="row g-0">
                <div class="col-md-7 col-12 d-flex justify-content-center align-items-center">
                    <div class="leftBox">
                        <!--<figure data-anim>
                                <img src="<?php /*echo $dir; */ ?>/assets/images/invets-manage-img.png" alt="">
                            </figure>-->
                        <div class="diagramDv">
                            <div class="logo">
                                <svg xmlns="http://www.w3.org/2000/svg" width="53.293" height="58.003"
                                     viewBox="0 0 53.293 58.003">
                                    <g id="Group_184" data-name="Group 184" transform="translate(-52.728 -30.798)">
                                        <g id="Group_181" data-name="Group 181" transform="translate(52.728 30.798)">
                                            <g id="Group_179" data-name="Group 179" transform="translate(0 4.671)">
                                                <path id="Path_55" data-name="Path 55"
                                                      d="M93.259,65.685c-4.345,2.814-9.465,7.668-13.387,7.668A3.882,3.882,0,0,1,75.8,70.031a4.89,4.89,0,0,1-.05-.7c.116-3.6,2.4-6.6,3.748-8.061-.116-.427-.209-.843-.281-1.22-.423-2.23-2.1-14.643-2.1-14.643s-.957-7.333-6.242-8.83c.136.849,3.076,15.248,3.566,17.834a24.47,24.47,0,0,1,.227,5.706c-.291,1.94-2.079,3.871-4.583,5.617,0,0-9.16,6.585-17.356,7.515v4.722H98.8V63.133a17.029,17.029,0,0,0-5.537,2.552"
                                                      transform="translate(-52.729 -36.572)" fill="#9e7156"/>
                                                <g id="Group_178" data-name="Group 178" transform="translate(0 46.064)">
                                                    <path id="Path_56" data-name="Path 56"
                                                          d="M57.635,100.205c-.153-.181-.317-.379-.49-.6s-.356-.446-.543-.683l-.543-.684q-.262-.326-.472-.6l-.216-.277a3.868,3.868,0,0,0,.481-.138,2.738,2.738,0,0,0,.7-.366,2.158,2.158,0,0,0,.557-.605,1.847,1.847,0,0,0,.053-1.7,1.448,1.448,0,0,0-.489-.528,2.1,2.1,0,0,0-.706-.282,3.965,3.965,0,0,0-.828-.083l-.311,0c-.347,0-.905,0-1.344,0s-.591,0-.629-.006l-.125-.009.006.125q.03.664.054,1.3c.017.41.025.834.025,1.3v1.728c0,.457-.008.9-.025,1.3-.016.43-.054,1.267-.054,1.267l-.006.117h1.22l-.006-.117s-.039-.837-.056-1.267-.024-.865-.024-1.3v-.621h.222c.2.208.419.46.669.767s.51.638.758.959.472.621.676.9.457.633.457.633l.034.047h1.5L58,100.613S57.788,100.384,57.635,100.205Zm-3.773-3.819c0-.351.006-.716.019-1.082l.036-1c.085-.009.183-.017.292-.023.148-.011.336-.015.576-.015a2.637,2.637,0,0,1,.51.053,1.349,1.349,0,0,1,.464.19,1.051,1.051,0,0,1,.341.37,1.186,1.186,0,0,1,.131.593,1.292,1.292,0,0,1-.168.688,1.36,1.36,0,0,1-.432.437,1.772,1.772,0,0,1-.583.233,3.761,3.761,0,0,1-.986.057,1.774,1.774,0,0,1-.2-.021Z"
                                                          transform="translate(-52.728 -93.54)" fill="#53575a"/>
                                                    <path id="Path_57" data-name="Path 57"
                                                          d="M69.362,100.08c-.217.016-.447.03-.691.04s-.491.015-.744.015h-.6c0-.117-.008-.268-.01-.452,0-.248,0-.563,0-.944v-1c0-.1,0-.191,0-.26,0-.032,0-.061,0-.09.1,0,.229,0,.375,0,.2,0,.423.007.654.01s.46.012.689.024.6.051.6.051l.122.011v-.858l-.121.011s-.3.024-.466.035l-.371.021-.165.009c-.193.009-.407.019-.641.025-.2.006-.43.009-.68.009,0-.117,0-.269,0-.452,0-.247,0-.5,0-.763,0-.245,0-.465.014-.657q.013-.243.022-.507.421,0,.732.015c.233.011.448.022.646.036s.384.027.562.04.589.039.589.039L70,94.5v-.839l-.116,0s-.6.025-.894.036c-.588.019-1.194.019-1.787,0-.3-.011-.6-.022-.9-.036l-.123-.006.006.123c.021.429.039.858.056,1.286s.024.859.024,1.3v1.73c0,.443-.008.879-.024,1.3s-.035.852-.056,1.281l-.006.123.123-.006c.293-.014.591-.02.909-.02h1.825c.3,0,.9.02.9.02l.116,0v-.822l-.131.022S69.572,100.065,69.362,100.08Z"
                                                          transform="translate(-55.297 -93.54)" fill="#53575a"/>
                                                    <path id="Path_58" data-name="Path 58"
                                                          d="M81.62,93.707c-.842.019-1.745.019-2.587,0-.422-.011-1.3-.036-1.3-.036l-.116,0v.875l.126-.015s.3-.036.472-.049.36-.025.55-.035.38-.016.57-.02l.437,0c.019.668.028,1.33.028,1.97v1.728c0,.423-.008.862-.025,1.3s-.055,1.267-.055,1.267l-.006.117h1.22l-.006-.117s-.039-.837-.056-1.267-.025-.872-.025-1.3V96.388c0-.651.009-1.314.028-1.97l.437,0q.284.006.569.02c.191.01.375.022.551.035s.472.049.472.049l.125.015v-.875l-.115,0S82.043,93.7,81.62,93.707Z"
                                                          transform="translate(-57.482 -93.541)" fill="#53575a"/>
                                                    <path id="Path_59" data-name="Path 59"
                                                          d="M95.89,99.911q-.206-.462-.463-1.051c-.17-.39-.353-.815-.547-1.269s-.389-.915-.582-1.377-.386-.921-.574-1.372-.362-.873-.522-1.261l-.029-.07h-.339l-.029.067q-.785,1.8-1.558,3.539c-.51,1.145-1.578,3.474-1.588,3.5l-.073.159h1l.024-.078c0-.007.219-.7.39-1.167s.354-.931.555-1.422q.273-.009.539-.012c.413-.006.845-.006,1.273,0,.2,0,.388.006.578.012.08.2.165.413.256.632l.3.723c.1.237.185.464.265.681s.189.553.189.553l.026.078h1.328l-.083-.163S96.024,100.214,95.89,99.911Zm-2.175-2.484c-.133,0-.265.007-.4.009-.361.007-.713.008-1.1,0-.129,0-.255-.006-.378-.008l.928-2.162Z"
                                                          transform="translate(-59.768 -93.511)" fill="#53575a"/>
                                                    <path id="Path_60" data-name="Path 60"
                                                          d="M108.606,100.007c-.01,0-.972.105-1.4.114-.4.011-.819.015-1.254.015q-.01-.693-.019-1.553-.01-.93-.01-2.195c0-.447.009-.883.024-1.3s.056-1.3.056-1.3l0-.117h-1.219l.006.117s.039.875.055,1.3.025.861.025,1.3v1.729c0,.436-.008.875-.025,1.3s-.035.85-.055,1.265l-.006.122.122,0c.333-.014.636-.02.926-.02h1.855c.307,0,.921.02.921.02l.125,0v-.813Z"
                                                          transform="translate(-62.671 -93.542)" fill="#53575a"/>
                                                </g>
                                            </g>
                                            <g id="Group_180" data-name="Group 180" transform="translate(0.003)">
                                                <path id="Path_61" data-name="Path 61"
                                                      d="M52.732,30.8v39.12c8.763-1.264,16.534-6.826,16.534-6.826,3.1-2.021,4.757-3.8,4.757-4.217a57.118,57.118,0,0,0-1.16-5.929S69.7,37.209,69.473,35.7a4.542,4.542,0,0,1,.67-2.853s6.768.236,8.258,11.38,1.722,13.821,2.346,15.4,1.218,2.823,2.734,2.495a2.921,2.921,0,0,0,1.752-1.159,6.111,6.111,0,0,0-1.539-2.773l.227-2.431a7.268,7.268,0,0,1,2.006,4.506c0,2.5-.8,3.813-2.967,4.126-1.744.25-2.834-2-3.414-4.043a19.15,19.15,0,0,0-2.73,6.122,3.651,3.651,0,0,0-.05.993,2.512,2.512,0,0,0,2.691,2.509c2.233,0,6.161-2.556,10.95-5.771a24.329,24.329,0,0,1,8.391-4.3V30.8ZM80.3,40.971h0c-.175-.08-.2-.163-.132-.388a5.506,5.506,0,0,1,.212-.557c.092-.214.217-.29.442-.21a2.337,2.337,0,0,1,.624.333,3.54,3.54,0,0,1,1.181,1.705c.065.184.2.551.2.551l.057.133s.2-1.319.313-1.9a7,7,0,0,1,.251-.835,1.1,1.1,0,0,1,.129-.246c.222-.341.481-.481.743-.408.334.093.747.521.714.825a4.079,4.079,0,0,1-.279.812c-.148-.193-.236-.316-.334-.433-.33-.391-.587-.39-.808.068a4.792,4.792,0,0,0-.335,1.133c-.124.622-.2,1.252-.311,1.878a1.162,1.162,0,0,1-.193.421A8.2,8.2,0,0,0,81.338,41.2a.292.292,0,0,1-.327.091h0a.182.182,0,0,1-.035-.013l0,0C80.829,41.213,80.3,40.971,80.3,40.971ZM84.635,53l-2.491-1.459.916-1.567-2.192-1.288,1.174-2.006,2.492,1.462-.915,1.568,2.2,1.287Z"
                                                      transform="translate(-52.732 -30.798)" fill="#9e7156"/>
                                            </g>
                                        </g>
                                        <g id="Group_182" data-name="Group 182" transform="translate(101.194 74.398)">
                                            <path id="Path_62" data-name="Path 62"
                                                  d="M113.4,85.656q0-.362-.011-.726l-.191,0c-.066,0-.133,0-.2.006l-.193.013c-.061,0-.167.018-.167.018v-.221l.452.012c.15,0,.3,0,.452,0s.3,0,.452,0l.452-.012v.221s-.1-.014-.167-.018l-.193-.013c-.066,0-.133,0-.2-.006l-.191,0q-.011.364-.011.726v.6c0,.152,0,.3.009.455s.019.454.019.454h-.344s.014-.3.019-.454.009-.3.009-.455Z"
                                                  transform="translate(-112.636 -84.702)" fill="#9e7156"/>
                                            <path id="Path_63" data-name="Path 63"
                                                  d="M115.567,84.691q.241.5.489.968t.517.964c.024-.044.057-.109.1-.193s.1-.18.155-.286l.189-.347c.068-.125.137-.251.2-.379s.138-.253.2-.378.129-.242.188-.349h.045c.026.224.05.442.074.65s.046.415.068.617.045.4.069.6.077.6.077.6h-.329s-.052-.621-.077-.916-.057-.592-.1-.894h-.006q-.241.442-.475.879t-.443.913h-.032q-.206-.438-.438-.885t-.463-.907h-.007q-.024.218-.054.472c-.02.17-.038.337-.054.5s-.031.321-.042.468-.018.368-.018.368h-.245s.061-.4.095-.608.063-.424.093-.637.057-.422.084-.629.049-.4.068-.591Z"
                                                  transform="translate(-113.119 -84.691)" fill="#9e7156"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <h3>Investment Management</h3>
                            <h3>Asset Management</h3>
                            <h3>Design &amp; Construction</h3>
                            <h3>Acquisitions &amp; Development</h3>
                            <p>Integrated Living</p>
                            <p>Placemaking</p>
                            <p>Community Cohesion</p>
                            <p>Sustainability</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-12">
                    <div class="rightBox">
                        <h3>Investment & Asset Management </h3>

                        <p>
                            With operational expertise and market trend analysis, we can evaluate an asset’s intrinsic
                            value, as well as cost of acquisition, development, and enhancing the financial performance
                            of properties.
                        </p>

                        <h3>Design and Construction</h3>

                        <p>
                            We build highly integrated residential communities with innovative designs, premium quality
                            and full access to amenities.
                        </p>

                        <h3>Acquisition and Development</h3>

                        <p>
                            We are strategically focused on acquiring premier locations in Saudi Arabia, which has
                            strong economic and favourable demographics long-term.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="whyRetal animToStart">
        <div class="container">
            <h4>Our Goals</h4>
            <h3><span><span>Why </span></span><span><span>Retal?</span></span></h3>

            <p class="desc">
                We ensure our developments are in coherence with the strategic vision to maximise shareholders wealth
                and contribute to the nation’s economy.
            </p>

            <div class="iconCircleDv">
                <div class="circleRow">
                    <div class="circleDv">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.042" height="31.2"
                                 viewBox="0 0 11.042 31.2">
                                <g id="Group_1387" data-name="Group 1387" transform="translate(-6620.575 1684.997)">
                                    <line id="Line_270" data-name="Line 270" y1="5.558"
                                          transform="translate(6626.096 -1684.997)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_549" data-name="Path 549"
                                          d="M6621.075-1662.895a5.02,5.02,0,0,0,5.021,5.02,5.021,5.021,0,0,0,5.021-5.02,5.021,5.021,0,0,0-5.021-5.021,5.021,5.021,0,0,1-5.021-5.021,5.02,5.02,0,0,1,5.021-5.02,5.021,5.021,0,0,1,5.021,5.02"
                                          transform="translate(0 -1.481)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_271" data-name="Line 271" y1="5.558"
                                          transform="translate(6626.096 -1659.355)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </div>
                        <h5>High Investment <br/>Returns</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 0.4s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.213" height="27.663"
                                 viewBox="0 0 17.213 27.663">
                                <path id="Path_47" data-name="Path 47"
                                      d="M964.417,5426.267l-1.433-12.333h3.771l.72-7.973h-2.28l-.86,2.389h-2.389l-.669-3.345h-2.453l-.769,3.345h-2.389l-.86-2.389h-2.28l.72,7.973h3.771l-1.433,12.333h-3.251l-.4,5.4h16.137l-.4-5.4Z"
                                      transform="translate(-951.393 -5404.505)" fill="none" stroke="#000"
                                      stroke-miterlimit="10" stroke-width="1"/>
                            </svg>

                        </div>
                        <h5>Strategic <br/>Locations</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 0.8s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24.304" height="24.178"
                                 viewBox="0 0 24.304 24.178">
                                <path id="Path_23" data-name="Path 23"
                                      d="M1211.336,5419.178l-.024,6.385a3,3,0,0,0,2.978,3.011l7.086.05a3,3,0,0,0,2.142-.878l10.219-10.219a3,3,0,0,0,0-4.243l-6.748-6.748a3,3,0,0,0-4.242,0l-10.533,10.532A3,3,0,0,0,1211.336,5419.178Z"
                                      transform="translate(-1210.812 -5404.946)" fill="none" stroke="#000"
                                      stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_17" data-name="Ellipse 17" cx="1.969" cy="1.969" r="1.969"
                                        transform="translate(4.875 15.701)" fill="none" stroke="#000"
                                        stroke-miterlimit="10" stroke-width="1"/>
                            </svg>

                        </div>
                        <h5>Intelligent <br/>Price</h5>
                    </div>
                </div>
                <div class="circleRow">
                    <div class="txtDv">
                        <p>We ensure our developments are in coherence with the strategic vision to maximise
                            shareholders wealth and contribute to the nation’s economy.</p>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 1.2s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="25.629" height="19.703"
                                 viewBox="0 0 25.629 19.703">
                                <g id="Group_83" data-name="Group 83" transform="translate(-824.137 -5723.339)">
                                    <path id="Path_25" data-name="Path 25"
                                          d="M843.34,5740.191v2.351h-18.7v-2.351c0-3.117,6.235-4.649,9.352-4.649S843.34,5737.074,843.34,5740.191Zm-13.414-12.241a4.063,4.063,0,1,1,4.063,4.063,4.234,4.234,0,0,1-2.868-1.184A3.88,3.88,0,0,1,829.926,5727.95Z"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                                <path id="Path_46" data-name="Path 46"
                                      d="M848.639,5728.1a2.108,2.108,0,0,0,.627-1.431,3.747,3.747,0,0,0-6.928,0,2.086,2.086,0,0,0,.61,1.429l2.854,3.124Z"
                                      transform="translate(-824.137 -5723.339)" fill="none" stroke="#000"
                                      stroke-miterlimit="10" stroke-width="1"/>
                            </svg>

                        </div>
                        <h5>Charismatic <br/>Leadership</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 01.6s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="13.74" height="27.91"
                                 viewBox="0 0 13.74 27.91">
                                <g id="Group_142" data-name="Group 142" transform="translate(-692.408 -5403.531)">
                                    <circle id="Ellipse_30" data-name="Ellipse 30" cx="2.543" cy="2.543" r="2.543"
                                            transform="translate(696.735 5407.951)" fill="none" stroke="#000"
                                            stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_19" data-name="Line 19" y2="4.419"
                                          transform="translate(699.278 5403.531)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_20" data-name="Line 20" x1="6.236" y2="18.242"
                                          transform="translate(693.042 5413.037)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_21" data-name="Line 21" x2="6.236" y2="18.242"
                                          transform="translate(699.278 5413.037)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_22" data-name="Line 22" x2="3"
                                          transform="translate(692.408 5423.037)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_23" data-name="Line 23" x2="3"
                                          transform="translate(703.148 5423.037)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>

                        </div>
                        <h5>High <br/>Quality</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 2s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.415" height="25.341"
                                 viewBox="0 0 22.415 25.341">
                                <g id="Group_1388" data-name="Group 1388" transform="translate(-6646.336 1677.588)">
                                    <path id="Path_550" data-name="Path 550"
                                          d="M6668.251-1652.247v-13.926l-10.708-10.708-10.708,10.708v13.926" fill="none"
                                          stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_551" data-name="Path 551"
                                          d="M6657.544-1662.845l2.231-2.232a3.157,3.157,0,0,1,4.463,0h0a3.156,3.156,0,0,1,0,4.463l-6.694,6.694-6.694-6.694a3.157,3.157,0,0,1,0-4.463h0a3.157,3.157,0,0,1,4.463,0Z"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>

                        </div>
                        <h5>Enhanced <br/>Lifestyle</h5>
                    </div>
                </div>
            </div>

            <!-- made by adnan -->
            <div class="circleDvMobile">
                <div class="circleBox">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="11.042" height="31.2" viewBox="0 0 11.042 31.2">
                            <g id="Group_1387" data-name="Group 1387" transform="translate(-6620.575 1684.997)">
                                <line id="Line_270" data-name="Line 270" y1="5.558"
                                      transform="translate(6626.096 -1684.997)" fill="none" stroke="#fff"
                                      stroke-miterlimit="10" stroke-width="1"/>
                                <path id="Path_549" data-name="Path 549"
                                      d="M6621.075-1662.895a5.02,5.02,0,0,0,5.021,5.02,5.021,5.021,0,0,0,5.021-5.02,5.021,5.021,0,0,0-5.021-5.021,5.021,5.021,0,0,1-5.021-5.021,5.02,5.02,0,0,1,5.021-5.02,5.021,5.021,0,0,1,5.021,5.02"
                                      transform="translate(0 -1.481)" fill="none" stroke="#fff" stroke-miterlimit="10"
                                      stroke-width="1"/>
                                <line id="Line_271" data-name="Line 271" y1="5.558"
                                      transform="translate(6626.096 -1659.355)" fill="none" stroke="#fff"
                                      stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </div>
                    <h5>High Investment <br/>Returns</h5>
                </div>

                <div class="circleBox" data-inlinecss="transition-delay: 0.4s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17.213" height="27.663"
                             viewBox="0 0 17.213 27.663">
                            <path id="Path_47" data-name="Path 47"
                                  d="M964.417,5426.267l-1.433-12.333h3.771l.72-7.973h-2.28l-.86,2.389h-2.389l-.669-3.345h-2.453l-.769,3.345h-2.389l-.86-2.389h-2.28l.72,7.973h3.771l-1.433,12.333h-3.251l-.4,5.4h16.137l-.4-5.4Z"
                                  transform="translate(-951.393 -5404.505)" fill="none" stroke="#000"
                                  stroke-miterlimit="10" stroke-width="1"/>
                        </svg>

                    </div>
                    <h5>Strategic <br/>Locations</h5>
                </div>
                <div class="circleBox" data-inlinecss="transition-delay: 0.8s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24.304" height="24.178"
                             viewBox="0 0 24.304 24.178">
                            <path id="Path_23" data-name="Path 23"
                                  d="M1211.336,5419.178l-.024,6.385a3,3,0,0,0,2.978,3.011l7.086.05a3,3,0,0,0,2.142-.878l10.219-10.219a3,3,0,0,0,0-4.243l-6.748-6.748a3,3,0,0,0-4.242,0l-10.533,10.532A3,3,0,0,0,1211.336,5419.178Z"
                                  transform="translate(-1210.812 -5404.946)" fill="none" stroke="#000"
                                  stroke-miterlimit="10" stroke-width="1"/>
                            <circle id="Ellipse_17" data-name="Ellipse 17" cx="1.969" cy="1.969" r="1.969"
                                    transform="translate(4.875 15.701)" fill="none" stroke="#000" stroke-miterlimit="10"
                                    stroke-width="1"/>
                        </svg>

                    </div>
                    <h5>Intelligent <br/>Price</h5>
                </div>

                <div class="circleBox" data-inlinecss="transition-delay: 1.2s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25.629" height="19.703"
                             viewBox="0 0 25.629 19.703">
                            <g id="Group_83" data-name="Group 83" transform="translate(-824.137 -5723.339)">
                                <path id="Path_25" data-name="Path 25"
                                      d="M843.34,5740.191v2.351h-18.7v-2.351c0-3.117,6.235-4.649,9.352-4.649S843.34,5737.074,843.34,5740.191Zm-13.414-12.241a4.063,4.063,0,1,1,4.063,4.063,4.234,4.234,0,0,1-2.868-1.184A3.88,3.88,0,0,1,829.926,5727.95Z"
                                      fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                            <path id="Path_46" data-name="Path 46"
                                  d="M848.639,5728.1a2.108,2.108,0,0,0,.627-1.431,3.747,3.747,0,0,0-6.928,0,2.086,2.086,0,0,0,.61,1.429l2.854,3.124Z"
                                  transform="translate(-824.137 -5723.339)" fill="none" stroke="#000"
                                  stroke-miterlimit="10" stroke-width="1"/>
                        </svg>

                    </div>
                    <h5>Charismatic <br/>Leadership</h5>
                </div>
                <div class="circleBox" data-inlinecss="transition-delay: 01.6s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13.74" height="27.91" viewBox="0 0 13.74 27.91">
                            <g id="Group_142" data-name="Group 142" transform="translate(-692.408 -5403.531)">
                                <circle id="Ellipse_30" data-name="Ellipse 30" cx="2.543" cy="2.543" r="2.543"
                                        transform="translate(696.735 5407.951)" fill="none" stroke="#000"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_19" data-name="Line 19" y2="4.419"
                                      transform="translate(699.278 5403.531)" fill="none" stroke="#000"
                                      stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_20" data-name="Line 20" x1="6.236" y2="18.242"
                                      transform="translate(693.042 5413.037)" fill="none" stroke="#000"
                                      stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_21" data-name="Line 21" x2="6.236" y2="18.242"
                                      transform="translate(699.278 5413.037)" fill="none" stroke="#000"
                                      stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_22" data-name="Line 22" x2="3" transform="translate(692.408 5423.037)"
                                      fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_23" data-name="Line 23" x2="3" transform="translate(703.148 5423.037)"
                                      fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>

                    </div>
                    <h5>High <br/>Quality</h5>
                </div>
                <div class="circleBox" data-inlinecss="transition-delay: 2s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="22.415" height="25.341"
                             viewBox="0 0 22.415 25.341">
                            <g id="Group_1388" data-name="Group 1388" transform="translate(-6646.336 1677.588)">
                                <path id="Path_550" data-name="Path 550"
                                      d="M6668.251-1652.247v-13.926l-10.708-10.708-10.708,10.708v13.926" fill="none"
                                      stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <path id="Path_551" data-name="Path 551"
                                      d="M6657.544-1662.845l2.231-2.232a3.157,3.157,0,0,1,4.463,0h0a3.156,3.156,0,0,1,0,4.463l-6.694,6.694-6.694-6.694a3.157,3.157,0,0,1,0-4.463h0a3.157,3.157,0,0,1,4.463,0Z"
                                      fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>

                    </div>
                    <h5>Enhanced <br/>Lifestyle</h5>
                </div>
            </div>
        </div>
    </section>

    <section class="ourVison">
        <div class="container">
            <div class="ourVisionTitle">
                <h6>
                    <small>
                        OUR VISION
                        <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                            <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)" fill="none"
                                  stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"></line>
                        </svg>
                    </small>
                </h6>
            </div>
        </div>

        <div class="theVisionDv">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="leftBox">
                        <figure data-anim>
                            <img src="<?php echo $dir; ?>/assets/images/our-vision-img.png" alt="">
                        </figure>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="rightBox">
                        <div class="rbFlex">
                            <h3>
                                The <span>Vision</span>
                            </h3>

                            <p>
                                As creative visionaries, we aim to shape the future by building future-proof
                                developments that stand the test of time. We transform vacant plots into landscape
                                poetry and create future possibilities. Our properties are classy, upscale and are built
                                to last.
                            </p>

                            <p>
                                We create vibrancy, diversity, inclusion and provide a meaningful quality of life to the
                                people. A significant purpose of business drives our vision. As a result, our
                                communities hold the reputation of being the next-generation urban development in Saudi
                                Arabia.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="values">
        <div class="container">
            <div class="valueBox">
                <h6>
                    <small>
                        VALUES
                        <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1" viewBox="0 0 31.652 1">
                            <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)" fill="none"
                                  stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"></line>
                        </svg>
                    </small>
                </h6>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Universal <i>Brand Values</i></h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="valueSliderDv">
            <div class="item">
                <div class="roundedBox">
                    <figure data-anim>
                        <svg xmlns="http://www.w3.org/2000/svg" width="138.363" height="138.363"
                             viewBox="0 0 138.363 138.363">
                            <g id="Group_1544" data-name="Group 1544" transform="translate(-2604.42 -5992.576)">
                                <circle id="Ellipse_183" data-name="Ellipse 183" cx="9.989" cy="9.989" r="9.989"
                                        transform="translate(2604.92 6110.462)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_184" data-name="Ellipse 184" cx="18.373" cy="18.373" r="18.373"
                                        transform="translate(2604.92 6093.692)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_185" data-name="Ellipse 185" cx="26.758" cy="26.758" r="26.758"
                                        transform="translate(2604.92 6076.923)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_186" data-name="Ellipse 186" cx="35.143" cy="35.143" r="35.143"
                                        transform="translate(2604.92 6060.154)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_187" data-name="Ellipse 187" cx="43.527" cy="43.527" r="43.527"
                                        transform="translate(2604.92 6043.384)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_188" data-name="Ellipse 188" cx="51.912" cy="51.912" r="51.912"
                                        transform="translate(2604.92 6026.615)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_189" data-name="Ellipse 189" cx="60.297" cy="60.297" r="60.297"
                                        transform="translate(2604.92 6009.846)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_190" data-name="Ellipse 190" cx="68.681" cy="68.681" r="68.681"
                                        transform="translate(2604.92 5993.076)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </figure>

                    <div class="content">
                        <h5>Mastery</h5>

                        <p>
                            We explore new territories and aesthetics to implement craft mastery for iconic precincts
                            and desirable communities.
                        </p>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="roundedBox">
                    <figure data-anim>
                        <svg xmlns="http://www.w3.org/2000/svg" width="138.363" height="138.363"
                             viewBox="0 0 138.363 138.363">
                            <g id="Group_1544" data-name="Group 1544" transform="translate(-2604.42 -5992.576)">
                                <circle id="Ellipse_183" data-name="Ellipse 183" cx="9.989" cy="9.989" r="9.989"
                                        transform="translate(2604.92 6110.462)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_184" data-name="Ellipse 184" cx="18.373" cy="18.373" r="18.373"
                                        transform="translate(2604.92 6093.692)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_185" data-name="Ellipse 185" cx="26.758" cy="26.758" r="26.758"
                                        transform="translate(2604.92 6076.923)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_186" data-name="Ellipse 186" cx="35.143" cy="35.143" r="35.143"
                                        transform="translate(2604.92 6060.154)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_187" data-name="Ellipse 187" cx="43.527" cy="43.527" r="43.527"
                                        transform="translate(2604.92 6043.384)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_188" data-name="Ellipse 188" cx="51.912" cy="51.912" r="51.912"
                                        transform="translate(2604.92 6026.615)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_189" data-name="Ellipse 189" cx="60.297" cy="60.297" r="60.297"
                                        transform="translate(2604.92 6009.846)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_190" data-name="Ellipse 190" cx="68.681" cy="68.681" r="68.681"
                                        transform="translate(2604.92 5993.076)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </figure>

                    <div class="content">
                        <h5>Mastery</h5>

                        <p>
                            We explore new territories and aesthetics to implement craft mastery for iconic precincts
                            and desirable communities.
                        </p>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="roundedBox">
                    <figure data-anim>
                        <svg xmlns="http://www.w3.org/2000/svg" width="137.579" height="138.363"
                             viewBox="0 0 137.579 138.363">
                            <g id="Group_1543" data-name="Group 1543" transform="translate(-2604.903 -5749.171)">
                                <ellipse id="Ellipse_176" data-name="Ellipse 176" cx="40.959" cy="24.356" rx="40.959"
                                         ry="24.356" transform="translate(2632.734 5749.671)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_177" data-name="Ellipse 177" cx="40.959" cy="24.356" rx="40.959"
                                         ry="24.356" transform="translate(2632.734 5838.321)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_178" data-name="Ellipse 178" cx="50.526" cy="31.071" rx="50.526"
                                         ry="31.071" transform="translate(2623.167 5757.835)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_179" data-name="Ellipse 179" cx="50.526" cy="31.071" rx="50.526"
                                         ry="31.071" transform="translate(2623.167 5816.944)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_180" data-name="Ellipse 180" cx="59.595" cy="37.881" rx="59.595"
                                         ry="37.881" transform="translate(2614.097 5765.771)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_181" data-name="Ellipse 181" cx="68.29" cy="44.325" rx="68.29"
                                         ry="44.325" transform="translate(2605.403 5774.027)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_182" data-name="Ellipse 182" cx="59.595" cy="37.816" rx="59.595"
                                         ry="37.816" transform="translate(2614.097 5795.474)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </figure>

                    <div class="content">
                        <h5>Mastery</h5>

                        <p>
                            We explore new territories and aesthetics to implement craft mastery for iconic precincts
                            and desirable communities.
                        </p>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="roundedBox">
                    <figure data-anim>
                        <svg xmlns="http://www.w3.org/2000/svg" width="138.363" height="138.363"
                             viewBox="0 0 138.363 138.363">
                            <g id="Group_1542" data-name="Group 1542" transform="translate(-2595.229 -5496.666)">
                                <rect id="Rectangle_447" data-name="Rectangle 447" width="80.693" height="80.693"
                                      transform="translate(2721.468 5565.848) rotate(135)" fill="none" stroke="#9e7156"
                                      stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_168" data-name="Ellipse 168" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2664.604 5508.983)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_169" data-name="Ellipse 169" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2664.604 5566.042)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_170" data-name="Ellipse 170" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2607.546 5508.983)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_171" data-name="Ellipse 171" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2607.546 5566.042)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_172" data-name="Ellipse 172" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2676.421 5537.513)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_173" data-name="Ellipse 173" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2636.075 5577.859)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_174" data-name="Ellipse 174" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2636.075 5497.166)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_175" data-name="Ellipse 175" cx="28.335" cy="28.335" r="28.335"
                                        transform="translate(2595.729 5537.513)" fill="none" stroke="#9e7156"
                                        stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </figure>

                    <div class="content">
                        <h5>Mastery</h5>

                        <p>
                            We explore new territories and aesthetics to implement craft mastery for iconic precincts
                            and desirable communities.
                        </p>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="roundedBox">
                    <figure data-anim>
                        <svg xmlns="http://www.w3.org/2000/svg" width="138.363" height="138.363"
                             viewBox="0 0 138.363 138.363">
                            <g id="Group_1544" data-name="Group 1544" transform="translate(-2604.42 -5992.576)">
                                <circle id="Ellipse_183" data-name="Ellipse 183" cx="9.989" cy="9.989" r="9.989"
                                        transform="translate(2604.92 6110.462)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_184" data-name="Ellipse 184" cx="18.373" cy="18.373" r="18.373"
                                        transform="translate(2604.92 6093.692)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_185" data-name="Ellipse 185" cx="26.758" cy="26.758" r="26.758"
                                        transform="translate(2604.92 6076.923)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_186" data-name="Ellipse 186" cx="35.143" cy="35.143" r="35.143"
                                        transform="translate(2604.92 6060.154)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_187" data-name="Ellipse 187" cx="43.527" cy="43.527" r="43.527"
                                        transform="translate(2604.92 6043.384)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_188" data-name="Ellipse 188" cx="51.912" cy="51.912" r="51.912"
                                        transform="translate(2604.92 6026.615)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_189" data-name="Ellipse 189" cx="60.297" cy="60.297" r="60.297"
                                        transform="translate(2604.92 6009.846)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_190" data-name="Ellipse 190" cx="68.681" cy="68.681" r="68.681"
                                        transform="translate(2604.92 5993.076)" fill="none" stroke="#85553a"
                                        stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </figure>

                    <div class="content">
                        <h5>Mastery</h5>

                        <p>
                            We explore new territories and aesthetics to implement craft mastery for iconic precincts
                            and desirable communities.
                        </p>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="roundedBox">
                    <figure data-anim>
                        <svg xmlns="http://www.w3.org/2000/svg" width="137.579" height="138.363"
                             viewBox="0 0 137.579 138.363">
                            <g id="Group_1543" data-name="Group 1543" transform="translate(-2604.903 -5749.171)">
                                <ellipse id="Ellipse_176" data-name="Ellipse 176" cx="40.959" cy="24.356" rx="40.959"
                                         ry="24.356" transform="translate(2632.734 5749.671)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_177" data-name="Ellipse 177" cx="40.959" cy="24.356" rx="40.959"
                                         ry="24.356" transform="translate(2632.734 5838.321)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_178" data-name="Ellipse 178" cx="50.526" cy="31.071" rx="50.526"
                                         ry="31.071" transform="translate(2623.167 5757.835)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_179" data-name="Ellipse 179" cx="50.526" cy="31.071" rx="50.526"
                                         ry="31.071" transform="translate(2623.167 5816.944)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_180" data-name="Ellipse 180" cx="59.595" cy="37.881" rx="59.595"
                                         ry="37.881" transform="translate(2614.097 5765.771)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_181" data-name="Ellipse 181" cx="68.29" cy="44.325" rx="68.29"
                                         ry="44.325" transform="translate(2605.403 5774.027)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                                <ellipse id="Ellipse_182" data-name="Ellipse 182" cx="59.595" cy="37.816" rx="59.595"
                                         ry="37.816" transform="translate(2614.097 5795.474)" fill="none"
                                         stroke="#85553a" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </figure>

                    <div class="content">
                        <h5>Mastery</h5>

                        <p>
                            We explore new territories and aesthetics to implement craft mastery for iconic precincts
                            and desirable communities.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="missionCultivate">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="leftBox">
                        <h6>
                            <small>
                                MISSION
                                <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1"
                                     viewBox="0 0 31.652 1">
                                    <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                          fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"></line>
                                </svg>
                            </small>
                        </h6>

                        <figure class="data-anim d-md-none d-lg-none">
                            <img src="<?php echo $dir; ?>/assets/images/mission-cultivate-img.png" alt="">
                        </figure>

                        <h3>
                            <i> Cultivating<br>purpose </i>
                            through <br> brand mission
                        </h3>

                        <p>
                            Our mission is to unlock the true potential of Saudi Arabia via strategic investments,
                            innovative solutions and urban developments aligned to the 2030 vision. <br>
                            We aim to stay one step ahead of our competitors to transform the future into reality by
                            providing the next generation residential and commercial building solutions.
                        </p>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="rightBox">
                        <figure class="data-anim">
                            <img src="<?php echo $dir; ?>/assets/images/mission-cultivate-img.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="guidingPrinciple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h6>
                            <small>
                                GUIDING PRINCIPLES
                                <svg xmlns="http://www.w3.org/2000/svg" width="31.652" height="1"
                                     viewBox="0 0 31.652 1">
                                    <line id="Line_82" data-name="Line 82" x2="31.652" transform="translate(0 0.5)"
                                          fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"></line>
                                </svg>
                            </small>
                        </h6>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-12">
                    <div class="leftBox">
                        <h3>
                            <span>Impactful</span><br>
                            Guiding Principles
                        </h3>
                    </div>
                </div>

                <div class="col-md-6 col-12">
                    <div class="rightBox">
                        <p>
                            We build places that tell a memorable story by positively impacting societal wellbeing.
                            These are stories that focus on human flourishing and enrichment. With a distinct point of
                            difference, our products fulfil every promise of urban development. These thriving places
                            foster community engagement and create opportunities for community cohesion.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2">

                </div>

                <div class="col-md-10 col-12">
                    <div class="sliderDvOverFlow">
                        <div class="gpSliderDv">
                            <div class="item">
                                <div class="slideBox">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/gpslide-img2.png" alt="">
                                    </figure>

                                    <h4>Empower</h4>

                                    <p>
                                        We lead with mastery and innovation. Our sense of perspective, collaborative
                                        mindset and holonomic thinking principles empower us to create vibrancy to give
                                        every citizen remarkable quality of life.
                                    </p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slideBox">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/gpslide-img3.png" alt="">
                                    </figure>

                                    <h4>Empower</h4>

                                    <p>
                                        We lead with mastery and innovation. Our sense of perspective, collaborative
                                        mindset and holonomic thinking principles empower us to create vibrancy to give
                                        every citizen remarkable quality of life.
                                    </p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slideBox">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/gpslide-img1.png" alt="">
                                    </figure>

                                    <h4>Empower</h4>

                                    <p>
                                        We lead with mastery and innovation. Our sense of perspective, collaborative
                                        mindset and holonomic thinking principles empower us to create vibrancy to give
                                        every citizen remarkable quality of life.
                                    </p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slideBox">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/gpslide-img2.png" alt="">
                                    </figure>

                                    <h4>Empower</h4>

                                    <p>
                                        We lead with mastery and innovation. Our sense of perspective, collaborative
                                        mindset and holonomic thinking principles empower us to create vibrancy to give
                                        every citizen remarkable quality of life.
                                    </p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="slideBox">
                                    <figure data-anim>
                                        <img src="<?php echo $dir; ?>/assets/images/gpslide-img3.png" alt="">
                                    </figure>

                                    <h4>Empower</h4>

                                    <p>
                                        We lead with mastery and innovation. Our sense of perspective, collaborative
                                        mindset and holonomic thinking principles empower us to create vibrancy to give
                                        every citizen remarkable quality of life.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="mediaCenter" class="popup">

    <!-- MEDIA CENTER HERO SECTION -->
    <!-- <div class="page-media-center escape-transHeader mb-35"> -->
    <div class="page-media-center mb-35">
        <div class="container">

            <div class="page-back-link mb-25">
                <a href="#" class="closePop">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195" viewBox="0 0 12.707 9.195">
                        <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                            <g id="Group_18" data-name="Group 18">
                                <g id="Group_17" data-name="Group 17">
                                    <line id="Line_5" data-name="Line 5" x1="12" transform="translate(0 4.244)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_14" data-name="Path 14" d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                          transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </g>
                        </g>
                    </svg>
                    BACK TO ABOUT</a>
            </div>
            <div class="media-center-title">
                <h1>Media Centre</h1>
            </div>

            <div class="row mc-intro-row">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-4 col-md-6">
                    <div class="mc-intro">
                        <span>ACHIEVEMENTS</span>
                        <h2>Retal wins Developer of the Year at ABA 2020</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod
                            tincidunt ut laoreet dolore magna</p>
                    </div>
                    <div class="mc-intro-action">
                        <a href="#">Read More</a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6">
                    <div class="mc-hero">
                        <figure data-anim>
                            <img src="assets/images/media-center-hero.png">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MEDIA CENTER SEARCH -->
    <div class="page-media-center-search mb--5">
        <div class="container">
            <div class="row">
                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-4 col-md-12">
                    <div class="pmcs-wrapper">
                        <input type="text" name="search_media_center" placeholder="SEARCH MEDIA CENTRE">
                        <button type="submit">
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.41" height="11.164"
                                 viewBox="0 0 11.41 11.164">
                                <g id="Group_1319" data-name="Group 1319" transform="translate(0.5 0.5)" opacity="0.25">
                                    <circle id="Ellipse_156" data-name="Ellipse 156" cx="4.339" cy="4.339" r="4.339"
                                            fill="rgba(0,0,0,0)" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_264" data-name="Line 264" x1="3.056" y1="2.776"
                                          transform="translate(7.518 7.518)" fill="none" stroke="#000"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12"></div>
            </div>
        </div>
    </div>

    <!-- MEDIA CENTER TABS -->
    <div class="container">
        <div class="row navDv">
            <div class="col-lg-1 col-md-12"></div>
            <div class="col-lg-11 col-md-12">
                <div class="mc-scrolling-tabs">
                    <ul>
                        <li>
                            <a href="#pressRelease" class="t-active">PRESS RELEASES</a>
                        </li>
                        <li>
                            <a href="#events">EVENTS</a>
                        </li>
                        <li>
                            <a href="#gallery">GALLERY</a>
                        </li>
                        <li>
                            <a href="#downloads">DOWNLOADS</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- MEDIA CENTER TABS CONTENT *PRESS -->
        <div class="row mc-tabs-row" id="pressRelease">
            <div class="col-lg-1 col-md-12"></div>
            <div class="col-lg-11 col-md-12">
                <div class="mc-tabs-head">
                    <div class="mc-tabs-title">
                        <h4>Press Releases</h4>
                    </div>
                    <div class="mc-tabs-filter">
                        <label>Filter By</label>
                        <ul>
                            <li>
                                <a href="#">Achievements</a>
                            </li>
                            <li>
                                <a href="#" class="f-active">Year</a>
                            </li>
                            <li>
                                <a href="#">Month</a>
                            </li>
                            <li>
                                <a href="#">Subject</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mc-tabs-content-wrap">
                    <div class="mc-filter-name">
                        <h6>2021</h6>
                    </div>
                    <div class="row mc-content-row">
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit,
                                            sed diam </h3>
                                        <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit,
                                            sed diam </h3>
                                        <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit,
                                            sed diam </h3>
                                        <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit,
                                            sed diam </h3>
                                        <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit,
                                            sed diam </h3>
                                        <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Lorem ipsum dolor sit amet, consectetuer adip iscing elit,
                                            sed diam </h3>
                                        <p class="i-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                                            sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna </p>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="mc-content-load">
                        <a href="#">LOAD MORE <span>+</span></a>
                    </div>
                </div>

            </div>
        </div>

        <!-- MEDIA CENTER TABS CONTENT *EVENTS -->
        <div class="row mc-tabs-row d-none" id="events">
            <div class="col-lg-1 col-md-12"></div>
            <div class="col-lg-11 col-md-12">
                <div class="mc-tabs-head">
                    <div class="mc-tabs-title">
                        <h4>EVENTS</h4>
                    </div>
                    <div class="mc-tabs-filter">
                        <label>Filter By</label>
                        <ul>
                            <li>
                                <a href="#" class="f-active">Year</a>
                            </li>
                            <li>
                                <a href="#">Month</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mc-tabs-content-wrap">
                    <div class="mc-filter-name">
                        <h6>2021</h6>
                    </div>
                    <div class="row mc-content-row">
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                        <a href="#" class="i-action">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="mc-content-load">
                        <a href="#">LOAD MORE <span>+</span></a>
                    </div>
                </div>

            </div>
        </div>

        <!-- MEDIA CENTER TABS CONTENT *GALLERY -->
        <div class="row mc-tabs-row d-none" id="gallery">
            <div class="col-lg-1 col-md-12"></div>
            <div class="col-lg-11 col-md-12">
                <div class="mc-tabs-head">
                    <div class="mc-tabs-title">
                        <h4>GALLERY</h4>
                    </div>
                    <div class="mc-tabs-filter">
                        <label>Filter By</label>
                        <ul>
                            <li>
                                <a href="#">Achievements</a>
                            </li>
                            <li>
                                <a href="#" class="f-active">Year</a>
                            </li>
                            <li>
                                <a href="#">Month</a>
                            </li>
                            <li>
                                <a href="#">Subject</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mc-tabs-content-wrap">
                    <div class="mc-filter-name">
                        <h6>2021</h6>
                    </div>
                    <div class="row mc-content-row">
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-6">
                            <div class="info-box">
                                <div class="info-box-top">
                                    <div class="info-box-feature">
                                        <figure data-anim>
                                            <img src="assets/images/dummy_image_1.png" alt="">
                                        </figure>
                                    </div>
                                </div>
                                <div class="info-box-bottom">
                                    <div class="info-box-content">
                                        <span class="i-date">AUGUST 22, 2021</span>
                                        <h3 class="i-title">Reference Event Name</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="mc-content-load">
                        <a href="#">LOAD MORE <span>+</span></a>
                    </div>
                </div>

            </div>
        </div>

        <!-- MEDIA CENTER TABS CONTENT *GALLERY -->
        <div class="row mc-tabs-row d-none" id="downloads">
            <div class="col-lg-1 col-md-12"></div>
            <div class="col-lg-11 col-md-12">

                <div class="mc-tabs-head">
                    <div class="mc-tabs-title">
                        <h4>Downloads</h4>
                    </div>
                </div>
                <div class="mc-tabs-content-wrap">
                    <div class="row download-row">
                        <div class="col-lg-4 col-md-6 ">
                            <div class="download-box">
                                <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg"
                                     width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                    <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                        <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3"
                                              transform="translate(-707.46 -7101.118)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                    <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="download-box">
                                <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg"
                                     width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                    <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                        <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3"
                                              transform="translate(-707.46 -7101.118)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                    <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="download-box">
                                <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg"
                                     width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                    <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                        <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3"
                                              transform="translate(-707.46 -7101.118)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                    <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="download-box">
                                <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg"
                                     width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                    <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                        <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3"
                                              transform="translate(-707.46 -7101.118)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                    <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="download-box">
                                <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg"
                                     width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                    <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                        <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3"
                                              transform="translate(-707.46 -7101.118)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                    <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </svg>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 ">
                            <div class="download-box">
                                <h6><a href="#">RETAL CORPORATE BROCHURE</a></h6>
                                <svg id="Group_2026" data-name="Group 2026" xmlns="http://www.w3.org/2000/svg"
                                     width="11.748" height="13.742" viewBox="0 0 11.748 13.742">
                                    <g id="Group_646" data-name="Group 646" transform="translate(2.874 6.242)">
                                        <path id="Path_188" data-name="Path 188" d="M707.46,7101.118l3,3,3-3"
                                              transform="translate(-707.46 -7101.118)" fill="none" stroke="#000"
                                              stroke-miterlimit="10" stroke-width="1"/>
                                    </g>
                                    <line id="Line_116" data-name="Line 116" y2="9.242" transform="translate(5.874)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_117" data-name="Line 117" x2="11.748" transform="translate(0 13.242)"
                                          fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="download-details">
                        <div class="row">
                            <div class="col-md-4">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                    minim veniam, quis nostrud exerci tation.
                                </p>
                                <p>For more info contact: <a href="mailto:marketing@retal.ae">marketing@retal.ae</a></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

</div>

<div id="ceoMessageDv" class="popup">
    <section class="hero">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-3">
                    <div class="leftBox">
                        <div class="cm-flex">
                            <a href="#" class="anchorLink closePop">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                     viewBox="0 0 12.707 9.195">
                                    <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                                        <g id="Group_18" data-name="Group 18">
                                            <g id="Group_17" data-name="Group 17">
                                                <line id="Line_5" data-name="Line 5" x1="12"
                                                      transform="translate(0 4.244)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path id="Path_14" data-name="Path 14"
                                                      d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                      transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                BACK TO ABOUT
                            </a>

                            <h3 class="anim-head"><span class="letters">CEO's</span><br> <span
                                        class="letters fst-italic">Message</span></h3>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="rightBox">
                        <figure data-anim><img src="assets/images/ceoMessage-img.png" alt=""></figure>

                        <a href="#" class="pointerLink">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="21" viewBox="0 0 13 21">
                                <g id="Group_1011" data-name="Group 1011" transform="translate(-1458.191 -745.576)">
                                    <rect id="Rectangle_230" data-name="Rectangle 230" width="12" height="20" rx="6"
                                          transform="translate(1458.691 746.076)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_216" data-name="Line 216" y2="4"
                                          transform="translate(1464.691 750.076)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contentTxt">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="fullBox">
                        <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as
                            an opportunity to build legacies and timeless homes for future generations.</p>
                        <p>By enhancing the appeal of a precinct, we give people a reason to stay and linger. For Retal,
                            urban development creates a meaningful connection between the past, present and future. It
                            begins by listening to the voices of the people and understanding what matters—then
                            delivering without compromise.</p>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="leftBox">
                        <p>Our developments are aligned with Saudi Arabia’s 2030 vision, and this is how we create value
                            for people, the community and the nation.</p>
                        <p>We foster growth, and nurture environments for people to invest, live and raise their
                            families. Bringing like-minded individuals is what we always aim for. By embracing our
                            design principles, cultural values and cultivating sense, we ensure that the built
                            environment brings an agreed vision to life. We create a legacy of impact and build
                            soulfully. Shaping a meaningful future is what we seek. We build brand affinity, credibility
                            through simplicity, honesty, commitment.</p>
                        <p>We acknowledge that human values are the foundation of authenticity, agility and change. And
                            due to that, we start every project with a clear purpose, unique vision, and absolute
                            confidence. We think progressively and build differently. With Retal, everyone can grow,
                            prosper, and achieve their goals. There are opportunities and chances for everyone.</p>
                    </div>

                    <div class="ceoDv">
                        <div class="titleDv">
                            <h4>Abdullah Faisal Al-Braikan</h4>
                            <h5>CEO</h5>
                        </div>

                        <div class="ceoSign">
                            <figure data-anim>
                                <img src="assets/images/sign.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="newsLink">
                        <p>WANT TO KNOW MORE?</p>

                        <figure data-anim>
                            <img src="assets/images/cm-popup-button-img.png">
                            <a href="#sustainabilityDv" class="popLnk">
                                <span>Sustainability</span>
                            </a>
                        </figure>

                        <i class="fal fa-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="sustainabilityDv" class="popup">
    <section class="hero">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-3">
                    <div class="leftBox">
                        <div class="cm-flex">
                            <a href="#" class="anchorLink closePop">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                     viewBox="0 0 12.707 9.195">
                                    <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                                        <g id="Group_18" data-name="Group 18">
                                            <g id="Group_17" data-name="Group 17">
                                                <line id="Line_5" data-name="Line 5" x1="12"
                                                      transform="translate(0 4.244)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path id="Path_14" data-name="Path 14"
                                                      d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                      transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                BACK TO ABOUT
                            </a>

                            <h3>
                                Committed<br>
                                <span>to sustainable</span><br>
                                <i>practices</i>
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="rightBox">
                        <figure data-anim>
                            <img src="assets/images/sustainability-hero-img.png" alt="">
                        </figure>

                        <a href="#" class="pointerLink">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="21" viewBox="0 0 13 21">
                                <g id="Group_1011" data-name="Group 1011" transform="translate(-1458.191 -745.576)">
                                    <rect id="Rectangle_230" data-name="Rectangle 230" width="12" height="20" rx="6"
                                          transform="translate(1458.691 746.076)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_216" data-name="Line 216" y2="4"
                                          transform="translate(1464.691 750.076)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contentTxt">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="fullBox">
                        <p>
                            At Retal, we are place-makers, timekeepers, and caregivers.
                            We perceive every vacant plot as an opportunity to build
                            legacies and timeless homes for future generations.
                        </p>

                        <p>
                            By enhancing the appeal of a precinct, we give people a reason
                            to stay and linger. For Retal, urban development creates a meaningful
                            connection between the past, present and future. It begins by listening
                            to the voices of the people and understanding what matters—then
                            delivering without compromise.
                        </p>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="leftBox">
                        <p>
                            Our developments are aligned with Saudi Arabia’s 2030 vision, and this is how we create
                            value for people, the community and the nation.
                        </p>

                        <p>
                            We foster growth, and nurture environments for people to invest, live and raise their
                            families. Bringing like-minded individuals is what we always aim for. By embracing our
                            design principles, cultural values and cultivating sense, we ensure that the built
                            environment brings an agreed vision to life. We create a legacy of impact and build
                            soulfully. Shaping a meaningful future is what we seek. We build brand affinity, credibility
                            through simplicity, honesty, commitment.
                        </p>

                        <p>
                            We acknowledge that human values are the foundation of authenticity, agility and change. And
                            due to that, we start every project with a clear purpose, unique vision, and absolute
                            confidence. We think progressively and build differently. With Retal, everyone can grow,
                            prosper, and achieve their goals. There are opportunities and chances for everyone.
                        </p>
                    </div>

                    <div class="ceoDv">
                        <div class="titleDv">
                            <h4>Abdullah Faisal Al-Braikan</h4>
                            <h5>CEO</h5>
                        </div>

                        <div class="ceoSign">
                            <figure data-anim>
                                <img src="assets/images/sign.png" alt="">
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="newsLink">
                        <p>WANT TO KNOW MORE?</p>

                        <figure data-anim>
                            <img src="assets/images/sustainability-popup-img.png">
                            <a href="#csrDv" class="popLnk">
                                <span>CSR</span>
                            </a>
                        </figure>

                        <i class="fal fa-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="csrDv" class="popup">
    <section class="hero">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-3">
                    <div class="leftBox">
                        <div class="cm-flex">
                            <a href="#" class="anchorLink closePop">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.707" height="9.195"
                                     viewBox="0 0 12.707 9.195">
                                    <g id="Group_1372" data-name="Group 1372" transform="translate(0.707 0.354)">
                                        <g id="Group_18" data-name="Group 18">
                                            <g id="Group_17" data-name="Group 17">
                                                <line id="Line_5" data-name="Line 5" x1="12"
                                                      transform="translate(0 4.244)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path id="Path_14" data-name="Path 14"
                                                      d="M-1515.5,866.983l-4.244-4.244,4.244-4.244"
                                                      transform="translate(1519.74 -858.496)" fill="none" stroke="#000"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                BACK TO ABOUT
                            </a>

                            <h3>
                                Retal<br>
                                <span>Al Khayer</span>
                            </h3>

                            <p>
                                Putting Social Responsibility into Action and Committed Responsibility
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="rightBox">
                        <figure data-anim>
                            <img src="assets/images/csr-hero-img.png" alt="">
                        </figure>

                        <a href="#" class="pointerLink">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="21" viewBox="0 0 13 21">
                                <g id="Group_1011" data-name="Group 1011" transform="translate(-1458.191 -745.576)">
                                    <rect id="Rectangle_230" data-name="Rectangle 230" width="12" height="20" rx="6"
                                          transform="translate(1458.691 746.076)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_216" data-name="Line 216" y2="4"
                                          transform="translate(1464.691 750.076)" fill="none" stroke="#fff"
                                          stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contentTxt">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="fullBox">
                        <p>
                            Retal commits to undertake the pillars of social responsibility that are compatible with
                            world economic goals of sustainable development, the Kingdom’s vision, and its national
                            strategy for social responsibility throughout its career, based on professional
                            specialisation, integration of engineering services, and highly experienced human resources.<br>
                            By fulfilling the following responsibilities:
                        </p>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="leftBox">
                        <p>
                            1. Providing the highest urbanisation quality to the real estate industry and supporting
                            initiatives to improve quality of life and environmental activities.<br>
                            2. Working to accomplish future goals and consistent positive results that meet
                            international environmental standards, including recording them in social responsibility
                            reports and the company’s annual report on a regular basis.<br>
                            3. Supporting the national economy by using locally sourced materials and committing to
                            purchasing as much as possible from local suppliers to help small and medium businesses.<br>
                            4. Developing the digital structure of the company’s products in order to provide reports
                            that benefit the national economy and local products.<br>
                            5. Developing housing solutions for low-income individuals to enable concerned authorities
                            to offer suitable housing that meets their needs and improves their quality of life in
                            comparison to the middle classes.<br>
                            6. promoting social responsibility advisory services for the company’s members and
                            clients.<br>
                            7. Assist in creating job opportunities for community people by engaging in on-the-job
                            training and programs offered by the firm.<br>
                        </p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="newsLink">
                        <p>WANT TO KNOW MORE?</p>

                        <figure data-anim>
                            <img src="assets/images/csr-popup-btn-img.png">
                            <a href="#ceoMessageDv" class="popLnk">
                                    <span>
                                        CEO’s<br>
                                        Message
                                    </span>
                            </a>
                        </figure>

                        <i class="fal fa-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<?php include "includes/footer.php"; ?>
<?php include "includes/footer-scripts.php"; ?>
</body>

</html>