<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'home';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body>
    <?php include "includes/header.php"; ?>

    <section class="contactSlider">

        <div class="container">
            <ul class="pagination root" data-active="callcenter">
                <li class="active" data-slide="callcenter">Sales Centers</li>
                <li data-slide="inquiries">Inquiries & Feedback</li>
                <li data-slide="faq">Faq</li>
                <li data-slide="joinus">Careers</li>
            </ul>
        </div>

        <div class="sliderDv">
            <div class="item callcenter active">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h2><span><span>Call Center & </span></span><span><span>Customer Care</span></span></h2>
                            <ul class="phonemail">
                                <li><a href="#"><svg xmlns="http://www.w3.org/2000/svg" width="15.401" height="15.402" viewBox="0 0 15.401 15.402">
                                            <g id="Group_1800" data-name="Group 1800" transform="translate(-28.31 -267.953)">
                                                <path id="Path_250" data-name="Path 250" d="M318.508,446.327l-3.2-3.2,1.372-4.2h3.506a13.572,13.572,0,0,1-14.373,14.374v-3.507l4.2-1.372,3.2,3.2" transform="translate(-277 -170.467)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                        </svg>
                                        920021022</a></li>
                                <li><a href="mailto:sales@retal.com.sa"><svg xmlns="http://www.w3.org/2000/svg" width="16.15" height="11.822" viewBox="0 0 16.15 11.822">
                                            <g id="Group_1799" data-name="Group 1799" transform="translate(-28.31 -324.205)">
                                                <rect id="Rectangle_197" data-name="Rectangle 197" width="15.15" height="10.822" transform="translate(28.81 324.705)" fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                                                <path id="Path_251" data-name="Path 251" d="M305.81,502.172l7.575,6.493,7.575-6.493" transform="translate(-277 -177.467)" fill="none" stroke="#9e7156" stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                        </svg>
                                        sales@retal.com.sa</a></li>
                            </ul>
                            <ul class="pagination">
                                <li class="active" data-slide="callcenter">Sales Centers</li>
                                <li data-slide="inquiries">Inquiries & Feedback</li>
                                <li data-slide="faq">Faq</li>
                                <li data-slide="joinus">Careers</li>
                            </ul>
                            <div class="addresswidget">
                                <div>
                                    <h3>Khobar</h3>
                                    <p>Retal Business Center <br/>King Faisal Road, <br/>Al Rawabi, Khobar, 31952</p>
                                    <a href="#" class="btn btn-outline-primary">GET DIRECTIONS <i class="far fa-map-marker-alt"></i></a>
                                </div>

                                <div>
                                    <h3>Riyadh</h3>
                                    <p>Tamkeen Tower, 7252, <br/>Olaya St. Al Yasmeen, <br/>Riyadh, 13325</p>
                                    <a href="#" class="btn btn-outline-primary">GET DIRECTIONS <i class="far fa-map-marker-alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <figure><img src="assets/images/contact-map.png"/></figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item inquiries">
                <div class="container">
                    <h2><span><span>Inquiries & Feedback</span></span></h2>
                    <ul class="pagination">
                        <li data-slide="callcenter">Sales Centers</li>
                        <li class="active" data-slide="inquiries">Inquiries & Feedback</li>
                        <li data-slide="faq">Faq</li>
                        <li data-slide="joinus">Careers</li>
                    </ul>
                    <p>Call us through our toll free 920 021 022 for all inquiries related to Retal's projects. <br/>If calling from outside of the KSA please contact us on +966(13)8071777</p>
                    <div class="formDv">
                        <div class="rowFields">
                            <div class="fieldDv dropDown">
                                <select>
                                    <option value="-1">Select your request</option>
                                    <option>Option 01</option>
                                    <option>Option 02</option>
                                    <option>Option 03</option>
                                </select>
                            </div>
                            <div class="fieldDv txt">
                                <input type="text" placeholder="Full name"/>
                            </div>
                            <div class="fieldDv txt">
                                <input type="text" placeholder="Phone"/>
                            </div>
                            <div class="fieldDv txt">
                                <input type="text" placeholder="Email"/>
                            </div>
                        </div>
                        <div class="fieldDv txtarea">
                            <textarea>Message</textarea>
                        </div>
                        <div class="row botRow">
                            <div class="col-md-6">
                                <div class="btnDv">
                                    <input type="submit" value="Send" class="btn"/>
                                    <button class="btn">Cancel</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="agree">
                                    <p><input type="checkbox"/> I have read the <a href="#">Privacy Policy</a></p>
                                    <p><input type="checkbox"/> I agree with the <a href="#">Terms & Conditions</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="item faq">
                <div class="container">
                    <h2><span><span>FAQ</span></span></h2>
                    <ul class="pagination">
                        <li data-slide="callcenter">Sales Centers</li>
                        <li data-slide="inquiries">Inquiries & Feedback</li>
                        <li class="active" data-slide="faq">Faq</li>
                        <li data-slide="joinus">Careers</li>
                    </ul>

                    <div class="faqTabs">
                        <ul class="tabPaginate">
                            <li data-tab="general" class="active">General</li>
                            <li data-tab="projects">Projects</li>
                            <li data-tab="legal">Legal</li>
                            <li data-tab="finance">Finance</li>
                        </ul>
                        <div class="tabItem general active">
                            <div class="accord active">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tabItem projects">
                            <div class="accord active">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tabItem legal">
                            <div class="accord active">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                        </div>
                        <div class="tabItem finance">
                            <div class="accord active">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                            <div class="accord">
                                <div class="head">What is retal? <i class="fal fa-chevron-down"></i><i class="fal fa-times"></i></div>
                                <div class="inner">
                                    <p>Clients contact details can be updated by sending an e-mail to customers' services, contacting the communications center at (920021022) or by visiting the customers services center or Retal sales center, to notify updates.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="newsLink">
                        <p>Want to now more?</p>
                        <figure><img src="assets/images/contact-img-02.png"/><a href="#"><span>News</span></a></figure>
                        <i class="fal fa-arrow-right"></i>
                    </div>
                </div>

            </div>
            <div class="item joinus">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="pagination">
                                <li data-slide="callcenter">Sales Centers</li>
                                <li data-slide="inquiries">Inquiries & Feedback</li>
                                <li data-slide="faq">Faq</li>
                                <li class="active" data-slide="joinus">Careers</li>
                            </ul>
                            <figure><img src="assets/images/contact-img-03.png"/></figure>
                        </div>
                        <div class="col-md-6">
                            <h2><span><span>Join</span></span><span><span>our</span></span><span><span>community</span></span></h2>
                            <div class="formDv">
                                <div class="fieldDv"><input type="email" placeholder="Email" /></div>
                                <div class="fieldDv"><input type="password" placeholder="Password" /></div>
                                <p>Do not have an account ? <a href="#">Get one</a></p>
                                <div class="btnDv">
                                    <input type="submit" value="Login" class="btn"/>
                                    <button class="btn">SignUp</button>
                                </div>
                                <p>Forgot your password? <a href="#">Click here</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php include "includes/footer.php"; ?>
    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>