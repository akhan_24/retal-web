<?php include "includes/vars.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php $currentPage = 'home';
    $currentPageSub = ''; ?>
    <meta charset="UTF-8">
    <title><?php echo $sitenameUpper; ?></title>
    <meta name="description" content="<?php echo $sitenameUpper; ?>">
    <?php include "includes/header-scripts.php"; ?>
</head>

<body class="home transHeader">
    <?php include "includes/header.php"; ?>


    <section class="bannerSlider">

        <script>
            var dayLightData = {
                noon : {
                    image: 'Repeat-Grid-1.png',
                    captionLarge: 'Ewan almaali 00',
                    captionSmall: 'Riyadh',
                    slideDescription: 'Noon<br><span>to start fresh.</span>',
                    videoLink: 'video',
                    slideLink: 'explore'
                },
                sunrise : {
                    image: '1.jpg',
                    captionLarge: 'Ewan almaali 01',
                    captionSmall: 'Riyadh 1',
                    slideDescription: 'Sunrise<br><span>to start fresh 1</span>',
                    videoLink: 'video1',
                    slideLink: 'explore1'
                },
                night : {
                    image: '2.jpg',
                    captionLarge: 'Ewan almaali 02',
                    captionSmall: 'Riyadh 2',
                    slideDescription: 'Night<br><span>to start fresh 2</span>',
                    videoLink: 'video2',
                    slideLink: 'explore2'
                },
                sunset : {
                    image: '3.jpg',
                    captionLarge: 'Ewan almaali 03',
                    captionSmall: 'Riyadh 3',
                    slideDescription: 'Sunset<br><span>to start fresh 3</span>',
                    videoLink: 'video3',
                    slideLink: 'explore3'
                },
                evening : {
                    image: '4.jpg',
                    captionLarge: 'Ewan almaali 04',
                    captionSmall: 'Riyadh 4',
                    slideDescription: 'Evening<br><span>to start fresh 4</span>',
                    videoLink: 'video4',
                    slideLink: 'explore4'
                }
            }
        </script>

        <div class="dayLightSlider">
            <div class="dayLightWrapper dlLoading">
                <div class="loading">
                    <div class="loaderWrapper"><div class="loader"></div></div>
                </div>
                <div class="dayLightGallery">
                    <img class="dayLightImage" data-slide="evening" src="<?php echo $dir; ?>/assets/images/sliderImgs/nesaj_town_Riyadh.jpg">
                    <img class="dayLightImage" data-slide="sunset" src="<?php echo $dir; ?>/assets/images/sliderImgs/NesajTown_Photo-001.jpg">
                    <img class="dayLightImage" data-slide="night" src="<?php echo $dir; ?>/assets/images/sliderImgs/retal_square.jpg">
                    <img class="dayLightImage" data-slide="sunrise" src="<?php echo $dir; ?>/assets/images/sliderImgs/the_grand.jpg">
                    <img class="dayLightImage " data-slide="noon" src="<?php echo $dir; ?>/assets/images/sliderImgs/Group-2.jpg">
                    <div class="dayLightGalleryOverlay"></div>
                </div>
                <div class="dayLightGalleryOverlay"></div>
                <div class="dayLightGraph">
                    <div class="outer" >
                        <svg class="radial-progress"  viewBox="0 0 80 80">
                            <circle class="graph incomplete" cx="40" cy="40" r="35"></circle>
                            <g class="bulletGroup">
                                <circle class="bullet bullet4 noon" data-slide="noon" fill="#fff" r="1" cx="40" cy="5"></circle>
                                <circle class="bullet bullet0 evening" data-slide="evening" fill="#fff" r="1" cx="73" cy="29"></circle>
                                <circle class="bullet bullet1 sunset" data-slide="sunset" fill="#fff" r="1" cx="61" cy="68"></circle>
                                <circle class="bullet bullet2 night" data-slide="night" fill="#fff" r="1" cx="19" cy="68"></circle>
                                <circle class="bullet bullet3 sunrise" data-slide="sunrise" fill="#fff" r="1" cy="29" cx="7"></circle>
                            </g>
                            <g>
                                <text class="percentage" x="50%" y="0">Noon</text>
                                <text class="percentage" x="102%" y="40%">Evening</text>
                                <text class="percentage" x="82%" y="92%">Sunset</text>
                                <text class="percentage" x="12%" y="86%">Night</text>
                                <text class="percentage" x="0%" y="32%">Sunrise</text>
                            </g>
                            <text class="percentage captionLarge" x="50%" y="50%" ></text>
                            <text class="percentage captionSmall" x="50%" y="55%" ></text>
                        </svg>
                        <svg class="radial-progress dayLightBar" data-percentage="82" viewBox="0 0 80 80" style="transform: rotate(-90deg);">
                            <circle id="bar" class="graph complete" cx="40" cy="40" r="35"></circle>
                        </svg>
                    </div>
                </div>
            </div>
            <!-- Bottom Action-->
            <div class="bottomcontainer">
                <div class="dlBottomcontainer">
                    <div class="dlBottomWrapper ">
                        <div class="dlDescription"></div>
                    </div>
                </div>
                <div class="dlBActionContainer">
                    <div class="dlBActionWrapper ">
                        <div class="dlAction dlActionUp">
                            <a class="dlVideoLink" href="">
                                <img src="https://retalweb.m2web.dev/wp-content/plugins/day-light-slider/images/btn-play.png">
                            </a>
                        </div>
                        <div class="dlAction dlActionDown">
                            <div class="elementor-button-wrapper">
                                <a href="" class="dlEPLink elementor-button-link elementor-button elementor-size-sm" role="button">
									<span class="elementor-button-content-wrapper">
										<span class="elementor-button-icon elementor-align-icon-right"></span>
										<span class="elementor-button-text">EXPLORE PROJECT</span>
									</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Bottom Action END-->
        </div>
        <?php /* ?>
        <div class="counterSec"></div>
        <div class="sliderSec">
            <div class="item">
                <figure><img src="assets/images/home-slider-01.png"/></figure>
                <div class="txtDv">
                    <h3>Sunrises <span>to start fresh</span></h3>
                    <div class="btnDv">
                        <a href="#" class="vid">
                            <svg xmlns="http://www.w3.org/2000/svg" width="51" height="51" viewBox="0 0 51 51">
                                <g id="Group_6" data-name="Group 6" transform="translate(531.175 -1197.323)">
                                    <circle id="Ellipse_3" data-name="Ellipse 3" cx="25" cy="25" r="25" transform="translate(-530.675 1197.823)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_3" data-name="Path 3" d="M-509.543,1217.617v6.76a.12.12,0,0,0,.175.106l6.522-3.416a.12.12,0,0,0,0-.212l-6.522-3.345A.12.12,0,0,0-509.543,1217.617Z" transform="translate(1.96 1.825)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </a>
                        <a href="#" class="txtBtn btn btn-outline-primary">EXPLORE PROJECT</a>
                    </div>
                </div>
            </div>
        </div>
        <?php */ ?>
    </section>
    <section class="featuredProperties">
        <div class="container">
            <h4>Featured Properties</h4>
        </div>
        <div class="fetaurSliderDv">
            <div class="item">
                <h3><span><span>Ewan</span></span> <span><span>Al Nawras</span></span></h3>
                <p>Luxury Residential</p>
                <figure><img src="assets/images/feture-slider-01.png"/></figure>
                <div><a class="btn btn-outline">Explore Project <i class="far fa-plus"></i></a></div>
            </div>
            <div class="item">
                <h3><span><span>Ewan</span></span> <span><span>Al Nawras</span></span></h3>
                <p>Luxury Residential</p>
                <figure><img src="assets/images/feture-slider-01.png"/></figure>
                <div><a class="btn btn-outline">Explore Project <i class="far fa-plus"></i></a></div>
            </div>
            <div class="item">
                <h3><span><span>Ewan</span></span> <span><span>Al Nawras</span></span></h3>
                <p>Luxury Residential</p>
                <figure><img src="assets/images/feture-slider-01.png"/></figure>
                <div><a class="btn btn-outline">Explore Project <i class="far fa-plus"></i></a></div>
            </div>
            <div class="item">
                <h3><span><span>Ewan</span></span> <span><span>Al Nawras</span></span></h3>
                <p>Luxury Residential</p>
                <figure><img src="assets/images/feture-slider-01.png"/></figure>
                <div><a class="btn btn-outline">Explore Project <i class="far fa-plus"></i></a></div>
            </div>
        </div>
    </section>
    <section class="interactiveMap ltri homeTxtImgSec animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4>Featured Properties</h4>
                    <h3 class="anim-head"><span class="letters">Crafting in</span> <span class="letters">strategic locales</span></h3>
                    <p>Our developments create a majestic pursuit by infusing character into a place. They provide an extraordinary value, occupy strategic locales and generate high returns on investments.</p>
                    <p>Click below to explore our footprint around Saudi Arabia.</p>
                    <a class="btn btn-outline">Explore Project <i class="fal fa-globe-americas"></i></a>
                </div>
                <div class="col-md-8">
                    <figure><img src="assets/images/interactive-map.png"/></figure>
                </div>
            </div>
        </div>

    </section>
    <section class="whyRetal animToStart">
        <div class="container">
            <h4>Our Goals</h4>
            <h3 class="anim-head"><span class="letters">Why </span><span class="letters">Retal?</span></h3>

            <div class="iconCircleDv">
                <div class="circleRow">
                    <div class="circleDv">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="11.042" height="31.2" viewBox="0 0 11.042 31.2">
                                <g id="Group_1387" data-name="Group 1387" transform="translate(-6620.575 1684.997)">
                                    <line id="Line_270" data-name="Line 270" y1="5.558" transform="translate(6626.096 -1684.997)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_549" data-name="Path 549" d="M6621.075-1662.895a5.02,5.02,0,0,0,5.021,5.02,5.021,5.021,0,0,0,5.021-5.02,5.021,5.021,0,0,0-5.021-5.021,5.021,5.021,0,0,1-5.021-5.021,5.02,5.02,0,0,1,5.021-5.02,5.021,5.021,0,0,1,5.021,5.02" transform="translate(0 -1.481)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_271" data-name="Line 271" y1="5.558" transform="translate(6626.096 -1659.355)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>
                        </div>
                        <h5>High Investment <br/>Returns</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 0.4s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.213" height="27.663" viewBox="0 0 17.213 27.663">
                                <path id="Path_47" data-name="Path 47" d="M964.417,5426.267l-1.433-12.333h3.771l.72-7.973h-2.28l-.86,2.389h-2.389l-.669-3.345h-2.453l-.769,3.345h-2.389l-.86-2.389h-2.28l.72,7.973h3.771l-1.433,12.333h-3.251l-.4,5.4h16.137l-.4-5.4Z" transform="translate(-951.393 -5404.505)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>

                        </div>
                        <h5>Strategic <br/>Locations</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 0.8s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24.304" height="24.178" viewBox="0 0 24.304 24.178">
                                <path id="Path_23" data-name="Path 23" d="M1211.336,5419.178l-.024,6.385a3,3,0,0,0,2.978,3.011l7.086.05a3,3,0,0,0,2.142-.878l10.219-10.219a3,3,0,0,0,0-4.243l-6.748-6.748a3,3,0,0,0-4.242,0l-10.533,10.532A3,3,0,0,0,1211.336,5419.178Z" transform="translate(-1210.812 -5404.946)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <circle id="Ellipse_17" data-name="Ellipse 17" cx="1.969" cy="1.969" r="1.969" transform="translate(4.875 15.701)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>

                        </div>
                        <h5>Intelligent <br/>Price</h5>
                    </div>
                </div>
                <div class="circleRow">
                    <div class="txtDv">
                        <p>We ensure our developments are in coherence with the strategic vision to maximise shareholders wealth and contribute to the nation’s economy.</p>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 1.2s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="25.629" height="19.703" viewBox="0 0 25.629 19.703">
                                <g id="Group_83" data-name="Group 83" transform="translate(-824.137 -5723.339)">
                                    <path id="Path_25" data-name="Path 25" d="M843.34,5740.191v2.351h-18.7v-2.351c0-3.117,6.235-4.649,9.352-4.649S843.34,5737.074,843.34,5740.191Zm-13.414-12.241a4.063,4.063,0,1,1,4.063,4.063,4.234,4.234,0,0,1-2.868-1.184A3.88,3.88,0,0,1,829.926,5727.95Z" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                                <path id="Path_46" data-name="Path 46" d="M848.639,5728.1a2.108,2.108,0,0,0,.627-1.431,3.747,3.747,0,0,0-6.928,0,2.086,2.086,0,0,0,.61,1.429l2.854,3.124Z" transform="translate(-824.137 -5723.339)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </svg>

                        </div>
                        <h5>Charismatic <br/>Leadership</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 01.6s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="13.74" height="27.91" viewBox="0 0 13.74 27.91">
                                <g id="Group_142" data-name="Group 142" transform="translate(-692.408 -5403.531)">
                                    <circle id="Ellipse_30" data-name="Ellipse 30" cx="2.543" cy="2.543" r="2.543" transform="translate(696.735 5407.951)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_19" data-name="Line 19" y2="4.419" transform="translate(699.278 5403.531)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_20" data-name="Line 20" x1="6.236" y2="18.242" transform="translate(693.042 5413.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_21" data-name="Line 21" x2="6.236" y2="18.242" transform="translate(699.278 5413.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_22" data-name="Line 22" x2="3" transform="translate(692.408 5423.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <line id="Line_23" data-name="Line 23" x2="3" transform="translate(703.148 5423.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>

                        </div>
                        <h5>High <br/>Quality</h5>
                    </div>
                    <div class="circleDv" data-inlinecss="transition-delay: 2s;">
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.415" height="25.341" viewBox="0 0 22.415 25.341">
                                <g id="Group_1388" data-name="Group 1388" transform="translate(-6646.336 1677.588)">
                                    <path id="Path_550" data-name="Path 550" d="M6668.251-1652.247v-13.926l-10.708-10.708-10.708,10.708v13.926" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                    <path id="Path_551" data-name="Path 551" d="M6657.544-1662.845l2.231-2.232a3.157,3.157,0,0,1,4.463,0h0a3.156,3.156,0,0,1,0,4.463l-6.694,6.694-6.694-6.694a3.157,3.157,0,0,1,0-4.463h0a3.157,3.157,0,0,1,4.463,0Z" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                </g>
                            </svg>

                        </div>
                        <h5>Enhanced <br/>Lifestyle</h5>
                    </div>
                </div>
            </div>

            <!-- made by adnan -->
            <div class="circleDvMobile">
                <div class="circleBox">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="11.042" height="31.2" viewBox="0 0 11.042 31.2">
                            <g id="Group_1387" data-name="Group 1387" transform="translate(-6620.575 1684.997)">
                                <line id="Line_270" data-name="Line 270" y1="5.558" transform="translate(6626.096 -1684.997)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                <path id="Path_549" data-name="Path 549" d="M6621.075-1662.895a5.02,5.02,0,0,0,5.021,5.02,5.021,5.021,0,0,0,5.021-5.02,5.021,5.021,0,0,0-5.021-5.021,5.021,5.021,0,0,1-5.021-5.021,5.02,5.02,0,0,1,5.021-5.02,5.021,5.021,0,0,1,5.021,5.02" transform="translate(0 -1.481)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_271" data-name="Line 271" y1="5.558" transform="translate(6626.096 -1659.355)" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>
                    </div>
                    <h5>High Investment <br/>Returns</h5>
                </div>

                <div class="circleBox" data-inlinecss="transition-delay: 0.4s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17.213" height="27.663" viewBox="0 0 17.213 27.663">
                            <path id="Path_47" data-name="Path 47" d="M964.417,5426.267l-1.433-12.333h3.771l.72-7.973h-2.28l-.86,2.389h-2.389l-.669-3.345h-2.453l-.769,3.345h-2.389l-.86-2.389h-2.28l.72,7.973h3.771l-1.433,12.333h-3.251l-.4,5.4h16.137l-.4-5.4Z" transform="translate(-951.393 -5404.505)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                        </svg>

                    </div>
                    <h5>Strategic <br/>Locations</h5>
                </div>
                <div class="circleBox" data-inlinecss="transition-delay: 0.8s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24.304" height="24.178" viewBox="0 0 24.304 24.178">
                            <path id="Path_23" data-name="Path 23" d="M1211.336,5419.178l-.024,6.385a3,3,0,0,0,2.978,3.011l7.086.05a3,3,0,0,0,2.142-.878l10.219-10.219a3,3,0,0,0,0-4.243l-6.748-6.748a3,3,0,0,0-4.242,0l-10.533,10.532A3,3,0,0,0,1211.336,5419.178Z" transform="translate(-1210.812 -5404.946)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            <circle id="Ellipse_17" data-name="Ellipse 17" cx="1.969" cy="1.969" r="1.969" transform="translate(4.875 15.701)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                        </svg>

                    </div>
                    <h5>Intelligent <br/>Price</h5>
                </div>

                <div class="circleBox" data-inlinecss="transition-delay: 1.2s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25.629" height="19.703" viewBox="0 0 25.629 19.703">
                            <g id="Group_83" data-name="Group 83" transform="translate(-824.137 -5723.339)">
                                <path id="Path_25" data-name="Path 25" d="M843.34,5740.191v2.351h-18.7v-2.351c0-3.117,6.235-4.649,9.352-4.649S843.34,5737.074,843.34,5740.191Zm-13.414-12.241a4.063,4.063,0,1,1,4.063,4.063,4.234,4.234,0,0,1-2.868-1.184A3.88,3.88,0,0,1,829.926,5727.95Z" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                            <path id="Path_46" data-name="Path 46" d="M848.639,5728.1a2.108,2.108,0,0,0,.627-1.431,3.747,3.747,0,0,0-6.928,0,2.086,2.086,0,0,0,.61,1.429l2.854,3.124Z" transform="translate(-824.137 -5723.339)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                        </svg>

                    </div>
                    <h5>Charismatic <br/>Leadership</h5>
                </div>
                <div class="circleBox" data-inlinecss="transition-delay: 01.6s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13.74" height="27.91" viewBox="0 0 13.74 27.91">
                            <g id="Group_142" data-name="Group 142" transform="translate(-692.408 -5403.531)">
                                <circle id="Ellipse_30" data-name="Ellipse 30" cx="2.543" cy="2.543" r="2.543" transform="translate(696.735 5407.951)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_19" data-name="Line 19" y2="4.419" transform="translate(699.278 5403.531)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_20" data-name="Line 20" x1="6.236" y2="18.242" transform="translate(693.042 5413.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_21" data-name="Line 21" x2="6.236" y2="18.242" transform="translate(699.278 5413.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_22" data-name="Line 22" x2="3" transform="translate(692.408 5423.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <line id="Line_23" data-name="Line 23" x2="3" transform="translate(703.148 5423.037)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>

                    </div>
                    <h5>High <br/>Quality</h5>
                </div>
                <div class="circleBox" data-inlinecss="transition-delay: 2s;">
                    <div class="darkCircle">
                        <svg xmlns="http://www.w3.org/2000/svg" width="22.415" height="25.341" viewBox="0 0 22.415 25.341">
                            <g id="Group_1388" data-name="Group 1388" transform="translate(-6646.336 1677.588)">
                                <path id="Path_550" data-name="Path 550" d="M6668.251-1652.247v-13.926l-10.708-10.708-10.708,10.708v13.926" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                                <path id="Path_551" data-name="Path 551" d="M6657.544-1662.845l2.231-2.232a3.157,3.157,0,0,1,4.463,0h0a3.156,3.156,0,0,1,0,4.463l-6.694,6.694-6.694-6.694a3.157,3.157,0,0,1,0-4.463h0a3.157,3.157,0,0,1,4.463,0Z" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                            </g>
                        </svg>

                    </div>
                    <h5>Enhanced <br/>Lifestyle</h5>
                </div>
            </div>
        </div>
    </section>
    <section class="ourCompany lirt homeTxtImgSec animToStart">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h4>Our Company</h4>
                    <h3 class="anim-head"><span class="letters">A vision</span> <span class="letters">of the future</span></h3>
                    <p>As creative visionaries, we aim to shape the future by building future-proof developments that stand the test of time. We transform vacant plots into landscape poetry and create future possibilities.</p>
                    <div class="countDv">
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <h5>Total number of projects</h5>
                                <p>156</p>
                            </div>
                            <div class="col-md-6 col-6">
                                <h5>Total built up area</h5>
                                <p>10,179 Sq.M.</p>
                            </div>
                            <div class="col-md-6 col-6">
                                <h5>Total project value</h5>
                                <p>12.5 Billion SAR</p>
                            </div>
                            <div class="col-md-6 col-6">
                                <h5>Units under construction</h5>
                                <p>8,028</p>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-primary">Dive deep into our journey <i class="fal fa-arrow-right"></i></a>
                </div>
                <div class="col-md-8">
                    <figure><img src="assets/images/our-company.png"/></figure>
                </div>
            </div>
        </div>

    </section>
    <section class="botCarousel">
        <div class="botHomeSlider">
            <div class="item">
                <figure data-anim><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim data-anim-delay="200"><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim data-anim-delay="400"><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim data-anim-delay="600"><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim data-anim-delay="200"><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim data-anim-delay="400"><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="item">
                <figure data-anim data-anim-delay="600"><img src="assets/images/botslider-01.png"/></figure>
                <h5>About Us</h5>
                <h3>CEO's Message</h3>
                <p>At Retal, we are place-makers, timekeepers, and caregivers. We perceive every vacant plot as an opportunity to build legacies and timeless places for future generations. By enhancing the appeal of a precinct…</p>
                <a class="btn btn-outline">DISCOVER THE VISION <i class="fal fa-arrow-right"></i></a>
            </div>
        </div>
    </section>


    <?php include "includes/footer.php"; ?>

    <?php include "includes/footer-scripts.php"; ?>
</body>

</html>