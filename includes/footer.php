<footer>
    <div class="followSubsDv">
        <div class="followDv">
            <p>Follow Us</p>
            <ul>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
        <div class="subsDv">
            <p>Subscribe</p>
            <div class="fieldDv">
                <input type="text" placeholder="ENTER EMAIL HERE" />
            </div>
        </div>
    </div>
    <div class="botSec01">
        <div class="container">
            <div class="row borderRow">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md col-6">
                            <div class="widget">
                                <h3>Retal's Properties</h3>
                                <ul>
                                    <li><a href="#">Commercial</a></li>
                                    <li><a href="#">The Valley</a></li>
                                    <li><a href="#">Al Khobar Uptown</a></li>
                                    <li><a href="#">Residential</a></li>
                                    <li><a href="#">Ewan</a></li>
                                    <li><a href="#">Ayala</a></li>
                                    <li><a href="#">Mixed Use</a></li>
                                    <li><a href="#">Retal Rise Remal</a></li>
                                    <li><a href="#">Chalet</a></li>
                                </ul>
                                <a href="#"><i class="far fa-arrow-right"></i> View All</a>
                            </div>
                        </div>
                        <div class="col-md col-6">
                            <div class="widget">
                                <h3>Investors</h3>
                                <ul>
                                    <li><a href="#">Share Information</a></li>
                                    <li><a href="#">Financial Information</a></li>
                                    <li><a href="#">Annual Reports</a></li>
                                    <li><a href="#">Disclosures</a></li>
                                    <li><a href="#">Press Release</a></li>
                                    <li><a href="#">Investor Resources</a></li>
                                    <li><a href="#">Credit Ratings</a></li>
                                    <li><a href="#">Corporate Governance</a></li>
                                    <li><a href="#">Dividends</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md col-6">
                            <div class="widget">
                                <h3>About</h3>
                                <ul>
                                    <li><a href="#">CEO’s Message</a></li>
                                    <li><a href="#">Corporate Information</a></li>
                                    <li><a href="#">Media Center</a></li>
                                    <li><a href="#">Sustainability</a></li>
                                    <li><a href="#">Retal Al Khayer</a></li>
                                    <li><a href="#">Blog</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md col-6">
                            <div class="widget">
                                <h3>Contact</h3>
                                <ul>
                                    <li><a href="#">Career</a></li>
                                    <li><a href="#">FAQs</a></li>
                                    <li><a href="#">Retal Intranet</a></li>
                                    <li><a href="#">Customer Portal</a></li>
                                    <li><a href="#">Contacts Form</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget offcies">
                        <h3>Our Offices</h3>
                        <div>
                            <h3>Khobar</h3>
                            <p>Retal Business Center <br/>King Faisal Road, <br/>Al Rawabi, Khobar, 31952</p>
                            <a href="#" class="btn btn-outline-primary">GET DIRECTIONS <i class="far fa-map-marker-alt"></i></a>
                        </div>

                        <div>
                            <h3>Riyadh</h3>
                            <p>Tamkeen Tower, 7252, <br/>Olaya St. Al Yasmeen, <br/>Riyadh, 13325</p>
                            <a href="#" class="btn btn-outline-primary">GET DIRECTIONS <i class="far fa-map-marker-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottomLine">
        <div class="container d-md-flex">
            <p>All Rights Reserved &copy; Retal Developments 2022</p>
            <ul>
                <li><a href="#">Disclaimer</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms of Use</a></li>
            </ul>
        </div>
    </div>
</footer>

<div class="search-pro">
    <a type="button" class="btn btn-searchPro">SEARCH PROPERTIES</a>
    <a type="button" class="btn btn-call">
        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
            <g id="Group_165" data-name="Group 165" transform="translate(-1268 -676)">
                <path id="Path_54" data-name="Path 54" d="M9.839,10.212a9.411,9.411,0,0,1,2.835.739c1.062.553.55,1.794.044,2.587-.74,1.159-1.767,1.274-3.037,1.126A12.454,12.454,0,0,1-.652,4.338C-.8,3.065-.684,2.036.472,1.294,1.262.787,2.5.274,3.052,1.338A9.473,9.473,0,0,1,3.789,4.18c.2,2.289-3.046,1.558-1.5,3.3l2.656,2.651,1.625,1.607c1.713,1.511,1-1.72,3.272-1.522Z" transform="translate(1268.703 675.286)" fill-rule="evenodd"/>
                <circle id="Ellipse_33" data-name="Ellipse 33" cx="1.5" cy="1.5" r="1.5" transform="translate(1275 676)"/>
                <circle id="Ellipse_34" data-name="Ellipse 34" cx="1.5" cy="1.5" r="1.5" transform="translate(1279 676)"/>
                <circle id="Ellipse_35" data-name="Ellipse 35" cx="1.5" cy="1.5" r="1.5" transform="translate(1279 680)"/>
                <circle id="Ellipse_36" data-name="Ellipse 36" cx="1.5" cy="1.5" r="1.5" transform="translate(1275 680)"/>
            </g>
        </svg>
    </a>
    <ul>
        <li>
            <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="10.707" height="10.708" viewBox="0 0 10.707 10.708">
                    <g id="Group_1754" data-name="Group 1754" transform="translate(-1266.796 -676.416)">
                        <path id="Path_754" data-name="Path 754" d="M1267.15,676.771l10,10" fill="none" stroke="#000" stroke-width="1"/>
                        <path id="Path_755" data-name="Path 755" d="M0,0,10,10" transform="translate(1277.149 676.77) rotate(90)" fill="none" stroke="#000" stroke-width="1"/>
                    </g>
                </svg>
            </a>
        </li>
        <li>
            <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                    <g id="Group_1736" data-name="Group 1736" transform="translate(-1268 -676)">
                        <path id="Path_54" data-name="Path 54" d="M9.839,10.212a9.411,9.411,0,0,1,2.835.739c1.062.553.55,1.794.044,2.587-.74,1.159-1.767,1.274-3.037,1.126A12.454,12.454,0,0,1-.652,4.338C-.8,3.065-.684,2.036.472,1.294,1.262.787,2.5.274,3.052,1.338A9.473,9.473,0,0,1,3.789,4.18c.2,2.289-3.046,1.558-1.5,3.3l2.656,2.651,1.625,1.607c1.713,1.511,1-1.72,3.272-1.522Z" transform="translate(1268.703 675.286)" fill-rule="evenodd"/>
                    </g>
                    <path id="Path_200" data-name="Path 200" d="M1587.7,4099l2.583,2.583,2.583-2.583" transform="translate(4108.582 -1587.285) rotate(90)" fill="none" stroke="#000" stroke-miterlimit="10" stroke-width="1"/>
                    <path id="Path_750" data-name="Path 750" d="M-14583-9038.03h6.493V-9033" transform="translate(14590 9041.028)" fill="none" stroke="#000" stroke-width="1"/>
                </svg>
                <span>Call me back</span>
            </a>
        </li>
        <li>
            <a href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                    <g id="Group_1737" data-name="Group 1737" transform="translate(-1268 -676)">
                        <path id="Path_54" data-name="Path 54" d="M9.839,10.212a9.411,9.411,0,0,1,2.835.739c1.062.553.55,1.794.044,2.587-.74,1.159-1.767,1.274-3.037,1.126A12.454,12.454,0,0,1-.652,4.338C-.8,3.065-.684,2.036.472,1.294,1.262.787,2.5.274,3.052,1.338A9.473,9.473,0,0,1,3.789,4.18c.2,2.289-3.046,1.558-1.5,3.3l2.656,2.651,1.625,1.607c1.713,1.511,1-1.72,3.272-1.522Z" transform="translate(1268.703 675.286)" fill-rule="evenodd"/>
                    </g>
                </svg>
                <span>Call Now</span>
            </a>
        </li>
        <li>
            <a href="#">
                <svg id="Group_1739" data-name="Group 1739" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18.081" viewBox="0 0 18 18.081">
                    <defs>
                        <clipPath id="clip-path">
                            <rect id="Rectangle_505" data-name="Rectangle 505" width="18" height="18.081"/>
                        </clipPath>
                    </defs>
                    <g id="Group_1738" data-name="Group 1738" clip-path="url(#clip-path)">
                        <path id="Path_751" data-name="Path 751" d="M6.8,5.214c-.168-.373-.344-.38-.5-.387-.131-.006-.28-.005-.429-.005a.823.823,0,0,0-.6.28,2.51,2.51,0,0,0-.784,1.868A4.356,4.356,0,0,0,5.4,9.285a9.192,9.192,0,0,0,3.825,3.38c1.892.746,2.277.6,2.687.56a2.262,2.262,0,0,0,1.512-1.065,1.871,1.871,0,0,0,.131-1.065c-.056-.093-.205-.149-.429-.261s-1.325-.654-1.53-.728-.355-.112-.5.112-.578.728-.709.878-.261.168-.485.056a6.125,6.125,0,0,1-1.8-1.111A6.745,6.745,0,0,1,6.849,8.491c-.131-.224-.014-.345.1-.457s.224-.261.336-.392a1.535,1.535,0,0,0,.224-.373.412.412,0,0,0-.019-.392C7.432,6.764,7,5.656,6.8,5.214Zm8.58-2.586A8.963,8.963,0,0,0,1.271,13.438L0,18.081l4.751-1.246a8.956,8.956,0,0,0,4.283,1.091h0a8.963,8.963,0,0,0,6.34-15.3M9.038,16.412h0a7.44,7.44,0,0,1-3.791-1.038l-.272-.161-2.819.739L2.9,13.2l-.177-.282a7.449,7.449,0,1,1,6.31,3.49" fill-rule="evenodd"/>
                    </g>
                </svg>
                <span>Whatsapp</span>
            </a>
        </li>
    </ul>
</div>

