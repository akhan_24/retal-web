<?php
$sitename='Retal Web';
$sitenameUpper='Retal Web';
$sitedomain='retal.ae';
//$siteblogurl='https://blog.labstirr.com/';
//$phonenumber='+971 54 377 7770';

?>


<?php
$whitelist = array(
    '127.0.0.1',
    '::1'
);

if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
    $dir = 'https://'.$_SERVER['SERVER_NAME'];
} else {
    $folder = $_SERVER['REQUEST_URI'];
    $folder = substr($folder, 0, strpos($folder, '/', 1));
    $dir = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].$folder;
}
?>