(function ( $ ) { 
    $.fn.dayLightSlider = function( options ) {
        var settings = $.extend({
            startingCursorOffset: 4, //0=evening,1=sunset,2=night,3=sunrise,4=noon
			endCursorOffset: 0, //0,1,2,3,4,5
			degreeConstant: 18,
			blockDegree: 360/5,
			animationInterval: 1000,
			imageLoaded: 0,
			bulletIndex: [],
			currentSlide: 4
        }, options );

		var dayLightSlide = {
			config: {},
            init: function(elm, options){
				config = options;
				config.elm = elm;
				this.setTimeSlot();     
				this.setInitialBullet();
				this.preloadImage();               
				this.setBarRotation(); 			               
				this.setEvents();             
            },               
			setTimeSlot: function(){	
				var d = new Date();	

				var night =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 4, 47, 59, 0);
				var sunrise =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 9, 35, 59, 0);
				var noon =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 14, 23, 59, 0);
				var evening =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 19, 11, 59, 0);
				var sunset =  new Date(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59, 59, 0);

				if(d.getTime() <= night ){
					config.startingCursorOffset = 2;
				} else if(d.getTime() <= sunrise ){
					config.startingCursorOffset = 3;
				} else if(d.getTime() <= noon ){
					config.startingCursorOffset = 4;
				} else if(d.getTime() <= evening ){
					config.startingCursorOffset = 0
				} else if(d.getTime() <= sunset ){
					config.startingCursorOffset = 1;
				}				
			},
			resetBullet: function(num){
				setTimeout(function(){
						//console.log(config.bulletIndex[num], num+' === '+$('circle.'+config.bulletIndex[num]).length);
						//console.log(config.bulletIndex);
						$('circle.'+config.bulletIndex[num]).removeClass('playedCircle');
					}, config.animationInterval/config.bulletIndex.length*num)
			},
			reverseBulletSelection: function(){
				$this = this;
				for(var i=0; i< config.bulletIndex.length; i++){
					//console.log(config.bulletIndex[i]);
					$this.resetBullet(i); 
				}				
			},
			changeSlide: function(){		
				var $this = this;
				//console.log('currentSlide === ', config.currentSlide);
				$('.dlDescription').removeClass('play');
				$('.dlActionUp').removeClass('play');
				$('.dlActionDown').removeClass('play');	
				$this.setActiveBullet();
				setTimeout(function(){
					
					$this.setActiveContents($('.activeCircle').data('slide'));	
					$('.dlDescription').addClass('play');
					$('.dlActionUp').addClass('play');
					$('.dlActionDown').addClass('play');
				}, 1000)
				
			},
			resetAnim: function(){	
				var $this = this;
				var $circle = $('#bar');

				$this.reverseBulletSelection(); 
				$circle.animate({'stroke-dashoffset': 219.9114857512855}, config.animationInterval, function(){	
					$('.activeCircle').removeClass('activeCircle');
					$this.playDayLight(); 
				})

					return;




				$circle.css({'stroke-dashoffset': 219.9114857512855});	
				setTimeout(function(){
					$this.setActiveContents($('.activeCircle').data('slide'));
					$('.dlDescription').addClass('play');
						$('.dlActionUp').addClass('play');
						$('.dlActionDown').addClass('play');
						setTimeout(function(){
							$this.playDayLight(); 
						}, 1000);
				}, 2000)				
				config.elm.parent().addClass('dlReady');
				//
			},
			playDayLight: function(){			
				var $this = this;			
				var $circle = $('#bar');	
				var percent = (20*5);			
				var pct = this.getDashOffset(percent);
				
				var slideInterval = setInterval(function () {
					var eachSlot = ((2*3.14159*$circle.attr('r')) /5);
					var sd = parseFloat($circle.css("stroke-dashoffset"));
					//console.log('stroke-dashoffset === ', $circle.css("stroke-dashoffset"));
					//console.log('stroke-dashoffset === ', sd);
					
					
					slideNumber = sd/eachSlot;					
					slideNumber = Math.floor(slideNumber);
					//console.log('slideNumber === ', slideNumber);
					
					if(config.currentSlide !== slideNumber){
						config.currentSlide = slideNumber;
						$this.changeSlide()
					}
					
					//console.log('slideNumber === ', (100-(slideNumber*20)));
					
				}, 500);

				$circle.animate({'stroke-dashoffset': pct}, 35000, function(){	
					//config.endCursorOffset = config.endCursorOffset+1;
					//console.log('stroke-dashoffset === ', $circle.css("stroke-dashoffset"));
					clearInterval(slideInterval);
					//$this.setActiveBullet();
					$this.changeSlide()
					

					//$this.reverseBulletSelection(); 
					//$this.setActiveBullet();
					//$this.setActiveContents($('.activeCircle').data('slide'));	



					/*$('.dlDescription').addClass('play');
					$('.dlActionUp').addClass('play');
					$('.dlActionDown').addClass('play');*/
					$this.resetAnim();
					//$this.playDayLight(); 
				})
					return;

				$circle.animate({'stroke-dashoffset': pct}, config.animationInterval, function(){	
					config.endCursorOffset = config.endCursorOffset+1;
					$this.setActiveContents($('.activeCircle').data('slide'));	
					
					$('.dlDescription').addClass('play');
					$('.dlActionUp').addClass('play');
					$('.dlActionDown').addClass('play');

					

					setTimeout(function(){
						$this.setActiveBullet();	
						//$('.activeCircle').fadeOut().fadeIn().fadeOut().fadeIn();
					}, 3000);

					setTimeout(function(){
							if(config.endCursorOffset == 5){
								//$('.activeCircle').removeClass('activeCircle');
								//$('.playedCircle').removeClass('playedCircle');
								//config.endCursorOffset = 0;
							}
							//$('.blink').removeClass('blink')
							$('.dlDescription').removeClass('play');
							$('.dlActionUp').removeClass('play');
							$('.dlActionDown').removeClass('play');
							$this.playDayLight(); 
						},5000);

					/*if(config.endCursorOffset == 5){
						config.endCursorOffset = 0;						
						setTimeout(function(){
							$('.dlDescription').removeClass('play');
							$('.dlActionUp').removeClass('play');
							$('.dlActionDown').removeClass('play');
							$this.resetDayLight(); 
						},3000);						
					}else{
						setTimeout(function(){
							$('.dlDescription').removeClass('play');
							$('.dlActionUp').removeClass('play');
							$('.dlActionDown').removeClass('play');
							$this.playDayLight(); 
						},3000);
					}*/
				})				
			},
            resetDayLight: function(){ //deprecated
				var $this = this;
				var percent = (20*config.endCursorOffset);			
				var pct = this.getDashOffset(percent);
				//console.log('pct === ', pct);
				var $circle = $('#bar');
				$('.activeCircle').removeClass('activeCircle');
					$('.playedCircle').removeClass('playedCircle');
					$this.setActiveContents($('.activeCircle').data('slide'));	
				$circle.animate({'stroke-dashoffset': pct}, config.animationInterval/2, function(){					
					$this.playDayLight(); 
				})
					
			},           
            setActiveContents: function(slide){
				if(typeof(slide) == 'undefined'){
					slide = $('.initialBullet').data('slide')
				}
				var currentSlide =  config.slideData[slide];
				$('.captionLarge').text(currentSlide.captionLarge);
				$('.captionSmall').text(currentSlide.captionSmall);
				$('.dlDescription').html(currentSlide.slideDescription);
				$('.dayLightImage').removeClass('onImage');
				$('img[data-slide="'+$('.activeCircle').data('slide')+'"]').addClass('onImage');

				setTimeout(function(){
					$('.offImage').eq(0).remove();
				},1500);

				$('.captionLarge').text(currentSlide.captionLarge);
				$('.captionSmall').text(currentSlide.captionSmall);
				$('.dlVideoLink').attr('href', currentSlide.videoLink);
				$('.dlEPLink').attr('href', currentSlide.slideLink);
			},
            setActiveBullet: function(){ 
				var currentCircle = ($('.activeCircle').length) ? $('.activeCircle') : $('.initialBullet');
				var nextCircle = (currentCircle.next().length) ? currentCircle.next() : $('.bullet:first');

				currentCircle.removeClass('activeCircle');
				nextCircle.addClass('activeCircle').addClass('playedCircle');
			},
            setInitialBullet: function(){ 
				if(config.startingCursorOffset == 4){
					$('circle.noon').addClass('initialBullet activeCircle')
				}
				if(config.startingCursorOffset == 3){
					$('circle.sunrise').addClass('initialBullet activeCircle')
				}
				if(config.startingCursorOffset == 2){
					$('circle.night').addClass('initialBullet activeCircle')
				}
				if(config.startingCursorOffset == 1){
					$('circle.sunset').addClass('initialBullet activeCircle')
				}
				if(config.startingCursorOffset == 0){
					$('circle.evening').addClass('initialBullet activeCircle')
				}
				
				var initialBullet = $('.initialBullet');
				var prevBullet = '';
				//config.bulletIndex[0] = initialBullet.data('slide');
				//console.log($('circle.bullet ').length)
				for(var i=0; i< $('circle.bullet ').length; i++){
					prevBullet = (prevBullet == '') ? initialBullet.prev() : prevBullet.prev();
					prevBullet = (prevBullet.length) ? prevBullet : $('circle.bullet:last ');

					config.bulletIndex[i] = prevBullet.data('slide');
					//console.log(prevBullet)
				}

					//console.log(config.bulletIndex)

				$('img[data-slide="'+$('.activeCircle').data('slide')+'"]').addClass('onImage');

			},
            getDashOffset: function(percent){ 
				var $circle = $('#bar');
				if (isNaN(percent)) {
				 percent = 100; 
				}
				else{
				  var r = $circle.attr('r');
				  var c = Math.PI*(r*2);
				 
				  if (percent < 0) { percent = 0;}
				  if (percent > 100) { percent = 100;}
				  var pct = ((100-percent)/100)*c;
				  return pct;
				}
			},
            setBarRotation: function(){ 
				jQuery('.dayLightBar').css({
					    transform: 'rotate('+ ((config.blockDegree*config.startingCursorOffset)-config.degreeConstant) +'deg)'
				})
			},
            setEvents: function(){    
				$this = this;
				$(window).resize(function(){
					$this.setGalleryView(); 
				})
			},            
			setGalleryView: function(){				
				var onImage = $('.onImage');
				var theImage = [];
				theImage.width = onImage.data('width');
				theImage.height = onImage.data('height');				

				var availableWidth = config.elm.width();
				var availableHeight = config.elm.height();

				if(availableHeight > availableWidth){
					imgHeight = availableHeight;
					imgWidth = availableHeight*theImage.width/theImage.height;
				}else{
					var imgWidth = availableWidth;
					var imgHeight = availableWidth*theImage.height/theImage.width;	
					
					if(imgHeight < availableHeight){
						imgHeight = availableHeight;
						imgWidth = availableHeight*theImage.width/theImage.height;
					}
				}
	
				config.elm.find('.dayLightImage').css({
					width: imgWidth+'px',
					height: imgHeight+'px',
					marginLeft: ((availableWidth-imgWidth) /2)+'px',
					marginTop: ((availableHeight-imgHeight) /2)+'px'
				});
			},			
			preloadImage: function(){
				var $this = this;
				$('.dayLightImage').each(function(){
					var theImage = new Image();
					var elem = $(this);
					theImage.src = elem.attr('src');
					((theImage.readyState) && (theImage.readyState == "loaded" || theImage.readyState == "complete"))? theImage.onreadystatechange = function () {$this.preloadCallback(theImage, elem);	} : theImage.onload = function () {$this.preloadCallback(theImage, elem);};	
				});								
			},
			preloadCallback: function(theImage, elem){
				elem.attr('data-width', theImage.width).attr('data-height',theImage.height)
				config.imageLoaded = config.imageLoaded+1;				
				if(Object.keys(config.slideData).length == config.imageLoaded){
					this.setGalleryView();
					this.removeLoading();
				}
			},
			setInitialAnim: function(){	
				var $this = this;
				var $circle = $('#bar');
				$circle.css({'stroke-dashoffset': 219.9114857512855});	
				setTimeout(function(){
					$this.setActiveContents($('.activeCircle').data('slide'));
					$('.dlDescription').addClass('play');
						$('.dlActionUp').addClass('play');
						$('.dlActionDown').addClass('play');
						setTimeout(function(){
							$this.playDayLight(); 
						}, 1000);
				}, 2000)				
				config.elm.parent().addClass('dlReady');
				//
			},
			removeLoading: function(){
				$('.dlLoading').removeClass('dlLoading').find('.loading').remove();
				this.setInitialAnim();
				//this.playDayLight(); 
			}
		}

		return dayLightSlide.init(this, settings);
			
    };
 
}( jQuery ));

if(!typeof(dayLightData)){
	var dayLightData = {
		noon : {
			image: 'Repeat-Grid-1.png',
				captionLarge: 'Ewan almaali',
				captionSmall: 'Riyadh',
				slideDescription: 'Noon<br><span>to start fresh.</span>',
				videoLink: 'video',
				slideLink: 'explore'
		},
		sunrise : {
			image: '1.jpg',
				captionLarge: 'Ewan almaali 1',
				captionSmall: 'Riyadh 1',
				slideDescription: 'Sunrise<br><span>to start fresh 1</span>',
				videoLink: 'video1',
				slideLink: 'explore1'
		},
		night : {
			image: '2.jpg',
				captionLarge: 'Ewan almaali 2',
				captionSmall: 'Riyadh 2',
				slideDescription: 'Night<br><span>to start fresh 2</span>',
				videoLink: 'video2',
				slideLink: 'explore2'
		},
		sunset : {
			image: '3.jpg',
				captionLarge: 'Ewan almaali 3',
				captionSmall: 'Riyadh 3',
				slideDescription: 'Sunset<br><span>to start fresh 3</span>',
				videoLink: 'video3',
				slideLink: 'explore3'
		},
		evening : {
			image: '4.jpg',
				captionLarge: 'Ewan almaali 4',
				captionSmall: 'Riyadh 4',
				slideDescription: 'Evening<br><span>to start fresh 4</span>',
				videoLink: 'video4',
				slideLink: 'explore4'
		}
	}
}

jQuery(document).ready(function( ) {
	jQuery('.dayLightWrapper').dayLightSlider({
		slideData: dayLightData
	});
});