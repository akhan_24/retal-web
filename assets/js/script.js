(function ($) {
    $.fn.countTo = function (options) {
        options = options || {};

        return $(this).each(function () {
            // set options for current element
            var settings = $.extend({}, $.fn.countTo.defaults, {
                from:            $(this).data('from'),
                to:              $(this).data('to'),
                speed:           $(this).data('speed'),
                refreshInterval: $(this).data('refresh-interval'),
                decimals:        $(this).data('decimals')
            }, options);

            // how many times to update the value, and how much to increment the value on each update
            var loops = Math.ceil(settings.speed / settings.refreshInterval),
                increment = (settings.to - settings.from) / loops;

            // references & variables that will change with each update
            var self = this,
                $self = $(this),
                loopCount = 0,
                value = settings.from,
                data = $self.data('countTo') || {};

            $self.data('countTo', data);

            // if an existing interval can be found, clear it first
            if (data.interval) {
                clearInterval(data.interval);
            }
            data.interval = setInterval(updateTimer, settings.refreshInterval);

            // initialize the element with the starting value
            render(value);

            function updateTimer() {
                value += increment;
                loopCount++;

                render(value);

                if (typeof(settings.onUpdate) == 'function') {
                    settings.onUpdate.call(self, value);
                }

                if (loopCount >= loops) {
                    // remove the interval
                    $self.removeData('countTo');
                    clearInterval(data.interval);
                    value = settings.to;

                    if (typeof(settings.onComplete) == 'function') {
                        settings.onComplete.call(self, value);
                    }
                }
            }

            function render(value) {
                var formattedValue = settings.formatter.call(self, value, settings);
                $self.html(formattedValue);
            }
        });
    };

    $.fn.countTo.defaults = {
        from: 0,               // the number the element should start at
        to: 0,                 // the number the element should end at
        speed: 1000,           // how long it should take to count between the target numbers
        refreshInterval: 100,  // how often the element should be updated
        decimals: 0,           // the number of decimal places to show
        formatter: formatter,  // handler for formatting the value before rendering
        onUpdate: null,        // callback method for every time the element is updated
        onComplete: null       // callback method for when the element finishes updating
    };

    function formatter(value, settings) {
        return value.toFixed(settings.decimals);
    }
}(jQuery));

function count($this, options) {
    // var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
}



var callback_error = function(element) {
    console.log(element.getAttribute("data-src"));
    element.src =
        "https://via.placeholder.com/440x560/?text=Error+Placeholder";
};
var callback_enter = function(element) {
    console.log(element.getAttribute("data-src"));
};


jQuery(function($){

    $('[href="#"]').attr('href','javascript:;');
    $('p').addClass('animToStart');

    $('body').on('keypress','.numberOnly',function(i){var keycode = i.which;if (!(i.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {return false;}});



    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy",
        //callback_enter: callback_enter,
        callback_error: callback_error
        // ... more custom settings?
    });

    $('.mobileMenu nav').html($('header nav').html());

    $('body').on('click','.menuBtn',function(){
        $('html').toggleClass('openmenu');
    }).on('click','.menuCloseBtn',function(){
        $('html').removeClass('openmenu');
    }).on('click','.search-pro .btn-call',function(){
        $(this).toggleClass('active');
    });

    $('.faqTabs').on('click','.tabPaginate [data-tab]',function(){
        var tgt = $(this).attr('data-tab');
        $(this).addClass('active').siblings().removeClass('active');
        $('.faqTabs .tabItem.'+tgt).addClass('active').siblings().removeClass('active');
    });

    $('.accord').on('click',function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }else{
            $(this).addClass('active').siblings().removeClass('active');
        }
    });

    $('.contactSlider').on('click','[data-slide]', function(){
        var tgt = $(this).attr('data-slide');
        $(this).addClass('active').siblings().removeClass('active');
        $(this).parent().attr('data-active',tgt);
        $('.contactSlider .item.active').addClass('anim');
        setTimeout(function(){
            $('.contactSlider .item.active').removeClass('active anim');
            $('.contactSlider .item.'+tgt).addClass('active anim');
            setTimeout(function(){
                $('.contactSlider .item.'+tgt).removeClass('anim');
            },1000);
        },500);
    });

    $('.mc-scrolling-tabs li a').on('click', function(){
        var tgt = $(this).attr('href');
        $(this).addClass('t-active').parent().siblings().find('a').removeClass('t-active');
        console.log(tgt);
        $(tgt).removeClass('d-none').siblings().not('.navDv').addClass('d-none');
        return false;
    });

    $('.propertyPageSlide .sliderDv').on('click','.item',function(){
        var tgt = $(this).attr('data-filter');
        $('.propertyPageSlide .sliderDv .item').removeClass('active');
        $(this).addClass('active');
    });



    if($(window).width()>=768){

        $('.propertyPageSlide .sliderDv').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            slidesToShow: 3,
            arrows:true,
            dots:false,
            centerMode: false,
            mobileFirst: false,
            prevArrow: '<div class="prev"><i class="fal fa-arrow-right"></i></div>',
            nextArrow: '<div class="next"><i class="fal fa-arrow-left"></i></div>',
        });

    }

    if($(window).width()<768){
        $('.mobiSlider').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            slidesToShow: 1,
            arrows:false,
            dots:true,
            mobileFirst: true
        });
    }




    allSlick();
    transHeader(0);
    inlineCssInitiate();
    arsalanScripts();
    aboutScript();

    $(window).scroll(function(){

        inlineCssInitiate();

        var windowScroll = $(window).scrollTop();

        transHeader(windowScroll);

        var positionAnimStart = windowScroll + ($(window).height() / 2);
        $('.animToStart').each(function(){
            var curItemTop = $(this).offset().top;
            if(curItemTop < positionAnimStart){
                $(this).removeClass('animToStart');
            }
        });
        $('[data-anim]').each(function(){
            var  $this = $(this), curItemTop = $(this).offset().top, timeOut;
            timeOut = $this.attr('data-anim-delay');
            if(timeOut){
                $this.addClass('delay'+timeOut);
                if($this.children('style').length == 0){
                    $this.append('<style>body section figure[data-anim].activeAnim.delay'+timeOut+':before{transition-delay:'+timeOut+'ms;}</style>');
                }
            }
            if(curItemTop < positionAnimStart){
                $this.addClass('activeAnim');
            }
        });
        $('.anim-head').each(function(){
            var curItemTop = $(this).offset().top;
            // console.log(windowScroll, curItemTop);
            if(curItemTop < positionAnimStart){
                letterAnim($(this));
            }
        });


    });



$(window).on('load',function(){

    $(window).trigger('scroll');

    setTimeout(function(){
        $(window).trigger('resize');
    },5000);
    var loadedOut = setInterval(function(){
        if($('html').hasClass('loaded')){clearInterval(loadedOut);}
        $('html').addClass('loaded');
    },2000);

    // AOS
    // AOS.init();

    // Form
    //formScripts();


    $('[data-svg]').each(function(){
        var $this = $(this), itm = $this.attr('data-svg');
        $this.load(itm);
        //console.log(itm);
    });

    /*nsZoomZoom();
    $(window).resize(function() {nsZoomZoom();});*/



    /*if($(window).width() > 780){
        setTimeout(function(){
            $("html").niceScroll({
                scrollspeed: 100,
            });
        }, 1000);
    }*/

});



}); // Ready End


function transHeader(windowScroll){
    if($('body').hasClass('transHeader')){
        if(windowScroll > 300){
            $('body').removeClass('transHeaderActive');
        }else{
            $('body').addClass('transHeaderActive');
        }
    }
}

function formScripts(){

    var inputPhone, inputPhone2, iti, iti2;

    inputPhone = document.querySelector("#phonefield");
    iti = window.intlTelInput(inputPhone, {utilsScript: pathrootdir+"/assets/intl-tel-input/js/utils.js",});

    inputPhone2 = document.querySelector("#phonefield2");
    iti2 = window.intlTelInput(inputPhone2, {utilsScript: pathrootdir+"/assets/intl-tel-input/js/utils.js",});

    setTimeout(function(){
        /*$.get("https://ipinfo.io", function (response) {
			iti.setCountry(response.country);
        }, "jsonp");*/
        $(".formDv form").validate();
        // $(".formOuterDv form").validate();

    },1000);
}

function inlineCssInitiate(){

    $('[data-inlinecss]').each(function(){
        if($(this).offset().top<($(window).scrollTop()+$(window).height())){
            var i=$(this).attr('data-inlinecss');
            $(this).attr('style',i).removeAttr('data-inlinecss');
        }

    });
}


function goToByScroll(e){jQuery("html,body").animate({scrollTop:jQuery(e).offset().top},1e3,"",function(){jQuery(this).stop(true,true)})}


function nsZoomZoom() {
    htmlWidth = $('html').innerWidth();
    bodyWidth = 1366;
    if (htmlWidth < bodyWidth){scale = 1} else {scale = htmlWidth / bodyWidth;}
    $(".screen-resize").css('-moz-transform', 'scale(' + scale + ')');
    // $(".screen-resize").css('transform', 'scale(' + scale + ')');
    $(".screen-resize").css('zoom', '' + scale + '');
}

var headerItem = 0;
function letterAnim(t){
    // Wrap every letter in a span
    var textWrapper = t.find('.letters');
    if(!t.hasClass('animStart')){
        // textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

        textWrapper.each(function(){
            $(this).html($(this).html().replace('<br>','*'));
            $(this).html($(this).text().replace(/\S/g, "<anim class='letter'>$&</anim>"));
            $(this).html($(this).html().replace('<anim class="letter">*</anim>','<br>'));
        });
        headerItem++;
        t.addClass('animStart current'+headerItem);

        anime.timeline({loop: false})
            .add({
                targets: '.anim-head.animStart.current'+headerItem+' .letter',
                scale: [0.3,1],
                opacity: [0,1],
                translateZ: 0,
                easing: "easeOutExpo",
                duration: 600,
                delay: (el, i) => 70 * (i+1),
                complete: function(anim) {
                    // completeLogEl.value = 'completed : ' + anim.completed;
                    $('.anim-head.current'+headerItem).removeClass('current'+headerItem);
                }
            });
    }
}

function allSlick(){

    $('.fetaurSliderDv').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        arrows:true,
        dots:false,
        centerMode: true,
        mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-left"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-right"></i></div>',
    });

    $('.botHomeSlider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 2,
        arrows:true,
        dots:false,
        centerMode: true,
        // mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-left"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-right"></i></div>',
        responsive:[
            /* {
                 breakpoint: 3000,
                 settings: {
                     slidesToShow: 4,
                 }
             },
             {
                 breakpoint: 2000,
                 settings: {
                     slidesToShow: 3,
                 }
             },
             {
                 breakpoint: 1500,
                 settings: {
                     slidesToShow: 2,
                 }
             },*/
        ],
    });

    $('.timelineSliderDv').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        arrows:true,
        dots:true,
        customPaging : function(slider, i) {
            var title = $(slider.$slides[i]).find('[data-year]').data('year');
            return '<a class="pager__item"> '+title+' </a>';},
        centerMode: true,
        // mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-right"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-left"></i></div>',
        responsive:[
            /*{
                breakpoint: 512,
                settings: {
                    slidesToShow: 1,
                    arrows:false,
                }
            },*/
        ],
    });

    $('.valueSliderDv').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows:true,
        dots:false,
        centerMode: false,
        // mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-right"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-left"></i></div>',
        responsive:[
            {
                breakpoint: 512,
                settings: {
                    slidesToShow: 1,
                    arrows:false,
                }
            },
        ],
    });

    $('.gpSliderDv').slick({
        autoplay: true,
        autoplaySpeed: 5000000000,
        slidesToShow: 2,
        arrows:true,
        dots:false,
        centerMode: true,
        // mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-right"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-left"></i></div>',
        responsive:[
            {
                breakpoint: 512,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows:false,
                    centerMode: false,
                }
            },
        ],
    });

    $('.subscribeSliderDv').slick({
        autoplay: true,
        autoplaySpeed: 5000000000,
        slidesToShow: 2,
        arrows:true,
        dots:false,
        centerMode: true,
        // mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-right"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-left"></i></div>',
        responsive:[
            {
                breakpoint: 512,
                settings: {
                    slidesToShow: 2,
                    slidesToShow: 1,
                    centerMode: true,
                }
            },
        ],
    });
}

function arsalanScripts(){

    $('body').on('click','.menu-toggle',function(){
        $('.menu-close').slideToggle();
    });


    $('#fullpage').fullpage({
        //options here
        autoScrolling:true,
        scrollHorizontally: true,
        navigation: true,
        sectionSelector: '.section',
        scrollOverflow:true,
        anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourthPage', 'fifthPage', 'sixthPage', 'lastPage'],
        menu: '#fp-menu'

    });

    var $status = $('.slideInfo');
    var $slickElement = $('.aboutSlider');

    $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.html('<span class="main-slide">' + i + '</span><span class="separate-slide">—</span><span class="total-slide">' + slick.slideCount + '</span>');
    });
    $('.aboutSlider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        arrows:true,
        dots: false,
        mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-left"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-right"></i></div>',
    });

    $('.eventSlider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        arrows:true,
        dots: false,
        mobileFirst: true,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-left"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-right"></i></div>',
    });

    $('.projectSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.projectSlider-nav',
        prevArrow: '<div class="prev"><img src="assets/svg/slider-arrow-left-dark.svg"/></div>',
        nextArrow: '<div class="next"><img src="assets/svg/slider-arrow-right-dark.svg"/></div>',
    });

    $('.projectSlider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.projectSlider',
        arrows: false,
        dots: false,
        centerMode: false,
        focusOnSelect: true
    });

    $('.ptSliderDv').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        loop:false,
        prevArrow: '<div class="prev"><i class="fal fa-arrow-left"></i></div>',
        nextArrow: '<div class="next"><i class="fal fa-arrow-right"></i></div>',
    });
    
}

function aboutScript(){
    $('.popLnk').on('click',function(){
        var tgt = $(this).attr('href');
        $('.popup').removeClass('active');
        $(tgt).addClass('active');
        $('.slick-slider').each(function(){
            $(this).slick('slickNext');
        });
        return false;
    });
    $('.closePop').on('click',function(){
        $(this).parents('.popup').removeClass('active');
        return false;
    });
}







 

